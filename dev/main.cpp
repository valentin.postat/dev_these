//
// Created by v on 05/02/2021.
//
#include<iostream>

#include "LCC_HL.h"

#define CHECK std::cout << "check" << std::endl;
#define _ENDL std::cout << std::endl;

enum Face {
    Compressed, Triangular, Quadrangular
};


void test_open_show() {
    /**
    0  ___  1
    |       |
    |       |
    3  ---  2
    */
    // Create with the builder
    LCC_HL lcc_hl(LinearCellComplexBuilder::basic());
    // Switch on drawing
    lcc_hl.set_draw(false);
    // Display some characteristics
    lcc_hl.display();
    // Draw
    lcc_hl.draw();
    // Get all vertex
    std::vector <std::size_t> v = lcc_hl.get_all_vertex_ids();
    // Edges 2, tri 3, quad 4
    std::vector <std::size_t> incident_edge, incident_tri, incident_quad;

    for (const auto &value: v) {
        std::cout << "#Vertice_id " << value << "\n#on boundary " << lcc_hl.in_boundary(value) << std::endl;
        incident_edge = lcc_hl.incident_edge(value);
        std::cout << "#Edge" << std::endl;
        for (std::vector<std::size_t>::iterator it = incident_edge.begin();
             it != incident_edge.end(); it += 2) {
            std::cout << *it << " " << *(std::next(it)) << std::endl;
        }
        incident_tri = lcc_hl.incident_tri(value);
        int cpt = 0;
        std::cout << "#Triangle" << std::endl;
        for (std::vector<std::size_t>::iterator it = incident_tri.begin();
             it != incident_tri.end(); it += 3) {
            std::cout << *it << " " << *(std::next(it, 1)) << *(std::next(it, 2)) << std::endl;
        }
        std::cout << "#Quad" << std::endl;
        incident_quad = lcc_hl.incident_quad(value);
        for (std::vector<std::size_t>::iterator it = incident_quad.begin();
             it != incident_quad.end(); it += 4) {
            std::cout << *it << " " << *(std::next(it, 1)) << " " << *(std::next(it, 2)) << " " << *(std::next(it, 3))
                      << std::endl;
            cpt++;
        }
    }
    lcc_hl.display();

    std::for_each(v.begin(), v.end(), [](std::size_t const &value) { std::cout << value << " "; });
    std::cout << std::endl;
    lcc_hl.draw();
    lcc_hl.display();
    v = lcc_hl.get_all_vertex_ids();
    std::for_each(v.begin(), v.end(), [](std::size_t const &value) { std::cout << value << " "; });


    auto edge = lcc_hl.incident_edge(0);
    auto tri = lcc_hl.incident_tri(0);
    auto quad = lcc_hl.incident_quad(0);
    std::cout << "\n#Edges " << edge.size() << std::endl;
    for (auto it = edge.begin();
         it != edge.end(); it += 2) {
        std::cout << *it << " " << *(std::next(it)) << std::endl;
    }
    std::cout << "#Tri " << tri.size() << std::endl;
    for (auto it = tri.begin();
         it != tri.end(); it += 3) {
        std::cout << *it << " " << *(std::next(it, 1)) << " " << *(std::next(it, 2)) << std::endl;
    }
    std::cout << "#Quad " << quad.size() << std::endl;
    for (auto it = quad.begin();
         it != quad.end(); it += 4) {
        std::cout << *it << " " << *(std::next(it, 1)) << " " << *(std::next(it, 2)) << " " << *(std::next(it, 3))
                  << std::endl;
    }
    // get all ids
    // get all faces
}

void test_modelf(bool show) {
    LCC_HL lcc_hl1(LinearCellComplexBuilder::basic_modelf());
    //std::vector <std::size_t> line1 = {3, 0, 1, 4, 7};
    std::vector <std::size_t> line1 = {3, 0, 1, 4, 7, 5, 6, 2, 3};
    //line1 = {5, 1, 0};
    lcc_hl1.set_draw(false);
    lcc_hl1.draw();
    lcc_hl1.locate_all_vertex();

    lcc_hl1.pillowing_out(line1, 0.6, false);
    /**Work too
     * */
    //lcc_hl1.pillowing(line1, 2, 0.4);

    lcc_hl1.set_draw(show);
    lcc_hl1.draw();

    lcc_hl1.display();
    lcc_hl1.set_draw(show);

}

void test_four_edge_open(bool show) {
    LCC_HL lcc_hl(LinearCellComplexBuilder::basic_six_quads());
    lcc_hl.set_draw(show);
    lcc_hl.display();
    lcc_hl.draw();
    lcc_hl.locate_all_vertex();
    /// OK 4
    //std::vector <std::size_t> v2 = {0, 1, 5, 8, 11, 10, 5, 6};
    /// OK 4
    //std::vector <std::size_t> v2 = {0, 1, 5, 10, 7, 1, 2};
    //std::vector <std::size_t> v2 = {0, 1, 5, 10, 7, 1, 2};
    /// OK 4
    //std::vector <std::size_t> v2 = {4, 0, 1, 5, 8, 11, 10, 7, 1, 2, 6, 9, 8, 5};
    /// KO parce que le dernier {1} n'est plus un sommet voisin {5}
    // std::vector <std::size_t> v2 = {4, 0, 1, 5, 8, 11, 10, 7, 1, 2, 6, 9, 8, 5, 1};
    /// OK 7
    //std::vector <std::size_t> v2 = {4, 0, 1, 5, 8, 11, 10, 7, 1, 2};
    // OK 4
    //std::vector <std::size_t> v2 = {0, 1, 5, 8, 11, 10, 7, 1, 2, 6, 9, 8, 5, 1, 0};
    /// OK 4
    //std::vector <std::size_t> v2 = {0, 1, 5, 8, 11, 10, 5, 1, 7, 4, 0} ;
    /// OK 1 cur
    //std::vector <std::size_t> v2 = {0, 3, 2, 1, 5, 6, 9, 8, 11, 10, 5, 1, 7, 4, 0} ;
    //0 1 5 8 11  8 11 10 7 1 2 6 9 8 5 1 0
    /// OK 4
    // Franck réunion
    std::vector <std::size_t> v2 = {0, 1, 5, 8, 11, 10, 7, 1, 2, 6, 9, 8, 5, 1, 0};
    // KO 0 opération non prise en compte
    //v2 = {3, 2, 6, 9, 8, 5, 1, 5, 8, 11, 10, 7, 4, 0, 3};
    // KO 1
    //v2 = {0, 3, 2, 1, 5, 6, 9, 8, 11, 10, 7, 4, 0};
    // OK Symétrique 3
    //v2= {0, 1, 5, 8, 9, 6, 2, 1, 7, 10, 11, 8, 5, 1, 0};
    lcc_hl.pillowing(v2, 4, 0.4);
    lcc_hl.draw();
    lcc_hl.display();
}

void test_open_edge_two_split_vertice_six_quads(bool show) {
    LCC_HL lcc_hl(LinearCellComplexBuilder::basic_six_quads());
    // Switch on drawing
    lcc_hl.set_draw(show);
    // Display some characteristics
    lcc_hl.display();
    // Draw
    lcc_hl.draw();
    //
    lcc_hl.locate_all_vertex();
    /**
    for (std::vector<std::size_t>::iterator it = line.begin(); it != line.end(); ++it) {
        lcc_hl.show_tri(*it);
        lcc_hl.show_quad(*it);
    }*/
    std::vector <std::size_t> line;
    float w = 0.5;
    line = {0, 1, 2};//12,5};
    lcc_hl.pillowing(line, 3, w);
    lcc_hl.set_draw(show);
    lcc_hl.draw();
    line = {5, 10, 7, 1, 5};
    lcc_hl.pillowing(line, 1, w);
    lcc_hl.set_draw(show);
    lcc_hl.draw();
    line = {3, 2, 6, 9, 8, 11, 10, 7, 4, 0, 3};
    lcc_hl.pillowing(line, 0, 0.7);
    lcc_hl.set_draw(show);
    lcc_hl.draw();

    line = {0, 4, 7, 10, 11, 8, 9, 6, 2, 3, 0};
    lcc_hl.pillowing_out(line, 1.0);
    lcc_hl.set_draw(show);
    lcc_hl.draw();
}



int main(int argc, char **argv) {
    //test_open_edge_one_split_vertice();
    //test_open_edge_two_split_vertice();
    bool show = false;
    if (argc > 1 && strcmp(argv[1], "-d") == 0) {
        show = true;
    } else if (argc > 1 && strcmp(argv[1], "-h") == 0) {
        std::cout << "Usage:\n./test_lcc_hl [-h]|[-d]\n[-h]:To show this message\n[-d]: To show drawing" << std::endl;
        return EXIT_SUCCESS;
    }
    //test_open_edge_two_split_vertice_six_quads(show);
    //test_modelf(show);
    //test_four_edge_open(show);
    //test_modelf(true);
    std::cout << __cplusplus << std::endl;
    return EXIT_SUCCESS;
}