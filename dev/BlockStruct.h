//
// Created by v on 07/06/2021.
//

#ifndef DEV_THESE_BLOCKSTRUCT_H
#define DEV_THESE_BLOCKSTRUCT_H

#include <iostream>
#include "LCC_HL.h"

typedef std::size_t Vertex_id;
typedef std::size_t Cell_id;
//typedef std::vector<std::size_t> ;

class BlockStruct {
    /** Structure de bloc
     * **/

private:
    LCC_HL lcc_hl;

public:

    BlockStruct(std::vector<float> x, std::vector<float> y, std::vector <std::vector<std::size_t>> adj,
                std::vector <std::size_t> constraints) : lcc_hl(x, y, adj, constraints) {}


    inline std::size_t nb_vertex() { return lcc_hl.nb_vertex(); }

    //inline std::size_t nb_edges() { return lcc_hl.nb_edges(); }

    inline std::size_t nb_cells() { return lcc_hl.nb_cells(); }

    /// Return all cells id
    std::vector <std::size_t> all_cells_id() { return lcc_hl.get_cells_id(); }

    inline std::vector <std::size_t> all_vertex_id() { return lcc_hl.get_all_vertex_ids(); }

    std::vector<double> get_vertex_point(Vertex_id id_vtx) {
        Point p = lcc_hl.point_of_vertex(id_vtx);
        return std::vector<double>({p.x(), p.y(), 0.});
    }

    std::vector <std::size_t> vertex_of_cell(Cell_id id_cell);

    void set_vertex_point(Vertex_id id_vtx, float x, float y);

    /** Actions */
    /** handle non-conform cases */
    bool split(Cell_id id_cell);

    std::vector <std::size_t> pillowing(std::vector <std::size_t> path, Vertex_id id_vtx, float weight);

    /** Neighborhood */
    /** Given a Vertex_id return all vertex incident **/
    std::vector <std::size_t> incident_vertex_of_vertex(Vertex_id id_vtx);

    /** Given a Cell_id return all vertex incident to the cell **/
    std::vector <std::size_t> incident_vertex_of_cell(Cell_id id_cell);

    /** Given a Vertex_id return all Cells incident **/
    std::vector <std::size_t> incident_cell_of_vertex(Vertex_id id_vtx);

    /** Given a Cell_id return all Cells incident **/
    std::vector <std::size_t> incident_cell_of_cell(Cell_id id_cell);

    /** Topology */
    std::vector <std::vector<size_t>> adj_vertex();

    std::vector <std::vector<size_t>> adj_cells();
};

#endif //DEV_THESE_BLOCKSTRUCT_H
// Iterator
// https://internalpointers.com/post/writing-custom-iterators-modern-cpp