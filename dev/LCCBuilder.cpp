//
// Created by v on 17/12/2020.
//



#include "LCCBuilder.h"

#include <vector>
// for sleep()
#include <unistd.h>
#include <assert.h>

//typedef LCC_2::Vector                                      Vector;
//typedef LCC_2::FT                                          FT;

#define DEBUG_CHECK 0
#define MY_CHECK if(DEBUG_CHECK);std::cout << "check" << std::endl;

LCC_2 LinearCellComplexBuilder::basic(bool show_creation) {
    LCC_2 lcc;
    Dart_handle d = lcc.make_quadrangle(Point(0.0, 0.0), Point(1.0, 0.0), Point(1.0, 1.0), Point(0.0, 1.0));
    CGAL_assertion(lcc.is_valid());
    setup_ids(lcc, 0);
    if (show_creation) { CGAL::draw(lcc); }
    return lcc;
}

LCC_2 LinearCellComplexBuilder::basic_four_quads(bool show_creation) {
    LCC_2 lcc;
    // Dart between p0 & p1
    // Creating quads
    // Down left
    Dart_handle d1 = lcc.make_quadrangle(Point(0.0, 0.0), Point(1.0, 0.0), Point(1.0, 1.0), Point(0.0, 1.0));
    // Down middle
    Dart_handle d2 = lcc.make_quadrangle(Point(1.0, 0.0), Point(2.0, 0.0), Point(2.0, 1.0), Point(1.0, 1.0));
    lcc.sew<2>(lcc.alpha<0, 1>(d1), lcc.alpha<1>(d2));
    if (show_creation) { CGAL::draw(lcc); }
    // Up left
    Dart_handle d3 = lcc.make_quadrangle(Point(0.0, 1.0), Point(1.0, 1.0), Point(1.0, 2.0), Point(0.0, 2.0));
    // Up middle
    Dart_handle d4 = lcc.make_quadrangle(Point(1.0, 1.0), Point(2.0, 1.0), Point(2.0, 2.0), Point(1.0, 2.0));
    lcc.sew<2>(lcc.alpha<0, 1>(d3), lcc.alpha<1>(d4));
    lcc.sew<2>(lcc.alpha<1, 0, 1>(d1), d3);
    lcc.sew<2>(lcc.alpha<1, 0, 1>(d2), d4);
    if (show_creation) { CGAL::draw(lcc); }
    // End of the generated quads sew
    CGAL_assertion(lcc.is_valid());
    setup_ids(lcc, 0);
    CGAL_assertion(lcc.is_valid());
    return lcc;
}


LCC_2 LinearCellComplexBuilder::basic_six_quads(bool show_creation) {
    LCC_2 lcc;
    // Dart between p0 & p1
    // Creating quads
    // Down left
    Dart_handle d1 = lcc.make_quadrangle(Point(0.0, 0.0), Point(1.0, 0.0), Point(1.0, 1.0), Point(0.0, 1.0));
    // Down middle
    Dart_handle d2 = lcc.make_quadrangle(Point(1.0, 0.0), Point(2.0, 0.0), Point(2.0, 1.0), Point(1.0, 1.0));
    lcc.sew<2>(lcc.alpha<0, 1>(d1), lcc.alpha<1>(d2));
    if (show_creation) { CGAL::draw(lcc); }
    // Up left
    Dart_handle d3 = lcc.make_quadrangle(Point(0.0, 1.0), Point(1.0, 1.0), Point(1.0, 2.0), Point(0.0, 2.0));
    // Up middle
    Dart_handle d4 = lcc.make_quadrangle(Point(1.0, 1.0), Point(2.0, 1.0), Point(2.0, 2.0), Point(1.0, 2.0));
    lcc.sew<2>(lcc.alpha<0, 1>(d3), lcc.alpha<1>(d4));
    lcc.sew<2>(lcc.alpha<1, 0, 1>(d1), d3);
    lcc.sew<2>(lcc.alpha<1, 0, 1>(d2), d4);
    if (show_creation) { CGAL::draw(lcc); }
    // Down right
    Dart_handle d5 = lcc.make_quadrangle(Point(2.0, 0.0), Point(3.0, 0.0), Point(3.0, 1.0), Point(2.0, 1.0));
    // Up right
    Dart_handle d6 = lcc.make_quadrangle(Point(2.0, 1.0), Point(3.0, 1.0), Point(3.0, 2.0), Point(2.0, 2.0));
    lcc.sew<2>(lcc.alpha<1, 0, 1>(d5), d6);
    if (show_creation) { CGAL::draw(lcc); }
    lcc.sew<2>(lcc.alpha<0, 1>(d2), lcc.alpha<1>(d5));
    lcc.sew<2>(lcc.alpha<0, 1>(d4), lcc.alpha<1>(d6));
    if (show_creation) { CGAL::draw(lcc); }
    // End of the generated quads sew
    CGAL_assertion(lcc.is_valid());
    setup_ids(lcc, 0);
    // Setup
    for (LCC_2::Dart_range::iterator
                 it = lcc.darts().begin(), itend = lcc.darts().end();
         it != itend; ++it) {
        if (lcc.attribute<2>(it) == NULL)
            lcc.set_attribute<2>(it, lcc.create_attribute<2>());
    }
    int i = 0;


    /*
    for (LCC_2::Attribute_range<2>::type::iterator
                 it = lcc.attributes<2>().begin(), itend = lcc.attributes<2>().end();
         it != itend; ++it) {
        lcc.info_of_attribute<2>(it) = i;
        i++;
    }

    // Show
    for (LCC_2::Attribute_range<2>::type::iterator
                 it = lcc.attributes<2>().begin(), itend = lcc.attributes<2>().end();
         it != itend; ++it) {

        std::cout << lcc.info_of_attribute<2>(it) << "; ";
    }
    */

    return lcc;
}

LCC_2 LinearCellComplexBuilder::basic_quadv(bool show_creation) {
    LCC_2 lcc;
    // Dart between p0 & p1
    // Creating quad
    Dart_handle d1 = lcc.make_quadrangle(Point(0.0, 0.0), Point(1.0, 0.0), Point(0.8, 1.0), Point(0.2, 1.0));
    if (show_creation) { CGAL::draw(lcc); }
    CGAL_assertion(lcc.is_valid());
    setup_ids(lcc, 0);
    CGAL_assertion(lcc.is_valid());
    return lcc;
}


LCC_2 LinearCellComplexBuilder::basic_modelf(bool show_creation) {
    LCC_2 lcc;
    // Dart between p0 & p1
    // Creating quads
    // Down left
    Dart_handle d1 = lcc.make_quadrangle(Point(0.0, 0.0), Point(1.0, 0.0), Point(1.0, 1.0), Point(0.0, 1.0));
    // Down middle
    Dart_handle d2 = lcc.make_quadrangle(Point(1.0, 0.0), Point(2.0, 0.0), Point(2.0, 1.0), Point(1.0, 1.0));
    lcc.sew<2>(lcc.alpha<0, 1>(d1), lcc.alpha<1>(d2));
    if (show_creation) { CGAL::draw(lcc); }
    // Up middle
    Dart_handle d3 = lcc.make_quadrangle(Point(1.0, 1.0), Point(2.0, 1.0), Point(2.0, 2.0), Point(1.0, 2.0));
    lcc.sew<2>(lcc.alpha<1, 0, 1>(d2), d3);
    if (show_creation) { CGAL::draw(lcc); }
    CGAL_assertion(lcc.is_valid());
    setup_ids(lcc, 0);
    CGAL_assertion(lcc.is_valid());
    return lcc;
}

Dart_handle LinearCellComplexBuilder::get_dart_from_int(Dart_handle dh, int pos, LCC_2 lcc) {
    Dart_handle res;
    switch (pos) {
        // Bas
        case 1:
            res = dh;
            break;
            // Haut
        case 10:
            res = lcc.alpha<1, 0, 1>(dh);
            break;
            // Droite
        case 100:
            res = lcc.alpha<0, 1>(dh);
            break;
            // Gauche
        case 1000:
            res = lcc.alpha<1>(dh);
            break;
        default:
            throw "get_dart_from_int";
    }
    return res;
}

LCC_2 LinearCellComplexBuilder::grid_like(std::vector<float> x, std::vector<float> y,
                                          std::vector <std::vector<std::size_t>> adj,
                                          std::vector <std::size_t> constraints) {
    std::cout << "LinearCellComplexBuilder::grid_like" << std::endl;
    LCC_2 lcc;
    std::size_t nb_cell;
    assert(x.size() == y.size());
    assert(x.size() == constraints.size());
    int dim_quad = 4;
    Vertex_attribute_handle v;
    Dart_handle dh;
    std::vector <Dart_handle> vdh;
    std::size_t vertex_ids = 0;
    // Creation des cellules
    lcc.set_automatic_attributes_management(true);
    std::size_t cell_cpt=0;
    for (std::size_t i = 0; i < x.size(); i += 4) {
        dh = lcc.make_quadrangle(Point(x[i], y[i]), Point(x[i + 1], y[i + 1]), Point(x[i + 2], y[i + 2]),
                                 Point(x[i + 3], y[i + 3]));
        LCC_2::Attribute_handle<2>::type ah = lcc.create_attribute<2>();
        lcc.info_of_attribute<2>(ah) = { cell_cpt, 3100 + cell_cpt };
        cell_cpt++;
        lcc.info_of_attribute<0>(lcc.vertex_attribute(dh)) = {vertex_ids, constraints[vertex_ids]};
        vertex_ids++;
        lcc.info_of_attribute<0>(lcc.vertex_attribute(lcc.alpha<0>(dh))) = {vertex_ids, constraints[vertex_ids]};
        vertex_ids++;
        lcc.info_of_attribute<0>(lcc.vertex_attribute(lcc.alpha<0, 1, 0>(dh))) = {vertex_ids, constraints[vertex_ids]};
        vertex_ids++;
        lcc.info_of_attribute<0>(lcc.vertex_attribute(lcc.alpha<0, 1, 0, 1, 0>(dh))) = {vertex_ids, constraints[vertex_ids]};
        vertex_ids++;
        vdh.push_back(dh);
        for(LCC_2::Dart_of_orbit_range<0, 1>::iterator dit(lcc.darts_of_orbit<0, 1>(dh).begin()),ditend(lcc.darts_of_orbit<0,1>(dh).end()); dit != ditend; ++dit){
            if (lcc.attribute<2>(dit) == NULL)
                lcc.set_attribute<2>(dit, ah);
        }
    }
    int cpt = 0;
    // Couture des cellules adjacentes
    Dart_handle src, des;
    for (std::size_t i = 0; i < adj.size(); i++) {
        for (std::size_t j = 0; j < adj[0].size(); j++) {
            if (adj[i][j] != 0 && adj[j][i] != 0) {
                src = get_dart_from_int(vdh[i], adj[i][j], lcc);
                des = get_dart_from_int(vdh[j], adj[j][i], lcc);
                if (lcc.is_sewable<2>(src, des)) {
                    lcc.sew<2>(src, des);
                }
            }
        }
    }

    std::cout << "Attributes<0>Cellule" << std::endl;
    for (LCC_2::Attribute_range<0>::type::iterator it = lcc.attributes<0>().begin(), itend = lcc.attributes<0>().end();
         it != itend; ++it) {
        auto v = lcc.info_of_attribute<0>(it);
        std::cout << v[0] << " " << v[1] << std::endl;
    }

    std::cout << "Attributes<2>Cellule" << std::endl;
    for (LCC_2::Attribute_range<2>::type::iterator it = lcc.attributes<2>().begin(), itend = lcc.attributes<2>().end();
         it != itend; ++it) {
        auto v = lcc.info_of_attribute<2>(it);
        std::cout << v[0] << " " << v[1] << std::endl;
    }

    lcc.set_automatic_attributes_management(false);
    assert(lcc.is_valid());
    return lcc;
}

// TODO
static LCC_2 from_file(std::istream &ais) {

}