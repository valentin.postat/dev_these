//
// Created by v on 04/02/2021.
//

#ifndef DEV_THESE_LCC_HL_H
#define DEV_THESE_LCC_HL_H

// Vertex
#define __ID 0
#define __CSTR 1
#define __PREV_VALENCE 2

#include "LCCBuilder.h"

#include <iostream>
#include <fstream>


class LCC_HL {

private:
    /** Current id of the map map_vtx*/
    std::size_t current_id;
    /** linear cell complex 2D */
    LCC_2 lcc;
    /** map  id_vtx ,<0>Attribute */
    Map_id_vtx map_vtx;
    /** Current id of the map_cell */
    std::size_t current_face_id;
    /** map id_cell ,<2>Attribute */
    Map_id_cell map_cell;
    /** Marquer les sommets non conforme pour la face **/
    size_type conformity_mark;
    /**Marquer les faces prioritaire après une action spécifique **/
    size_type priority_mark;
    /** Marquer les sommets sur lequel a été fait le pillowing **/
    size_type pillowing_mark;
    /** Marquer les brins sur un layout externe ajouté **/
    size_type border_mark;
    /** Mark of the boundary layer **/
    size_type ghost_layer_mark;
    /** Mark of the selected darts for pillow **/
    size_type selection;


    /***/
    // Map_criticity map_crit;
    /***/
    // Map_conformity map_conf;
    Map_critconf map_crit_conf;

    /**Récupération d'élément d'une lecture json**/
    struct Map_value {
        std::size_t permutation;
        std::string true_word;
        bool reverse;
    };
    /**Récupération d'élément d'une lecture json unique*.json**/
    struct JSON_unique {
        std::vector <std::size_t> candidat;
        std::vector <std::size_t> neighbor;
        std::vector<bool> reverse;
    };

    std::map <std::string, Map_value> map_unique_crit;
    std::map <std::string, Map_value> map_unique_conf;
    std::map <std::string, JSON_unique> map_unique_crit_index;
    std::map <std::string, JSON_unique> map_unique_conf_index;

    /** switch for show with Qt*/
    bool show;
public:
    /** Default **/
    LCC_HL() : LCC_HL(LinearCellComplexBuilder::basic_modelf(false)) {}

    LCC_HL(std::vector<float> x, std::vector<float> y, std::vector <std::vector<std::size_t>> adj,
           std::vector <std::size_t> constraints);

    void writer_paraview(std::string filename);


    LCC_HL(const LCC_HL &other) {
        this->lcc = LCC_2(other.lcc);

        std::cout << "#Valid?" << this->lcc.is_valid() << std::endl;
        try {
            this->selection = lcc.get_new_mark();
        }
        catch (LCC_2::Exception_no_more_available_mark) {
            std::cerr << "No more free mark, exit." << std::endl;
            exit(-1);
        }
        try {
            this->conformity_mark = lcc.get_new_mark();
        }
        catch (LCC_2::Exception_no_more_available_mark) {
            std::cerr << "No more free mark, exit." << std::endl;
            exit(-1);
        }
        try {
            this->priority_mark = lcc.get_new_mark();
        }
        catch (LCC_2::Exception_no_more_available_mark) {
            std::cerr << "No more free mark, exit." << std::endl;
            exit(-1);
        }
        try {
            this->pillowing_mark = lcc.get_new_mark();
        }
        catch (LCC_2::Exception_no_more_available_mark) {
            std::cerr << "No more free mark, exit." << std::endl;
            exit(-1);
        }
        std::cout << " SIZE " << other.lcc.darts().size() << std::endl;
        for (LCC_2::Dart_const_range::const_iterator it(other.lcc.darts().begin()), it2(
                this->lcc.darts().begin()), it2end(this->lcc.darts().end()), itend(other.lcc.darts().end());
             it != itend; ++it, ++it2) {
            if (other.lcc.is_marked(it, other.conformity_mark))
                this->lcc.mark(it2, this->conformity_mark);
            if (other.lcc.is_marked(it, other.priority_mark))
                this->lcc.mark(it2, this->priority_mark);
            if (other.lcc.is_marked(it, other.pillowing_mark))
                this->lcc.mark(it2, this->pillowing_mark);
        }
        std::size_t id;
        for (LCC_2::Attribute_range<0>::type::iterator it = lcc.attributes<0>().begin(), itend = lcc.attributes<0>().end();
             it != itend; ++it) {
            id = lcc.info_of_attribute<0>(it)[0];
            this->map_vtx.insert(std::pair<std::size_t, LCC_2::Vertex_attribute_handle>(id, it));
        }
        for (LCC_2::Attribute_range<2>::type::iterator it = lcc.attributes<2>().begin(), itend = lcc.attributes<2>().end();
             it != itend; ++it) {
            id = lcc.info_of_attribute<2>(it)[0];
            this->map_cell.insert(std::pair<std::size_t, LCC_2::Attribute_handle<2>::type>(id, it));
        }
        this->current_face_id = other.current_face_id;
        this->current_id = other.current_id;
        load_maps();
    }

    LCC_HL(std::string observation_conformity, std::string observation_criticity);
    //! NOT WORKING
    void remove_ghost_layer(){
        bool is_all_orbit_marked;
        std::vector<std::size_t> id_vtx;     //! to delete
        std::vector<std::size_t> id_cell;    //! to delete
        std::cout << "marked darts " << lcc.number_of_marked_darts(ghost_layer_mark) << std::endl;
        std::cout << "marked vtx:" << std::endl;
        for(LCC_2::One_dart_per_cell_range<0>::iterator it = lcc.one_dart_per_cell<0>().begin(), itend = lcc.one_dart_per_cell<0>().end() ; it!=itend;++it){
            if(lcc.is_whole_cell_marked<0>(it,ghost_layer_mark)){
                std::cout << lcc.info<0>(it)[0] << " " << std::endl;
                map_vtx.erase(lcc.info<0>(it)[0]);
                lcc.erase_vertex_attribute(lcc.attribute<0>(it));
            }
        }
        std::cout << "marked cell:" << std::endl;
        for(LCC_2::One_dart_per_cell_range<2>::iterator it = lcc.one_dart_per_cell<2>().begin(), itend = lcc.one_dart_per_cell<2>().end() ; it!=itend;++it){
            if(lcc.is_whole_cell_marked<2>(it,ghost_layer_mark)){
                lcc.unmark_cell<2>(it, ghost_layer_mark);
                std::cout << lcc.info<2>(it)[0] << " " << lcc.number_of_marked_darts(ghost_layer_mark) << std::endl;
                erase_cell(lcc.info<2>(it)[0]);
            }
        }

        /*
        for( LCC_2::Dart_range::iterator it = lcc.darts().begin(), itend =lcc.darts().begin(); it != itend ; ++it ){
            is_all_orbit_marked = true;
            Dart_handle d = lcc.dart_handle(*it);
            if( lcc.is_marked(d,border_mark) ){

                for( LCC_2::Dart_of_orbit_range<1,2>::iterator ito = lcc.darts_of_orbit<1,2>(d).begin(), itendo =lcc.darts_of_orbit<1,2>(d).end(); ito != itendo ; ++ito ){
                    if(!lcc.is_marked(ito,border_mark)){
                        is_all_orbit_marked = false;
                    }
                }
                if (is_all_orbit_marked){
                    map_vtx.erase(lcc.info<0>(d)[0]);
                    lcc.erase_vertex_attribute(lcc.attribute<0>(d));
                }
            }
            if (lcc.is_whole_cell_marked<2>(d,border_mark)){
                erase_cell(lcc.info<2>(d)[0]);
            }
        }
        //! And attributes in the map ?
        lcc.erase_marked_darts(border_mark);*/
        std::cout << "ghost layer mark " << lcc.number_of_marked_darts(ghost_layer_mark) << std::endl;
        assert(lcc.number_of_marked_darts(ghost_layer_mark) == 0);
    }

    std::size_t number_of_crit_negatif() {
        std::size_t number_of_crit_negatif = 0, id;
        for (LCC_2::Attribute_range<0>::type::iterator it = lcc.attributes<0>().begin(), itend = lcc.attributes<0>().end();
             it != itend; ++it) {
            id = lcc.info_of_attribute<0>(it)[0];
            if (get_constraint(id) == criticity::diminution)
                number_of_crit_negatif++;
        }
        return number_of_crit_negatif;
    }

    std::size_t number_of_crit_positif() {
        std::size_t number_of_crit_positif = 0, id;
        for (LCC_2::Attribute_range<0>::type::iterator it = lcc.attributes<0>().begin(), itend = lcc.attributes<0>().end();
             it != itend; ++it) {
            id = lcc.info_of_attribute<0>(it)[0];
            if (get_constraint(id) == criticity::augmentation)
                number_of_crit_positif++;
        }
        return number_of_crit_positif;
    }

    std::vector <std::size_t> gains() {
        return {this->number_of_crit_negatif(), this->number_of_crit_positif(), this->number_of_non_conf()};
    }

    std::size_t number_of_non_conf() {
        return lcc.number_of_marked_darts(this->conformity_mark) / 2;
    }

    void placer_conformite(Dart_handle dh, std::string observation_conformite);

    void placer_criticite(Dart_handle dh, std::string observation_criticite);

    void cells_connected_components();


    Dart_handle first_marked_dart(Dart_handle d);


    std::vector <std::string> representation_unique(std::size_t face_id);

    std::string representation_conformite(std::size_t face_id);

    std::string representation_criticite(std::size_t face_id);

    std::string representation_conformite_unique(std::size_t face_id);

    std::string representation_criticite_unique(std::size_t face_id);

    void set_map_nc(Dart_handle d, std::size_t conformity) {
        std::size_t dart_id = lcc.mdarts.index(d);
        Map_critconf::iterator it = map_crit_conf.find(dart_id);
        if (it != map_crit_conf.end()) {
            it->second = conformity;
        } else {
            map_crit_conf.insert(std::pair<std::size_t, std::size_t>(dart_id, conformity));
        }
    }

    bool is_pillowing_marked(std::size_t id_vtx) {
        Dart_handle dh = lcc.dart_of_attribute<0>(map_vtx[id_vtx]);
        bool marked = 0;
        LCC_2::One_dart_per_incident_cell_range<2, 0>::iterator it = lcc.one_dart_per_incident_cell<2, 0>(dh).begin();
        LCC_2::One_dart_per_incident_cell_range<2, 0>::iterator itend = lcc.one_dart_per_incident_cell<2, 0>(dh).end();
        while (!marked && it != itend) {
            if (lcc.is_marked(it, pillowing_mark) || lcc.is_marked(lcc.alpha<1>(it), pillowing_mark)) {
                marked = 1;
            }
            ++it;
        }
        return marked;
    }

    void mark_pillowing(std::size_t id_vtx) {
        Dart_handle dh = lcc.dart_of_attribute<0>(map_vtx[id_vtx]);
        for (LCC_2::One_dart_per_incident_cell_range<2, 0>::iterator it = lcc.one_dart_per_incident_cell<2, 0>(
                dh).begin(), itend = lcc.one_dart_per_incident_cell<2, 0>(dh).end(); it != itend; ++it) {
            lcc.mark(it, pillowing_mark);
            lcc.mark(lcc.alpha<1>(it), pillowing_mark);
        }
    }

    void unmark_pillowing(std::size_t id_vtx) {
        Dart_handle dh = lcc.dart_of_attribute<0>(map_vtx[id_vtx]);
        for (LCC_2::One_dart_per_incident_cell_range<2, 0>::iterator it = lcc.one_dart_per_incident_cell<2, 0>(
                dh).begin(), itend = lcc.one_dart_per_incident_cell<2, 0>(dh).end(); it != itend; ++it) {
            lcc.unmark(it, pillowing_mark);
            lcc.unmark(lcc.alpha<1>(it), pillowing_mark);
        }
    }

    /**
     * Add a ghost layer of cells (quads) on the border of 2-G-map
     * */
    void add_ghost_layer();


    void select_inside(){
        std::cout << "Selection" << std::endl;
        for (LCC_2::One_dart_per_cell_range<1>::iterator it=lcc.one_dart_per_cell<1>().begin(),itend=lcc.one_dart_per_cell<1>().end(); it!=itend; ++it)
        {
            if( lcc.alpha<2>(it) != it ) {
                //vd.push_back(it);
                //vd.push_back(lcc.alpha<0>(it));
                lcc.mark(it, selection);
                lcc.mark(lcc.alpha<0>(it),selection);
            }
        }

    }

    void select_cell(std::size_t cell_id){
        lcc.mark_cell<2>(lcc.dart_of_attribute<2>(map_cell[cell_id]),selection);
    }

    void select_edge(std::size_t vtx_a ,std::size_t vtx_b){
        Dart_handle d= darts_of_edge(vtx_a,vtx_b);
        lcc.mark(d,selection);
        lcc.mark(lcc.alpha<0>(d),selection);
    }

    void select_outside(){
        std::cout << "Selection" << std::endl;
        for (LCC_2::One_dart_per_cell_range<1>::iterator it=lcc.one_dart_per_cell<1>().begin(),itend=lcc.one_dart_per_cell<1>().end(); it!=itend; ++it)
        {
            if( lcc.alpha<2>(it) == it ) {
                //vd.push_back(it);
                //vd.push_back(lcc.alpha<0>(it));
                lcc.mark(it, selection);
                lcc.mark(lcc.alpha<0>(it),selection);
            }
        }
    }

    void select_validation(){
        size_t count;
        size_t selected_before=lcc.number_of_marked_darts(selection);
        std::cout << "Nb <0> " << lcc.one_dart_per_cell<0>().size() << std::endl;
        std::vector<Dart_handle> extended;
        for (LCC_2::One_dart_per_cell_range<0>::iterator it=lcc.one_dart_per_cell<0>().begin(),itend=lcc.one_dart_per_cell<0>().end(); it!=itend; ++it)
        {
            count = 0;
            for( LCC_2::Dart_of_orbit_range<1,2>::iterator ito = lcc.darts_of_orbit<1,2>(it).begin(), itendo =lcc.darts_of_orbit<1,2>(it).end(); ito != itendo ; ++ito ){
                if(lcc.is_marked(ito,selection)){
                    count++;
                }
            }
            std::cout << "count " << count << std::endl;
            if( (count % 2) != 0 ){
                for( LCC_2::Dart_of_orbit_range<1,2>::iterator ito = lcc.darts_of_orbit<1,2>(it).begin(), itendo =lcc.darts_of_orbit<1,2>(it).end(); ito != itendo ; ++ito ){
                    if( lcc.is_marked(ito,ghost_layer_mark) && lcc.is_marked(lcc.alpha<2>(ito),ghost_layer_mark) ){
                        std::cout << "Extend Selection for " << lcc.info<0>(it)[0] << " " << lcc.info<0>(ito)[0] << " " << lcc.info<0>(ito)[0] << std::endl;
                        extended.push_back(ito);
                        extended.push_back(lcc.alpha<0>(ito));

                        break;
                    }
                }
            }
        }
        for (auto & e : extended) {
            lcc.mark(e,selection);
        }
        std::cout << "Selected " << selected_before << " after " << lcc.number_of_marked_darts(selection) << std::endl;
    }

    void pillowing_2D(std::vector<std::size_t> ep);

    //! Show barycenters of adj 2-cell of the point
    Point barycenters(std::size_t vtx_id){
        return barycenter_orbit(dart_of_vertex_id(vtx_id));
    }

    Point barycenter_orbit(Dart_handle d){
        std::cout << "Barycenter" << std::endl;
        Point barycenter = Point (.0,.0);
        float count = float(lcc.one_dart_per_incident_cell<2,0>(d).size());
        for(LCC_2::One_dart_per_incident_cell_range<2,0>::iterator it = lcc.one_dart_per_incident_cell<2,0>(d).begin(),itend = lcc.one_dart_per_incident_cell<2,0>(d).end(); it != itend ; ++it){
            //std::cout << lcc.info<0>(it)[0] << lcc.barycenter<2>(it) << std::endl;
            barycenter = Point(barycenter.x() + lcc.barycenter<2>(it).x() , barycenter.y() + lcc.barycenter<2>(it).y());
        }
        return Point(barycenter.x()/count, barycenter.y()/count);
    }

    void unmark_selection(){
        lcc.unmark_all(this->selection);
    }

    /**
     * Remove compressed face (alpha0101(d) == d )
     * */
    void remove_cell_0101();

    void pillowing_3D();

    void unmark_cell_prio(std::size_t id_face) {
        Dart_handle dh = lcc.dart_of_attribute<2>(map_cell[id_face]), dtmp = dh;
        do {
            lcc.unmark(dtmp, priority_mark);
            lcc.unmark(lcc.alpha<1>(dtmp), priority_mark);
            dtmp = lcc.alpha<0, 1>(dtmp);

        } while (dtmp != dh);
        //lcc.unmark_cell<2>(dh,priority_mark);
    }

    void unmark_orb_conformite(std::size_t id_vtx) {
        Dart_handle dh = lcc.dart_of_attribute<0>(map_vtx[id_vtx]);
        bool marked = 0;
        LCC_2::One_dart_per_incident_cell_range<2, 0>::iterator it = lcc.one_dart_per_incident_cell<2, 0>(dh).begin();
        LCC_2::One_dart_per_incident_cell_range<2, 0>::iterator itend = lcc.one_dart_per_incident_cell<2, 0>(dh).end();
        while (!marked && it != itend) {
            lcc.unmark(it, conformity_mark);
            lcc.unmark(lcc.alpha<1>(it), conformity_mark);
            if (get_conform_vertex(lcc.info_of_attribute<2>(lcc.attribute<2>(it))[0]).size() > 4) {
                lcc.mark(it, conformity_mark);
                lcc.mark(lcc.alpha<1>(it), conformity_mark);
            }
            ++it;
        }
    }

    float distance(Point a, Point b) {
        return sqrt(((b.x() - a.x()) * (b.x() - a.x())) + ((b.y() - a.y()) * (b.y() - a.y())));
    }

    // TODO a_43
    void action_linking(std::size_t id_face) {
        // - Une face
        // - Edge opposé
        // - Insertion
        // - Lien

        Dart_handle dh = lcc.dart_of_attribute<2>(map_cell[id_face]), dtmp = dh,placed;
        std::vector<Dart_handle> oppositions;
        do {
            if (lcc.is_marked(dtmp, conformity_mark)){
                placed = dtmp;
            }else{
                oppositions.push_back(dtmp);
            }
            dtmp = lcc.alpha<0, 1>(dtmp);
        } while (dtmp != dh);

        // Premiere paire d'arête
        Dart_handle up, down, init_up, init_down,tmp;
        init_up = lcc.alpha<0,1>(oppositions[0]);
        init_down = lcc.alpha<1,0,1>(oppositions[3]);

        if(!lcc.is_marked(init_up,conformity_mark) || !lcc.is_marked(init_down,conformity_mark)){
            init_up = lcc.alpha<0,1>(oppositions[1]);
            init_down = lcc.alpha<1,0,1>(oppositions[0]);
        }
        if(!lcc.is_marked(init_up,conformity_mark)){
            tmp = init_up;
            init_up = init_down;
            init_down = tmp;
        }

        float current;
        float min = distance(lcc.point(init_up),lcc.point(init_down));
        std::pair<Dart_handle,Dart_handle> couple_min(init_up,init_down);
        up = init_up;
        std::cout << "#DISTANCE " << get_id(init_up) << get_id(init_down) << " " << min << " " << get_id(lcc.alpha<0>(up)) << std::endl;
        while( lcc.is_marked(up,conformity_mark) ){
            std::cout << "#up " << get_id(up) << " " << get_id(lcc.alpha<0>(up)) << std::endl;
            down = init_down;
            while( lcc.is_marked(down,conformity_mark)){
                std::cout << "#down " << get_id(down) << " " << get_id(lcc.alpha<0>(down)) << std::endl;
                current = distance(lcc.point(up),lcc.point(down));
                std::cout << get_id(up) << get_id(down) << " " << current << std::endl;
                if( current < min ){
                    min = current;
                    couple_min = std::pair<Dart_handle,Dart_handle>(up,down);
                }
                down = lcc.alpha<0,1>(down);
            }
            up = lcc.alpha<0,1>(up);
        }
        lcc.unmark(couple_min.first,conformity_mark);
        lcc.unmark(lcc.alpha<1>(couple_min.first),conformity_mark);
        lcc.unmark(couple_min.second,conformity_mark);
        lcc.unmark(lcc.alpha<1>(couple_min.second),conformity_mark);
        split_cell(get_id(couple_min.first), get_id(couple_min.second));

    }

    void unmark_orb_conformite_vec(std::vector <std::size_t> ids_vtx) {
        for (auto &id: ids_vtx) {
            unmark_orb_conformite(id);
        }
    }

    /**
     * mise a jour des non conformité sur les face incidentes au sommet 'id_vtx'
     * **/
    void update_nc(std::size_t id_vtx) {
        // Count the number of vertex in cell if > 4 : one dart is marker or added to the map
        Dart_handle dh = dart_of_vertex_id(id_vtx);
        //if(!in_boundary(id_vtx)) {
        std::cout << "id_vtx" << id_vtx << " 0-cell " << lcc.info<0>(dh)[0] << std::endl;
        std::cout << "id_vtx" << id_vtx << " 2-cell " << lcc.info<2>(dh)[0] << std::endl;
        for (LCC_2::One_dart_per_incident_cell_range<2, 0>::iterator it = lcc.one_dart_per_incident_cell<2, 0>(
                dh).begin(), itend = lcc.one_dart_per_incident_cell<2, 0>(dh).end(); it != itend; ++it) {
            /**
            if (get_vertex_of_cell(lcc.info_of_attribute<2>(lcc.attribute<2>(it))[0]).size() > 4) {
                // or add map of mark...
                lcc.mark(it, conformity_mark);
                lcc.mark(lcc.alpha<1>(it), conformity_mark);
                std::cout << "Marked couple of dart vertex " << lcc.info<0>(it)[0] << " in face "
                          << lcc.info<2>(it)[0] << std::endl;
            } else {
                lcc.unmark(it, conformity_mark);
                lcc.unmark(lcc.alpha<1>(it), conformity_mark);
            }
            */
            if (get_conform_vertex(lcc.info_of_attribute<2>(lcc.attribute<2>(it))[0]).size() > 4) {
                lcc.mark(it, conformity_mark);
                lcc.mark(lcc.alpha<1>(it), conformity_mark);
            }/**else if(get_conform_vertex(lcc.info_of_attribute<2>(lcc.attribute<2>(it))[0]).size() == 4 ){
                    lcc.unmark(it, conformity_mark);
                    lcc.unmark(lcc.alpha<1>(it), conformity_mark);
                }**/
        }
        //}
    }

    void set_point(std::size_t id_vtx, Point p) {
        lcc.point_of_vertex_attribute(vertex_handle_of_vertex_id(id_vtx)) = p;
    }

    void set_point_py(std::size_t id_vtx, float x,float y) {
        Point p(x,y);
        lcc.point_of_vertex_attribute(vertex_handle_of_vertex_id(id_vtx)) = p;
    }

    /**
     * mise a jour des criticité sur les sommets critiques
     * **/
    void update_crit(std::vector <std::size_t> id_vertex, std::vector <std::size_t> valence_old,
                     std::vector <std::size_t> crit) {
        std::size_t index = 0;
        std::size_t valence_new;
        
        for (std::vector<std::size_t>::iterator it = id_vertex.begin(); it != id_vertex.end(); ++it, index++) {
            if (map_vtx.find(*it) != map_vtx.end()) {
                valence_new = valence(*it);
                std::cout << "found " << *it << " old " << valence_old[index] << " new " << valence_new << " crit "
                          << crit[index] << std::endl;
                if (crit[index] == criticity::augmentation && valence_old[index] < valence_new) {
                    set_constraint_vtx(id_vertex[index], criticity::neutral);
                } else if (crit[index] == criticity::diminution && valence_old[index] > valence_new) {
                    set_constraint_vtx(id_vertex[index], criticity::neutral);
                }/*else if (crit[index] == criticity::neutral && valence_old[index] > valence_new ){
                    set_constraint_vtx(id_vertex[index], criticity::augmentation);
                }*/else if (crit[index] == criticity::neutral && valence_old[index] < valence_new && in_boundary(*it)) {
                    set_constraint_vtx(id_vertex[index], criticity::diminution);
                } else if (crit[index] == criticity::diminution && valence_old[index] < valence_new &&
                           in_boundary(*it)) {
                    set_constraint_vtx(id_vertex[index], criticity::diminution);
                }
            }
        }
    }

    /**
     * Return 0 if conform else  1
     * if not in
     *
     * */
    std::size_t get_from_map_nc(Dart_handle d) {
        if (map_crit_conf.find(lcc.mdarts.index(d)) != map_crit_conf.end()) {
            return map_crit_conf[lcc.mdarts.index(d)];
        } else if (map_crit_conf.find(lcc.mdarts.index(lcc.alpha<1>(d))) != map_crit_conf.end()) {
            return map_crit_conf[lcc.mdarts.index(lcc.alpha<1>(d))];
        } else {
            return 0;
        }
    }

    void show_map_nc() {
        for (std::map<std::size_t, std::size_t>::iterator it = map_crit_conf.begin(); it != map_crit_conf.end(); ++it)
            std::cout << it->first << " => " << it->second << '\n';
    }

    /** Vertex Attribute Field **/
    std::size_t get_field(std::size_t vtx_id, std::size_t rank);


    void place_from_vertex_id(std::size_t face_id) {
        std::vector <std::string> crit_conf = representation_unique(face_id);
        //JSON_unique conf = map_unique_conf_index[];
        //JSON_unique crit = map_unique_crit_index[];
        //best_candidate(face_id,)
    }

    /**
     * Place le brin de manière unique en fonction des combinaisons possibles
     * @param ids_vtx   :=
     * @param indices   := indices
     * de ids_vtx à comparer
     * @param neighbor  :=
     * @param reverse   :=
     * **/
    void best_candidate(std::size_t face_id, std::vector <std::size_t> ids_vtx, std::vector <std::size_t> indices,
                        std::vector <std::size_t> neighbor, std::vector<bool> reverse) {
        // max_ind = ids_vt[]
        std::size_t max_ind = 0;
        std::size_t max_value = ids_vtx[0];
        std::cout << "ids_vtx" << std::endl;
        for (auto it = ids_vtx.begin(); it != ids_vtx.end(); ++it)
            std::cout << *it << " ";
        std::cout << "\nindices" << std::endl;
        for (auto it = indices.begin(); it != indices.end(); ++it)
            std::cout << *it << " ";
        std::cout << "\nneighbor" << std::endl;
        for (auto it = neighbor.begin(); it != neighbor.end(); ++it)
            std::cout << *it << " ";
        std::cout << "\nreverse" << std::endl;
        for (auto it = reverse.begin(); it != reverse.end(); ++it)
            std::cout << *it << " ";
        bool max_reverse;
        std::size_t offset;
        std::cout << "\nbest_candidate" << std::endl;

        if(ids_vtx.size() != 0){
            for (std::size_t i = 1; i < indices.size(); i++) {
                //std::cout << "max " << max_ind << " ( " << ids_vtx[indices[max_ind]] << ", " << ids_vtx[neighbor[max_ind]] << ") " << std::endl;
                //std::cout << "cur " << i << " ( " << ids_vtx[indices[i]] << ", " << ids_vtx[neighbor[i]] << ") " << std::endl;
                if (ids_vtx[indices[max_ind]] < ids_vtx[indices[i]]) {
                    max_ind = i;
                    max_value = ids_vtx[indices[i]];
                    max_reverse = reverse[i];
                } else if (ids_vtx[indices[max_ind]] == ids_vtx[indices[i]] &&
                           ids_vtx[neighbor[i]] > ids_vtx[neighbor[max_ind]]) {
                    max_ind = i;
                    max_value = ids_vtx[indices[i]];
                    max_reverse = reverse[i];
                }
            }

            offset = indices[max_ind];
            std::cout << "best_candidate end index" << max_ind << " id " << max_value << " " << neighbor[max_ind] << " "
                      << neighbor[max_ind] << " offset " << offset << std::endl;
            //place_from_nc_map(face_id,offset,max_reverse);
            place_dart(face_id, offset, max_reverse);
        }
    }

    void set_vertex_of_cell(std::size_t face_id, std::size_t vertex_id);

    void set_orientation(std::size_t id_cell, std::size_t id_vtx_a, std::size_t id_vtx_b);

    void load_maps();

    /** Place le brins lié à un att'ribut cellule afin d'avoir une représentation unique avant action */
    void place_from_nc_map(std::size_t face_id, std::size_t permutation, bool reverse);

    bool is_face_prio_marked(std::size_t face_id) {
        LCC_2::Attribute_handle<2>::type at = this->map_cell[face_id];
        Dart_handle dh = lcc.dart_of_attribute<2>(at);
        LCC_2::Dart_of_orbit_range<0, 1>::iterator it(lcc.darts_of_orbit<0, 1>(dh).begin());
        LCC_2::Dart_of_orbit_range<0, 1>::iterator itend(lcc.darts_of_orbit<0, 1>(dh).end());
        bool marked = false;
        while (!marked && it != itend) {
            if (lcc.is_marked(it, priority_mark)) {
                marked = true;
            }
            ++it;
        }
        return marked;
    }

    bool is_vtx_prio_marked(std::size_t vtx_id, std::size_t face_id) {
        LCC_2::Attribute_handle<0>::type at = this->map_vtx[vtx_id];
        Dart_handle dh = lcc.dart_of_attribute<0>(at);
        bool marked = false;
        LCC_2::One_dart_per_incident_cell_range<2, 0>::iterator it = lcc.one_dart_per_incident_cell<2, 0>(dh).begin();
        LCC_2::One_dart_per_incident_cell_range<2, 0>::iterator itend = lcc.one_dart_per_incident_cell<2, 0>(dh).end();
        while (!marked && it != itend) {
            if (lcc.info<2>(it)[0] == face_id &&
                (lcc.is_marked(it, priority_mark) || lcc.is_marked(lcc.alpha<1>(it), priority_mark))) {
                marked = true;
                lcc.unmark(it, priority_mark);
                lcc.unmark(lcc.alpha<1>(it), priority_mark);
            }
            ++it;
        }
        return marked;
    }

    void remove_prio_mark(std::size_t face_id) {
        LCC_2::Attribute_handle<2>::type at = this->map_cell[face_id];
        Dart_handle dh = lcc.dart_of_attribute<2>(at);
        std::cout << "#NPRIO_DART" << lcc.number_of_marked_darts(priority_mark) << std::endl;
        lcc.unmark_all(priority_mark);
    }

    void place_dart(std::size_t face_id, std::size_t permutation, bool reverse) {
        LCC_2::Attribute_handle<2>::type at = this->map_cell[face_id];
        Dart_handle dh = lcc.dart_of_attribute<2>(at);
        while (permutation > 0) {
            dh = lcc.alpha<0, 1>(dh);
            permutation--;
        }
        if (reverse) {
            dh = lcc.alpha<1>(dh);
        }
        lcc.set_dart_of_attribute<2>(at, dh);
    }

    /** Place le brins lié à un attribut cellule afin d'avoir une représentation unique avant action */
    void place_from_crit_map(std::size_t face_id, std::size_t permutation, bool reverse);

    void set_vertex_of_cell_of_attr(std::size_t face_id, std::size_t vertex_id);

    std::string rpz(std::size_t face_id);

    std::string rpz_crit(std::size_t face_id);

    std::vector <std::size_t> get_fields(std::size_t vtx_id);

    void set_field(std::size_t vtx_id, std::size_t rank, std::size_t value);

    void set_fields(std::size_t vtx_id, std::vector <std::size_t> vnew);

    /**
     * Return true if the structure is valid*/
    bool is_valid() {
        return this->lcc.is_valid();
    }

    /** Return vertex non_conform **/
    std::vector <std::size_t> get_non_conform_vertex(std::size_t face_id);

    /** Actions**/
    /** Cells operations c **/
    std::size_t split_cell(std::size_t vtx_a, std::size_t vtx_b);

    std::size_t split_edge(std::size_t id_vtx_a, std::size_t id_vtx_b);

    std::size_t split_cell_d(std::size_t face_id, std::size_t vtx_a, std::size_t vtx_b);

    /**
     * Calcul le milieu de la face du dart d (moyenne des coord des sommets la composant)
     * **/
    Point compute_face_midle(Dart_handle d);

    /** Actions Conformes **/
    /** pillowing 1 sommet **/
    void s_1(std::size_t face_id, std::size_t offset);

    /** pillowing 2 sommet **/
    void s_2(std::size_t face_id, std::size_t offset);

    /** pillowing 3 sommet **/
    void s_3(std::size_t face_id, std::size_t offset);

    void s_3_bis(std::size_t face_id, std::size_t offset);

    /** pillowing 4 sommet **/
    void s_4(std::size_t face_id);


    /*placement 1.1 1.3*/
    void nc_1(std::size_t face_id, bool opposite);

    /*placement 1.2 */
    void nc_1_bis(std::size_t face_id, bool opposite);

    void nc_2(std::size_t face_id, bool opposite);


    /// Do nothing
    void c_1(std::size_t face_id);

    std::vector <std::size_t> c_2(std::size_t face_id);

    void c_3(std::size_t face_id);

    void c_4(std::size_t face_id);

    void c_5(std::size_t face_id);

    /** Actions Non Conformes **/
    /** Cells operations nc **/
    void nc_5(std::size_t face_id);

    void nc_6(std::size_t face_id);

    /** Opposite */
    void nc_6a(std::size_t face_id);

    /** Near */
    void nc_6b(std::size_t face_id);

    void nc_7(std::size_t face_id);

    void nc_8(std::size_t face_id);

    void c_9(std::size_t face_id);

    /**----- actions non--conformes ------*/

    void a_0(std::size_t face_id);

    void a_1(std::size_t face_id);

    void a_2(std::size_t face_id);

    void a_3(std::size_t face_id);

    void a_4(std::size_t face_id);

    void a_5(std::size_t face_id);

    void a_6(std::size_t face_id);

    void a_7(std::size_t face_id);

    void a_8(std::size_t face_id);

    void a_9(std::size_t face_id);

    void a_10(std::size_t face_id);

    void a_11(std::size_t face_id);

    void a_12(std::size_t face_id);

    void a_13(std::size_t face_id);

    void a_14(std::size_t face_id);

    void a_15(std::size_t face_id);

    void a_16(std::size_t face_id);

    void a_17(std::size_t face_id);

    void a_18(std::size_t face_id);

    void a_19(std::size_t face_id);

    void a_20(std::size_t face_id);

    void a_21(std::size_t face_id);

    void a_22(std::size_t face_id);

    void a_23(std::size_t face_id);

    void a_24(std::size_t face_id);

    void a_24_bis(std::size_t face_id);

    void a_25(std::size_t face_id);

    void a_26(std::size_t face_id);

    void a_27(std::size_t face_id);

    void a_28(std::size_t face_id);

    void a_29(std::size_t face_id);

    void a_30(std::size_t face_id);

    void a_31(std::size_t face_id);

    void a_32(std::size_t face_id);

    void a_33(std::size_t face_id);

    void a_34(std::size_t face_id);

    void a_35(std::size_t face_id);

    void a_36(std::size_t face_id);

    void a_37(std::size_t face_id);

    void a_38(std::size_t face_id);

    void a_39(std::size_t face_id);

    void a_40(std::size_t face_id);

    void a_41(std::size_t face_id);

    void a_42(std::size_t face_id);

    void a_34_bis(std::size_t face_id);

    void a_34_edge_placement(Dart_handle d) {
        Dart_handle d0 = lcc.alpha<0>(d), d01 = lcc.alpha<0, 1>(d), d010 = lcc.alpha<0, 1, 0>(d), d02 = lcc.alpha<0, 2>(
                d), d021 = lcc.alpha<0, 2, 1>(d), d0101 = lcc.alpha<0, 1, 0, 1>(d);
        lcc.unlink_alpha<1>(d0101);
        lcc.unlink_alpha<1>(d0);
        lcc.unlink_alpha<1>(d02);

        lcc.set_attribute<2>(d0, lcc.attribute<2>(d));
        lcc.link_alpha<1>(d0101, d0);
        lcc.set_attribute<2>(d0101, lcc.attribute<2>(d));
        lcc.link_alpha<1>(d010, d02);
        lcc.set_attribute<2>(d010, lcc.attribute<2>(lcc.alpha<2>(d)));
        lcc.link_alpha<1>(d01, d021);
        lcc.set_attribute<2>(d02, lcc.attribute<2>(lcc.alpha<2>(d)));

        if (!lcc.is_free<2>(lcc.alpha<2>(d0101))) {
            lcc.mark(lcc.alpha<2>(d0101), conformity_mark);
            lcc.mark(lcc.alpha<2>(d010), conformity_mark);
        }
        lcc.mark(lcc.alpha<1>(d021), conformity_mark);
        lcc.mark(lcc.alpha<1>(d01), conformity_mark);

    }

    inline std::size_t number_of_darts() {
        return lcc.number_of_darts();
    }

    /* set a point for the vertex id_vtx on the segment point id_a and id_b at a distance alpha in [0;1]
     * */
    void set_point_from_vtx(std::size_t id_vtx, std::size_t id_a, std::size_t id_b, float alpha) {
        set_point(id_vtx, compute_point(point_of_vertex(id_a), point_of_vertex(id_b), alpha));
    }


    /**Return all critical cells**/
    std::vector <std::size_t> critical_cells();

    /**Return all conform cells**/
    std::vector <std::size_t> conform_cells();

    /**Return all non conform cells**/
    std::vector <std::size_t> non_conform_cells();

    LCC_2::Attribute_handle<2>::type create_face_handle();

    std::size_t get_id(Dart_handle dh);

    std::size_t get_constraint(Dart_handle dh);

    std::vector <std::size_t> get_crit_vertex(std::size_t face_id);

    std::vector <std::size_t> get_conform_vertex(std::size_t face_id) {
        Dart_handle dh = lcc.dart_of_attribute<2>(map_cell[face_id]), dtmp;
        std::vector <std::size_t> vertex_ids;
        dtmp = dh;
        do {
            if (!lcc.is_marked(dtmp, conformity_mark)) {
                vertex_ids.push_back(lcc.info<0>(dtmp)[0]);
            }
            dtmp = lcc.alpha<0, 1>(dtmp);
        } while (dtmp != dh);
        return vertex_ids;
    }


    std::vector <std::size_t> get_criticity_of_vertex(std::vector <std::size_t> vtx_ids) {
        std::vector <std::size_t> criticity;
        criticity.reserve(vtx_ids.size());
        for (auto it = vtx_ids.begin(); it != vtx_ids.end(); ++it) {
            criticity.push_back(get_constraint(*it));
        }
        return criticity;
    }

    std::size_t get_id(Vertex_attribute_handle vah);

    std::size_t get_constraint(Vertex_attribute_handle vah);

    std::size_t get_constraint(std::size_t vertex_id);

    void set_constraint_vtx(std::size_t id_vtx, std::size_t constraint);

    std::size_t nb_vertex() {
        return this->lcc.number_of_attributes<0>();
    }

    /*std::size_t nb_edges() {
        return this->lcc.number_of_attributes<1>();
    }*/

    std::size_t nb_cells() {
        return this->lcc.number_of_attributes<2>();
    }

    void writer();

    /** Return 0 if conform 1 otherwise
     * */
    int is_orb_conform(std::size_t id_vtx) {
        Dart_handle dh = lcc.dart_of_attribute<0>(map_vtx[id_vtx]);
        bool marked = 0;
        LCC_2::One_dart_per_incident_cell_range<2, 0>::iterator it = lcc.one_dart_per_incident_cell<2, 0>(dh).begin();
        LCC_2::One_dart_per_incident_cell_range<2, 0>::iterator itend = lcc.one_dart_per_incident_cell<2, 0>(dh).end();
        while (!marked && it != itend) {
            if (lcc.is_marked(it, conformity_mark) || lcc.is_marked(lcc.alpha<1>(it), conformity_mark)) {
                marked = 1;
            }
            ++it;
        }
        return marked;
    }

    /**
     * Example: LCC_HL lcc_hl(LinearCellComplexBuilder::basic());
     * @param lcc
     */
    LCC_HL(const LCC_2 &lcc);

    ~LCC_HL() { std::cout << "LCC_HL:Destroyed" << std::endl; };

    void reset();

    /**return the valence of a vertex*/
    std::size_t valence(std::size_t id_vertex);

    /**return valence of a list of vertex [0] ids [1] valence*/
    std::vector <std::vector<std::size_t>> valence_l(std::vector <std::size_t> vertex_ids) {
        std::vector <std::vector<std::size_t>> res(2);
        for (auto it = vertex_ids.begin(); it != vertex_ids.end(); ++it) {
            if (map_vtx.find(*it) != map_vtx.end()) {
                res[0].push_back(*it);
                res[1].push_back(valence(*it));
            }
        }
        return res;
    }

    /**
     * Draw the lcc_hl with qt*/
    void draw();

    std::vector <std::size_t> get_all_cells();

    /**
     * Return true if vtx id_ is neighbor to the id_a */
    bool is_neigh(size_t id_a, size_t id_b);

    /*
    Array of neighbors
     */
    std::vector <std::size_t> neigborhood(std::size_t id_vertex);

    /**
     * Display informations about the lcc_lh */
    void display();

    /**
     * @param show
     * Allow to switch on/off the draw method
     */
    void set_draw(bool show);

    /**
     * @return all vertex_ids of the lcc_hl
     */
    std::vector <std::size_t> get_all_vertex_ids();


    /**
     * Get all compressed faces of a vertex 'id' via a vector of dart handle
     * @param id
     * @return
     */
    std::vector <size_t> get_cells_id();

    std::vector <std::size_t> get_vertex_of_cell(std::size_t id_cell);

    std::vector <Dart_handle> get_compressed_faces(std::size_t id);

    std::vector <Dart_handle> get_all_compressed_faces(std::size_t id);

    std::vector <Dart_handle> get_triangular_faces(std::size_t id);

    std::vector <Dart_handle> get_all_non_quad_faces(std::size_t id);

    Vertex_attribute_handle get_vertex_handle(std::size_t id);

    Dart_handle darts_of_edge(std::size_t id_a, std::size_t id_b);

    std::vector <std::size_t> neighbor(std::size_t id_vertex);

    std::vector <std::size_t> iteration_vertex(std::size_t id_vertex);

    std::vector <std::size_t> neighbor_cells(std::size_t id_cell);

    std::vector <std::size_t> iteration_cell(std::size_t id_vertex);

    Vertex_attribute_handle create_vertex_handle(Point p);

    void erase_vertex(std::size_t id);

    void remove_vtx(std::size_t vtx_id) {
        Dart_handle d = lcc.dart_of_attribute<0>(map_vtx[vtx_id]);
        lcc.remove_cell<0>(d);
        map_vtx.erase(vtx_id);
    }

    std::size_t number_of_vertex_attribute() {
        return lcc.number_of_attributes<0>();
    }

    void erase_cell(std::size_t id_cell);

    void open_edge(std::size_t id_a, std::size_t id_b);

    bool are_incident_vertex(std::size_t id_a, std::size_t id_b);

    void locate_all_vertex();

    /**
     * OK
     * Deuxième plus bas niveau avec darts
     * On boundary
     * */
    std::size_t one_split_vertice(Dart_handle dl, Dart_handle dr, const Point &p);

    /**
     * Inside
     * **/
    std::size_t one_split_vertice_bis(Dart_handle dl, Dart_handle dr, const Point &p);


    std::size_t two_split_vertice(Dart_handle dl, Dart_handle dr, const Point &p);

    /**
     * @param v
     */
    //void pillowing(std::vector <std::size_t> v);


    /**
     * Set the 'entity' of a vertex pointed by Dart_handle 'd'
     * */
    void set_entity(Dart_handle d);


    /**
     * @param v
     * @param w coefficient to compute the new point
     * @param open_one_compressed_vertice compute first and last element to pillow (one edge open)
     */
    void pillowing_out(std::vector <std::size_t> v, float w, bool open_one_compressed_vertice = false);

    /**
     * @param path
     * @return
     */
    void check_path(std::vector <Dart_handle> &path, std::vector<Dart_handle>::iterator &it_curr);

    /**
     * Pillowing with side vertice 'id' neighbour of the first element of 'v'
     * @param v
     * @param id
     * @param w
     * return new _ids of vertex
     */
    std::vector <std::size_t>
    pillowing(std::vector <std::size_t> v, std::size_t id, float w, bool open_one_compressed_vertice = false);

    /**
     * Compute new point using Darts dl,dr only for 2-edge open vertice
     * */
    Point compute_new_point(Dart_handle dl, Dart_handle dr, float w, bool in = true);

    /**
     * Compute p3 point on the segment [p1,p2] with alpha in [0.0,1.0]
     * */
    Point compute_point(Point p1, Point p2, float alpha);

    /**
     * @param dl
     * @param dr
     * @param w
     * @param in
     * @return
     */
    Point compute_new_point_border(Dart_handle dl, Dart_handle dr, float w, bool in = true);

    /**
     * @param id
     */
    void three_split_vertice(std::size_t id);

    /**
     * @param id
     */
    void four_split_vertice(std::size_t id);

    /**
     * @param id
     * @return
     */
    Dart_handle dart_of_vertex_id(std::size_t id);

    /**
     * @param id
     */
    void show_edge(std::size_t id);

    /**
     * @param id
     */
    void show_tri(std::size_t id);

    /**
     * @param id
     */
    void show_quad(std::size_t id);

    /**
     * @param id
     * @return
     */
    Vertex_attribute_handle vertex_handle_of_vertex_id(std::size_t id);

    //!Après remarque franck(discord)
    /**
     * @param id : id of vertex
     * @return all incident of the vertex
     * */
    std::vector <std::size_t> incident_edge(std::size_t id, bool return_self_id = false);

    /**
     * @param id : id of vertex
     * @return triangle(s) incident of the vertex
     * */
    std::vector <std::size_t> incident_tri(std::size_t id);

    /**
     * @param id : id of vertex
     * @return  quad(s) incident of the vertex
     * */
    std::vector <std::size_t> incident_quad(std::size_t id);

    bool on_boundary(std::size_t id_a, std::size_t id_b);


    /**
     * @param face_id
     * @return bool
     * Retourne quels sont les sommets pour lesquelles l'action a34 est valable.
    **/
    std::vector<bool> is_a_34_available(std::size_t face_id);

    /**
     * Return true if it's on a corner false else
     *
     * */
    bool corner_vertex(std::size_t vertex_id) {

    }

    std::size_t action_negativ(std::size_t face_id);

    /**
     * @param id : id of vertex
     * @return true if the vertex is on the boundary else false
     */
    bool in_boundary(std::size_t id);

    //Si tu est bord quelle est la dimension de l'entité géométrique sur laquelle tu vis (0 ou 1) et son id
    std::size_t entity(std::size_t id);

    /**
     * Provide a path : vector of darts for each 'vertice_id' in 'v'.
     * The first dart will be to the first
     * */
    std::vector <Dart_handle> same_side_with(std::vector <std::size_t> v, std::size_t required_id);

    /**
     * @param dl
     * @param dr
     * @return
     */
    bool same_side(Dart_handle dl, Dart_handle dr);

    /**
     * @param ids
     * @return
     */
    std::vector <Dart_handle> same_side(std::vector <std::size_t> ids);

    /**
     * @param dl
     * @param dr
     * @return
     */
    Dart_handle same_side_fix(Dart_handle dl, Dart_handle dr);

    /**
     * @param v_dh
     */
    void switch_side(std::vector <Dart_handle> &v_dh);

    /**
     * @param id
     * @return
     */
    Point point_of_vertex(std::size_t id);


};


#endif //DEV_THESE_LCC_HL_H
