# state file generated using paraview version 5.4.1

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1003, 581]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [1.0, 0.5, 3.0]
renderView1.StereoType = 0
renderView1.CameraPosition = [0.8303668827369219, -11.8574101557926, 3.528887471592512]
renderView1.CameraFocalPoint = [0.9999999999999902, 0.5000000000000131, 3.000000000000007]
renderView1.CameraViewUp = [-0.018438114652045697, 0.043005442150823775, 0.9989046840782608]
renderView1.CameraParallelScale = 3.2015621187164243
renderView1.Background = [0.32, 0.34, 0.43]

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

FileNames=['/home/v/Documents/These/dev_these/dev_these/dev/runs/981/exec000_action34_on_5_a.vtk']
FileNames2=['/home/v/Documents/These/dev_these/dev_these/dev/VTK_example2_VTK.vtk']
# create a new 'Legacy VTK Reader'
vTK_example_VTKvtk = LegacyVTKReader(FileNames=FileNames)

# create a new 'Extract Edges'
extractEdges1 = ExtractEdges(Input=vTK_example_VTKvtk)

# create a new 'Tube'
tube1 = Tube(Input=extractEdges1)
tube1.Scalars = ['POINTS', 'my_point_id']
tube1.Vectors = ['POINTS', 'vectors']
tube1.Radius = 0.05

# create a new 'Extract Surface'
extractSurface1 = ExtractSurface(Input=vTK_example_VTKvtk)

# create a new 'Glyph'
glyph1 = Glyph(Input=vTK_example_VTKvtk,
    GlyphType='Sphere')
glyph1.Scalars = ['POINTS', 'my_point_id']
glyph1.Vectors = ['POINTS', 'vectors']
glyph1.Orient = 0
glyph1.ScaleFactor = 0.6000000000000001
glyph1.GlyphMode = 'Every Nth Point'
glyph1.GlyphTransform = 'Transform2'

# init the 'Sphere' selected for 'GlyphType'
glyph1.GlyphType.Radius = 0.2

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get color transfer function/color map for 'GlyphVector'
glyphVectorLUT = GetColorTransferFunction('GlyphVector')
glyphVectorLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'GlyphVector'
glyphVectorPWF = GetOpacityTransferFunction('GlyphVector')
glyphVectorPWF.ScalarRangeInitialized = 1

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from glyph1
glyph1Display = Show(glyph1, renderView1)
# trace defaults for the display properties.
glyph1Display.Representation = 'Surface'
glyph1Display.ColorArrayName = ['POINTS', 'GlyphVector']
glyph1Display.LookupTable = glyphVectorLUT
glyph1Display.MapScalars = 0
glyph1Display.OSPRayScaleArray = 'GlyphScale'
glyph1Display.OSPRayScaleFunction = 'PiecewiseFunction'
glyph1Display.SelectOrientationVectors = 'GlyphVector'
glyph1Display.ScaleFactor = 0.6600000202655792
glyph1Display.SelectScaleArray = 'GlyphScale'
glyph1Display.GlyphType = 'Arrow'
glyph1Display.GlyphTableIndexArray = 'GlyphScale'
glyph1Display.DataAxesGrid = 'GridAxesRepresentation'
glyph1Display.PolarAxes = 'PolarAxesRepresentation'

# show color legend
glyph1Display.SetScalarBarVisibility(renderView1, True)

# show data from tube1
tube1Display = Show(tube1, renderView1)
# trace defaults for the display properties.
tube1Display.Representation = 'Surface'
tube1Display.ColorArrayName = ['POINTS', '']
tube1Display.OSPRayScaleArray = 'my_point_id'
tube1Display.OSPRayScaleFunction = 'PiecewiseFunction'
tube1Display.SelectOrientationVectors = 'vectors'
tube1Display.ScaleFactor = 0.5100000191479922
tube1Display.SelectScaleArray = 'my_point_id'
tube1Display.GlyphType = 'Arrow'
tube1Display.GlyphTableIndexArray = 'my_point_id'
tube1Display.DataAxesGrid = 'GridAxesRepresentation'
tube1Display.PolarAxes = 'PolarAxesRepresentation'

# show data from extractSurface1
extractSurface1Display = Show(extractSurface1, renderView1)
# trace defaults for the display properties.
extractSurface1Display.Representation = 'Surface'
extractSurface1Display.ColorArrayName = ['POINTS', '']
extractSurface1Display.MapScalars = 0
extractSurface1Display.OSPRayScaleArray = 'my_point_id'
extractSurface1Display.OSPRayScaleFunction = 'PiecewiseFunction'
extractSurface1Display.SelectOrientationVectors = 'vectors'
extractSurface1Display.ScaleFactor = 0.6000000000000001
extractSurface1Display.SelectScaleArray = 'my_point_id'
extractSurface1Display.GlyphType = 'Arrow'
extractSurface1Display.GlyphTableIndexArray = 'my_point_id'
extractSurface1Display.DataAxesGrid = 'GridAxesRepresentation'
extractSurface1Display.PolarAxes = 'PolarAxesRepresentation'

# setup the color legend parameters for each legend in this view

# get color legend/bar for glyphVectorLUT in view renderView1
glyphVectorLUTColorBar = GetScalarBar(glyphVectorLUT, renderView1)
glyphVectorLUTColorBar.WindowLocation = 'UpperRightCorner'
glyphVectorLUTColorBar.Title = 'GlyphVector'
glyphVectorLUTColorBar.ComponentTitle = 'Magnitude'

# ----------------------------------------------------------------
# finally, restore active source
SetActiveSource(extractSurface1)
# ----------------------------------------------------------------
