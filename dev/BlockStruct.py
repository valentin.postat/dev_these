# python3.8
import matplotlib.pyplot as plt
import pybinding as env
import numpy as np
import copy
import Precompute
import os


# Gym
class MyEnv:
    def __init__(self):
        pass

    def reset(self):
        pass

    def step(self):
        pass


# not gym
class back_env:

    def __init__(self):
        self.lcc_hl = None
        pass

    def feasible_action(self, observation):
        pass

    def action_assign(self):
        """
        self.actions_list = [self.lcc_hl.s_1, self.lcc_hl.s_1, self.lcc_hl.s_1, self.lcc_hl.s_1, self.lcc_hl.s_2,
                             self.lcc_hl.s_2,
                             self.lcc_hl.s_2,
                             self.lcc_hl.s_2, self.lcc_hl.s_3, self.lcc_hl.s_3, self.lcc_hl.s_3, self.lcc_hl.s_3,
                             self.lcc_hl.s_4]
        """
        self.actions_list = [self.lcc_hl.a_0,
                             self.lcc_hl.a_1,
                             self.lcc_hl.a_2,
                             self.lcc_hl.a_3,
                             self.lcc_hl.a_4,
                             self.lcc_hl.a_5,
                             self.lcc_hl.a_6,
                             self.lcc_hl.a_7,
                             self.lcc_hl.a_8,
                             self.lcc_hl.a_9,
                             self.lcc_hl.a_10,
                             self.lcc_hl.a_11,
                             self.lcc_hl.a_12,
                             self.lcc_hl.a_13,
                             self.lcc_hl.a_14,
                             self.lcc_hl.a_15,
                             self.lcc_hl.a_16,
                             self.lcc_hl.a_17,
                             self.lcc_hl.a_18,
                             self.lcc_hl.a_19,
                             self.lcc_hl.a_20,
                             self.lcc_hl.a_21,
                             self.lcc_hl.a_22,
                             self.lcc_hl.a_23,
                             self.lcc_hl.a_24,
                             self.lcc_hl.a_25,
                             self.lcc_hl.a_26,
                             self.lcc_hl.a_27,
                             self.lcc_hl.a_28,
                             self.lcc_hl.a_29,
                             self.lcc_hl.a_30,
                             self.lcc_hl.a_31,
                             self.lcc_hl.a_32,
                             self.lcc_hl.a_33,
                             self.lcc_hl.a_34,
                             self.lcc_hl.a_35,
                             self.lcc_hl.a_36,
                             self.lcc_hl.a_37,
                             self.lcc_hl.a_38,
                             self.lcc_hl.a_39,
                             self.lcc_hl.a_40,
                             self.lcc_hl.a_41,
                             self.lcc_hl.a_42,
                             self.lcc_hl.action_linking
                             ]

    def evaluate_gains(self, a ):
        """Evaluate the gain of an action"""
        pass

    def is_valid_blocking(self):
        return self.lcc_hl.number_of_non_conf == 0

    def constraints(self):
        """Return the constraints as a triplet  (non conformity, criticty + ,criticity -) """
        return np.array(self.lcc_hl.gains())

    def obs_face(self, target_face):
        return self.lcc_hl.representation_unique(target_face)

    def get_obs_reflex_agents(self):
        faces = self.lcc_hl.get_cells_id()
        obs = []  # obs, face_id
        for f in faces:
            obs.append([self.obs_face(f), f])
        return obs

    def execute_action(self, a, cell_id):
        assert a <= len(self.actions_list)
        self.lcc_hl.representation_unique(cell_id)
        # ^------ TODO REMOVE THAT just place well the dart
        # Get old constraint
        vtx_id = self.lcc_hl.get_vertex_of_cell(cell_id)
        vtx_id, vold = self.lcc_hl.valence_l(vtx_id)
        # Call the action
        self.actions_list[a].__call__(cell_id)
        # s'
        # Environment compute new state s' from s
        vtx_new, vnew = self.lcc_hl.valence_l(vtx_id)
        criticity = [self.lcc_hl.get_constraint(x) for x in vtx_id]
        self.lcc_hl.update_crit(vtx_new, vold, criticity)

    def from_graph_constraint(self, graph_of_cells, constraints_vertex, show=False):
        """
        xs : coords x
        ys : coords y
        adj_mat : matrix of cells with shared boundaries
        constraints : int linked to xs,ys index
        """
        self.graph_of_cells = copy.deepcopy(graph_of_cells)
        self.constraints_vertex = copy.deepcopy(constraints_vertex)
        pre = Precompute.Precompute()
        print(graph_of_cells)
        print(constraints_vertex)
        self.params = xs, ys, adj_mat, constraints = pre.get_x_y_constraints_adj(copy.deepcopy(self.graph_of_cells),
                                                                                 copy.deepcopy(self.constraints_vertex), show)
        self.lcc_hl = env.LCC_HL(*self.params)
        """Init list with cells not processed"""
        self.cells = self.lcc_hl.get_cells_id()
        self.action_assign()
        self.prio_cell_sort = []

    def from_back_env(self, other):
        self.lcc_hl = env.LCC_HL(other.lcc_hl)
        self.action_assign()

    def from_obs(self, conformity, criticity):
        """
        :param conformity:
        :param criticity:
        :return:
        """
        self.params = conformity, criticity
        self.lcc_hl = env.LCC_HL(*self.params)
        self.cells = self.lcc_hl.get_cells_id()
        self.action_assign()
        self.prio_cell_sort = []

    def reset(self):
        """Reset the environment"""
        self.lcc_hl = env.LCC_HL(*self.params)
        self.cells = self.lcc_hl.get_cells_id()
        self.prio_cell_sort = []
        self.action_assign()

    def update(self, show=0, output_file=''):

        self.lcc_hl.writer()
        out_vtk = output_file.replace('png',"vtk")
        self.lcc_hl.writer_paraview(out_vtk)
        self.render(show, output_file)

    def render(self, show=0, output_file=''):
        """
        :param show:
        :param output_file:
        :return:
        """
        # os.system("python3 ReadLegacyUnstructuredGrid.py example.vtk")
        assert output_file == '' or (output_file[-4:] == '.png')
        if output_file != '':
            output_file = ' ' + output_file
        os.system("python3 ReadLegacyUnstructuredGrid.py --show " + str(show) + " example.vtk" + output_file)

    def get_obs_vertex_based(self, vertex_id, vision, d=0):
        """
        :param vertex_id:
        :param vision: unsigned int : number of neighbor considered
        :param d distance of vision (d)
        :return:
        """
        neighborhood = [0] * vision
        border = [0] * vision
        set = [0] * vision
        critical = [0] * vision

        neigh = self.lcc_hl.incident_edge(vertex_id, False)
        taille = min(len(neighborhood), len(neigh))
        for i in range(0, taille):
            border[i] = self.lcc_hl.on_boundary(vertex_id, neigh[i])
            critical[i] = self.lcc_hl.get_constraint(neigh[i])
        return vertex_id, critical, border, set

    def step(self, actions, actions_list, parameters_list):
        """
        Sauvegarde d'éléments,
        Execute les actions,
        Met a jour les informations sur l'environnement,
        Calcule la récompense,
        Calcule condition d'arret

        :param actions:
        :param actions_list:
        :return:
        """
        done = False
        obs = {}
        reward = {}
        print(actions)
        for k, v in actions.items():
            print("action", v, "len action_list", len(actions_list))
            print("execution de l'action", v[0], "sur la cellule", v[1], len(self.lcc_hl.get_non_conform_vertex(v[1])))
            face_id = v[1]
            action = v[0]

            actions_list[action].__call__(face_id, *parameters_list[action])

            reward[k] = self.reward(crit)
            print("Récompense obtenue", reward[k])
            if self.end_condition():
                done = True
                break

        for k in actions.items():
            print("k", k)
            obs[k[0]] = self.get_obs()
        print("#OBS", obs)  # return <obs>, <reward: float>, <done: bool>, <info: dict>
        return obs, reward, done

    def get_obs(self):
        """
        :return: obs of cells
        """
        if len(self.cells) == 0:
            self.cells = self.lcc_hl.get_cells_id()
        # cell_id = self.cells.pop(0)
        cell_id = self.get_cell_to_process()
        return [self.get_obs_cell(cell_id, 0), cell_id]

    def priority(self, cell_id):
        """
        Retourne la priorité d'une cellule 'cell_id' max en premier
        0 -> conforme et non critique
        1-8 -> non-conforme
        9-12 -> critique
        """
        crit, conf = self.lcc_hl.representation_unique(cell_id)

        non_conform_vtx = conf.count('1')
        crit_vertex_conform_vtx = crit.count('1') + crit.count('2')
        prio = 0
        if crit_vertex_conform_vtx:
            # 1
            prio = 8 + crit_vertex_conform_vtx
        elif non_conform_vtx:
            prio = non_conform_vtx
        # Regle de propagation de 35
        if self.lcc_hl.is_face_prio_marked(cell_id):
            prio = 20
        if crit.count('2'):
            prio = 40
        print("#priority:", prio, cell_id, crit, conf)
        return prio

    # """Reward fonction en général R(o,o') o avant action o' après action envoyer les sommets critiques """

    def get_prio(self, cells_id):
        """Retourne une cellule à traiter un tuple (priorite_de_traitement,cellule_id) pour chaque cellule_id tri décroissant"""
        p = []
        faces = []
        for c in cells_id:
            prio = self.priority(c)
            if prio > 0:
                p.append(prio)
                faces.append(c)
        print(cells_id)
        print(p)
        # if len(self.prio_cell_sort) == 0:
        self.prio_cell_sort = sorted(zip(p, faces), reverse=True)
        if len(self.prio_cell_sort) == 0:
            return -1
        return self.prio_cell_sort.pop(0)[1]

    def get_obs_cell(self, cell_id, vision=0):
        """
        :return : critical [0|1], conform_n [4,8] , cell
        """
        repr = self.lcc_hl.representation_criticite_unique(cell_id)
        return repr

    def end_condition(self):
        """
        return True if it's done False if not
        """
        vertex = self.lcc_hl.get_all_vertex_ids()
        no_non_conform_vertex = True
        no_critical_vertex = True
        for x in vertex:
            # Test if critical
            if self.lcc_hl.get_field(x, 1) != 0:
                no_critical_vertex = False
                break
            # Test if conform
            elif self.lcc_hl.get_field(x, 3) != 0:
                no_non_conform_vertex = True
                break

        return no_critical_vertex and no_non_conform_vertex

    def reward(self, crit):
        """ for all critical vertex in cell as c, if valence(c) == target_valence , reward +=1, else 0 """
        reward = 0
        for c in crit:
            if self.lcc_hl.valence(c) == self.lcc_hl.get_field(c, 1):
                """Remove the mark on the crit vertex"""
                self.lcc_hl.set_field(c, 1, 0)
                reward += 1
        return reward


def show(self):
    self.lcc_hl.set_draw(1)
    self.lcc_hl.draw()
    self.lcc_hl.set_draw(0)


def show_env_state(self, show_model=False):
    self.lcc_hl.set_draw(show_model)
    print("#Edges")
    for i in self.lcc_hl.get_all_vertex_ids():
        print(i, " : ", self.lcc_hl.incident_edge(i, False))
    print("#Quads")
    for i in self.lcc_hl.get_all_vertex_ids():
        print(i, " : ", self.lcc_hl.incident_quad(i))
    print("#Point ")
    for i in self.lcc_hl.get_all_vertex_ids():
        print(i, "\t", self.lcc_hl.point_of_vertex(i), "\t", self.lcc_hl.get_constraint(i))
    print("#input_constraints")
    print(self.constraints_vertex)
    print("#input_topo")
    print(self.graph_of_cells)
    print("#All quads")
    q = self.split_quad()
    for x in q:
        print(x)
    self.lcc_hl.draw()


def split_quad(self):
    quad_list = []
    quads = self.lcc_hl.get_all_quads()
    for x in np.array_split(quads, len(quads) / 4):
        quad_list.append(list(x))
    return quad_list


"""Cell + action"""




def check_operations():
    graphs_cell = [[1, 1, 1], [1, 1, 1], [1, 1, 1]]
    constraints = [[1, 1, 1, 1], [0, 0, 0, 0], [0, 0, 0, 0], [1, 1, 1, 1]]
    b = back_env()
    b.from_graph_constraint(graphs_cell, constraints)
    ext = ".png"
    name = "c_1"
    b.reset()
    b.lcc_hl.c_1(0)
    b.update(1, name + ext)

    name = "c_2"
    b.reset()
    b.lcc_hl.c_2(0)
    b.update(1, name + ext)

    name = "c_3"
    b.reset()
    b.lcc_hl.c_3(0)
    b.update(1, name + ext)

    name = "c_4"
    b.reset()
    b.lcc_hl.c_4(0)
    b.update(1, name + ext)

    name = "c_5"
    b.reset()
    b.lcc_hl.c_5(0)
    b.update(1, name + ext)

    name = "nc_5"
    b.reset()
    b.lcc_hl.split_edge(14, 2)
    b.lcc_hl.nc_5(4)
    b.update(1, name + ext)

    name = "nc_6a"
    b.reset()
    b.lcc_hl.split_edge(14, 2)
    b.lcc_hl.split_edge(18, 6)
    b.lcc_hl.nc_6a(4)
    b.update(1, name + ext)

    # nc_6b
    name = "nc_6b"
    b.reset()
    b.lcc_hl.split_edge(14, 2)
    b.lcc_hl.split_edge(6, 2)
    b.lcc_hl.nc_6b(4)
    b.update(1, name + ext)

    # nc_7
    name = "nc_7"
    b.reset()
    b.lcc_hl.set_field(b.lcc_hl.split_edge(14, 2), 3, 1)
    b.lcc_hl.set_field(b.lcc_hl.split_edge(6, 2), 3, 1)
    b.lcc_hl.set_field(b.lcc_hl.split_edge(14, 18), 3, 1)
    b.lcc_hl.nc_7(4)
    b.update(1, name + ext)

    # nc_8
    print("nc_8")
    name = "nc_8"
    b.reset()
    b.lcc_hl.split_edge(14, 2)
    b.lcc_hl.split_edge(6, 2)
    b.lcc_hl.split_edge(14, 18)
    b.lcc_hl.split_edge(18, 6)
    b.lcc_hl.nc_8(4)
    b.update(1, name + ext)


def test_prepare_act():
    # Graph des cellules
    g = [[0, 1], [1, 1]]
    # Contraintes
    c = [[0, 2, 0], [2, 0, 0], [0, 0, 0]]
    print("g:", g)
    print("c:", c)
    selected = 3
    sommets = [0, 1, 2, 3]
    a = prepare_action_conformes(selected_vertice=selected, selected_actions=0, sommets=sommets.copy())


def plotting(xs, ys, tmp_carr, ids):
    plt.scatter(xs, ys, c=tmp_carr, label="Black -> Constraints, Yellow -> Neutral")
    for x in range(len(ids)):
        plt.text(xs[x] + 0.2, ys[x], str(ids[x]))
    plt.legend()
    plt.show()


def show_plot(b):
    vertex = b.lcc_hl.get_all_vertex_ids()
    pillowing = {}
    for x in vertex:
        pillowing[x] = 0
    xs = []
    ys = []
    p = []
    ids = []
    for k, v in pillowing.items():
        x = b.lcc_hl.point_of_vertex(k).x()
        y = b.lcc_hl.point_of_vertex(k).y()
        print(k, v, x, y)
        xs.append(x)
        ys.append(y)
        p.append(v)
        ids.append(k)
    plotting(xs, ys, p, ids)
