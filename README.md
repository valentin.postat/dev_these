# Dev These
Programmes effectués durant la thèse.

Installation **CGAL 5.1.1**:

Pour disposer de l'interface, il est nécessaire d'installer un paquet de qt:    
``` {.bash}
sudo apt-get install libqt5svg5-dev
```

Pour l'installation de cgal (clone + install):    
https://github.com/CGAL/cgal/blob/master/INSTALL.md

Depuis les sources, les scripts de création des makefile sont disponibles dans :   
``` {.bash}
/path/to/cgal.git/Scripts/scripts/cgal_create_cmake_script
```

Faire un dossier contenant les sources de cgal et pybind11.
L'arbrescence doit être de ce type:
``` {.bash}
dev_these/cgal-5.1.x-branch/
dev_these/pybind11/
dev_these/*.py|\*.cpp|\*.hpp
```
Ensuite pour compiler un programme de dev_these/: 
``` {.bash}
cd dev_these/
mkdir -p build/debug
cd build/debug
cmake -DCMAKE_BUILD_TYPE=Debug -DCGAL_DIR:PATH=../../../cgal-5.1.x-branch/ ../..
make pybinding
```

La compilation prend un peu de temps mais n'est plus appelé ensuite grace au binding python.

Pour lancer un entrainement:
make pybinding && pyton3.8 main.py


