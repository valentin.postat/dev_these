# state file generated using paraview version 5.4.1

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1003, 465]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.StereoType = 0
renderView1.CameraPosition = [0.0, 0.0, 3.2903743041222895]
renderView1.CameraParallelScale = 0.8516115354228021
renderView1.Background = [0.32, 0.34, 0.43]

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'Sphere'
sphere1 = Sphere()

# create a new 'Add Field Arrays'
addFieldArrays1 = AddFieldArrays(Input=sphere1,
    FileName='')

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from addFieldArrays1
addFieldArrays1Display = Show(addFieldArrays1, renderView1)
# trace defaults for the display properties.
addFieldArrays1Display.Representation = 'Surface'
addFieldArrays1Display.ColorArrayName = [None, '']
addFieldArrays1Display.OSPRayScaleArray = 'Normals'
addFieldArrays1Display.OSPRayScaleFunction = 'PiecewiseFunction'
addFieldArrays1Display.SelectOrientationVectors = 'None'
addFieldArrays1Display.ScaleFactor = 0.1
addFieldArrays1Display.SelectScaleArray = 'None'
addFieldArrays1Display.GlyphType = 'Arrow'
addFieldArrays1Display.GlyphTableIndexArray = 'None'
addFieldArrays1Display.DataAxesGrid = 'GridAxesRepresentation'
addFieldArrays1Display.PolarAxes = 'PolarAxesRepresentation'

# ----------------------------------------------------------------
# finally, restore active source
SetActiveSource(addFieldArrays1)
# ----------------------------------------------------------------