#ifndef DEV_THESE_TYPESFORLCC_H
#define DEV_THESE_TYPESFORLCC_H

#include <CGAL/Linear_cell_complex_for_generalized_map.h>
#include <CGAL/draw_linear_cell_complex.h>
#include <cstdlib>
#include <iostream>
#include <CGAL/Cell_attribute.h>

#define assert_msg(x) !(std::cerr << "Assertion failed: " << x << std::endl)

// Test adding an index to Vertex_handle
struct Average_functor {
    template<class CellAttribute>
    void operator()(CellAttribute &ca1, CellAttribute &ca2) { ca1.info() = ca2.info(); }
};


struct Sum_functor {
    template<class Cell_attribute>
    void operator()(Cell_attribute &ca1, Cell_attribute &ca2) { ca1.info() = ca1.info() + ca2.info(); }
};

struct Divide_by_two_functor {
    template<class Cell_attribute>
    void operator()(Cell_attribute &ca1, Cell_attribute &ca2) {
        ca1.info() = (ca1.info() / 2);
        ca2.info() = (ca1.info());
    }
};

/// The std::size_t is the type added to Attribute.
/// We need to add a type for more.
struct Myitem {
    template<class Refs>
    struct Dart_wrapper {
        /** Here for Attributes */
        /** Vertex : points and id */
        typedef CGAL::Cell_attribute_with_point <Refs, std::vector<std::size_t>, CGAL::Tag_true> Vertex_attribute;
        /** Facet : an int */
        /// typedef std::tuple<void, void, Facet_attribute> Attributes;
        typedef CGAL::Cell_attribute <Refs, std::vector<std::size_t>, CGAL::Tag_true> Facet_attribute;
        /// <0> Vertex attribute <1> Void <2> Facet_Attribute
        typedef std::tuple<Vertex_attribute, void, Facet_attribute> Attributes;
    };
};

typedef CGAL::Linear_cell_complex_traits<2, CGAL::Exact_predicates_inexact_constructions_kernel> Traits;
typedef CGAL::Linear_cell_complex_for_generalized_map<2, 2, Traits, Myitem> LCC_2;

// DEPRECIATED old version
//typedef CGAL::Linear_cell_complex_for_generalized_map<2> LCC_2;
//typedef CGAL::Linear_cell_complex_for_generalized_map<2,Myitem> LCC_2;
// Essayer avec les items
typedef LCC_2::Dart_handle Dart_handle;
typedef LCC_2::Point Point;

typedef LCC_2::Vertex_attribute_handle Vertex_attribute_handle;
typedef LCC_2::Attribute_handle<2>::type Facet_attribute;

typedef std::map <std::size_t, LCC_2::Vertex_attribute_handle> Map_id_vtx;
typedef std::map <std::size_t, LCC_2::Attribute_handle<2>::type> Map_id_cell;

typedef LCC_2::size_type size_type;

enum criticity
{
    neutral = 0,
    augmentation = 1,
    diminution = 2,
    diminution_interne = 3 // Possibilité d'action de diminution arrête au bord
};


class Info_nc_c{
    private:
    std::size_t criticity;
    std::size_t conformity;
public:
    Info_nc_c():criticity(0),conformity(0){}
    Info_nc_c(std::size_t criticity ,std::size_t conformity):criticity(criticity),conformity(conformity){}
    Info_nc_c(std::size_t criticity):Info_nc_c(criticity,0){}

};

/***/
typedef std::map <std::size_t, std::size_t> Map_critconf;




/*
struct Merge_functor {
    // operator() automatically called before a merge.
    void operator()(Facet_attribute &ca1, Facet_attribute &ca2) {
        //ca1.info()=ca1.info()+ca2.info(); // Update can be done incrementally.
        //std::cout<<"After on merge faces: info of face1="<<ca1.info()<<", info of face2=" << ca2.info() << std::endl;
        std::vector<std::size_t> inf1=ca1->info(),inf2=ca2->info();
        if(inf1[0] > inf2[0]){
            ca1->info() = ca2->info();
        }
        std::cout << "Merging" << std::endl;

    }
};
*/

struct Merge_functor {
    // operator() automatically called before a merge.
    void operator()(Vertex_attribute_handle &ca1, Vertex_attribute_handle &ca2) {
        //ca1.info()=ca1.info()+ca2.info(); // Update can be done incrementally.
        //std::cout<<"After on merge faces: info of face1="<<ca1.info()<<", info of face2=" << ca2.info() << std::endl;
        ca1->info() = ca2->info();
        std::cout << "Merging" << std::endl;
    }
};

// Functor called when one face is split in two.
struct Split_functor {
    Split_functor(LCC_2 &amap) : mmap(amap) {}
    // operator() automatically called after a split.
    void operator()(Facet_attribute &ca1, Facet_attribute &ca2) {
        // We need to reinitalize the weight of the two faces
        /*GMap_3::size_type nb1 = mmap.darts_of_cell<2>(ca1.dart()).size();
        GMap_3::size_type nb2 = mmap.darts_of_cell<2>(ca2.dart()).size();
        mmap.info<2>(ca1.dart()) *= (double(nb1) / (nb1 + nb2));
        mmap.info<2>(ca2.dart()) *= (double(nb2) / (nb1 + nb2));
        std::cout << "After on split faces: info of face1=" << ca1.info()
                  << ", info of face2=" << ca2.info() << std::endl;
        */
        std::cout << "Spliting" << std::endl;
    }

private:
    LCC_2 &mmap;
};

#endif //DEV_THESE_TYPESFORLCC_H
