# python3.8
import matplotlib.pyplot as plt

import pybinding as env
import numpy as np
import copy

class Precompute:
    """
    Compute arguments : coords x,y , adj matrix of cells, constraints
    Grid dim [n,p] contient dimension de la grille
    Grid constraints contient les contraintes
    Exemple
    grim_dim: [3,2] -> n,p
    [[0,1],
    [1,1],
    [0,1]]
    contraints: [4,3] -> n,p   ([3+1,2+1])
    [[0,1,1]
    [0,1,1],
    [1,1,1],
    [0,1,1]]
    Pour donner ce model:
             1 ----- 1
             |       |
             |       |
      0------1 ----- 1
      |      |       |
      |      |       |
      1----- 1 ----- 1
             |       |
             |       |
             1-------1
    """
    # s : step
    # t : taille cellule
    def give_points_x_y(self, i, j, s=5, t=5):
        # Coordonnées
        # bas gauche
        p1 = [i * s, j * s]
        # bas droit
        p2 = [i * s + t, j * s]
        # haut droit
        p3 = [i * s + t, j * s + t]
        # haut gauche
        p4 = [i * s, j * s + t]
        return [p1[0], p2[0], p3[0], p4[0]], [p1[1], p2[1], p3[1], p4[1]]

    def get_coords(self, g, c):
        # Compter nombre de quad
        nb_cell = 0
        x, y = [], []
        # Récupération des points pour chaque quads dans g
        for i in reversed(range(len(g))):
            for j in range(len(g[0])):
                if (g[i][j] == 1):
                    # Compter nombre quad
                    nb_cell += 1
                    # Récupération des points(changement de d'indice pour attribuer dans l'ordre "montant de la matrice")
                    a, b = self.give_points_x_y(j, len(g) - 1 - i)
                    # Concaténation dans les x,y
                    x = [*x, *a]
                    y = [*y, *b]

        return x, y, nb_cell

    # pos_a : [0:1]
    # pos_b : [0:1]0
    def is_adj(self, pos_a, pos_b, g):
        a = np.array(pos_a)
        b = np.array(pos_b)
        if (np.sum(np.absolute(a - b)) == 1):
            print(a, " adj of ", b)
        else:
            print(a, " not adj of ", b)

    # g: Graphe d'adj de cellule
    # Retourne g avec:
    # 0 devient None
    # 1 devient cellule indéxé
    def index(self, g):
        nb_cell = 0
        g_new = list(g)
        for i in reversed(range(len(g))):
            for j in range(len(g[0])):
                print(i, j)
                if (g[i][j] != 0):
                    g[i][j] = nb_cell
                    nb_cell += 1
                else:
                    g_new[i][j] = None
        return g_new, nb_cell

    # Génération d'une matrice d'adjacence a partir de g où chaque valeur est déterminé par l'emplacement de la couture
    # Haut, bas, gauche, droite
    def attribuer_adj(self, g, nb_cell):
        adj_mat = np.zeros((nb_cell, nb_cell), dtype=np.int64)
        orientation = {"bas": 1, "haut": 10, "droite": 100, "gauche": 1000}
        for i in range(len(g)):
            for j in range(len(g[0])):
                res = []
                res_dix = 0
                src = g[i][j]
                if src is not None:
                    # Si on est pas sur le bord et le voisin existe
                    if i < len(g) - 1 and g[i + 1][j] is not None:
                        adj_mat[src, g[i + 1][j]] = orientation["bas"]
                    if i > 0 and g[i - 1][j] is not None:
                        adj_mat[src, g[i - 1][j]] = orientation["haut"]
                    if j < len(g[0]) - 1 and g[i][j + 1] is not None:
                        adj_mat[src, g[i][j + 1]] = orientation["droite"]
                    if j > 0 and g[i][j - 1] is not None:
                        adj_mat[src, g[i][j - 1]] = orientation["gauche"]
        return adj_mat

    def get_x_y_constraints_adj(self, g, c, show=False):
        print(g)
        print(c)
        assert len(g) + 1 == len(c)
        assert len(g[0]) + 1 == len(c[0])
        # Récupérations des coordonnées xs,ys des cellules et le nombre de cellule nb_cell
        xs, ys, nb_cell = self.get_coords(g, c)
        # Coordonnées
        # Indexation des cellule dans g
        g, nb_cell = self.index(g)
        # Génération d'une matrice d'adjacence des cellule à partir de g indexé pour effectuer les coutures(sew)
        adj_mat = self.attribuer_adj(g, nb_cell)
        c_arr = []
        for i in reversed(range(len(g))):
            for j in range(len(g[0])):
                if g[i][j] is not None:
                    print("here")
                    bas_gauche = c[i + 1][j]
                    ht_gauche = c[i][j]
                    ht_droit = c[i][j + 1]
                    bas_droit = c[i + 1][j + 1]
                    a = [bas_gauche, bas_droit, ht_droit, ht_gauche]
                    c_arr = [*c_arr, *a]

        assert (len(xs) == len(ys) and len(ys) == len(c_arr))
        tmp_carr = [x * 15 for x in c_arr]
        if show:
            plt.scatter(xs, ys, c=tmp_carr, label="Black -> Constraints, Yellow -> Neutral")
            plt.legend()
            plt.show()

        return xs, ys, adj_mat, c_arr
