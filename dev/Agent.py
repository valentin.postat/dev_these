from Policy import *


class Agent:
    """id"""

    def __init__(self, id, policy, map_obs):
        """
        :param id:
        :param policy:
        :param map_obs:
        """
        self.id = id
        self.policy = policy
        self.map_obs = map_obs

    """Action, Obs"""

    def decide(self, i, obs):
        """
        :param i:
        :param obs:
        :return:
        """
        print("OBS", obs)
        index = self.map_obs[obs]['index']
        a = self.policy.decide(i, index)
        return a

    def update_policy(self, reward, state, action, state_next):
        """
        :param reward:
        :param state:
        :param action:
        :param state_next:
        :return:
        """
        state_index = self.map_obs[state]['index']
        state_next_index = self.map_obs[state_next]['index']
        self.policy.update(reward, state_index, action, state_next_index)
