# Created by the script cgal_create_CMakeLists
# This is the CMake script for compiling a set of CGAL applications.

cmake_minimum_required(VERSION 3.4...3.18)

project(operations LANGUAGES CXX)


# CGAL and its components
find_package( CGAL QUIET COMPONENTS  )
find_package(CGAL COMPONENTS Qt5)

if(CGAL_Qt5_FOUND)
  add_definitions(-DCGAL_USE_BASIC_VIEWER -DQT_NO_KEYWORDS)
endif()

if ( NOT CGAL_FOUND )

  message(STATUS "This project requires the CGAL library, and will not be compiled.")
  return()  

endif()


# Boost and its components
find_package( Boost REQUIRED )

#set_property(TARGET tgt PROPERTY CXX_STANDARD 17)
if ( NOT Boost_FOUND )

  message(STATUS "This project requires the Boost library, and will not be compiled.")

  return()  

endif()

# include for local directory

# include for local package


# Creating entries for target: operations
# ############################
# Ancien executable test todo remove
add_executable( operations LCCBuilder.cpp)

add_to_cached_list( CGAL_EXECUTABLE_TARGETS operations )
if(CGAL_Qt5_FOUND)
    target_link_libraries(operations PUBLIC CGAL::CGAL_Qt5)
endif()
# Link the executable to CGAL and third-party libraries
target_link_libraries(operations PRIVATE CGAL::CGAL )


add_executable( test_lcc_hl main.cpp LCC_HL.cpp LCCBuilder.cpp)
#na

#na
add_to_cached_list( CGAL_EXECUTABLE_TARGETS test_lcc_hl )
if(CGAL_Qt5_FOUND)
    target_link_libraries(test_lcc_hl PUBLIC CGAL::CGAL_Qt5)
endif()
# Link the executable to CGAL and third-party libraries
target_link_libraries(test_lcc_hl PRIVATE CGAL::CGAL )


# --------------------------------- PYBINDING ---------------------------------
add_subdirectory(pybind11)
#find_package(pybind11 REQUIRED)
pybind11_add_module(pybinding pybinding.cpp LCC_HL.cpp LCCBuilder.cpp TypesForLCC.cpp)
add_to_cached_list( CGAL_EXECUTABLE_TARGETS pybind_example )
if(CGAL_Qt5_FOUND)
    target_link_libraries(pybinding PUBLIC CGAL::CGAL_Qt5)
endif()

target_link_libraries(pybinding PRIVATE CGAL::CGAL )
target_compile_definitions(pybinding PRIVATE)
add_custom_command(TARGET pybinding POST_BUILD
    COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:pybinding> ${PROJECT_BINARY_DIR}/gym_example-master/gym-example/gym_example/envs
    COMMENT "Created ${PROJECT_BINARY_DIR}/test_base.exe"
)


# --------------------------------- BLOCKSTRUCT ---------------------------------
add_executable(test_BlockStruct main.cpp BlockStruct.cpp LCC_HL.cpp LCCBuilder.cpp)
#na

#na
add_to_cached_list( CGAL_EXECUTABLE_TARGETS test_BlockStruct )
if(CGAL_Qt5_FOUND)
    target_link_libraries(test_BlockStruct PUBLIC CGAL::CGAL_Qt5)
endif()
# Link the executable to CGAL and third-party libraries
target_link_libraries(test_BlockStruct PRIVATE CGAL::CGAL )



# https://pybind11.readthedocs.io/en/stable/compiling.html#building-with-cmake
# ATTENTION PYTHON 3.8 uniquement









