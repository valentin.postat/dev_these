from BlockStruct import *

graphs_cell = []
constraints = []
# [0] Simple quad
graphs_cell.append([[1]])
constraints.append([[1, 0], [0, 0]])
# [1] 3 quads
graphs_cell.append([[0, 1], [1, 1]])
constraints.append([[0, 1, 0], [1, 0, 0], [0, 0, 0]])
# [2] 3 quads bis
graphs_cell.append([[0, 1], [1, 1]])
constraints.append([[0, 1, 0], [1, 1, 0], [0, 0, 0]])
# [3] L
graphs_cell.append([[1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 1, 1]])
constraints.append([[1, 0, 0, 0], [1, 0, 0, 0], [1, 0, 0, 0], [1, 0, 0, 0], [1, 1, 1, 0], [1, 1, 1, 0]])
# [4] 3*3
graphs_cell.append([[1, 1, 1], [1, 1, 1], [1, 1, 1]])
constraints.append([[3, 3, 3, 3], [0, 0, 0, 0], [0, 0, 0, 0], [3, 3, 3, 3]])
# [5] Grid 5*3 quads
graphs_cell.append([[1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1]])
constraints.append([[1, 1, 1, 1], [1, 0, 0, 1], [1, 0, 0, 1], [0, 0, 0, 0], [0, 0, 0, 0], [1, 1, 1, 1]])

# [2] 3 quads bis
graphs_cell.append([[0, 1], [1, 1]])
constraints.append([[0, 2, 2], [2, 3, 2], [1, 2, 1]])

b = back_env(graphs_cell[4], constraints[4])  # 4
# b.show_env_state()

print("vertex_id", "critical", "border", "set")
for i in range(1, 4):
    print("Vision", i)
for vtx in b.lcc_hl.get_all_vertex_ids():
    print(vtx)

for c in b.lcc_hl.get_cells_id():
    print(c, b.lcc_hl.get_vertex_of_cell(c))

## Vertex id
vertex_neigh = []
for v in b.lcc_hl.get_all_vertex_ids():
    vertex_neigh.append([v, b.lcc_hl.neigborhood(v)])

vertex_iter = []
for v in b.lcc_hl.get_all_vertex_ids():
    vertex_iter.append([v, b.lcc_hl.iteration_vertex(v)])

print(b.lcc_hl.get_all_vertex_ids())
for v in b.lcc_hl.get_all_vertex_ids():
    c = b.lcc_hl.iteration_vertex(v)

cell_neigh = []
for c in b.lcc_hl.get_cells_id():
    neigh = b.lcc_hl.neighbor_cells(c);
    print(c, neigh)
cell_neigh.append([c, neigh])

cell_iter = []
for c in b.lcc_hl.get_cells_id():
    neigh = b.lcc_hl.iteration_cell(c);
    cell_iter.append([c, neigh])

print("#Vertex neighbour")
for v in vertex_neigh:
    print(v[0], v[1])

print("#Vertex Iterate")
for v in vertex_iter:
    print(v[0], v[1], len(v[1]))

print("#Cell neighbour")
for c in cell_neigh:
    print(c[0], c[1])

print("#Cell Iterate")
for c in cell_iter:
    print(c[0], c[1], len(c[1]))

print("#Vertex of Cell")
print("#Cells", b.lcc_hl.get_cells_id())
print("#Vertex", b.lcc_hl.get_all_vertex_ids())

for x in b.lcc_hl.get_all_vertex_ids():
    print(x, b.lcc_hl.point_of_vertex(x), b.lcc_hl.get_constraint(x))
# b.show()
cells = b.lcc_hl.get_cells_id()
print("#Cells")
prev = len(b.lcc_hl.iteration_cell(0))
for x in cells:
    curr = len(b.lcc_hl.iteration_cell(x))
assert (curr == prev)

t0_cells = b.lcc_hl.get_cells_id()

# show_plot(b)
# b.lcc_hl.writer()
# b.render()

print("#Vertex")
print("id \t fields")
for x in b.lcc_hl.get_all_vertex_ids():
    print(x, b.lcc_hl.get_fields(x))

print("#Cells")
print("id \t Vertex")
for x in b.lcc_hl.get_cells_id():
    print(x, b.lcc_hl.get_vertex_of_cell(x))

for x in b.lcc_hl.get_all_vertex_ids():
    cstr = b.lcc_hl.get_field(x, 1)
    prev = b.lcc_hl.get_field(x, 2)
    new = b.lcc_hl.valence(x)
    recompense = 0
    if cstr == 1 and new - prev > 0:
        recompense = 1
    print(x, "cstr", cstr, "prev", prev, "new", new, recompense)

# b.lcc_hl.writer()
# b.render()
# new_vertex_a = b.lcc_hl.c_1(0)
# new_vertex_b = b.lcc_hl.c_1(2)
b.lcc_hl.writer()
# b.render()
for x in b.lcc_hl.get_all_vertex_ids():
    cstr = b.lcc_hl.get_field(x, 1)
    prev = b.lcc_hl.get_field(x, 2)
    new = b.lcc_hl.valence(x)
    recompense = 0
    if cstr == 1 and new - prev > 0:
        recompense = 1
    elif cstr == 2 and new - prev < 0:
        recompense = 1
    print(x, "cstr", cstr, "prev", prev, "new", new, recompense)

old = b.lcc_hl.get_cells_id()
# b.lcc_hl.split_cell(4, 2)

b.lcc_hl.split_edge(0, 1)  # 35
b.lcc_hl.split_edge(2, 3)  # 36
b.lcc_hl.split_cell_d(0, 35, 36)
b.lcc_hl.split_cell(4, 14)
# b.lcc_hl.writer()
# b.render()
# b.reset()
# b.lcc_hl.writer()
# b.render()
"""
for x in b.lcc_hl.get_cells_id():
    vtx = b.lcc_hl.get_vertex_of_cell(x)
    if len(vtx) == 4:
        b.lcc_hl.split_cell(x, vtx[0])
"""
print(old)

# b.lcc_hl.c_2(4)
# b.update()
b.reset()
to_treat = [0, 2, 4, 6, 8]
for x in to_treat:
    b.lcc_hl.c_3(x)
b.update()
b.reset()
print("Cell_id\tNeighborhood\tPriority")
prio_w = {}
prio_wb = {}
for c in b.lcc_hl.get_cells_id():
    vtx = b.lcc_hl.get_vertex_of_cell(c)
    w = 0  # Crit +1
    wb = 0  # Border_crit +2
    for x in vtx:
        is_crit = b.lcc_hl.get_field(x, 1)
        in_boundary = b.lcc_hl.in_boundary(x)
        w += is_crit
        print(is_crit, in_boundary)
        if is_crit and in_boundary:
            wb += 5
        elif is_crit:
            wb += 1
    print(c, b.lcc_hl.neighbor_cells(c), w, wb)
    prio_w[c] = w
    prio_wb[c] = wb

p_w = sorted(prio_w.items(), key=lambda x: x[1], reverse=True)
p_wb = sorted(prio_wb.items(), key=lambda x: x[1], reverse=True)

higher_weights = max(p_wb, key=lambda item: item[1])
higher_cells = dict([item for item in p_wb if item[1] == higher_weights[1]])
print(higher_cells)
id_prio = sorted(higher_cells.items(), key=lambda x: x[0], reverse=False)
t0 = []

ids = [item[0] for item in id_prio]
for x in ids:
    t0.append(x)
    vtx = set(b.lcc_hl.get_vertex_of_cell(x))
    neigh = b.lcc_hl.neighbor_cells(x)
    for v in neigh:
        print(v)
        neigh_vtx = set(b.lcc_hl.get_vertex_of_cell(v))
        if len(vtx.intersection(neigh_vtx)):
            try:
                ids.remove(x)
            except ValueError:
                pass

res = False
mp = {}
for x in b.lcc_hl.get_cells_id():
    b.lcc_hl.get_vertex_of_cell(x)
    vtx_a = set(b.lcc_hl.neighbor_cells(x))
    print("#", x, vtx_a)
    for y in b.lcc_hl.neighbor_cells(x):
        vtx_b = set(b.lcc_hl.get_vertex_of_cell(y))
        if len(vtx_a.intersection(vtx_b)) == 1:
            res = True
        print(y, vtx_b, res)

# Check operations
check_operations()
b.reset()
cells = b.lcc_hl.get_cells_id()

print("Functors")
b = back_env(graphs_cell[0], constraints[0])
actions = [b.lcc_hl.c_1, b.lcc_hl.c_2, b.lcc_hl.c_3, b.lcc_hl.c_4, b.lcc_hl.c_5, b.lcc_hl.nc_5, b.lcc_hl.nc_6,
           b.lcc_hl.nc_7, b.lcc_hl.nc_8]
# Un masque par obs de cellule interne
feasible_actions = {0: [1, 1, 1, 1, 1, 0, 0, 0, 0], 1: [0, 0, 0, 0, 0, 1, 0, 0, 0], 2: [0, 0, 0, 0, 0, 0, 1, 0, 0],
                    3: [0, 0, 0, 0, 0, 0, 0, 1, 0], 4: [0, 0, 0, 0, 0, 0, 0, 0, 1]}

b.update()
actions[0].__call__(0)
actions[2].__call__(0)
actions[3].__call__(1)
b.update()
f= feasible_actions[len(b.lcc_hl.get_non_conform_vertex(0))]
f.index(1)
