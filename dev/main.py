import numpy

from BlockStruct import *
from Agent import *
import random
import json
import pandas
import os
from Q_learning_pol import *
import numpy as np
from logger import *


# LAST8TEST
def action_last_test():
    b = back_env()
    b.from_graph_constraint([[1, 1, 1], [1, 1, 1], [1, 1, 1]], [[0, 0, 0, 0], [0, 1, 1, 0], [0, 1, 1, 0], [0, 0, 0, 0]])
    target_face = 4
    observation = b.lcc_hl.representation_unique(target_face)
    fa = get_feasible_action(observation)
    for a in fa:
        b.reset()
        b.actions_list = [b.lcc_hl.a_0,
                          b.lcc_hl.a_1,
                          b.lcc_hl.a_2,
                          b.lcc_hl.a_3,
                          b.lcc_hl.a_4,
                          b.lcc_hl.a_5,
                          b.lcc_hl.a_6,
                          b.lcc_hl.a_7,
                          b.lcc_hl.a_8,
                          b.lcc_hl.a_9,
                          b.lcc_hl.a_10,
                          b.lcc_hl.a_11,
                          b.lcc_hl.a_12,
                          b.lcc_hl.a_13,
                          b.lcc_hl.a_14,
                          b.lcc_hl.a_15,
                          b.lcc_hl.a_16,
                          b.lcc_hl.a_17,
                          b.lcc_hl.a_18,
                          b.lcc_hl.a_19,
                          b.lcc_hl.a_20,
                          b.lcc_hl.a_21,
                          b.lcc_hl.a_22,
                          b.lcc_hl.a_23,
                          b.lcc_hl.a_24,
                          b.lcc_hl.a_25,
                          b.lcc_hl.a_26,
                          b.lcc_hl.a_27,
                          b.lcc_hl.a_28,
                          b.lcc_hl.a_29,
                          b.lcc_hl.a_30,
                          b.lcc_hl.a_31,
                          b.lcc_hl.a_32,
                          b.lcc_hl.a_33,
                          b.lcc_hl.a_34,
                          b.lcc_hl.a_35]

        print("Observation: ", observation)
        print("Actions faisable: ", fa)
        print("Action selectionnée: ", a)
        b.update(0)

        vtx_id = b.lcc_hl.get_vertex_of_cell(target_face)
        vold = b.lcc_hl.valence_l(vtx_id)
        # ACTIONS
        print("#Action " + str(a) + " sur " + str(target_face))
        b.actions_list[a].__call__(target_face)
        vnew = b.lcc_hl.valence_l(vtx_id)
        criticity = [b.lcc_hl.get_constraint(x) for x in vtx_id]
        b.lcc_hl.update_crit(vtx_id, vold, criticity)
        print("vtx_id", vtx_id)

        representation_voisins = []
        b.update(0)
        for c in [0, 1, 2, 3, 5, 6, 7, 8]:
            print(str(c))
            print(str(b.lcc_hl.representation_conformite(c)))
            print(str(b.lcc_hl.representation_unique(c)))
        b.update(0)

    b.reset()
    # b.update(1)
    b.lcc_hl.split_edge(14, 2)
    b.lcc_hl.split_edge(14, 18)
    # b.update(1)
    b.lcc_hl.update_nc(35)
    b.lcc_hl.update_nc(36)
    # b.update(1)

    rp = b.lcc_hl.representation_unique(4)
    print(rp)
    print(b.lcc_hl.get_vertex_of_cell(4))
    val = input("Enter your value: ")
    b.update(1)
    b.lcc_hl.a_23(4)
    b.update(0)
    b.reset()


# utils
def list_to_string(li):
    return str(li).strip('[]').replace(' ', '').replace(',', '')


def test_actions():
    more, less = build_table_nc_all()
    observations = [x for x in less.keys()]
    feasible_actions_conformity = []

    all_obs = []
    # actions_list = [b.lcc_hl.s_1, b.lcc_hl.s_1, b.lcc_hl.s_1, b.lcc_hl.s_1, b.lcc_hl.s_2, b.lcc_hl.s_2, b.lcc_hl.s_2,b.lcc_hl.s_2, b.lcc_hl.s_3, b.lcc_hl.s_3, b.lcc_hl.s_3, b.lcc_hl.s_3, b.lcc_hl.s_4]
    # parameters_list = [[0], [1], [2], [3], [0], [1], [2], [3], [0], [1], [2], [3], []]
    for o in observations:
        # fa = feasible_actions(feasible_actions_conformity, o)
        b = back_env()
        b.from_obs(o, "")
        b.lcc_hl.representation_unique(0)
        all_obs.append(b.get_obs())
    print(all_obs)


def write(show, name):
    output_file = " " + str(k.count('1')) + "_" + "obs" + "" + str(name) + ".png"
    os.system("python3 ReadLegacyUnstructuredGrid.py --show " + str(0) + " example.vtk" + output_file)


def generate_obs_png():
    more, less = build_table_nc_all()
    for k in less.keys():
        x = env.LCC_HL(k, "")
        x.writer()
        show = 0
        output_file = " " + str(k.count('1')) + "_" + "obs" + "" + k + "" + ".png"
        os.system("python3 ReadLegacyUnstructuredGrid.py --show " + str(show) + " example.vtk" + output_file)


def learning(graph_cell, constraint):
    """ Reinforcement Learning """
    b = back_env()
    b.from_graph_constraint(graph_cell, constraint)

    # Parameters
    states_n = 2 ** 4
    actions_n = 13
    num_episodes = 10

    feasible_actions = [1] * 13
    agent_n = 1
    """ #5 number of non-conform point {0,1,2,3,4} , #2 contain critical vertex {0,1} 0 : no 1 : yes  """
    policy = Policy([2, 2, 2, 2], actions_n, feasible_actions)

    agents = []

    states = []
    states_list = []
    actions_history = []
    states_history = []
    cumul_reward_list = []
    cumul_reward = 0
    b.end_condition()

    for i in range(agent_n):
        agents.append(Agent(i, policy))
    # Learning on multiple executions
    """Episodes"""
    for i in range(num_episodes):
        b.reset()
        obs = []
        actions_list = [b.lcc_hl.c_1, b.lcc_hl.c_2, b.lcc_hl.c_3, b.lcc_hl.c_4, b.lcc_hl.c_5, b.lcc_hl.nc_5,
                        b.lcc_hl.nc_6,
                        b.lcc_hl.nc_7, b.lcc_hl.nc_8]
        processed_cells = []
        for a in range(agent_n):
            obs.append(b.get_obs())
        d = False
        s = obs
        print(obs)
        assert len(obs) == 1
        count = 0
        processed_cells = []
        """One game (une partie)"""
        while True and count < 10:
            actions = {}
            # Agents Decide
            for agent_id in range(agent_n):
                actions[agent_id] = agents[agent_id].decide(i, s[agent_id])
            # Step Game
            # b.update(show=1, output_file=str(count) + '.png')
            # s1 = obs(t+1)
            s1, r, d = b.step(actions, actions_list)
            cumul_reward += sum(r.values())
            # Update Policy
            for agent_id in range(agent_n):
                agents[agent_id].policy.update(r[agent_id], s[agent_id], actions[agent_id], s1[agent_id])
            s = s1
            actions_history.append(actions)
            states_history.append(s)
            processed_cells.append(obs[0][1])
            print("processed_cells", processed_cells)
            print("is_finished", d)
            count += 1
            if d:
                break
            # #b.update()
    # b.update(show=1, output_file=str(count) + '.png')
    states_list.append(states)
    actions_list.append(actions)
    cumul_reward_list.append(cumul_reward)
    print("cumul_reward: ", cumul_reward)
    print("Score over time: " + str(sum(cumul_reward_list[-100:]) / 100.0))

    print(agents[0].policy.Q[0])
    print(agents[0].policy.feasible_actions[obs[0]])
    print(obs)
    print(agents[0].policy.feasible_actions[obs[0][0][0]])


def reflex_model(graphs_cell, constraints):
    b = back_env(graphs_cell[4], constraints[4])
    b = back_env(graphs_cell[1], constraints[1])
    actions = [b.lcc_hl.c_1, b.lcc_hl.c_2, b.lcc_hl.c_3, b.lcc_hl.c_4, b.lcc_hl.c_5, b.lcc_hl.nc_5, b.lcc_hl.nc_6,
               b.lcc_hl.nc_7, b.lcc_hl.nc_8]

    # Un masque par obs de cellule interne
    # feasible_actions = {0: [0, 1, 1, 1, 1, 0, 0, 0, 0], 1: [0, 0, 0, 0, 0, 1, 0, 0, 0], 2: [0, 0, 0, 0, 0, 0, 1, 0, 0],3: [0, 0, 0, 0, 0, 0, 0, 1, 0], 4: [0, 0, 0, 0, 0, 0, 0, 0, 1]}
    feasible_actions = [[0, 1, 1, 1, 1, 0, 0, 0, 0], [0, 0, 0, 0, 0, 1, 0, 0, 0], [0, 0, 0, 0, 0, 0, 1, 0, 0],
                        [0, 0, 0, 0, 0, 0, 0, 1, 0], [0, 0, 0, 0, 0, 0, 0, 0, 1]]
    # feasible_actions = {0: 1, 1: 5, 2: 6,3: 7, 4: 8}
    policy = Policy([5, 2], 9, feasible_actions)
    # b.update()

    vertex = b.lcc_hl.get_all_vertex_ids()
    cells = b.lcc_hl.get_cells_id()
    print("vtx", vertex)
    print("cells", cells)
    iter_max = 1
    agent = Agent(0, policy)
    treated_cells = []
    to_treat = cells
    current = {}
    history_actions = []
    # b.update()
    for t in range(iter_max):
        cells = b.lcc_hl.get_cells_id()
        print("Cells to treat", cells)
        random.shuffle(cells)
        for x in cells:
            vtx = b.lcc_hl.get_non_conform_vertex(x)
            c_vtx = b.lcc_hl.get_vertex_of_cell(x)
            action = agent.decide(t, len(b.lcc_hl.get_non_conform_vertex(x)))
            # f.index(1)
            print("Cell", x, "nbvtx", len(vtx), vtx, "\nACTION_IND", action)
            current = {"time": t, "cell": x, "action": action}
            history_actions.append(current)
            print("Historique", history_actions)
            # f.index(1)
            if (len(c_vtx) - len(vtx) == 4):
                # reward, obs = \
                actions[action].__call__(x)

        # #b.update()

    b = back_env([[1]], [[1, 1], [1, 1]])
    cells = b.lcc_hl.get_cells_id()
    print("#Obs", b.get_obs_cell(cells[0], 1))
    # b.update()

    """Execute"""
    # b.update()
    b.lcc_hl.c_2(cells[0])
    b.reset()
    # b.update()


def get_program_parameters():
    import argparse
    description = 'Display a vtkUnstructuredGrid that contains eleven linear cells.'
    epilogue = '''
    This example also shows how to add a vtkCategoryLegend to a visualization.
   '''
    parser = argparse.ArgumentParser(description=description, epilog=epilogue
                                     )

    parser.add_argument('--show', help='0 to not render(just write)\n1 to render interaciv', type=int)
    args = parser.parse_args()
    print(args)
    return args.filename, args.fout, args.show


def generate_too_many_obs(less):
    """
    :param less: dict()[str] = list
    :return: too_many_obs
    """
    too_many = {}
    false = 0
    true = 1
    for x in less.keys():
        print(x, ":")
        for i in range(len(x)):
            word = x[i:] + x[:i]
            print("nb_permut", i, word, "reverse", false)
            if word not in too_many:
                too_many[word] = {"nb_permut": i, "true_word": x, "reverse": false}
        revers = x[::-1]
        for i in range(len(revers)):
            word = revers[i:] + revers[:i]
            print("nb_permut", i, word, "reverse", true)
            if word not in too_many:
                too_many[word] = {"nb_permut": i, "true_word": x, "reverse": true}
    print(too_many)
    return too_many


def write_min_word(less):
    for word in list(less.keys()):
        print(word)
        b = back_env([[1]], [[0, 0], [0, 0]])
        x = word
        vtx = b.lcc_hl.get_vertex_of_cell(0)
        new_vtxs = []
        for i in range(len(x)):
            print(x[i])
            if (x[i] == "1"):
                new_vtx = b.lcc_hl.split_edge(vtx[i - 1], vtx[(i) % len(vtx)])
                vtx = vtx[0:i] + [new_vtx] + vtx[i:]
                new_vtxs.append(new_vtx)

        for i in new_vtxs:
            b.lcc_hl.set_field(i, 1, 1)
        # b.update(0, "obs_with_possible_action" + word + ".png")
        del b


def build_table_crit():
    too_many_crit = dict()
    less_crit = dict()
    comb = ["0000", "0001", "0002", "0011", "0012", "0022", "0101", "0102", "0111", "0112", "0121", "1221", "1212",
            "0202",
            "0212",
            "0221", "0222", "1111", "2222"]
    for x in comb:
        less_crit[x] = []
    for x in less_crit.keys():
        print(x)

    too_many_crit = generate_too_many_obs(less_crit)
    return too_many_crit, less_crit


def build_table_nc_all():
    too_many_obs = dict()
    """Construit la table qui associe mot minimum pour une observation (non conformité) à une action faisable décalage vers la droite
    ex: 
    00001 ---(permuation:1)---> 00010 (reversed:False)  
    """

    less = dict()
    less_name = dict
    # --------------------

    names = ["1_1", "2_1", "2_2", "2_4", "4_1", "4_2", "4_3", "4_4", "6_1", "6_2", "6_3", "8_1"]
    # -------------------
    # 0.
    less["0000"] = []
    # 1.1 ----
    less["00001"] = []
    # 2.1
    less["001001"] = []
    # 2.2
    less["000011"] = []
    # 2.3
    less["000101"] = []
    # -------------------- Obs terminal
    # 3
    less["0001011"] = []
    less["0010011"] = []
    less["0010101"] = []
    # 01011010

    # --------------------
    # 4.1
    less["01010101"] = []
    # 4.2
    less["00110011"] = []
    # 4.3 00101011
    less["00101011"] = []
    # 4.4
    less["00011011"] = []
    # 4.5
    less["00101101"] = []
    # 5 ------------------------ Obs terminal
    # 5.1
    less["001101011"] = []
    # 5.2
    less["001011011"] = []
    less["010101011"] = []
    # --------------------
    # 6.1
    less["0011011011"] = []
    # 6.2
    less["0101011011"] = []
    # 6.3
    less["0101101011"] = []
    # 7 ------------------------ Obs terminal
    # 7.1
    less["01011011011"] = []
    # 8.1
    less["011011011011"] = []
    too_many_obs = generate_too_many_obs(less)
    return too_many_obs, less


def build_table_nc():
    too_many_obs = dict()
    """Construit la table qui associe mot minimum pour une observation (non conformité) à une action faisable décalage vers la droite
    ex: 
    00001 ---(permuation:1)---> 00010 (reversed:False)  
    """

    less = dict()
    less_name = dict
    # --------------------

    names = ["1_1", "2_1", "2_2", "2_4", "4_1", "4_2", "4_3", "4_4", "6_1", "6_2", "6_3", "8_1"]

    # 0.
    less["0000"] = []
    # 1.1 ----
    less["00001"] = []
    # 2.1
    less["001001"] = []
    # 2.2
    less["000011"] = []
    # 2.3
    less["000101"] = []
    # --------------------
    # 4.1
    less["01010101"] = []
    # 4.2
    less["00110011"] = []
    # 4.3
    less["00110101"] = []
    # 4.4
    less["00011011"] = []
    # 6.1
    less["0101101101"] = []
    # 6.2
    less["0011011011"] = []
    # 6.3
    less["0101011011"] = []
    # 8.1
    less["011011011011"] = []
    too_many_obs = generate_too_many_obs(less)
    return too_many_obs


def test_others():
    b = back_env(graphs_cell[1], constraints[1])
    prio_cell_sort = []

    # b.lcc_hl.c_2(prio_cell_sort.pop(0)[1])
    print("#1", prio_cell_sort)

    # #b.update(show=1)
    # b.lcc_hl.c_2(prio_cell_sort.pop(0)[1])
    print("#2", prio_cell_sort)
    # #b.update(show=1)
    # build_table_nc()
    show = 0
    b.lcc_hl.rpz(0)
    print(b.lcc_hl.get_vertex_of_cell(0))
    del b
    b = back_env([[1]], [[0, 0], [0, 0]])
    # b.update(show=show)
    cell = b.lcc_hl.get_cells_id()
    vtx = b.lcc_hl.get_vertex_of_cell(cell[0])
    print(vtx)
    new_vtx = b.lcc_hl.split_edge(vtx[0], vtx[1])
    b.lcc_hl.set_field(new_vtx, 1, 1)
    b.lcc_hl.set_field(new_vtx, 3, 1)
    new_vtx = b.lcc_hl.split_edge(new_vtx, vtx[1])
    b.lcc_hl.set_field(new_vtx, 1, 1)
    b.lcc_hl.set_field(new_vtx, 3, 1)
    # b.update(show=show)
    rpz = b.lcc_hl.rpz(0)
    too_many = build_table_nc()
    print(too_many[str(rpz)])
    tmc = build_table_crit()
    del b
    b = back_env([[1]], [[1, 0], [0, 0]])
    rpz = b.lcc_hl.rpz(0)
    print(rpz)
    del b
    # check_operations()
    name_action = "s_1"
    for i in range(4):
        b = back_env([[1]], [[1, 0], [0, 0]])
        cell = b.lcc_hl.get_cells_id()
        vtx = b.lcc_hl.get_vertex_of_cell(cell[0])
        b.lcc_hl.s_1(cell[0], i)
        # b.update(show, name_action + "_offset_" + str(i) + "start" + str(vtx[0]) + "s_1o" + ".png")
        del b

    b = back_env([[1]], [[1, 0], [0, 0]])
    name_action = "s_2"
    for i in range(4):
        b.reset()
        cell = b.lcc_hl.get_cells_id()
        vtx = b.lcc_hl.get_vertex_of_cell(cell[0])
        b.lcc_hl.s_2(cell[0], i)
        # b.update(show, name_action + "_offset_" + str(i) + "start" + str(vtx[0]) + "s_1o" + ".png")

    b.reset()
    name_action = "s_4"
    cell = b.lcc_hl.get_cells_id()
    b.lcc_hl.s_4(cell[0])
    # b.update(0, name_action + "start" + str(vtx[0]) + ".png")

    b = back_env([[1, 1]], [[0, 0, 0], [0, 0, 0]])
    cell = b.lcc_hl.get_cells_id()
    vtx_a = b.lcc_hl.get_vertex_of_cell(cell[0])
    vtx_b = b.lcc_hl.get_vertex_of_cell(cell[1])
    vtx = list(set(vtx_a).intersection(vtx_b))
    new_vtx = b.lcc_hl.split_edge(vtx[0], vtx[1])
    # b.lcc_hl.set_field(new_vtx, 3, 1)
    new_vtx = b.lcc_hl.split_edge(new_vtx, vtx[1])
    # b.lcc_hl.set_field(new_vtx, 3, 1)

    b.lcc_hl.rpz(1)
    b.lcc_hl.place_from_nc_map(1, too_many[b.lcc_hl.rpz(1)]["nb_permut"], False)
    print(b.lcc_hl.rpz(1))
    vtx = b.lcc_hl.get_vertex_of_cell(1)
    c1 = b.lcc_hl.split_cell_d(1, vtx[-1], vtx[1])
    c2 = b.lcc_hl.split_cell_d(1, vtx[-2], vtx[-4])

    v3 = b.lcc_hl.split_edge(vtx[-1], vtx[1])
    v4 = b.lcc_hl.split_edge(vtx[-2], vtx[-4])
    n5 = b.lcc_hl.split_cell_d(1, v3, v4)
    # b.update(0)
    del b

    b = back_env([[1]], [[0, 0], [0, 0]])
    cell = b.lcc_hl.get_cells_id()
    vtx = b.lcc_hl.get_vertex_of_cell(cell[0])
    new_vtx = b.lcc_hl.split_edge(vtx[0], vtx[1])
    # b.lcc_hl.set_field(new_vtx, 1, 1)
    b.lcc_hl.set_field(new_vtx, 3, 1)
    new_vtx = b.lcc_hl.split_edge(new_vtx, vtx[1])
    # b.lcc_hl.set_field(new_vtx, 1, 1)
    b.lcc_hl.set_field(new_vtx, 3, 1)

    b.lcc_hl.place_from_nc_map(cell[0], too_many[b.lcc_hl.rpz(cell[0])]["nb_permut"], False)
    vtx = b.lcc_hl.get_vertex_of_cell(cell[0])
    v1 = b.lcc_hl.split_edge(vtx[1], vtx[2])
    v2 = b.lcc_hl.split_edge(v1, vtx[2])
    c2 = b.lcc_hl.split_cell_d(cell[0], vtx[5], v1)
    c3 = b.lcc_hl.split_cell_d(cell[0], vtx[4], v2)
    # b.update(0)

    b.reset()
    cell = b.lcc_hl.get_cells_id()
    vtx = b.lcc_hl.get_vertex_of_cell(cell[0])
    n1 = b.lcc_hl.split_edge(vtx[0], vtx[1])
    n2 = b.lcc_hl.split_edge(vtx[1], vtx[2])
    # non conform
    b.lcc_hl.set_field(n1, 3, 1)
    b.lcc_hl.set_field(n2, 3, 1)
    # b.update(0)

    b.reset()
    cell = b.lcc_hl.get_cells_id()
    vtx = b.lcc_hl.get_vertex_of_cell(cell[0])
    n1 = b.lcc_hl.split_edge(vtx[0], vtx[1])
    print(too_many[b.lcc_hl.rpz(cell[0])]["nb_permut"])
    b.lcc_hl.place_from_nc_map(cell[0], too_many[b.lcc_hl.rpz(cell[0])]["nb_permut"], False)
    b.lcc_hl.nc_1(cell[0], True)
    # b.update(1)

    b = back_env([[1, 1, 1], [1, 1, 1], [1, 1, 1]], [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]])
    # b.lcc_hl.s_3(4, 0)

    b.lcc_hl.s_3(4, 0)
    # b.update(1)


def dump_maps():
    too_much_nc, less_nc = build_table_nc_all()
    too_much_crit, less_crit = build_table_crit()

    with open('map_conformity.json', 'w') as outfile:
        json.dump(too_much_nc, outfile)

    with open('map_criticity.json', 'w') as outfile:
        json.dump(too_much_crit, outfile)


def read_fa():
    with open("feasible_actions.json", 'r') as infile:
        fa = json.load(infile)
    return fa


def get_feasible_action(obs):
    crit, conf = obs
    fa_conf = read_fa()
    if crit == "" or conf == "":
        return [None]
    fa = fa_conf[conf]
    fa += get_feasible_criticity_action_from_obs(crit)
    return fa


def get_feasible_criticity_action_from_obs(crit_obs):
    found = False
    if (crit_obs.find('2') == -1):
        return []
    return [34]  # , 35]


def test_split():
    b = back_env([[1, 1], [1, 1]], [[0, 0, 0], [0, 0, 0], [0, 0, 0]])
    # b.update(1)

    b.lcc_hl.split_cell(2, 5)
    b.lcc_hl.split_cell(2, 14)
    b.lcc_hl.split_cell(2, 11)
    b.lcc_hl.split_cell(2, 0)
    # b.update(1)


def other_main():
    b = back_env()
    b.from_obs("00001", "")
    # b.update(0)
    b = back_env()
    b.from_obs("0000", "1111")
    # b.update(0)
    b = back_env()
    b.from_graph_constraint([[1]], [[1, 0], [1, 0]])
    # b.update(1)
    b.lcc_hl.representation_criticite(0)
    b.lcc_hl.representation_conformite(0)

    show = 0
    # Actions avec paramètres (rotations)
    actions_list = [b.lcc_hl.s_1, b.lcc_hl.s_1, b.lcc_hl.s_1, b.lcc_hl.s_1, b.lcc_hl.s_2, b.lcc_hl.s_2, b.lcc_hl.s_2,
                    b.lcc_hl.s_2, b.lcc_hl.s_3, b.lcc_hl.s_3, b.lcc_hl.s_3, b.lcc_hl.s_3, b.lcc_hl.s_4]
    # rotations
    parameters_list = [[0], [1], [2], [3], [0], [1], [2], [3], [0], [1], [2], [3], []]
    representation = []
    crit_vtx_a = []
    crit_vtx_b = []
    for i in range(len(actions_list)):
        print("#", i)
        b.reset()
        actions_list = [b.lcc_hl.s_1, b.lcc_hl.s_1, b.lcc_hl.s_1, b.lcc_hl.s_1, b.lcc_hl.s_2, b.lcc_hl.s_2,
                        b.lcc_hl.s_2,
                        b.lcc_hl.s_2, b.lcc_hl.s_3, b.lcc_hl.s_3, b.lcc_hl.s_3, b.lcc_hl.s_3, b.lcc_hl.s_4]
        parameters_list = [[0], [1], [2], [3], [0], [1], [2], [3], [0], [1], [2], [3], []]
        crit, conf = b.lcc_hl.representation_unique(0)
        representation.append([crit, conf])
        vtx_id = b.lcc_hl.get_vertex_of_cell(0)
        vold = b.lcc_hl.valence_l(vtx_id)
        # ACTIONS
        face_id = 0
        actions_list[i].__call__(face_id, *parameters_list[i])
        vnew = b.lcc_hl.valence_l(vtx_id)
        criticity = [b.lcc_hl.get_constraint(x) for x in vtx_id]
        b.lcc_hl.update_crit(vtx_id, vold, criticity)
        for v in vtx_id:
            b.lcc_hl.update_nc(v)

        criticity = [b.lcc_hl.get_constraint(x) for x in vtx_id]
        crit_vtx_b.append([i, vtx_id, criticity, b.lcc_hl.get_criticity_of_vertex(vtx_id), vold, vnew])
        vold_str = str(vold).strip('[]').replace(' ', '').replace(',', '')
        vnew_str = str(vnew).strip('[]').replace(' ', '').replace(',', '')
        vtx_id_str = str(vtx_id).strip('[]').replace(' ', '').replace(',', '')
        criticity_str = str(criticity).strip('[]').replace(' ', '').replace(',', '')
        if show:
            b.update(0, "id_" + list_to_string(vtx_id) + "_iter_" + str(i) + "_old_" + list_to_string(
                vold) + "_vnew_" + list_to_string(vnew_str) + "_criticity_" + list_to_string(criticity_str) + ".png")
        b.reset()

    print(len(actions_list))
    print(len(parameters_list))
    for x in representation:
        print(x)
    print("index\t", "id\t", "crit_before\t", "crit_after\t", "vold\t", "vnew\t")

    for x in range(len(crit_vtx_b)):
        print(crit_vtx_b[x])
    m, l = build_table_crit()

    b = back_env()
    for x in list(l.keys()):
        b.from_obs("0000", x)
        rp = b.lcc_hl.representation_criticite_unique(0)
        # b.update(0, "crit_" + x + ".png")
        assert rp in l.keys()

    # Conform only
    states_conform = ['0000', '0001', '0002', '0011', '0012', '0022', '0101', '0102', '0111', '0112', '0121', '0202',
                      '0212',
                      '0221', '0222', '1111', '1212', '1221', '2222']

    # states_conform = ['0000', '0001', '0011', '0111', '1111']

    states_criticity = ['']
    n_states = len(states_conform)

    map_states_index = {}
    state_map = []
    count = 0
    fa = [1] * 13
    for x in states_conform:
        map_states_index[x] = {'index': count, 'feasible_action': fa}
        state_map.append(map_states_index[x])
        count += 1

    b = back_env()
    b.from_graph_constraint([[1]], [[1, 1], [0, 0]])
    # b.update(1)

    episode_length = 1

    actions_n = 13
    agent = Agent(0, Policy([n_states], actions_n, []), map_states_index)

    history = []
    states_to_learn = ['0001', '0010', '0011', '0101', '0111', '1111']
    shuffled = states_to_learn.copy()
    random.shuffle(shuffled)

    to_learn = list(reversed(states_to_learn)) + shuffled

    for sc in to_learn:  # [0:3]
        b = back_env()
        b.from_obs("", sc)
        for e in range(episode_length):
            b.reset()
            actions_list = [b.lcc_hl.s_1, b.lcc_hl.s_1, b.lcc_hl.s_1, b.lcc_hl.s_1, b.lcc_hl.s_2, b.lcc_hl.s_2,
                            b.lcc_hl.s_2,
                            b.lcc_hl.s_2, b.lcc_hl.s_3, b.lcc_hl.s_3, b.lcc_hl.s_3, b.lcc_hl.s_3, b.lcc_hl.s_4]
            parameters_list = [[0], [1], [2], [3], [0], [1], [2], [3], [0], [1], [2], [3], []]
            actions_n = len(actions_list)
            map_states_index['0000']['feasible_action'] = [1] + [0] * 12
            obs, face_id = b.get_obs()
            action = agent.decide(e, obs)
            # Store information before action for update
            # crit, conf = b.lcc_hl.representation_unique(face_id)
            vtx_id = b.lcc_hl.get_vertex_of_cell(face_id)
            vold = b.lcc_hl.valence_l(vtx_id)
            # Execute action
            actions_list[action].__call__(face_id, *parameters_list[action])
            # Get information after action
            vnew = b.lcc_hl.valence_l(vtx_id)
            criticity = [b.lcc_hl.get_constraint(x) for x in vtx_id]
            # Update
            # - criticity

            b.lcc_hl.update_crit(vtx_id, vold, criticity)
            # - conformity
            for x in vtx_id:
                b.lcc_hl.update_nc(x)
            # update.geo
            # Compute reward
            reward = 10.0
            for x in b.lcc_hl.get_criticity_of_vertex(vtx_id):
                if x != 0:
                    reward = 0.0
                    break

            next_observation, next_face_id = b.get_obs()
            history.append([reward, obs, action, next_observation])
            agent.update_policy(reward, obs, action, next_observation)

    column_labels = [x for x in range(actions_n)]
    row_labels = [x for x in states_conform]
    df = pandas.DataFrame(agent.policy.Q, columns=column_labels, index=row_labels)
    df

    history_df = pandas.DataFrame(history, columns=["reward", "obs", "action", "next_observation"])
    history_df.astype({'reward': 'float'}).dtypes
    history_df[history_df["reward"] > 5.0]
    history_df[(history_df.reward > 5) & (history_df.obs != "0001")]

    x = back_env()
    x.from_obs("00001", "")
    x.lcc_hl.representation_unique(0)
    x.lcc_hl.nc_1(0, 0)
    # x.update(1)
    x.reset()
    x.lcc_hl.representation_unique(0)
    x.lcc_hl.nc_1(0, 1)

    x = back_env()
    x.from_obs("", "0000")
    x.lcc_hl.representation_unique(0)
    x.lcc_hl.s_3(0, 0)
    # x.update(1)

    b.from_obs("0101011011", "")
    b.lcc_hl.representation_unique(0)
    b.lcc_hl.a_31(0)

    b = back_env()
    b.from_obs("0001011", "")
    b.lcc_hl.representation_unique(0)
    b = back_env()
    b.from_obs("01010101", "")

    b.lcc_hl.representation_unique(0)

    b.from_obs("00010101", "")

    # Voir avec 17
    # history = test_feasibles_actions()

    # more_action_availables


def test_feasibles_actions():
    feasibles_actions = read_fa()
    history = []
    count = 0
    for obs in feasibles_actions:
        b = back_env()
        b.from_obs(obs, "")

        count += 1
        for available_action in feasibles_actions[obs]:
            b.reset()
            face_id = 0
            # print("REPR",b.lcc_hl.representation_conformite(0))
            # if b.lcc_hl.representation_conformite(0) == "001010101":
            #    print("OBS",obs)
            #    b.update(1)

            observation_crit, observation_conf = b.lcc_hl.representation_unique(face_id)
            actions_list = [b.lcc_hl.a_0
                , b.lcc_hl.a_1
                , b.lcc_hl.a_2
                , b.lcc_hl.a_3
                , b.lcc_hl.a_4
                , b.lcc_hl.a_5
                , b.lcc_hl.a_6
                , b.lcc_hl.a_7
                , b.lcc_hl.a_8
                , b.lcc_hl.a_9
                , b.lcc_hl.a_10
                , b.lcc_hl.a_11
                , b.lcc_hl.a_12
                , b.lcc_hl.a_13
                , b.lcc_hl.a_14
                , b.lcc_hl.a_15
                , b.lcc_hl.a_16
                , b.lcc_hl.a_17
                , b.lcc_hl.a_18
                , b.lcc_hl.a_19
                , b.lcc_hl.a_20
                , b.lcc_hl.a_21
                , b.lcc_hl.a_22
                , b.lcc_hl.a_23
                , b.lcc_hl.a_24
                , b.lcc_hl.a_25
                , b.lcc_hl.a_26
                , b.lcc_hl.a_27
                , b.lcc_hl.a_28
                , b.lcc_hl.a_29
                , b.lcc_hl.a_30
                , b.lcc_hl.a_31
                , b.lcc_hl.a_32
                , b.lcc_hl.a_33]
            # "action_" + (2-len(str(available_action)))*"0"+str(available_action)+"_o_"+conf+".png"
            history.append([obs, available_action, ])
            crit, conf = b.lcc_hl.representation_unique(face_id)
            print(history)
            actions_list[available_action].__call__(face_id)
            b.update(0,
                     "action_" + (2 - len(str(available_action))) * "0" + str(available_action) + "_o_" + conf + ".png")
    # 35
    b = back_env()

    b.from_graph_constraint([[1, 0], [1, 1]], [[0, 0, 0], [0, 2, 0], [1, 0, 0]])
    history.append([obs, available_action, ])
    crit, conf = b.lcc_hl.representation_unique(face_id)
    b.update(1)
    b.lcc_hl.a_35(0)
    b.update(0, "action_" + (2 - len(str(35))) * "0" + str(35) + "_o_" + conf + ".png")

    # 34
    # 13 ?
    face_id = 2  # Attention ici
    b = back_env()
    b.from_graph_constraint([[0, 1, 0], [1, 1, 1], [0, 1, 0]], [[0, 0, 0, 0], [0, 2, 2, 0], [0, 2, 2, 0], [0, 0, 0, 0]])
    history.append([obs, available_action, ])
    crit, conf = b.lcc_hl.representation_unique(face_id)
    assert (crit == "2222")
    b.update(1)
    b.lcc_hl.a_34(2)
    b.update(0, "action_" + (2 - len(str(35))) * "0" + str(34) + "_o_" + conf + ".png")
    b.lcc_hl.a_34(2)
    b.update(0)
    b.lcc_hl.a_34(2)
    b.update(0)
    b.lcc_hl.a_34(2)
    b.update(1)
    ###

    ###
    return history


def test_random_action(graph, constraint):
    b = back_env()
    b.from_graph_constraint(graph, constraint)
    b.update(1)
    step = 5
    i = 0
    iter_max = 50
    # target obs action
    history = []
    run = True
    exec_number = 0
    exec_number = get_exec_number()
    path = "runs/"
    ret = os.system("mkdir " + path + str(exec_number))
    assert ret == 0
    while (i < iter_max and run):
        # b.update(1)
        target_face = b.get_prio(b.lcc_hl.get_cells_id())
        # Pas de face à traiter
        if target_face == -1:
            break
        print("#PRIO", b.prio_cell_sort)
        print(b.lcc_hl.representation_criticite(target_face))
        obs = b.lcc_hl.representation_unique(target_face)
        print(obs)
        crit, conf = obs
        # if (crit == "0000" and conf == "0000"):
        # run = False
        # break

        # Store last state
        print("Store")

        vtx_id = b.lcc_hl.get_vertex_of_cell(target_face)

        print("After store1")
        # Memoriser s
        vtx_id, vold = b.lcc_hl.valence_l(vtx_id)
        print("After store2")
        # AGENT
        feasible_action = get_feasible_action(obs)
        action = reflex(obs)  # random.choice(feasible_action)
        if len(feasible_action) and action is not None:
            # History update
            history.append([target_face, obs, action])
            # Environment execute action
            x = len(str(i))

            before_render = path + str(exec_number) + "/" + "exec" + "0" * (3 - len(str(i))) + str(i) + "_action" + str(
                action) + "_on_" + str(
                target_face) + "_a.png"
            print(before_render)
            b.update(0, before_render)
            print(history)
            assert (b.lcc_hl.is_valid())
            # b.lcc_hl.remove_prio_mark(target_face)
            if action is not None:

                b.actions_list[action].__call__(target_face)
                # Environment compute new state s' from s
                vtx_new, vnew = b.lcc_hl.valence_l(vtx_id)
                criticity = [b.lcc_hl.get_constraint(x) for x in vtx_id]
                print("#VTX_ID", vtx_id)
                print("#VTX_OLD_VAL", vold)
                print("#VTX_NEW", vtx_new)
                print("#VTX_NEW_VAL", vnew)
                print("#VTX_V ", vold)
                # TEST
                criticity_prim = [b.lcc_hl.get_constraint(x) for x in vtx_new]
                map_ids = dict(zip(vtx_id, vold))
                valence_old = []
                criticite = []
                for k in vtx_new:
                    valence_old.append(map_ids[k])
                    criticite.append(b.lcc_hl.get_constraint(k))
                # ENDTEST
                # old #b.lcc_hl.update_crit(vtx_new, vold, criticity)
                b.lcc_hl.update_crit(vtx_new, valence_old, criticite)
            after_render = path + str(exec_number) + "/" + "exec" + "0" * (3 - len(str(i))) + str(i) + "_action" + str(
                action) + "_on_" + str(target_face) + "_b.png"
            print(after_render)
            b.update(0, after_render)
            assert (b.lcc_hl.is_valid())
        else:
            b.lcc_hl.unmark_cell_prio(target_face)
        i += 1
    z = 0
    i = i - 1

    in_str = "cp " + "runs/" + str(exec_number) + "/exec000*a.png runs/" + str(exec_number) + "in"
    out_str = "cp " + "runs/" + str(exec_number) + "/exec" + "0" * (3 - len(str(i))) + str(i) + "*b* runs/" + str(
        exec_number) + "out"
    print(in_str)
    print(out_str)
    os.system(in_str)
    os.system(out_str)
    history_df = pandas.DataFrame(history, columns=["target_face", "obs", "action"])
    return history_df


def reflex_conform_action():
    b = back_env()
    c_id = 0

    b.from_obs("0000", "1111")
    b.update(1)
    b.lcc_hl.representation_unique(0)
    b.execute_action(a=0, cell_id=0)
    b.update(1)

    b.from_obs("0000", "1000")
    obs = b.lcc_hl.representation_unique(c_id)
    print("obs", obs)
    b.execute_action(a=3, cell_id=0)

    b = back_env()
    b.from_obs("0000", "1100")
    b.update(1)
    b.lcc_hl.representation_unique(0)
    b.execute_action(a=6, cell_id=0)
    b.update(1)

    b.from_obs("0000", "1110")
    b.lcc_hl.representation_unique(0)
    b.execute_action(a=12, cell_id=0)
    b.update(1)


def generate_all_placement():
    with open("map_conformity.json", 'r') as infile:
        mc = json.load(infile)
    mpo = {}
    for k, v in mc.items():
        print(k, v, v["true_word"])
        if v["true_word"] in mpo:
            mpo[v["true_word"]] = {"permutations": mpo[v["true_word"]]["permutations"] + [v["nb_permut"]],
                                   "reverse": mpo[v["true_word"]]["reverse"] + [v["reverse"]]}
            print(mpo[v["true_word"]])
        else:
            mpo[v["true_word"]] = {"permutations": [v["nb_permut"]], "reverse": [v["reverse"]]}
            print(mpo)
    generate_placement(mpo, 'unique_cell_nc.json')
    with open("map_criticity.json", 'r') as infile:
        mc = json.load(infile)
    mpo = {}
    for k, v in mc.items():
        print(k, v, v["true_word"])
        if v["true_word"] in mpo:
            mpo[v["true_word"]] = {"permutations": mpo[v["true_word"]]["permutations"] + [v["nb_permut"]],
                                   "reverse": mpo[v["true_word"]]["reverse"] + [v["reverse"]]}
            print(mpo[v["true_word"]])
        else:
            mpo[v["true_word"]] = {"permutations": [v["nb_permut"]], "reverse": [v["reverse"]]}
            print(mpo)
    generate_placement(mpo, 'unique_cell_crit.json')


"""
Génère les placements possible selon une observation
Une seule représentation doit être possible
"""


def generate_placement(obs, filename):
    candidate_of_word = {}
    for x in obs.keys():
        candidate_of_word[x] = {"candidats": [], "neighbor": [], "reverse": []}

    for k, v in candidate_of_word.items():
        print(k, v)

    for x in mpo.keys():
        word = x
        index = [w for w in range(len(x))]
        nb_permut = 0
        reversed = False
        print(x, len(x), index)
        for i in range(len(x)):
            cur_word = x[i:] + x[:i]
            cur_index = index[i:] + index[:i]
            if cur_word == x:
                candidate_of_word[x]["candidats"] += [cur_index[0]]
                candidate_of_word[x]["reverse"] += [int(reversed)]
                candidate_of_word[x]["neighbor"] += [cur_index[1]]
        reversed = True
        x = x[::-1]
        index = index[::-1]
        for i in range(len(x)):
            cur_word = x[i:] + x[:i]
            cur_index = index[i:] + index[:i]
            if cur_word == word:
                candidate_of_word[word]["candidats"] += [cur_index[0]]
                candidate_of_word[word]["reverse"] += [int(reversed)]
                candidate_of_word[word]["neighbor"] += [cur_index[1]]

    with open(filename, 'w') as outfile:
        json.dump(candidate_of_word, outfile)


def get_exec_number():
    number = 0
    with open("number.json", 'r+') as infile:
        mc = json.load(infile)
        infile.seek(0)
        mc["number"] += 1
        number = mc["number"]
        json.dump(mc, infile)
        infile.truncate()

    return number


def test_s_3():
    b = back_env()
    b.from_graph_constraint([[1, 1, 1], [1, 1, 1], [1, 1, 1]], [[1, 0, 0, 1], [0, 0, 0, 0], [0, 0, 0, 0], [1, 0, 0, 1]])
    b.actions_list = [b.lcc_hl.a_0,
                      b.lcc_hl.a_1,
                      b.lcc_hl.a_2,
                      b.lcc_hl.a_3,
                      b.lcc_hl.a_4,
                      b.lcc_hl.a_5,
                      b.lcc_hl.a_6,
                      b.lcc_hl.a_7,
                      b.lcc_hl.a_8,
                      b.lcc_hl.a_9,
                      b.lcc_hl.a_10,
                      b.lcc_hl.a_11,
                      b.lcc_hl.a_12,
                      b.lcc_hl.a_13,
                      b.lcc_hl.a_14,
                      b.lcc_hl.a_15,
                      b.lcc_hl.a_16,
                      b.lcc_hl.a_17,
                      b.lcc_hl.a_18,
                      b.lcc_hl.a_19,
                      b.lcc_hl.a_20,
                      b.lcc_hl.a_21,
                      b.lcc_hl.a_22,
                      b.lcc_hl.a_23,
                      b.lcc_hl.a_24,
                      b.lcc_hl.a_25,
                      b.lcc_hl.a_26,
                      b.lcc_hl.a_27,
                      b.lcc_hl.a_28,
                      b.lcc_hl.a_29,
                      b.lcc_hl.a_30,
                      b.lcc_hl.a_31,
                      b.lcc_hl.a_32,
                      b.lcc_hl.a_33,
                      b.lcc_hl.a_34,
                      b.lcc_hl.a_35]
    b.update(1)
    b.lcc_hl.is_valid()
    b.actions_list[10].__call__(0)
    b.update(1)
    b.lcc_hl.is_valid()
    b.actions_list[10].__call__(2)
    b.update(1)
    b.lcc_hl.is_valid()
    b.actions_list[10].__call__(6)
    b.update(1)
    b.lcc_hl.is_valid()
    b.actions_list[10].__call__(8)
    b.update(1)
    b.lcc_hl.is_valid()


def reflex(obs):
    """
    Modèle réflexe CAD un état est connue après représentation unique
    """
    criticity, conformity = obs
    if len(conformity) == 4:
        if criticity == "0102":
            return 35
        if criticity == "1111":
            return 0
        elif criticity == "0111":
            return 12
        elif criticity == "0011":
            return 6
        elif criticity == "0001":
            return 3
        elif criticity.count('2'):
            return 34
    elif criticity.count("1") == 1 and conformity == "00001":
        return 15
    elif criticity[1] == "1" and conformity == "000101":
        return 24
    elif criticity[1] == "1" and criticity[2] == "1" and conformity == "000011":
        return 18
    elif conformity == "000011" and criticity.count("1") == 1:
        return 20
    else:
        a = get_feasible_action(obs)
        if 34 in a:
            return 34
        if 19 in a:
            return 19

        if len(a) >= 1:
            return a[0]
        else:
            if 34 in a:
                return 34
            return None


def reflex_test():
    cross_g = [[0, 1, 0], [1, 1, 1], [0, 1, 0]]
    cross_c = [[0, 1, 1, 0], [1, 2, 2, 1], [1, 2, 2, 1], [0, 1, 1, 0]]
    ids_1 = [[1, 0], [2, 0], [0, 1], [0, 2], [1, 3], [2, 3], [3, 1], [3, 2]]
    ids_2 = [[1, 1], [1, 2], [2, 1], [2, 2]]
    for x in ids_1[:]:
        cross_c[x[0]][x[1]] = 0
        test_random_action(cross_g, cross_c)
        for y in ids_2[:]:
            cross_c[y[0]][y[1]] = 0
            test_random_action(cross_g, cross_c)


"""
Return the reward of an action at each transitionnal state
"""


def Reward(s1, s2, alpha=50, beta=10, gamma=1):
    s1_g = np.array(s1.lcc_hl.gains())
    s2_g = np.array(s2.lcc_hl.gains())
    print("s1", s1_g)
    print("s2", s2_g)
    gain = s2_g - s1_g
    recompense = alpha * gain[0] + beta * gain[1] + gamma * gain[2]
    return recompense


def score(g, alpha=50, beta=10, gamma=1):
    if not np.array(np.array(g) - np.zeros(len(g))).any():
        return 999
    return alpha * g[0] + beta * g[1] + gamma * g[2]


def sign(x):
    s = bool(x > 0) - bool(x < 0)
    return s


def signe_gains(constraint):
    return [sign(constraint[0]), sign(constraint[1]), sign(constraint[2])]


def array_to_signe_gains(constraint, number_of_agent_observed):
    print(constraint)
    shape = (len(constraint), len(constraint[0]))
    symbol = list(range(number_of_agent_observed * len(constraint)))
    empty = 10000
    symbol.append(empty)
    taille = len(constraint[0])
    if taille < number_of_agent_observed:
        shape = [len(constraint), taille + number_of_agent_observed - taille]
    observation_coordinator = np.full(shape, empty, dtype=int)
    symbol_map = {}
    symbol_map[empty] = 10000
    print("SMAP", symbol_map)
    symbol_counter = 0
    for i in range(len(constraint)):
        for j in range(taille):
            if constraint[i][j] not in symbol_map:
                symbol_map[constraint[i][j]] = symbol[symbol_counter]
                symbol_counter += 1
            observation_coordinator[i, j] = symbol_map[constraint[i][j]]
    return observation_coordinator, symbol_counter, symbol_map


def generate_all_coordinator_observation(n, m, symbole_n):
    """
    :param n: number of line
    :param m: number of columns
    :param symbole_n:
    :return:
    """
    symbols = np.arange(0, symbole_n, 1, dtype=int)
    empty = 10000
    symbol_empty = empty

    n_observation = m + 1 * ((n - 1) * m) ** (symbole_n + 1)
    #               ^ 1ereligne ^ reste  ^ symboles + symbole vide
    print(n_observation)
    symb_n = symbole_n
    prod = 1
    for i in range(symbole_n + 1, 1, -1):
        print(i)
        prod *= i
    print(prod)
    # Pour chaque case
    symbols = list(symbols) + [empty]
    set_symbols = set(symbols)
    print(symbols)
    print(set_symbols)


def obstruc(gains, actions, scores, number_of_agent_observed):
    obs_coord = [signe_gains(x) for x in gains]
    agents_ids = list(range(len(actions)))
    Z1 = [x for y, x in sorted(zip(scores, actions))]
    Z2 = [x for y, x in sorted(zip(scores, obs_coord))]
    # print("Z2", Z2)
    crit_moins, crit_plus, nc = map(list, zip(*Z2))
    # print("Z1", Z1); print("Z2", Z2); print("obs_coord", obs_coord); print("array observed")
    a_crit_moins = [x for y, x in sorted(zip(crit_moins, agents_ids))]
    a_crit_plus = [x for y, x in sorted(zip(crit_plus, agents_ids))]
    a_nc = [x for y, x in sorted(zip(nc, agents_ids))]
    print("#gains", gains)

    print("#a_crit_m", a_crit_moins)
    print("#a_crit_p", a_crit_plus)
    print("#a_crit_nc", a_nc)

    return array_to_signe_gains([a_crit_moins, a_crit_plus, a_nc], number_of_agent_observed)


def element_exist(arr):
    if (arr[0] != 0 or arr[-1] != 0):
        return 1
    else:
        return 0


def observation_coordinator_agents(gains, agents_ids):
    cm, cp, nc = map(list, zip(*gains))
    s_cm = [[y, x] for y, x in sorted(zip(cm, agents_ids))]
    s_cp = [[y, x] for y, x in sorted(zip(cp, agents_ids))]
    s_nc = [[y, x] for y, x in sorted(zip(nc, agents_ids))]
    s_cm, s_cm_a = map(list, zip(*s_cm))
    s_cp, s_cp_a = map(list, zip(*s_cp))
    s_nc, s_nc_a = map(list, zip(*s_nc))
    print(s_cm)
    print(s_cp)
    print(s_nc)
    observation_coordinator = [element_exist(s_cm), element_exist(s_cp), element_exist(s_nc)]
    agent = [s_cm_a[0], s_cp_a[0], s_nc_a[0]]
    return observation_coordinator, agent


def test_observation_coordinator_agents():
    cmcpnc = [[0, 1, 2], [0, -4, 3], [0, 1, -8]]
    agents_ids = range(len(cmcpnc[0]))
    o, agents = observation_coordinator_agents(cmcpnc, agents_ids)


def train_coordinator(filename):
    # Problems parameters
    max_episode = 10
    max_iteration_per_episode = 50

    iter_episode = 0
    i = 0
    # Coordinator parameters
    number_of_agent_observed = 3
    # policy = Policy(states=states, actions_n=action_n, feasible_actions=number_of_agent_observed)
    # agent_coord = Agent(0, policy=policy, map_obs=map_obs)
    gains_n = 3
    columns = 3
    agent_coordinator = Q_learning_pol(5040, 3)
    empty = 10000
    # Problem parameters
    in_graphs_cell = [[0, 1, 0], [1, 1, 1], [0, 1, 0], [1, 1, 1]]
    in_constraints = [[0, 1, 1, 0], [1, 2, 2, 1], [1, 2, 2, 1], [0, 0, 0, 0], [1, 0, 0, 1]]
    run_episodes = True
    observation_final = [0, 0, 0]
    history = []
    while run_episodes:
        # init env and save
        environment = back_env()
        environment.from_graph_constraint(in_graphs_cell, in_constraints)
        save_environment = back_env()
        save_environment.from_back_env(environment)
        # Train parameters
        run_one_episode = True
        i = 0
        while run_one_episode:
            # OBSERVATION
            actions = []  # couple list (action, face_id )
            gains = []  # triple ( c-,c+,nc )
            scores = []  # reward
            obs = environment.get_obs_reflex_agents()  # list face observation
            print(obs)
            for o in obs:
                a = [reflex(o[0]), o[1]]
                if a[0] is not None:
                    actions.append(a)
            print(actions)
            constraints = []
            # Decision for AL
            for a in actions:
                environment.execute_action(a[0], a[1])
                constraints.append(environment.constraints())
                gain = environment.constraints() - save_environment.constraints()
                gains.append(gain)
                scores.append(score(gain))
                # environment.update(0)
                environment.from_back_env(save_environment)
            # Observation_Coordinator
            agent_ids = range(len(actions))
            if len(actions) == 0:
                break
            coordinator_observation, agent = observation_coordinator_agents(gains, agent_ids)
            action_coordinator = agent_coordinator.decide(i, coordinator_observation)
            # ACTION
            environment.execute_action(actions[agent[action_coordinator]][0], actions[agent[action_coordinator]][1])
            environment.update(0)
            obs_next = environment.get_obs_reflex_agents()
            actions, gains, scores, constraints = [], [], [], []
            print("obs_next", obs_next)
            # OBSERVATION'
            for o in obs_next:
                a = [reflex(o[0]), o[1]]
                if a[0] is not None:
                    actions.append(a)
            print("#actions", actions)
            save_environment.from_back_env(environment)
            for a in actions:
                environment.execute_action(a[0], a[1])
                constraints.append(environment.constraints())
                gain = environment.constraints() - save_environment.constraints()
                gains.append(gain)
                scores.append(score(gain))
                # environment.update(0)
                environment.from_back_env(save_environment)
            if len(actions) == 0:
                break
            agent_ids = range(len(actions))
            coordinator_observation_next, agent = observation_coordinator_agents(gains, agent_ids)
            # UPDATE POLICY
            agent_coordinator.update(scores[agent[action_coordinator]], coordinator_observation, action_coordinator,
                                     coordinator_observation_next)
            i += 1
            run_one_episode = i < max_iteration_per_episode or coordinator_observation_next == observation_final

        environment.update(0, str(iter_episode) + "_" + str(i) + ".png")
        iter_episode += 1
        run_episodes = iter_episode < max_episode
        print(agent_coordinator.Q)

    agent_coordinator.save = filename
    agent_coordinator.write()
    # TODO
    # selected_action = agent_coord.decide(gains)1
    # reward = environment.execute_action(selected_action)
    # reward:= 999 if done valid blocking general triplet ( nc, c+, c- ) == (0,0,0)
    # autre rw = alpha * c_moins + beta * c_plus + gamma * nc
    # -999 if fail i.e no feasible actions
    # agent_coord.policy


def coordinator(in_graph, in_constraints, iter_max, weight_file):
    agent_coordinator = Q_learning_pol(1000, 3)
    agent_coordinator.load(weight_file)
    environment = back_env()
    environment.from_graph_constraint(in_graph, in_constraints)
    i = 0
    while i < iter_max:
        save_environment = back_env()
        save_environment.from_back_env(environment)
        actions = []  # couple list (action, face_id )
        gains = []  # triple ( c-,c+,nc )
        scores = []  # reward
        obs = environment.get_obs_reflex_agents()  # list face observation
        print(obs)
        for o in obs:
            a = [reflex(o[0]), o[1]]
            if a[0] is not None:
                actions.append(a)
        print(actions)
        constraints = []
        # Decision for AL
        for a in actions:
            environment.execute_action(a[0], a[1])
            constraints.append(environment.constraints())
            gain = environment.constraints() - save_environment.constraints()
            gains.append(gain)
            scores.append(score(gain))
            environment.from_back_env(save_environment)
        # Observation_Coordinator
        agent_ids = range(len(actions))
        if len(actions) == 0:
            break
        coordinator_observation, agent = observation_coordinator_agents(gains, agent_ids)
        action_coordinator = agent_coordinator.decide(i, coordinator_observation)
        # ACTION
        environment.execute_action(actions[agent[action_coordinator]][0], actions[agent[action_coordinator]][1])
        environment.update(0)
        i += 1
    environment.update(1)


if __name__ == "__main__":
    """ Some "polycube-like" entry without geom """
    graphs_cell = []
    constraints = []
    # [0] Simple quad
    graphs_cell.append([[1]])
    constraints.append([[1, 0], [0, 0]])
    # [1] 3 quads
    graphs_cell.append([[0, 1], [1, 1]])
    constraints.append([[0, 1, 1], [1, 0, 0], [1, 0, 0]])
    # [2] 3 quads bis
    graphs_cell.append([[0, 1], [1, 1]])
    constraints.append([[0, 1, 0], [0, 1, 0], [0, 0, 1]])
    # [3] L
    graphs_cell.append([[1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 0, 0], [1, 1, 1]])
    constraints.append([[1, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 1], [1, 0, 0, 1]])
    # [4] 3*3
    graphs_cell.append([[1, 1, 1], [1, 1, 1], [1, 1, 1]])
    constraints.append([[1, 0, 0, 1], [0, 0, 0, 0], [0, 0, 0, 0], [1, 0, 0, 1]])
    # [5] Grid 5*3 quads
    graphs_cell.append([[1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1]])
    constraints.append([[1, 1, 1, 1], [1, 0, 0, 1], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 1], [1, 1, 1, 1]])
    # [2] 3 quads bis
    graphs_cell.append([[0, 1], [1, 1]])
    constraints.append([[0, 1, 1], [1, 0, 0], [1, 0, 0]])

    graphs_cell.append([[0, 1], [1, 1]])
    constraints.append([[0, 1, 1], [1, 2, 0], [1, 0, 0]])

    # generate_all_placement()
    """
    test_gain()
    b = back_env()
    b.from_graph_constraint([[1]], [[1, 0], [0, 0]])
    before_gain_b = b.lcc_hl.gains()
    c = back_env()
    c.from_back_env(b)
    before_gain_c = np.array(c.lcc_hl.gains())
    print(b.lcc_hl.representation_unique(0))
    b.execute_action(3, 0)
    after_gain_b = np.array(b.lcc_hl.gains())
    b.update(1)
    c.update(1)
    print(Reward(b, c))
    """
    # -----------------------------------------------------------------------------------------------------------------
    """
    g_courbure = [[1, 0], [1, 1]]
    c_courbure = [[0, 0, 0], [0, 2, 0], [1, 0, 0]]

    test_random_action(g_courbure, c_courbure)

    g_donut = [[1, 1, 1], [1, 0, 1], [1, 1, 1]]
    c_donut = [[1, 0, 0, 1], [0, 2, 2, 0], [0, 2, 2, 0], [1, 0, 0, 1]]

    test_random_action(g_donut, c_donut)


    test_random_action(g_baby, c_baby)

    # g_baby = [[0, 1, 0], [1, 1, 1], [0, 1, 0], [1, 1, 1]]
    # c_baby = [[0, 1, 1, 0], [1, 0, 0, 1], [1, 2, 2, 1], [0, 0, 0, 0], [1, 0, 0, 1]]

    test_random_action(list(reversed(g_baby)), list(reversed(c_baby)))

    g_truc = [[1, 1, 1, 0, 1, 1, 1], [1, 0, 1, 0, 1, 0, 1], [1, 1, 1, 1, 1, 1, 1], [0, 0, 1, 0, 1, 0, 0],
              [0, 0, 1, 1, 1, 0, 0]]
    c_truc = [[1, 0, 0, 1, 1, 0, 0, 1], [0, 2, 2, 0, 0, 2, 2, 0], [0, 2, 2, 2, 2, 2, 2, 0], [0, 0, 0, 2, 2, 0, 2, 0],
              [0, 0, 0, 2, 2, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0]]

    test_random_action(g_truc, c_truc)

    g = [[0, 1, 0], [1, 1, 1], [0, 1, 0], [1, 1, 1]]
    c = [[0, 1, 1, 0], [1, 2, 2, 1], [1, 2, 2, 1], [0, 0, 0, 0], [1, 0, 0, 1]]

    g1 = [[1, 0], [1, 1]]
    c1 = [[1, 1, 0], [0, 2, 1], [0, 0, 1]]

    # history = test_random_action(g, c)

    history = test_random_action(g1, c1)

    for i in range(len(graphs_cell)):
        test_random_action(graphs_cell[i], constraints[i])

    g_x = [[1, 1, 1]]
    c_x = [[1, 0, 0, 0], [1, 0, 0, 1]]
    test_random_action(g_x, c_x)
 
    gc_t = []

    ga37 = [[[0, 1, 0], [1, 1, 1]], [[0, 0, 1, 0], [1, 0, 0, 1], [0, 0, 0, 0]]]
    gc_t.append(ga37)
    ga38 = [[[1, 0], [1, 1]], [[1, 1, 0], [0, 0, 1], [0, 0, 0]]]
    gc_t.append(ga38)
    ga39 = [[[0, 1, 0], [1, 1, 1]], [[0, 1, 0, 0], [1, 0, 0, 1], [1, 0, 0, 1]]]
    gc_t.append(ga39)
    ga40 = [[[0, 1, 0], [1, 1, 1], [0, 1, 0]], [[0, 1, 1, 0], [0, 0, 0, 0], [1, 0, 0, 1], [0, 0, 1, 0]]]
    gc_t.append(ga40)
    ga41 = [[[0, 1, 0], [1, 1, 1]], [[0, 1, 1, 0], [1, 0, 0, 1], [0, 0, 0, 1]]]
    gc_t.append(ga41)
    ga42 = [[[0, 1, 0], [1, 1, 1], [0, 1, 0]], [[0, 1, 1, 0], [1, 0, 0, 1], [1, 0, 0, 1], [0, 1, 0, 0]]]
    gc_t.append(ga42)

    gall = [[[0, 1, 0], [1, 1, 1]], [[0, 1, 1, 0], [1, 0, 2, 1], [0, 0, 0, 0]]]

    gall2 = [[[1, 1]], [[1, 0, 0], [1, 0, 1]]]

    test_random_action(gall[0], gall[1])
    test_random_action(gall2[0], gall2[1])

    test_random_action([[1, 1, 1, 0, 0], [1, 0, 1, 0, 0], [1, 1, 1, 1, 1], [0, 0, 1, 0, 1], [0, 0, 1, 1, 1]],
                       [[1, 0, 0, 1, 0, 0], [0, 2, 2, 0, 0, 0], [0, 2, 2, 0, 0, 1], [1, 0, 0, 2, 2, 0],
                        [0, 0, 0, 2, 2, 0], [0, 0, 1, 0, 0, 1]])

    # test_random_action([[0, 1, 1, 1, 0], [1, 1, 0, 1, 1], [0, 1, 1, 1, 0]],
    #                   [[0, 1, 0, 0, 1, 0], [1, 2, 2, 2, 2, 1], [1, 2, 2, 2, 2, 1], [0, 1, 0, 0, 1, 0]])

    test_random_action([[0, 0, 1, 0, 0], [0, 1, 1, 1, 0], [1, 1, 0, 1, 1], [0, 1, 1, 1, 0], [0, 0, 1, 0, 0]],
                       [[0, 0, 1, 1, 0, 0], [0, 1, 2, 2, 1, 0], [1, 2, 2, 2, 2, 1], [1, 2, 2, 2, 2, 1],
                        [0, 1, 2, 2, 1, 0], [0, 0, 1, 1, 0, 0]])

    """
    """Generate cross form"""

    """
    
    
    """
    """Test actions
    b = back_env();b.from_obs("0000", "0001");b.update(1)

    b.reset();b.execute_action(0,0);b.update(1);
    b.reset();b.execute_action(1,0);b.update(1);
    b.reset();b.execute_action(5,0);b.update(1);

    b = back_env()
    b.from_obs("0010011", "0000")
    b.update(1)
    b.lcc_hl.set_point_py(2, 7.5, 10)

    b.lcc_hl.action_linking(0)
    b.update(1)

    b = back_env()
    b.from_obs("0101011011", "0000")
    b.update(1)
    b.lcc_hl.action_linking(0)
    b.update(1)

    b = back_env()
    obs = "00001", "0100"
    b.from_obs("00001", "0100")
    b.update(1)
    a = reflex(obs)
    a = 16
    b.lcc_hl.representation_unique(0)
    b.execute_action(a, 0)
    b.update(1)
    """

    g = [[0, 0, 1, 1, 1, 1, 1, 1], [0, 0, 1, 0, 0, 0, 0, 1], [1, 1, 1, 1, 1, 1, 0, 1], [1, 0, 1, 0, 0, 1, 0, 1],
         [1, 1, 1, 0, 0, 1, 1, 1]]
    c = [[0, 0, 1, 0, 0, 0, 0, 0, 1],
         [0, 0, 0, 2, 0, 0, 0, 2, 0],
         [0, 0, 0, 2, 0, 0, 0, 0, 0],
         [0, 2, 2, 0, 0, 0, 0, 0, 0],
         [0, 2, 2, 0, 0, 0, 2, 2, 0],
         [1, 0, 0, 1, 0, 1, 0, 0, 1]]


    # test_random_action(g, c)
    # exit()
    g_cross, c_cross = [[0, 1, 0], [1, 1, 1], [0, 1, 0]], [[0, 1, 1, 0], [0, 0, 0, 1], [0, 2, 2, 0], [0, 0, 0, 0]]
    # test_random_action(g_cross, c_cross)
    # exit()

    # reflex_test()
    g_baby = [[0, 1, 0], [1, 1, 1], [0, 1, 0], [1, 1, 1]]
    c_baby = [[0, 1, 1, 0], [1, 2, 2, 1], [1, 2, 2, 1], [0, 0, 0, 0], [1, 0, 0, 1]]


    b = back_env()
    b.from_graph_constraint([[1,1,1],[1,0,1],[1,1,1]], [[1,1,1,1],[1,1,1,1],[1,1,1,1],[1,1,1,1]])
    b.update(0)
    b.lcc_hl.add_ghost_layer()
    b.update(1)
    b.lcc_hl.remove_ghost_layer()
    b.update(1)
    ext = ".png"
    print("#MODEL QUAD")
    b = back_env()
    b.from_graph_constraint([[1]], [[1,1],[1,1]])
    b.update(1)
    b.lcc_hl.select_outside()
    b.lcc_hl.add_ghost_layer()
    b.lcc_hl.select_validation()
    b.update(1)
    b.lcc_hl.pillowing_2D([])
    b.update(1)
    b.lcc_hl.remove_ghost_layer()
    b.update(1,"outa"+ext)

    print("#MODEL 4 QUAD")
    b = back_env()
    g = [[1,1],[1,1]]
    c = [[1,1,1],[1,1,1],[1,1,1]]
    g1 = [[1,1,1],[1,1,1],[1,1,1]]
    c1 = [[1,1,1,1],[1,1,1,1],[1,1,1,1],[1,1,1,1]]
    b.from_graph_constraint(g, c)
    b.update(1)
    b.lcc_hl.select_inside()
    b.lcc_hl.add_ghost_layer()
    b.lcc_hl.select_validation()
    b.update(1)
    b.lcc_hl.pillowing_2D([])
    b.update(1)
    b.lcc_hl.remove_ghost_layer()
    b.update(1,"outb"+ext)

    b = back_env()
    g = [[1,1],[1,1]]
    c = [[1,1,1],[1,1,1],[1,1,1]]
    b.from_graph_constraint(g, c)
    b.lcc_hl.select_cell(0)
    b.lcc_hl.add_ghost_layer()
    b.lcc_hl.select_validation()
    b.lcc_hl.pillowing_2D([])
    b.lcc_hl.remove_ghost_layer()
    b.update(1,"out0"+ext)

    b = back_env()
    g = [[1]]
    c = [[1,1],[1,1]]
    b.from_graph_constraint(g, c)
    b.update(1)
    b.lcc_hl.select_edge(0,1)
    b.lcc_hl.select_edge(1,2)
    b.lcc_hl.add_ghost_layer()
    b.lcc_hl.select_validation()
    b.lcc_hl.pillowing_2D([])
    b.lcc_hl.remove_ghost_layer()
    b.update(1,"out1"+ext)

    b = back_env()
    g = [[1,1],[1,1]]
    c = [[1,1,1],[1,1,1],[1,1,1]]
    b.from_graph_constraint(g, c)
    b.lcc_hl.select_cell(2)
    b.lcc_hl.select_edge(2,6)
    b.lcc_hl.select_edge(1,2)
    b.lcc_hl.add_ghost_layer()
    b.lcc_hl.select_validation()
    b.lcc_hl.pillowing_2D([])
    b.lcc_hl.remove_ghost_layer()
    b.update(1,"out2"+ext)


    selected_edges =[(0,3),(3,11),(11,10), (10,2),(2,6),(6,5),(5,1),(1,0)]
    b = back_env()
    g = [[1,1],[1,1]]
    c = [[1,1,1],[1,1,1],[1,1,1]]
    b.from_graph_constraint(g, c)
    for e in selected_edges:
        b.lcc_hl.select_edge(e[0], e[1])

    b.lcc_hl.add_ghost_layer()
    b.lcc_hl.select_validation()
    b.lcc_hl.pillowing_2D([])
    b.lcc_hl.remove_ghost_layer()
    b.update(1,"out3"+ext)
    b.lcc_hl.unmark_selection()
    b.lcc_hl.select_cell(0)
    b.lcc_hl.pillowing_2D([])
    #test_random_action(g_baby, c_baby)

    #train_coordinator("wheightsave.json")
    #coordinator(g_baby, c_baby, 50, "wheightsave.json")

    # generate_all_coordinator_observation(3, 2, 6)
    """
    qlp = Q_learning_pol(states_n=5, actions_n=2)
    qlp.set(np.array([0, 0, 0]), 900.0)
    qlp.set(np.array([10, 10, 10]), -900.0)
    print(qlp.Q)
    # test_random_action(g_baby, c_baby)
    qlp.insert_observation(np.array([0, 0, 0]))
    qlp.insert_observation(np.array([[0, 1], [0, 2]]))
    qlp.Q
    """
    # exit()

    # b.update(1)
    # c.update(1)
    # test_coord()

    # test_random_action([[0, 1, 0], [1, 1, 1], [0, 1, 0]], [[0, 1, 1, 0], [0, 0, 0, 1], [0, 2, 2, 0], [0, 0, 0, 0]])

    # graphs_cell = [[0, 1], [1, 1]]
    # constraints = [[0, 1, 1], [1, 2, 0], [1, 0, 0]]
    # environment = back_env()
    # environment.from_graph_constraint(graphs_cell, constraints)

    # test_random_action([[0, 1, 0], [1, 1, 1], [0, 1, 0]], [[0, 1, 1, 0], [0, 0, 0, 1], [1, 0, 2, 1], [0, 1, 1, 0]])

    # test_random_action(cross_g, cross_c)

    # test_random_action([[1, 0], [1, 1]], [[0, 0, 0], [0, 2, 0], [0, 0, 0]])

    """test
    cross_g = [[0, 1, 0], [1, 1, 1], [0, 1, 0]]
    cross_c = [[0, 1, 1, 0], [0, 0, 0, 1], [0, 2, 2, 1], [0, 1, 1, 0]]
    test_random_action(cross_g, cross_c)
    """
    """
    for g, c in gc_t:
        test_random_action(g, c)
    """
    """
    # test_random_action(graphs_cell[3], constraints[3])

    # b = back_env()
    # b.from_graph_constraint([[1, 1, 1], [1, 1, 1], [1, 1, 1]], [[1, 0, 0, 1], [0, 0, 0, 0], [0, 0, 0, 0], [1, 0, 0, 1]])
    # b.from_graph_constraint([[0, 1, 0], [1, 1, 1], [0, 1, 0], [1, 1, 1]],[[0, 1, 1, 0], [1, 0, 0, 1], [1, 0, 0, 1], [0, 0, 0, 0], [1, 0, 0, 1]])

    # b.update(1)

    # prio = b.get_prio(b.lcc_hl.get_cells_id())
    # print(prio)

    # print(history)
    # b = back_env()
    # b.from_graph_constraint([[1]], [[1, 1], [1, 1]])
    # history = test_random_action()
    # print(b.lcc_hl.representation_unique(0))
    # print(b.lcc_hl.get_vertex_of_cell(0))

    # print(get_exec_number())

    # df = pandas.read_json("history_test.json")
    b = back_env()
    b.from_graph_constraint([[1, 1, 1], [1, 1, 1], [1, 1, 1]], [[0, 0, 0, 0], [0, 0, 0, 0], [0, 2, 0, 0], [0, 0, 0, 0]])
    b.update(1)
    v1 = b.lcc_hl.split_edge(14, 2)

    v2 = b.lcc_hl.split_edge(v1, 2)
    v3 = b.lcc_hl.split_edge(6, 2)
    b.lcc_hl.update_nc(v1)
    b.lcc_hl.update_nc(v2)
    b.lcc_hl.update_nc(v3)
    b.execute_action(a=34, cell_id=4)
    b.update(1)

    rp1 = b.lcc_hl.representation_unique(1)
    rp3 = b.lcc_hl.representation_unique(3)
    rp4 = b.lcc_hl.representation_unique(4)
    print("#repre 1", rp1, b.lcc_hl.is_face_prio_marked(1))
    print("#repre 3", rp3, b.lcc_hl.is_face_prio_marked(3))
    print("#repre 4", rp4, b.lcc_hl.is_face_prio_marked(4))

    b = back_env()
    b.from_graph_constraint([[1, 1, 1], [1, 1, 1], [1, 1, 1]], [[0, 0, 0, 0], [0, 0, 0, 0], [0, 2, 0, 0], [0, 0, 0, 0]])
    b.update(1)
    v1 = b.lcc_hl.split_edge(14, 2)
    v2 = b.lcc_hl.split_edge(v1, 2)
    b.lcc_hl.update_nc(v1)
    b.lcc_hl.update_nc(v2)
    b.execute_action(a=34, cell_id=4)
    b.update(1)

    rp1 = b.lcc_hl.representation_unique(1)
    rp3 = b.lcc_hl.representation_unique(3)
    rp4 = b.lcc_hl.representation_unique(4)
    print("#repre 1", rp1, b.lcc_hl.is_face_prio_marked(1))
    print("#repre 3", rp3, b.lcc_hl.is_face_prio_marked(3))
    print("#repre 4", rp4, b.lcc_hl.is_face_prio_marked(4))
    """
