import numpy as np
import itertools
import random

class Policy:
    """
    """

    def __init__(self, states, actions_n, feasible_actions, lr=0.85, y=0.99):
        """
        :param states: etats [4,2,2]
        :param actions_n: nombre d'action
        :param lr: learning rate
        :param y: payoff paramètre d'ajustement de gain distant
        """
        self.states_n = states[0]
        for s in states[1:]:
            self.states_n *= s
        all_ranges = []
        for s in states:
            all_ranges.append([j for j in range(s)])

        self.Q_table = []
        for element in itertools.product(*all_ranges):
            self.Q_table.append(*element)

        print("self.Q_table", self.Q_table)
        self.actions_n = actions_n
        self.Q = np.zeros([self.states_n, actions_n])
        self.lr = lr
        self.y = y
        self.feasible_actions = np.array(feasible_actions)
        # self.Q2 = np.zeros([states_n, actions_n])

    def reset(self):
        """
        :return:
        """
        self.Q = np.zeros([self.states_n, self.actions_n])

    def decide(self, i, s):
        """
        :param i: iteration
        :param s: state
        :return:
        """
        print("s", s)

        """ Moins de fléxibilité pour les actions faisables attention """
        # self.Q2 = (self.Q[s, :] + np.random.normal(100, 10, self.actions_n) / 100 * ( 1. / (i + 1))) * self.feasible_actions[s, :]
        # print("feasible", self.feasible_actions[s], s)
        rand = random.random()
        if rand < 80:# Exploit
            self.Q2 = (self.Q[s, :] + np.random.normal(100, 10, self.actions_n) / 100 * (1. / (i + 1)))
        else: # Explore
            self.Q2 = np.random.normal(100, 10, self.actions_n) / 100 * (1. / (i + 1))
        # * feasible_actions[s]
        print(self.Q2)
        print("a", np.argmax(self.Q2))
        return np.argmax(self.Q2)


    def update(self, reward, state, action, state_next):
        """
        :param reward: récompense
        :param s : state (obs)
        :param s1 : state+1
        :param a : action
        :return:
        """
        print(state, action, state_next, reward)
        old_value = self.Q[state, action]
        self.Q[state, action] = self.Q[state, action] + self.lr * (
                    reward + self.y * np.max(self.Q[state_next, :]) - self.Q[state, action])
        new_value = self.Q[state, action]
        print("Q["+str(state)+","+str(action)+"]=", old_value, " updated to ", new_value)


