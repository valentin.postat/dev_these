#!/usr/bin/env python
# -*- coding: utf-8 -*-

import vtk


def main():
    colors = vtk.vtkNamedColors()
    filename, filename_out, show = get_program_parameters()

    # Create the reader for the data.
    # print('Loading ', filename)
    reader = vtk.vtkUnstructuredGridReader()
    reader.SetFileName(filename)
    reader.Update()

    extractEdges = vtk.vtkExtractEdges()
    extractEdges.SetInputConnection(reader.GetOutputPort())

    legendValues = vtk.vtkVariantArray()
    it = reader.GetOutput().NewCellIterator()
    it.InitTraversal()
    while not it.IsDoneWithTraversal():
        cell = vtk.vtkGenericCell()
        it.GetCell(cell)
        cellName = vtk.vtkCellTypes.GetClassNameFromTypeId(cell.GetCellType())
        # print(cellName, 'NumberOfPoints:', cell.GetNumberOfPoints(), 'CellDimension:', cell.GetCellDimension())
        legendValues.InsertNextValue(cellName)
        it.GoToNextCell()

    aPoints = vtk.vtkPoints()
    aPoints = reader.GetOutput()
    nb_pts = aPoints.GetNumberOfPoints()

    # print(reader.GetOutput().GetPointData())
    # Tube the edges
    tubes = vtk.vtkTubeFilter()
    tubes.SetInputConnection(extractEdges.GetOutputPort())

    tubes.SetRadius(.03)
    tubes.SetNumberOfSides(21)

    edgeMapper = vtk.vtkPolyDataMapper()
    edgeMapper.ScalarVisibilityOff()
    edgeMapper.SetInputConnection(tubes.GetOutputPort())
    # edgeMapper.SetScalarRange(0, 26)

    edgeActor = vtk.vtkActor()
    edgeActor.SetMapper(edgeMapper)
    # edgeActor.GetProperty().SetSpecular(0.6)
    edgeActor.GetProperty().SetSpecularPower(30)
    edgeActor.GetProperty().SetColor(colors.GetColor3d("Black"))

    # Glyph the points
    sphere = vtk.vtkSphereSource()
    sphere.SetPhiResolution(21)
    sphere.SetThetaResolution(21)
    sphere.SetRadius(0.20)

    # POINT
    pointMapper = vtk.vtkGlyph3DMapper()
    pointMapper.SetInputConnection(reader.GetOutputPort())
    pointMapper.SetSourceConnection(sphere.GetOutputPort())
    #
    # pointMapper.SetScalarModeToUsePointData()
    pointMapper.ScalingOff()
    # pointMapper.ScalarVisibilityOff()

    # Coloring point
    # geometryMapper.SetScalarModeToUseCellData()
    pointMapper.SetScalarRange(0, nb_pts)
    # pointMapper.SetScaleModeToScaleByVectorComponents()

    pointActor = vtk.vtkActor()
    pointActor.SetMapper(pointMapper)
    # pointActor.GetProperty().SetColor(colors.GetColor3d('Blue'))

    # Label the points
    # point_lut = reader.GetOutput().GetPointData().GetScalars().GetLookupTable()
    # Structural vertex ID
    labelMapper = vtk.vtkLabeledDataMapper()
    labelMapper.SetInputConnection(reader.GetOutputPort())
    labelMapper.SetLabelModeToLabelScalars()
    labelMapper.SetFieldDataName("Point_id")
    labelActor = vtk.vtkActor2D()
    labelActor.SetMapper(labelMapper)
    labelActor.GetProperty().SetColor(1.0, 1.0, 0.0)

    # Coloring the points
    """
    # Structural Cell ID
    cellMapper = vtk.vtkLabeledDataMapper()
    cellMapper.SetInputConnection(reader.GetOutputPort())
    labelMapper.SetLabelModeToLabelScalars()
    # cellMapper.SetLabelFormat("%g")
    cellMapper.SetFieldDataName("Cell_id")
    cellLabels = vtk.vtkActor2D()
    cellLabels.SetMapper(cellMapper)
    """

    # Create labels for cells
    cc = vtk.vtkCellCenters()
    cc.SetInputConnection(reader.GetOutputPort())
    labelCellMapper = vtk.vtkLabeledDataMapper()
    labelCellMapper.SetInputConnection(cc.GetOutputPort())
    labelCellMapper.SetLabelModeToLabelScalars()
    labelCellMapper.SetFieldDataName("Cell_id")
    cellLabels_ids = vtk.vtkActor2D()
    #cellLabels_ids = vtk.vtkTextActor()

    cellLabels_ids.SetMapper(labelCellMapper)
    cellLabels_ids.GetProperty().SetColor(colors.GetColor3d('White'))
    #cellLabels_ids.GetProperty().SetFontSize(10)
    # SetFontSize

    # The geometry
    geometryShrink = vtk.vtkShrinkFilter()
    geometryShrink.SetInputConnection(reader.GetOutputPort())
    geometryShrink.SetShrinkFactor(0.8)

    # NOTE: We must copy the originalLut because the CategoricalLegend
    # needs an indexed lookup table, but the geometryMapper uses a
    # non-index lookup table
    categoricalLut = vtk.vtkLookupTable()
    originalLut = reader.GetOutput().GetCellData().GetScalars().GetLookupTable()

    categoricalLut.DeepCopy(originalLut)
    categoricalLut.IndexedLookupOn()

    geometryMapper = vtk.vtkDataSetMapper()
    geometryMapper.SetInputConnection(geometryShrink.GetOutputPort())
    geometryMapper.SetScalarModeToUseCellData()
    geometryMapper.SetScalarRange(0, 11)

    geometryActor = vtk.vtkActor()
    geometryActor.SetMapper(geometryMapper)
    geometryActor.GetProperty().SetLineWidth(3)
    geometryActor.GetProperty().EdgeVisibilityOn()
    geometryActor.GetProperty().SetEdgeColor(0, 0, 0)

    contextView = vtk.vtkContextView()
    renderer = contextView.GetRenderer()
    renderWindow = contextView.GetRenderWindow()
    renderWindowInteractor = vtk.vtkRenderWindowInteractor()
    renderWindowInteractor.SetRenderWindow(renderWindow)

    renderer.AddActor(geometryActor)
    renderer.AddActor(pointActor)
    renderer.AddActor(labelActor)
    renderer.AddActor(edgeActor)
    # renderer.AddActor(cellLabels)
    renderer.AddActor(cellLabels_ids)
    renderer.SetBackground(colors.GetColor3d('SlateGray'))
    renderer.SetBackground(colors.GetColor3d('White'))

    aCamera = vtk.vtkCamera()
    # aCamera.Azimuth(-40.0)label cells
    # aCamera.Elevation(50.0)

    renderer.SetActiveCamera(aCamera)
    renderer.ResetCamera()

    #renderWindow.SetSize(640, 480)
    print(show)



    renderWindow.SetSize(1280, 960)
    renderWindow.SetWindowName('ReadLegacyUnstructuredGrid')

    renderWindow.Render()
    if show != 0:
        renderWindowInteractor.Start()
    else:
        renderWindow.OffScreenRenderingOn()


    # screenshot code:
    w2if = vtk.vtkWindowToImageFilter()
    w2if.SetInput(renderWindow)
    w2if.SetInputBufferTypeToRGB()
    w2if.ReadFrontBufferOff()
    w2if.Update()
    # Writing
    writer = vtk.vtkPNGWriter()
    writer.SetFileName(filename_out)
    writer.SetInputConnection(w2if.GetOutputPort())
    writer.Write()


def get_program_parameters():
    import argparse
    description = 'Display a vtkUnstructuredGrid that contains eleven linear cells.'
    epilogue = '''
    This example also shows how to add a vtkCategoryLegend to a visualization.
   '''
    parser = argparse.ArgumentParser(description=description, epilog=epilogue
                                     )

    parser.add_argument('--show', help='0 to not render(just write)\n1 to render interaciv', type=int)
    parser.add_argument('filename', help='VTKCellTypes.vtk.')
    parser.add_argument('fout', nargs='?', help="File to save in .png", default='TestScreenshot.png')
    args = parser.parse_args()
    print(args)
    return args.filename, args.fout, args.show


if __name__ == '__main__':
    main()
