// for pybind
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/functional.h>
#include <pybind11/chrono.h>

#include <iostream>
#include "LCC_HL.h"
#include "BlockStruct.h"
#include "TypesForLCC.h"

#define STRINGIFY(x) #x
#define MACRO_STRINGIFY(x) STRINGIFY(x)

namespace py = pybind11;

template <typename... Args>
using overload_cast_ = pybind11::detail::overload_cast_impl<Args...>;

/*py::class_<LinearCellComplexBuilder>(m, "LinearCellComplexBuilder")
.def(py::init<>())
.def_static("basic_quadv", &LinearCellComplexBuilder::basic_quadv)
.def_static("basic_four_quads", &LinearCellComplexBuilder::basic_four_quads)
.def_static("basic_six_quads", &LinearCellComplexBuilder::basic_six_quads)
.def_static("basic_modelf", &LinearCellComplexBuilder::basic_modelf);*/

PYBIND11_MODULE(pybinding, m){
py::class_<Point>(m,"Point")
    .def (py::init<>())
    .def("x",&Point::x)
    .def("y",&Point::y)
.def("__repr__",[](const Point &a) {return std::string(std::to_string(a.x())+" "+std::to_string(a.y()));});

py::class_<LCC_2>(m,"LCC_2")
    .def(py::init<>());

py::class_<LinearCellComplexBuilder>(m,"LinearCellComplexBuilder")
    .def(py::init<>());
    //.def(py::init(&LinearCellComplexBuilder::basic_six_quads))
    //.def("basic_six_quads", &LinearCellComplexBuilder::basic_six_quads)
    //.def("grid_like", &LinearCellComplexBuilder::grid_like);

py::class_<LCC_HL>(m,"LCC_HL")
    .def (py::init<>())
    .def(py::init<std::string,std::string>())
    .def(py::init<LCC_HL>())
    .def(py::init<std::vector<float>,std::vector<float>,std::vector<std::vector<std::size_t>>,std::vector<std::size_t>>())
    // not working :
    .def(py::init<LCC_2>())
    .def("draw", &LCC_HL::draw)
    .def("set_point_py", &LCC_HL::set_point_py)
    .def("set_draw",&LCC_HL::set_draw)
    .def("pillowing",&LCC_HL::pillowing)
    .def("pillowing_out",&LCC_HL::pillowing_out)
    .def("get_all_vertex_ids",&LCC_HL::get_all_vertex_ids)
    .def("incident_edge",&LCC_HL::incident_edge)
    .def("incident_quad",&LCC_HL::incident_quad)
    .def("incident_tri",&LCC_HL::incident_tri)
    .def("point_of_vertex",&LCC_HL::point_of_vertex )
    .def("reset",&LCC_HL::reset)
    .def("is_neigh",&LCC_HL::is_neigh)
    .def("on_boundary",&LCC_HL::on_boundary)
    .def("in_boundary",&LCC_HL::in_boundary)
    .def("neigborhood",&LCC_HL::neigborhood)
    //.def("get_constraint", static_cast<void (LCC_HL::*)(std::size_t)>(&LCC_HL::get_constraint));
    .def("valence",&LCC_HL::valence)
    .def("get_constraint", overload_cast_<const std::size_t>()(&LCC_HL::get_constraint))
    .def("get_cells_id",&LCC_HL::get_cells_id)
    .def("get_vertex_of_cell",&LCC_HL::get_vertex_of_cell)
    .def("get_cells_id",&LCC_HL::get_cells_id)
    .def("iteration_vertex",&LCC_HL::iteration_vertex)
    .def("iteration_cell",&LCC_HL::iteration_cell)
    .def("neigborhood",&LCC_HL::neigborhood)
    .def("neighbor_cells",&LCC_HL::neighbor_cells)
    .def("set_constraint_vtx", &LCC_HL::set_constraint_vtx)
    .def("get_field",&LCC_HL::get_field)
    .def("get_fields",&LCC_HL::get_fields)
    .def("set_field",&LCC_HL::set_field)
    .def("writer",&LCC_HL::writer)
    .def("number_of_darts",&LCC_HL::number_of_darts)
    .def("split_cell",&LCC_HL::split_cell)
    .def("split_edge",&LCC_HL::split_edge)
    .def("split_cell_d",&LCC_HL::split_cell_d)
    .def("get_non_conform_vertex",&LCC_HL::get_non_conform_vertex)
    .def("get_crit_vertex",&LCC_HL::get_crit_vertex)
    .def("c_1",&LCC_HL::c_1)
    .def("c_2",&LCC_HL::c_2)
    .def("c_3",&LCC_HL::c_3)
    .def("c_4",&LCC_HL::c_4)
    .def("c_5",&LCC_HL::c_5)
    .def("nc_5",&LCC_HL::nc_5)
    .def("nc_6",&LCC_HL::nc_6)
    .def("nc_6a",&LCC_HL::nc_6a)
    .def("nc_6b",&LCC_HL::nc_6b)
    .def("nc_7",&LCC_HL::nc_7)
    .def("nc_8",&LCC_HL::nc_8)
    .def("rpz",&LCC_HL::rpz)
    .def("rpz_crit",&LCC_HL::rpz_crit)
    .def("cells_connected_components",&LCC_HL::cells_connected_components)
    .def("set_vertex_of_cell",&LCC_HL::set_vertex_of_cell)
    .def("place_from_nc_map",&LCC_HL::place_from_nc_map)
    .def("s_1",&LCC_HL::s_1)
    .def("s_2",&LCC_HL::s_2)
    .def("s_3",&LCC_HL::s_3)
    .def("s_3_bis",&LCC_HL::s_3_bis)
    .def("s_4",&LCC_HL::s_4)
    .def("nc_1",&LCC_HL::nc_1)
    .def("nc_2",&LCC_HL::nc_2)
    .def("set_orientation",&LCC_HL::set_orientation)
    .def("show_map_nc",&LCC_HL::show_map_nc)
    .def("get_from_map_nc",&LCC_HL::get_from_map_nc)
    .def("set_map_nc",&LCC_HL::set_map_nc)
    .def("update_nc",&LCC_HL::update_nc)
    .def("update_crit",&LCC_HL::update_crit)
    .def("get_criticity_of_vertex",&LCC_HL::get_criticity_of_vertex)
    .def("get_conform_vertex",&LCC_HL::get_conform_vertex)
    .def("valence_l",&LCC_HL::valence_l)
    .def("load_map",&LCC_HL::load_maps)
    .def("representation_unique",&LCC_HL::representation_unique)
    .def("representation_conformite",&LCC_HL::representation_conformite)
    .def("representation_criticite",&LCC_HL::representation_criticite)
    .def("representation_conformite_unique",&LCC_HL::representation_conformite_unique)
    .def("representation_criticite_unique",&LCC_HL::representation_criticite_unique)
    .def("is_orb_conform",&LCC_HL::is_orb_conform)
    .def("update_crit",&LCC_HL::update_crit)
    .def("set_point",&LCC_HL::set_point)
    .def("a_0",&LCC_HL::a_0)
    .def("a_1",&LCC_HL::a_1)
    .def("a_2",&LCC_HL::a_2)
    .def("a_3",&LCC_HL::a_3)
    .def("a_4",&LCC_HL::a_4)
    .def("a_5",&LCC_HL::a_5)
    .def("a_6",&LCC_HL::a_6)
    .def("a_7",&LCC_HL::a_7)
    .def("a_8",&LCC_HL::a_8)
    .def("a_9",&LCC_HL::a_9)
    .def("a_10",&LCC_HL::a_10)
    .def("a_11",&LCC_HL::a_11)
    .def("a_12",&LCC_HL::a_12)
    .def("a_13",&LCC_HL::a_13)
    .def("a_14",&LCC_HL::a_14)
    .def("a_15",&LCC_HL::a_15)
    .def("a_16",&LCC_HL::a_16)
    .def("a_17",&LCC_HL::a_17)
    .def("a_18",&LCC_HL::a_18)
    .def("a_19",&LCC_HL::a_19)
    .def("a_20",&LCC_HL::a_20)
    .def("a_21",&LCC_HL::a_21)
    .def("a_22",&LCC_HL::a_22)
    .def("a_23",&LCC_HL::a_23)
    .def("a_24",&LCC_HL::a_24)
    .def("a_25",&LCC_HL::a_25)
    .def("a_26",&LCC_HL::a_26)
    .def("a_27",&LCC_HL::a_27)
    .def("a_28",&LCC_HL::a_28)
    .def("a_29",&LCC_HL::a_29)
    .def("a_30",&LCC_HL::a_30)
    .def("a_31",&LCC_HL::a_31)
    .def("a_32",&LCC_HL::a_32)
    .def("a_33",&LCC_HL::a_33)
    .def("a_34",&LCC_HL::action_negativ)
    .def("a_35",&LCC_HL::a_35)
    .def("unmark_orb_conformite",&LCC_HL::unmark_orb_conformite)
    .def("is_a_34_available",&LCC_HL::is_a_34_available)
    .def("erase_vertex",&LCC_HL::erase_vertex)
    .def("remove_vtx",&LCC_HL::remove_vtx)
    .def("number_of_vertex_attribute",&LCC_HL::number_of_vertex_attribute)
    .def("is_valid",&LCC_HL::is_valid)
    .def("is_face_prio_marked",&LCC_HL::is_face_prio_marked)
    .def("remove_prio_mark",&LCC_HL::remove_prio_mark)
    .def("is_pillowing_marked",&LCC_HL::is_pillowing_marked)
    .def("mark_pillowing",&LCC_HL::mark_pillowing)
    .def("unmark_pillowing",&LCC_HL::unmark_pillowing)
    .def("a_36",&LCC_HL::a_36)
    .def("a_37",&LCC_HL::a_37)
    .def("a_38",&LCC_HL::a_38)
    .def("a_39",&LCC_HL::a_39)
    .def("a_40",&LCC_HL::a_40)
    .def("a_41",&LCC_HL::a_41)
    .def("a_42",&LCC_HL::a_42)
    .def("action_linking",&LCC_HL::action_linking)
    .def("number_of_crit_negatif",&LCC_HL::number_of_crit_negatif)
    .def("number_of_crit_positif",&LCC_HL::number_of_crit_positif)
    .def("number_of_non_conf",&LCC_HL::number_of_non_conf)
    .def("gains",&LCC_HL::gains)
    .def("unmark_cell_prio",&LCC_HL::unmark_cell_prio)
    .def("writer_paraview",&LCC_HL::writer_paraview)
    .def("add_ghost_layer",&LCC_HL::add_ghost_layer)
    .def("pillowing_2D",&LCC_HL::pillowing_2D)
    .def("remove_ghost_layer",&LCC_HL::remove_ghost_layer)
    .def("select_inside",&LCC_HL::select_inside)
    .def("select_outside",&LCC_HL::select_outside)
    .def("select_validation",&LCC_HL::select_validation)
    .def("barycenters",&LCC_HL::barycenters)
    .def("select_cell",&LCC_HL::select_cell)
    .def("select_edge",&LCC_HL::select_edge)
    .def("unmark_selection",&LCC_HL::unmark_selection)

    //.def("set_point",overload_cast_<const Point>()(&LCC_HL::set_point))
    //.def("set_point", static_cast<void (LCC_HL::*)(const Point)>(&LCC_HL::set_point))
    ;




















//.def_static("basic_four_quads", &LinearCellComplexBuilder::basic_four_quads)
//.def_static("basic_six_quads", &LinearCellComplexBuilder::basic_six_quads)
//.def_static("basic_modelf", &LinearCellComplexBuilder::basic_modelf);

#ifdef VERSION_INFO
m.attr("__version__") = MACRO_STRINGIFY(VERSION_INFO);
#else
m.attr("__version__") = "dev";
#endif
}
/*m.doc() = R"pbdoc(
        Pybind11 example plugin
        -----------------------
        .. currentmodule:: cmake_example
        .. autosummary::
           :toctree: _generate
           add
           subtract
    )pbdoc";

m.def("add", &add, R"pbdoc(
        Add two numbers
        Some other explanation about the add function.
    )pbdoc");

m.def("subtract", [](int i, int j) { return i - j; }, R"pbdoc(
        Subtract two numbers
        Some other explanation about the subtract function.
    )pbdoc");*/