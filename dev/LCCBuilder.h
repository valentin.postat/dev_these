//
// Created by v on 17/12/2020.
//

#ifndef LCCBUILDER_H
#define LCCBUILDER_H

#include "TypesForLCC.h"

class LinearCellComplexBuilder {

private:
    static void setup_ids(LCC_2 &lcc, std::size_t begin) {
        /*Setup all ids for the lcc*/
        std::size_t id = begin;
        for (LCC_2::Vertex_attribute_range::iterator
                     it = lcc.vertex_attributes().begin(),
                     itend = lcc.vertex_attributes().end();
             it != itend; ++it) {
            lcc.info_of_attribute<0>(it) = {id, 0};
            id++;
        }
    }
public:

    /** Create a grid with at the "location" (down left) with number of point "width"*"height"
    separeted by "w" (square) **/
    /*TODO*/
    static LCC_2 grid_like(std::vector<float> x, std::vector<float> y, std::vector <std::vector<std::size_t>> adj,
                           std::vector <std::size_t> constraints);

    /**For gridlike method*/
    static Dart_handle get_dart_from_int(Dart_handle dh, int pos, LCC_2 lcc);

    /// Unimplemented
    static LCC_2 from_file(std::istream &ais);

    /** Create a LCC_2 quad used to test features*/
    static LCC_2 basic(bool show_creation = false);

    /** Create a LCC_2 four quad sewed used to test features*/
    static LCC_2 basic_four_quads(bool show_creation = false);

    /** Create a LCC_2 six quad sewed used to test features*/
    static LCC_2 basic_six_quads(bool show_creation = false);

    /** Franck model to pillow left*/
    static LCC_2 basic_modelf(bool show_creation = false);

    /** Create a basic quad */
    static LCC_2 basic_quadv(bool show_creation = false);
};

#endif //LCCBUILDER_H
