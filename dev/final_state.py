# state file generated using paraview version 5.4.1

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [1003, 465]
renderView1.InteractionMode = '2D'
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.OrientationAxesVisibility = 0
renderView1.CenterOfRotation = [7.5, 10.0, 0.0]
renderView1.StereoType = 0
renderView1.CameraPosition = [7.5, 10.0, 48.2962913144534]
renderView1.CameraFocalPoint = [7.5, 10.0, 0.0]
renderView1.CameraParallelScale = 12.5
renderView1.Background = [1.0, 1.0, 1.0]

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

filename=['/home/v/Documents/These/dev_these/dev_these/dev/exec021_action17_on_1_b.vtk']
#filename = sys.argv[1]

# create a new 'Legacy VTK Reader'
exec000_action34_on_5_avtk = LegacyVTKReader(FileNames=filename)

# create a new 'Calculator'
calculator1 = Calculator(Input=exec000_action34_on_5_avtk)
calculator1.ResultArrayName = 'v2'
calculator1.Function = 'vectors'

# create a new 'Glyph'
glyph1 = Glyph(Input=calculator1,
    GlyphType='Sphere')
glyph1.Scalars = ['POINTS', 'scalars']
glyph1.Vectors = ['POINTS', 'vectors']
glyph1.Orient = 0
glyph1.ScaleFactor = 2.0
glyph1.GlyphMode = 'All Points'
glyph1.GlyphTransform = 'Transform2'

# init the 'Sphere' selected for 'GlyphType'
glyph1.GlyphType.Radius = 0.2
glyph1.GlyphType.ThetaResolution = 30
glyph1.GlyphType.PhiResolution = 30

# create a new 'Extract Edges'
extractEdges1 = ExtractEdges(Input=exec000_action34_on_5_avtk)

# create a new 'Tube'
tube1 = Tube(Input=extractEdges1)
tube1.Scalars = ['POINTS', 'scalars']
tube1.Vectors = ['POINTS', 'vectors']
tube1.Radius = 0.1

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get color transfer function/color map for 'GlyphScale'
glyphScaleLUT = GetColorTransferFunction('GlyphScale')
glyphScaleLUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 15.5, 0.865003, 0.865003, 0.865003, 31.0, 0.705882, 0.0156863, 0.14902]
glyphScaleLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'GlyphScale'
glyphScalePWF = GetOpacityTransferFunction('GlyphScale')
glyphScalePWF.Points = [0.0, 0.0, 0.5, 0.0, 31.0, 1.0, 0.5, 0.0]
glyphScalePWF.ScalarRangeInitialized = 1

# get color transfer function/color map for 'scalars'
scalarsLUT = GetColorTransferFunction('scalars')
scalarsLUT.RGBPoints = [0.0, 0.231373, 0.298039, 0.752941, 32.5, 0.865003, 0.865003, 0.865003, 65.0, 0.705882, 0.0156863, 0.14902]
scalarsLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'scalars'
scalarsPWF = GetOpacityTransferFunction('scalars')
scalarsPWF.Points = [0.0, 0.0, 0.5, 0.0, 65.0, 1.0, 0.5, 0.0]
scalarsPWF.ScalarRangeInitialized = 1

# get color transfer function/color map for 'vectors'
vectorsLUT = GetColorTransferFunction('vectors')
vectorsLUT.RGBPoints = [1.0, 0.231373, 0.298039, 0.752941, 1.0001220703125, 0.865003, 0.865003, 0.865003, 1.000244140625, 0.705882, 0.0156863, 0.14902]
vectorsLUT.ScalarRangeInitialized = 1.0

# get opacity transfer function/opacity map for 'vectors'
vectorsPWF = GetOpacityTransferFunction('vectors')
vectorsPWF.Points = [1.0, 0.0, 0.5, 0.0, 1.000244140625, 1.0, 0.5, 0.0]
vectorsPWF.ScalarRangeInitialized = 1

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from exec000_action34_on_5_avtk
exec000_action34_on_5_avtkDisplay = Show(exec000_action34_on_5_avtk, renderView1)
# trace defaults for the display properties.
exec000_action34_on_5_avtkDisplay.Representation = 'Surface'
exec000_action34_on_5_avtkDisplay.ColorArrayName = ['POINTS', '']
exec000_action34_on_5_avtkDisplay.OSPRayScaleArray = 'scalars'
exec000_action34_on_5_avtkDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
exec000_action34_on_5_avtkDisplay.SelectOrientationVectors = 'vectors'
exec000_action34_on_5_avtkDisplay.ScaleFactor = 2.0
exec000_action34_on_5_avtkDisplay.SelectScaleArray = 'scalars'
exec000_action34_on_5_avtkDisplay.GlyphType = 'Arrow'
exec000_action34_on_5_avtkDisplay.GlyphTableIndexArray = 'scalars'
exec000_action34_on_5_avtkDisplay.DataAxesGrid = 'GridAxesRepresentation'
exec000_action34_on_5_avtkDisplay.PolarAxes = 'PolarAxesRepresentation'
exec000_action34_on_5_avtkDisplay.ScalarOpacityUnitDistance = 12.5

# show data from calculator1
calculator1Display = Show(calculator1, renderView1)
# trace defaults for the display properties.
calculator1Display.Representation = 'Surface'
calculator1Display.ColorArrayName = ['POINTS', 'scalars']
calculator1Display.LookupTable = scalarsLUT
calculator1Display.OSPRayScaleArray = 'scalars'
calculator1Display.OSPRayScaleFunction = 'PiecewiseFunction'
calculator1Display.SelectOrientationVectors = 'None'
calculator1Display.ScaleFactor = 2.0
calculator1Display.SelectScaleArray = 'scalars'
calculator1Display.GlyphType = 'Arrow'
calculator1Display.GlyphTableIndexArray = 'scalars'
calculator1Display.DataAxesGrid = 'GridAxesRepresentation'
calculator1Display.PolarAxes = 'PolarAxesRepresentation'
calculator1Display.ScalarOpacityFunction = scalarsPWF
calculator1Display.ScalarOpacityUnitDistance = 12.5

# show data from extractEdges1
extractEdges1Display = Show(extractEdges1, renderView1)
# trace defaults for the display properties.
extractEdges1Display.Representation = 'Surface'
extractEdges1Display.ColorArrayName = ['POINTS', 'vectors']
extractEdges1Display.LookupTable = vectorsLUT
extractEdges1Display.OSPRayScaleArray = 'scalars'
extractEdges1Display.OSPRayScaleFunction = 'PiecewiseFunction'
extractEdges1Display.SelectOrientationVectors = 'vectors'
extractEdges1Display.ScaleFactor = 2.0
extractEdges1Display.SelectScaleArray = 'scalars'
extractEdges1Display.GlyphType = 'Arrow'
extractEdges1Display.GlyphTableIndexArray = 'scalars'
extractEdges1Display.DataAxesGrid = 'GridAxesRepresentation'
extractEdges1Display.PolarAxes = 'PolarAxesRepresentation'

# show data from tube1
tube1Display = Show(tube1, renderView1)
# trace defaults for the display properties.
tube1Display.Representation = 'Surface'
tube1Display.ColorArrayName = ['POINTS', '']
tube1Display.DiffuseColor = [0.0, 0.0, 0.0]
tube1Display.EdgeColor = [0.0, 0.0, 0.0]
tube1Display.OSPRayScaleArray = 'scalars'
tube1Display.OSPRayScaleFunction = 'PiecewiseFunction'
tube1Display.SelectOrientationVectors = 'vectors'
tube1Display.ScaleFactor = 2.0346409499645235
tube1Display.SelectScaleArray = 'scalars'
tube1Display.GlyphType = 'Arrow'
tube1Display.GlyphTableIndexArray = 'scalars'
tube1Display.DataAxesGrid = 'GridAxesRepresentation'
tube1Display.PolarAxes = 'PolarAxesRepresentation'

# show data from glyph1
glyph1Display = Show(glyph1, renderView1)
# trace defaults for the display properties.
glyph1Display.Representation = 'Surface'
glyph1Display.ColorArrayName = ['POINTS', 'vectors']
glyph1Display.LookupTable = vectorsLUT
glyph1Display.MapScalars = 0
glyph1Display.OSPRayScaleArray = 'GlyphScale'
glyph1Display.OSPRayScaleFunction = 'PiecewiseFunction'
glyph1Display.SelectOrientationVectors = 'GlyphVector'
glyph1Display.ScaleFactor = 2.040000076591969
glyph1Display.SelectScaleArray = 'GlyphScale'
glyph1Display.GlyphType = 'Arrow'
glyph1Display.GlyphTableIndexArray = 'GlyphScale'
glyph1Display.DataAxesGrid = 'GridAxesRepresentation'
glyph1Display.PolarAxes = 'PolarAxesRepresentation'

# setup the color legend parameters for each legend in this view

# get color legend/bar for scalarsLUT in view renderView1
scalarsLUTColorBar = GetScalarBar(scalarsLUT, renderView1)
scalarsLUTColorBar.Title = 'scalars'
scalarsLUTColorBar.ComponentTitle = ''

# get color legend/bar for glyphScaleLUT in view renderView1
glyphScaleLUTColorBar = GetScalarBar(glyphScaleLUT, renderView1)
glyphScaleLUTColorBar.Title = 'GlyphScale'
glyphScaleLUTColorBar.ComponentTitle = ''

# get color legend/bar for vectorsLUT in view renderView1
vectorsLUTColorBar = GetScalarBar(vectorsLUT, renderView1)
vectorsLUTColorBar.WindowLocation = 'AnyLocation'
vectorsLUTColorBar.Position = [0.8305084745762711, 0.09247311827956987]
vectorsLUTColorBar.Title = 'vectors'
vectorsLUTColorBar.ComponentTitle = 'Magnitude'

# ----------------------------------------------------------------
# finally, restore active source
SetActiveSource(exec000_action34_on_5_avtk)
# ----------------------------------------------------------------

# Save all views in a tab
layout = GetLayout()
SaveScreenshot("allviews.png", layout)

# To save a specific target resolution, rather than using the
# the current view (or layout) size, and override the color palette.
SaveScreenshot("aviewResolution.png", renderView1,ImageResolution=[1500, 1500],OverrideColorPalette="Black Background")

