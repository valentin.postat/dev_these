import numpy as np
import random
import json
from ast import literal_eval
import math
import pandas
import itertools



class Q_learning_pol:

    def __init__(self, states_n, actions_n, lr=0.85, y=0.99):
        """
        :param states_n: number of states (borne sup)
        :param actions_n: number of actions ( exact )
        :param feasible_actions:
        :param lr: learning rate
        :param y: discount factor
        """
        self.Q = {}
        self.states_n = states_n
        self.actions_n = actions_n
        self.lr = lr
        self.y = y
        self.inserted_n = 0  # element inserted in out map
        self.inserted_counter = 0
        self.save = "wheightsave.json"


    def load(self,filename=""):
        if len(filename) == 0:
            filename = self.save
        to_r = {}
        f = open(self.save,'r')
        to_w = {}
        data = f.read()
        to_w = literal_eval(data)
        for k,v in to_w.items():
            self.Q[k] = np.array(v)
        f.close()

    def write(self):
        f = open(self.save, 'w')
        to_w = {}
        for k,v in self.Q.items():
            to_w[k] = list(v)
        f.write(str(to_w))
        f.close()

    def show(self):
        to_s = []
        print("")
        for k, v in self.Q.items():
            print(np.frombuffer(k, dtype=int),list(v))
            l = [float(f'{x:.1f}') for x in list(v)]
            to_s.append([np.frombuffer(k, dtype=int)]+l)
        print(pandas.DataFrame(to_s,columns=["obs","c-","c+","nc"]))

    def update(self, reward, observation, action, observation_next):
        """
        :param reward:
        :param observation:
        :param action:
        :param observation_next:
        :return:
        """
        print(observation, action, observation_next, reward)
        obs = np.array(observation).tobytes()
        obs_next = np.array(observation_next).tobytes()
        old_value = self.Q[obs]
        self.insert_observation(observation_next)
        self.Q[obs][action] = self.Q[obs][action] + self.lr * (
                reward + self.y * np.max(self.Q[obs_next][:]) - self.Q[obs_next][action])
        new_value = self.Q[obs][action]
        print("Q[" + str(obs) + "," + str(action) + "]=", old_value, " updated to ", new_value)

    def set(self, observation, Reward):
        obs = observation.tobytes()
        if obs not in self.Q:
            line = np.full((self.actions_n), float(Reward))
            self.Q[obs] = line
            self.inserted_counter += 1

    def insert_observation(self, observation):
        """
        :param observation:
        :return: True if in the map ; False
        """
        diff = self.states_n - self.inserted_counter + 1
        if diff < 0:
            raise Exception('Difference between #states and #inserted in Q-learning algorithm'.format(diff))

        obs = np.array(observation).tobytes()
        if obs not in self.Q:
            line = np.zeros((self.actions_n), float)
            self.Q[obs] = line
            self.inserted_counter += 1
            return True
        return False

    def reset(self):
        pass

    def decide(self, i, observation,exploit_only=False):
        """
        :param observation:
        :return:
        """
        inserted = self.insert_observation(observation)
        rand = random.random()
        obs = np.array(observation).tobytes()
        obsint = np.array(observation, dtype=float)
        if rand < 80 or exploit_only:  # Exploit
            self.Q2 = (self.Q[obs] + np.random.normal(100, 10, self.actions_n) / 100 * (1. / (i + 1)))
        else:  # Explore
            self.Q2 = np.random.normal(100, 10, self.actions_n) / 100 * (1. / (i + 1))
        # * feasible_actions[s]
        self.Q2 *= obsint
        print(self.Q2)
        print("a", np.argmax(self.Q2))
        return np.argmax(self.Q2)

    def decide_dep(self, i, observation,symb):
        """
        :param observation:
        :return:
        """
        inserted = self.insert_observation(observation)
        rand = random.random()
        obsint = np.array(observation,dtype=float)
        obs = np.array(observation).tobytes()
        assert self.actions_n > symb
        feasible = np.concatenate((np.ones(symb), np.zeros(self.actions_n-symb)))
        print("feasible", feasible)
        if rand < 80:  # Exploit
            self.Q2 = (self.Q[obs] + np.random.normal(100, 10, self.actions_n) / 100 * (1. / (i + 1)))
        else:  # Explore
            self.Q2 = np.random.normal(100, 10, self.actions_n) / 100 * (1. / (i + 1))
        # * feasible_actions[s]
        self.Q2 *= feasible
        print(self.Q2)
        print("a", np.argmax(self.Q2))
        return np.argmax(self.Q2)
