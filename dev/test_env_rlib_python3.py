#!/usr/bin/env python3.8
# encoding: utf-8

# Notre environnement custom sur carte généralisée

import pybinding as env

import argparse
import gym
from gym.spaces import Discrete, Box
import numpy as np
import os
import random

import argparse
import gym
from gym.spaces import Discrete, Box
import numpy as np
import os
import random

import ray
from ray import tune
from ray.tune import grid_search
from ray.rllib.env.env_context import EnvContext
from ray.rllib.models import ModelCatalog
from ray.rllib.models.tf.tf_modelv2 import TFModelV2
from ray.rllib.models.tf.fcnet import FullyConnectedNetwork
from ray.rllib.models.torch.torch_modelv2 import TorchModelV2
from ray.rllib.models.torch.fcnet import FullyConnectedNetwork as TorchFC
from ray.rllib.utils.framework import try_import_tf, try_import_torch
from ray.rllib.utils.test_utils import check_learning_achieved
import random
import BlockStruct


class SimpleCorridor(gym.Env):
    """Example of a custom env in which you have to walk down a corridor.
    You can configure the length of the corridor via the env config."""

    def __init__(self, config: EnvContext):
        self.end_pos = config["corridor_length"]
        self.cur_pos = 0
        self.action_space = Discrete(2)
        self.observation_space = Box(
            0.0, self.end_pos, shape=(1,), dtype=np.float32)
        # Set the seed. This is only used for the final (reach goal) reward.
        self.seed(config.worker_index * config.num_workers)

    def reset(self):
        self.cur_pos = 0
        return [self.cur_pos]

    def step(self, action):
        assert action in [0, 1], action
        if action == 0 and self.cur_pos > 0:
            self.cur_pos -= 1
        elif action == 1:
            self.cur_pos += 1
        done = self.cur_pos >= self.end_pos
        # Produce a random reward when we reach the goal.
        return [self.cur_pos], \
               random.random() * 2 if done else -0.1, done, {}

    def seed(self, seed=None):
        random.seed(seed)


class MySingleAgentEnv(gym.Env):

    def __init__(self, config: EnvContext):
        # self.my_env = env.LCC_HL()
        # self.my_env.set_draw(False)
        # ids = e.get_all_vertex_ids()
        # self.action_space = Discrete(v.size)
        self.observation_space = Box(0.0, self.end_pos, shape=(1,), dtype=np.float32)
        # Set the seed. This is only used for the final (reach goal) reward.
        self.seed(config.worker_index * config.num_workers)

    def reset(self):
        self.cur_pos = 0
        return [self.cur_pos]

    def step(self, action):
        assert action in [0, 1], action
        if action == 0 and self.cur_pos > 0:
            # pillowing
            print("pillowing")
        elif action == 1:
            print("do_nothing")
        done = self.cur_pos >= self.end_pos
        # Produce a random reward when we reach the goal.
        return [self.cur_pos], \
               random.random() * 2 if done else -0.1, done, {}

    def seed(self, seed=None):
        random.seed(seed)


class MyMultipleAgentEnv(gym.Env):
    # https://github.com/DerwenAI/gym_example/blob/master/gym-example/gym_example/envs/example_env.py
    # possible actions
    PILLOWING = 0
    DO_NOTHING = 1

    # possible positions (state)
    # LF_MIN = 1
    # RT_MAX = 10
    CHANGE = 0
    STAY = 1

    # land on the GOAL position within MAX_STEPS steps
    MAX_STEPS = 10

    # possible rewards
    REWARD_SUCCESS = 1
    REWARD_FAILURE = 0

    def __init__(self, config: EnvContext):
        # the action space ranges [0, 1] where:
        #  `0` pillowing
        #  `1` do_nothing
        self.action_space = gym.spaces.Discrete(2)
        # NB: Ray throws exceptions for any `0` value Discrete
        # observations so we'll make position a 1's based value
        self.observation_space = gym.spaces.Discrete(1)

    def reset(self):

        self.count = 0
        self.state = random.choice([self.CHANGE, self.STAY])
        self.reward = 0
        self.done = False
        self.info = {}
        return [self.self.state]

    def step(self, action):
        assert action in [0, 1], action

        if action == self.PILLOWING and self.cur_pos > 0:
            print("pillowing")
            if (self.state == self.CHANGE):
                self.reward = 1
            else:
                self.reward = 0
        elif action == self.DO_NOTHING:
            print("do_nothing")
            if (self.state == self.STAY):
                self.reward = 1
            else:
                self.reward = 0

        self.done = True
        try:
            assert self.observation_space.contains(self.state)
        except AssertionError:
            print("INVALID STATE", self.state)

        return [self.state, self.reward, self.done, self.info]

    def seed(self, seed=None):
        random.seed(seed)


if __name__ == "__main__":
    e = env.LCC_HL()
    v = e.get_all_vertex_ids()
    show_env_state(e, False)

    pil = [(1, [0, 3, 2, 1]), (6, [2, 1, 5, 6],), (7, [4, 1, 5, 7])]
    l = 0.2
    path = [0, 1, 2, 3]
    # e.pillowing([0, 1, 2, 3], 7, l, 1)
    q = e.get_all_quads()
    s_q = split_quad(q)
    print(s_q)
    # for i in range(0, len(pil)):
    #    e.pillowing(pil[i][1], pil[i][0], 0.2, 1)
    nc0 = [1, 0, 3]
    nc0 = [0, 1]
    c0 = 2
    c0 = 3
    nc1 = [1, 4, 7]
    nc1 = [1, 4]
    c1 = 5
    e.pillowing(nc0, c0, 0.2, 1)
    e.set_draw(1)
    e.draw()
    e.pillowing(nc1, c1, 0.2, 1)
    e.draw()
    # e.make_conforme([9, 1, 11], 2)
    # e.make_conforme([9, 1, 10], 2)
    e.make_conforme([10, 1, 9], 5)
    # e.pillowing([10,2,6,5,12],1,0.2,0)
    e.set_draw(True)
    e.draw()
    e.reset()
    e.pillowing([0, 1, 2, 6, 5], 3, 0.2, 0)
