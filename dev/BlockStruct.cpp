//
// Created by v on 07/06/2021.
//
#include "BlockStruct.h"

///std::vector <std::size_t> BlockStruct::incident_vertex(Vertex_id id_vtx){}

///std::vector <std::size_t> BlockStruct::incident_vertex(Cell_id id_cell){}

///std::vector <std::size_t> BlockStruct::incident_cell(Vertex_id id_vtx){}

///std::vector <std::size_t> BlockStruct::incident_cell(Cell_id id_cell){}



std::vector<std::size_t> BlockStruct::get_cell(Cell_id id_cell){
    std::vector<std::size_t> v;
    v.push_back(id_cell);
    auto v_id_vertex = lcc_hl.get_vertex_of_cell(id_cell);
    v.push_back(v_id_vertex.size());
    v.insert(v.end(),v_id_vertex.begin(),v_id_vertex.end());
    for(auto it = v_id_vertex.begin();it!= v_id_vertex.end();++it){
        v.push_back(lcc_hl.get_constraint(*it));
    }
    return v;
}