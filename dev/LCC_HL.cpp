//
// Created by v on 05/02/2021.
//

#include "LCC_HL.h"

/* Just for connected_componants() func*/
#include <boost/config.hpp>
#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/connected_components.hpp>
#include <boost/graph/graphviz.hpp>
//json
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

// Short alias for this namespace
namespace pt = boost::property_tree;

//
#include <jsoncpp/json/json.h>
//#include <json/json.h>
#include <fstream>

/** Cells operations c **/
std::size_t LCC_HL::split_edge(std::size_t id_vtx_a, std::size_t id_vtx_b) {
    std::size_t id_new_vtx;
    Dart_handle d = darts_of_edge(id_vtx_a, id_vtx_b), dnew;
    //lcc.set_automatic_attributes_management(true);
    Point dist = lcc.point_of_vertex_attribute(lcc.vertex_attribute(lcc.alpha<0>(d)));
    Point p = lcc.point_of_vertex_attribute(lcc.attribute<0>(d));
    dnew = lcc.insert_cell_0_in_cell_1(d);
    Point new_p((dist.x() + p.x()) / 2, (dist.y() + p.y()) / 2);
    //lcc.set_automatic_attributes_management(true);
    Vertex_attribute_handle vh = create_vertex_handle(new_p);
    lcc.set_vertex_attribute(dnew, vh);

    id_new_vtx = lcc.info_of_attribute<0>(vh)[0];
    set_field(id_new_vtx, 3, 1);
    //lcc.correct_invalid_attributes();
    return lcc.info_of_attribute<0>(vh)[0];
}

/** A partir d'une face_id retourne une représentation unique de cette face */
std::vector <std::string> LCC_HL::representation_unique(std::size_t face_id) {
    std::size_t nb_sommet = get_vertex_of_cell(face_id).size();
    /*
    if(before != after){
     return std::vector(std::string)({})
     }
     */
    std::string rp_conf, rp_crit;
    std::cout << "representation_unique face:" << face_id << std::endl;
    if (nb_sommet == 4) {
        rp_crit = representation_criticite_unique(face_id); // deplacement brin
        // id
        rp_conf = "0000"; // ou [0]
    } else {
        rp_conf = representation_conformite_unique(face_id); // deplacement brin
        // id
        rp_crit = representation_criticite(face_id); //
    }
    return std::vector<std::string>({rp_crit, rp_conf});
}

/** A partir d'une face_id retourne la représentation de conformité*/
std::string LCC_HL::representation_conformite(std::size_t face_id) {
    LCC_2::Attribute_handle<2>::type at = this->map_cell[face_id];
    Dart_handle dh = lcc.dart_of_attribute<2>(at), dtmp;
    std::string conformity;
    dtmp = lcc.alpha<0, 1>(dh);
    conformity += (lcc.is_marked(dh, conformity_mark)) ? "1" : "0";
    while (dh != dtmp) {
        conformity += (lcc.is_marked(dtmp, conformity_mark)) ? "1" : "0";
        dtmp = lcc.alpha<0, 1>(dtmp);
    }
    std::cout << "conformity " << conformity << std::endl;
    return conformity;
}

std::string LCC_HL::representation_criticite(std::size_t face_id) {
    LCC_2::Attribute_handle<2>::type at = this->map_cell[face_id];
    Dart_handle dh = lcc.dart_of_attribute<2>(at), dtmp;
    std::string criticity = "";
    dtmp = dh;
    std::size_t constraint;
    do{
        if (!lcc.is_marked(dtmp, conformity_mark)) {
            constraint = get_constraint(dtmp);
            // Pas de contrainte 2 si on ne peut pas traiter
            if(constraint == criticity::diminution && (lcc.is_free<2>(dtmp) || lcc.is_free<2>(lcc.alpha<1>(dtmp))))
                constraint = 0;
            criticity += std::to_string(constraint);
        }
        //std::cout << criticity << std::endl;
        dtmp = lcc.alpha<0, 1>(dtmp);
    } while (dh != dtmp);
    return criticity;
}

/** A partir d'une face_id retourne la représentation de criticité unique*/
std::string LCC_HL::representation_criticite_unique(std::size_t face_id) {
    std::string repr_before = representation_criticite(face_id), repr_after;
    Map_value mv = map_unique_crit[repr_before];
    place_from_nc_map(face_id, mv.permutation, mv.reverse);
    repr_after = representation_criticite(face_id);
    if( mv.true_word != repr_after )
        return "";
    JSON_unique ju = map_unique_crit_index[repr_after];
    best_candidate(face_id, get_vertex_of_cell(face_id),ju.candidat,ju.neighbor,ju.reverse);
    std::cout << "bef " << repr_before << " true_word " << mv.true_word << " af " << repr_after << " per "
              << mv.permutation << " rev " << mv.reverse << std::endl;
    //assert(mv.true_word == repr_after);
    return repr_after;
}

/** A partir d'une face_id retourne la représentation de conformité unique*/
std::string LCC_HL::representation_conformite_unique(std::size_t face_id) {
    std::string repr_before = representation_conformite(face_id), repr_after;
    std::cout << "start repr " << repr_before << std::endl;
    Map_value mv = map_unique_conf[repr_before];
    place_from_nc_map(face_id, mv.permutation, mv.reverse);
    repr_after = representation_conformite(face_id);
    if( mv.true_word != repr_after )
        return "";
    JSON_unique ju = map_unique_conf_index[repr_after];
    best_candidate(face_id, get_vertex_of_cell(face_id),ju.candidat,ju.neighbor,ju.reverse);
    std::cout << "bef " << mv.true_word << " af " << repr_after << " per " << mv.permutation << " rev " << mv.reverse
              << std::endl;
    //assert(mv.true_word == repr_after);
    if( mv.true_word != repr_after ){
        repr_after = "";
    }
    return repr_after;
}

std::string LCC_HL::rpz(std::size_t face_id) {
    LCC_2::Attribute_handle<2>::type at = this->map_cell[face_id];
    Dart_handle dh = lcc.dart_of_attribute<2>(at), dtmp;
    std::string vtx_ids, conformity, other;
    std::vector <std::size_t> vertex_ids = get_vertex_of_cell(face_id);
    for (std::vector<std::size_t>::iterator it = vertex_ids.begin(); it != vertex_ids.end(); ++it) {
        vtx_ids += std::to_string(*it);
        //conformity += std::to_string(get_field(*it, 3));
    }
    dtmp = lcc.alpha<0, 1>(dh);
    other += std::to_string(lcc.info<0>(dh)[0]);
    conformity += (lcc.is_marked(dh, conformity_mark)) ? "1" : "0";
    while (dh != dtmp) {
        other += std::to_string(lcc.info<0>(dtmp)[0]);
        conformity += (lcc.is_marked(dtmp, conformity_mark)) ? "1" : "0";
        dtmp = lcc.alpha<0, 1>(dtmp);
    }

    std::cout << vtx_ids << std::endl;
    std::cout << conformity << std::endl;
    std::cout << "other" << other << std::endl;
    std::cout << lcc.info<2>(dh)[0] << std::endl;
    return conformity;
}


std::string LCC_HL::rpz_crit(std::size_t face_id) {
    LCC_2::Attribute_handle<2>::type at = this->map_cell[face_id];
    Dart_handle dh = lcc.dart_of_attribute<2>(at), dtmp;
    std::string vtx_ids, conformity, other;
    std::vector <std::size_t> vertex_ids = get_vertex_of_cell(face_id);
    for (std::vector<std::size_t>::iterator it = vertex_ids.begin(); it != vertex_ids.end(); ++it) {
        vtx_ids += std::to_string(*it);
        conformity += std::to_string(get_field(*it, 1));
    }
    dtmp = lcc.alpha<0, 1>(dh);
    other += std::to_string(lcc.info<0>(dh)[0]);
    while (dh != dtmp) {
        other += std::to_string(lcc.info<0>(dtmp)[0]);
        dtmp = lcc.alpha<0, 1>(dtmp);
    }

    std::cout << vtx_ids << std::endl;
    std::cout << conformity << std::endl;
    std::cout << "other" << other << std::endl;
    std::cout << lcc.info<2>(dh)[0] << std::endl;
    return conformity;
}


void LCC_HL::load_maps() {
    // Create a root
    pt::ptree root;
    // Load the json file in this ptree
    pt::read_json("map_conformity.json", root);
    for (auto pair : root) {
        Map_value mpval;
        mpval.true_word = pair.second.get<std::string>("true_word", "");
        mpval.permutation = pair.second.get<std::size_t>("nb_permut", 0);
        mpval.reverse = pair.second.get<bool>("reverse", true);
        map_unique_conf.insert(std::pair<std::string, Map_value>(pair.first, mpval));
    }
    pt::read_json("map_criticity.json", root);
    for (auto pair : root) {
        Map_value mpval;
        mpval.true_word = pair.second.get<std::string>("true_word", "");
        mpval.permutation = pair.second.get<std::size_t>("nb_permut", 0);
        mpval.reverse = pair.second.get<std::size_t>("reverse", true);
        map_unique_crit.insert(std::pair<std::string, Map_value>(pair.first, mpval));
    }
    pt::read_json("unique_cell_crit.json", root);
    for (auto pair : root) {
        JSON_unique mpval;
        BOOST_FOREACH(boost::property_tree::ptree::value_type &v, pair.second.get_child("candidats"))
        {
            std::cout << v.second.data() << std::endl;
            mpval.candidat.push_back(v.second.get_value<std::size_t>());
        }
        BOOST_FOREACH(boost::property_tree::ptree::value_type &v, pair.second.get_child("neighbor"))
        {
            std::cout << v.second.data() << std::endl;
            mpval.neighbor.push_back(v.second.get_value<std::size_t>());
        }
        BOOST_FOREACH(boost::property_tree::ptree::value_type &v, pair.second.get_child("reverse"))
        {
            std::cout << v.second.data() << std::endl;
            mpval.reverse.push_back(v.second.get_value<bool>());
        }
        map_unique_crit_index.insert(std::pair<std::string, JSON_unique>(pair.first, mpval));
    }
    pt::read_json("unique_cell_nc.json", root);
    for (auto pair : root) {
        JSON_unique mpval;
        BOOST_FOREACH(boost::property_tree::ptree::value_type &v, pair.second.get_child("candidats"))
        {
            std::cout << v.second.data() << std::endl;
            mpval.candidat.push_back(v.second.get_value<std::size_t>());
        }
        BOOST_FOREACH(boost::property_tree::ptree::value_type &v, pair.second.get_child("neighbor"))
        {
            std::cout << v.second.data() << std::endl;
            mpval.neighbor.push_back(v.second.get_value<std::size_t>());
        }
        BOOST_FOREACH(boost::property_tree::ptree::value_type &v, pair.second.get_child("reverse"))
        {
            std::cout << v.second.data() << std::endl;
            mpval.reverse.push_back(v.second.get_value<bool>());
        }
        map_unique_conf_index.insert(std::pair<std::string, JSON_unique>(pair.first, mpval));
    }
    /*
    std::cout << "Map_crit" << std::endl;
    for (std::map<std::string, Map_value>::const_iterator it = map_unique_crit.begin();
         it != map_unique_crit.end(); ++it) {
        std::cout << it->first << " " << it->second.true_word << " " << it->second.permutation << "\n";
    }
    std::cout << "Map_conf" << std::endl;
    for (std::map<std::string, Map_value>::const_iterator it = map_unique_conf.begin();
         it != map_unique_conf.end(); ++it) {
        std::cout << it->first << " " << it->second.true_word << " " << it->second.permutation << "\n";
    }
    */

}

void LCC_HL::set_vertex_of_cell(std::size_t face_id, std::size_t vertex_id) {
    LCC_2::Attribute_handle<2>::type at = this->map_cell[face_id];
    Dart_handle dh = lcc.dart_of_attribute<2>(at), dtmp;
    dtmp = lcc.alpha<0, 1>(dh);
    if (lcc.info<0>(dh)[0] != vertex_id) {
        bool run = true;
        while ((dh != dtmp) && run) {
            if (lcc.info<0>(dtmp)[0] == vertex_id) {
                run = false;
                lcc.set_dart_of_attribute<2>(at, dtmp);
            } else {
                dtmp = lcc.alpha<0, 1>(dtmp);
            }
        }
    }
}

void LCC_HL::place_from_nc_map(std::size_t face_id, std::size_t permutation, bool reverse) {
    LCC_2::Attribute_handle<2>::type at = this->map_cell[face_id];
    Dart_handle dh = lcc.dart_of_attribute<2>(at);
    if (reverse) {
        dh = lcc.alpha<1, 0, 1>(dh);
    } else {
        dh = lcc.alpha<1>(dh);
    }
    while (permutation > 0) {
        dh = lcc.alpha<0, 1>(dh);
        permutation--;
    }
    if (!reverse) {
        dh = lcc.alpha<1>(dh);
    }
    lcc.set_dart_of_attribute<2>(at, dh);
}

void set_vertex_of_cell_of_attr(std::size_t face_id, std::size_t vertex_id) {

}

std::size_t LCC_HL::split_cell_d(std::size_t face_id, std::size_t vtx_a, std::size_t vtx_b) {
    auto v = get_vertex_of_cell(face_id);
    for (auto it = v.begin(); it != v.end(); ++it) {
        std::cout << *it << " ";
    }
    std::cout << std::endl;
    assert(std::find(v.begin(), v.end(), vtx_a) != v.end());
    assert(std::find(v.begin(), v.end(), vtx_b) != v.end());
    Dart_handle da = dart_of_vertex_id(vtx_a), db, mem;

    bool cell_found = false;
    /// Dart of the "vtx_a" Attr in cell "face_id"
    for (LCC_2::Dart_of_orbit_range<1, 2>::iterator it(lcc.darts_of_orbit<1, 2>(da).begin()), itend(
            lcc.darts_of_orbit<1, 2>(da).end()); it != itend; ++it) {
        if (lcc.info<2>(it)[0] == face_id) {
            da = it;
            cell_found = true;
            break;
        }
    }
    assert(cell_found);
    bool vtx_found = false;
    /// Dart of the "vtx_b" in the same cell "face_id"
    for (LCC_2::Dart_of_orbit_range<0, 1>::iterator it(lcc.darts_of_orbit<0, 1>(da).begin()), itend(
            lcc.darts_of_orbit<0, 1>(da).end()); it != itend; ++it) {
        if (lcc.info<0>(it)[0] == vtx_b) {
            db = it;
            vtx_found = true;
            break;
        }
    }
    assert(vtx_found);
    mem = lcc.alpha<1, 0>(da);
    LCC_2::Attribute_handle<2>::type fa = create_face_handle();
    std::cout << "Dist " << lcc.info<0>(lcc.alpha<0>(da))[0] << " " << lcc.info<0>(lcc.alpha<0>(db))[0] << "attr "
              << lcc.info_of_attribute<2>(fa)[0] << std::endl;

    std::cout << "2-Dist " << lcc.info<2>(da)[0] << " " << lcc.info<2>(db)[0] << std::endl;

    //lcc.set_automatic_attributes_management(true);
    lcc.insert_cell_1_in_cell_2(da, db);
    lcc.set_attribute<2>(lcc.alpha<1, 2>(da), fa);

    //lcc.set_automatic_attributes_management(false);
    return lcc.info_of_attribute<2>(fa)[0];
}

std::size_t LCC_HL::split_cell(std::size_t vtx_a, std::size_t vtx_b) {
    Dart_handle dh = dart_of_vertex_id(vtx_a), dtmp, da;
    bool run = true;
    for (LCC_2::One_dart_per_incident_cell_range<2, 0>::iterator it = lcc.one_dart_per_incident_cell<2, 0>(
            dh).begin(), itend = lcc.one_dart_per_incident_cell<2, 0>(dh).end(); (it != itend) && run; ++it) {
        dtmp = it;
        da = it;
        do {
            if (lcc.info<0>(dtmp)[0] == vtx_b) {
                std::cout << lcc.info<0>(dtmp)[0] << std::endl;
                run = false;
            } else {
                dtmp = lcc.alpha<0, 1>(dtmp);
            }
        } while (it != dtmp && run);
    }

    if (run) {
        std::cout << "error vtx " << vtx_a << " " << vtx_b << std::endl;
        return 0;
    }
    LCC_2::Attribute_handle<2>::type fa, fb;
    fa = create_face_handle();
    //lcc.set_automatic_attributes_management(true);
    fb = lcc.attribute<2>(da);
    Dart_handle dnew;
    if (lcc.is_insertable_cell_1_in_cell_2(da, lcc.alpha<1>(dtmp))) {
        dnew = lcc.insert_cell_1_in_cell_2(da, lcc.alpha<1>(dtmp));
        lcc.set_attribute<2>(lcc.alpha<2>(dnew), fa);
        lcc.set_attribute<2>(dnew, fb);
    }
    return fa->info()[0];
}

///


std::vector <std::size_t> LCC_HL::get_non_conform_vertex(std::size_t face_id) {
    std::vector <std::size_t> res;
    LCC_2::Attribute_handle<2>::type face = this->map_cell[face_id];
    Dart_handle dh = lcc.dart_of_attribute<2>(face),dtmp = dh;
    do{
        if( lcc.is_marked(dtmp,conformity_mark) ) {
            res.push_back(true);
        }else {
            res.push_back(false);
        }
        dtmp = lcc.alpha<0,1>(dtmp);
    } while (dtmp != dh);
    return res;
}

std::vector <std::size_t> LCC_HL::get_crit_vertex(std::size_t face_id) {
    std::vector <std::size_t> res;
    std::vector <std::size_t> vertex = get_vertex_of_cell(face_id);
    for (auto it = vertex.begin(); it != vertex.end(); ++it) {
        if (get_field(*it, 1)) {
            res.push_back(*it);
        }
    }
    return res;
}


//! Conforme
void LCC_HL::c_1(std::size_t face_id) {
    std::vector <std::size_t> vertex = get_vertex_of_cell(face_id);
    assert(vertex.size() == 4);
}

std::vector <std::size_t> LCC_HL::c_2(std::size_t face_id) {
    std::vector <std::size_t> vertex = get_vertex_of_cell(face_id);
    assert(vertex.size() == 4);
    std::size_t prev, cur, next, side_edg;
    bool crit = false;
    //std::vector<std::size_t> p;
    LCC_2::Attribute_handle<2>::type at_2 = this->map_cell[face_id];
    //at_2
    std::cout << "Here" << std::endl;
    Dart_handle dh = lcc.dart_of_attribute<2>(at_2);
    std::cout << "Here " << get_id(dh) << std::endl;
    for (LCC_2::Dart_of_cell_range<2>::iterator it =
            lcc.darts_of_cell<2>(dh).begin(),
                 itend = lcc.darts_of_cell<2>(dh).end(); it != itend; ++it) {
        if (get_constraint(it)) {
            crit = true;
            cur = get_id(it);
            prev = get_id(lcc.alpha<0>(it));
            next = get_id(lcc.alpha<1, 0>(it));
            side_edg = get_id(lcc.alpha<0, 1, 0>(it));
            break;
        }
    }
    std::vector <std::size_t> p = {prev, cur, next};
    std::cout << "c_1: " << prev << " " << cur << " " << next << std::endl;
    std::vector <std::size_t> v;
    if (crit) {
        v = pillowing(p, side_edg, 0.4, true);
        set_field(v[0], 3, 0);
        set_field(v[1], 3, !in_boundary(v[1]));
        set_field(v[2], 3, !in_boundary(v[2]));
    }
    return v;
}

void LCC_HL::s_1(std::size_t face_id, std::size_t offset) {
    std::vector <std::size_t> vertex = get_vertex_of_cell(face_id), vertex_offset;
    assert(vertex.size() == 4);
    std::size_t side_edg;
    offset = offset % vertex.size();
    vertex_offset.insert(vertex_offset.begin(), vertex.begin() + offset, vertex.end());
    vertex_offset.insert(vertex_offset.end(), vertex.begin(), vertex.begin() + offset);
    bool crit = false;
    vertex = vertex_offset;
    LCC_2::Attribute_handle<2>::type at_2 = this->map_cell[face_id];
    Dart_handle dh = lcc.dart_of_attribute<2>(at_2);
    std::vector <std::size_t> p = {vertex[0], vertex[1], vertex[2]};
    side_edg = vertex[3];
    std::vector <std::size_t> v_new;
    v_new = pillowing(p, side_edg, 0.4, true);
    update_nc(v_new[1]);
    update_nc(v_new[2]);
    mark_pillowing(vertex[0]);
    mark_pillowing(vertex[2]);
}

void LCC_HL::s_2(std::size_t face_id, std::size_t offset) {
    std::vector <std::size_t> vertex = get_vertex_of_cell(face_id), vertex_offset;
    assert(vertex.size() == 4);
    std::size_t side_edg;
    offset = offset % vertex.size();
    vertex_offset.insert(vertex_offset.begin(), vertex.begin() + offset, vertex.end());
    vertex_offset.insert(vertex_offset.end(), vertex.begin(), vertex.begin() + offset);
    bool crit = false;
    vertex = vertex_offset;
    LCC_2::Attribute_handle<2>::type at_2 = this->map_cell[face_id];
    Dart_handle dh = lcc.dart_of_attribute<2>(at_2);
    std::vector <std::size_t> p = {vertex[0], vertex[1], vertex[2], vertex[3]};
    side_edg = vertex[3];
    std::vector <std::size_t> v_new;
    v_new = pillowing(p, side_edg, 0.4, true);
    update_nc(v_new[0]);
    update_nc(v_new[1]);
    update_nc(v_new[2]);
    update_nc(v_new[3]);
}




Point LCC_HL::compute_point(Point p1, Point p2, float alpha) {
    assert(0.0 <= alpha <= 1.0);
    // vec directeur
    Point u(p2.x() - p1.x(), p2.y() - p1.y());
    return Point(p1.x() + u.x() * alpha, p1.y() + u.y() * alpha);
}

Point LCC_HL::compute_face_midle(Dart_handle d) {
    Point p(0.0, 0.0);
    Dart_handle dtmp = d;
    int count = 0;
    do {
        count++;
        p = Point(p.x() + lcc.point(dtmp).x(), p.y() + lcc.point(dtmp).y());
        dtmp = lcc.alpha<0, 1>(dtmp);
    } while (d != dtmp);
    Point res(p.x() / count, p.y() / count);
    return res;
}

void LCC_HL::s_3(std::size_t face_id, std::size_t offset) {
    //lcc.correct_invalid_attributes();
    assert(lcc.is_valid());
    std::cout << "s_3:begin on face " << face_id <<  std::endl;
    Dart_handle d = lcc.dart_of_attribute<2>(map_cell[face_id]);
    std::vector <std::size_t> vertex = get_vertex_of_cell(face_id);
    offset = offset % vertex.size();
    while (offset != 0) {
        d = lcc.alpha<0, 1>(d);
        offset--;
    }
    std::cout << "starter dart <x> " << lcc.info<0>(d)[0] << " <0> " << lcc.info<0>(lcc.alpha<0>(d))[0] << " <2> "
              << lcc.info<2>(d)[0] << std::endl;

    Dart_handle dn1, dn2;
    Dart_handle do1, do2, do3, do4;
    // old darts
    do1 = d;
    do2 = lcc.alpha<0, 1>(do1);
    do3 = lcc.alpha<0, 1>(do2);
    do4 = lcc.alpha<0, 1>(do3);
    Dart_handle dx = lcc.alpha<0, 1>(d);

    LCC_2::Vertex_attribute_handle vt_pa = lcc.vertex_attribute(do1), vt_pb = lcc.vertex_attribute(
            do2), vt_pc = lcc.vertex_attribute(do3), vt_pd = lcc.vertex_attribute(do4);

    LCC_2::Attribute_handle<2>::type at0 = create_face_handle(), at1 = create_face_handle(), at2 = create_face_handle();
    LCC_2::Attribute_handle<2>::type at3 = create_face_handle(), at4 = create_face_handle(), at5 = create_face_handle();
    LCC_2::Vertex_attribute_handle vt1, vt2, vt3, vt4, vt5, vt6;

    vt1 = create_vertex_handle(
            compute_point(lcc.point_of_vertex_attribute(vt_pb), lcc.point_of_vertex_attribute(vt_pa), 0.3));
    vt2 = create_vertex_handle(
            compute_point(lcc.point_of_vertex_attribute(vt_pb), lcc.point_of_vertex_attribute(vt_pc), 0.3));

    vt3 = create_vertex_handle(
            compute_point(lcc.point_of_vertex_attribute(vt_pb), lcc.point_of_vertex_attribute(vt_pd), 0.3));
    vt4 = create_vertex_handle(
            compute_point(lcc.point_of_vertex_attribute(vt_pa), lcc.point_of_vertex_attribute(vt_pc), 0.3));
    vt5 = create_vertex_handle(
            compute_point(lcc.point_of_vertex_attribute(vt_pd), lcc.point_of_vertex_attribute(vt_pb), 0.3));
    vt6 = create_vertex_handle(
            compute_point(lcc.point_of_vertex_attribute(vt_pc), lcc.point_of_vertex_attribute(vt_pa), 0.3));

    //lcc.insert_cell_0_in_cell_1(do1,vt1);
    //lcc.insert_cell_0_in_cell_1(do2,vt2);
    lcc.insert_cell_0_in_cell_1(do1);
    lcc.insert_cell_0_in_cell_1(do2);
    //std::cout << "do1 attr " << lcc.info<0>(do1)[0] << std::endl;
    //std::cout << "a0do1 attr " << lcc.info<0>(lcc.alpha<0>(do1))[0] << std::endl;

    Dart_handle c0 = lcc.make_combinatorial_polygon(4);
    lcc.set_attribute<2>(c0, at0);
    Dart_handle c1 = lcc.make_combinatorial_polygon(4);
    lcc.set_attribute<2>(c1, at1);
    Dart_handle c2 = lcc.make_combinatorial_polygon(4);
    lcc.set_attribute<2>(c2, at2);
    Dart_handle c3 = lcc.make_combinatorial_polygon(4);
    lcc.set_attribute<2>(c3, at3);
    Dart_handle c4 = lcc.make_combinatorial_polygon(4);
    lcc.set_attribute<2>(c4, at4);
    Dart_handle c5 = lcc.make_combinatorial_polygon(4);
    lcc.set_attribute<2>(c5, at5);
    std::cout << lcc.is_valid() << std::endl;

    // Linking internal faces
    // 0 1
    lcc.link_alpha<2>(c0, lcc.alpha<1, 0, 1>(c1));
    lcc.link_alpha<2>(lcc.alpha<0>(c0), lcc.alpha<1, 0, 1, 0>(c1));
    // 0 2
    lcc.link_alpha<2>(lcc.alpha<1>(c0), lcc.alpha<1, 0, 1, 0>(c2));
    lcc.link_alpha<2>(lcc.alpha<1, 0>(c0), lcc.alpha<1, 0, 1>(c2));
    // 0 3
    lcc.link_alpha<2>(lcc.alpha<1, 0, 1>(c0), lcc.alpha<1, 0, 1, 0>(c3));
    lcc.link_alpha<2>(lcc.alpha<1, 0, 1, 0>(c0), lcc.alpha<1, 0, 1>(c3));
    // 0 4
    lcc.link_alpha<2>(lcc.alpha<0, 1>(c0), lcc.alpha<1, 0, 1>(c4));
    lcc.link_alpha<2>(lcc.alpha<0, 1, 0>(c0), lcc.alpha<1, 0, 1, 0>(c4));
    // 1 2
    lcc.link_alpha<2>(lcc.alpha<1>(c1), lcc.alpha<0, 1>(c2));
    lcc.link_alpha<2>(lcc.alpha<1, 0>(c1), lcc.alpha<0, 1, 0>(c2));
    // 1 5
    lcc.link_alpha<2>(lcc.alpha<0, 1>(c1), lcc.alpha<1>(c5));
    lcc.link_alpha<2>(lcc.alpha<0, 1, 0>(c1), lcc.alpha<1, 0>(c5));
    // 2 3
    lcc.link_alpha<2>(lcc.alpha<1>(c2), lcc.alpha<0, 1>(c3));
    lcc.link_alpha<2>(lcc.alpha<1, 0>(c2), lcc.alpha<0, 1, 0>(c3));
    // 3 4
    lcc.link_alpha<2>(lcc.alpha<1>(c3), lcc.alpha<0, 1>(c4));
    lcc.link_alpha<2>(lcc.alpha<1, 0>(c3), lcc.alpha<0, 1, 0>(c4));
    // 4 5
    lcc.link_alpha<2>(lcc.alpha<1>(c4), lcc.alpha<0, 1, 0, 1>(c5));
    lcc.link_alpha<2>(lcc.alpha<1, 0>(c4), lcc.alpha<0, 1, 0, 1, 0>(c5));

    std::cout << lcc.is_valid() << std::endl;

    //edge exist ?
    bool b2 = false, b01 = false, b0101 = false, b010101 = false, b01010101 = false, b0101010101 = false;

    Dart_handle dh2 = lcc.alpha<2>(do1);
    Dart_handle dh0 = lcc.alpha<0, 2>(do1);
    //1
    Dart_handle dh01 = lcc.alpha<0, 1, 2>(do1);
    Dart_handle dh010 = lcc.alpha<0, 1, 0, 2>(do1);
    //2
    Dart_handle dh0101 = lcc.alpha<0, 1, 0, 1, 2>(do1);
    Dart_handle dh01010 = lcc.alpha<0, 1, 0, 1, 0, 2>(do1);
    //3
    Dart_handle dh010101 = lcc.alpha<0, 1, 0, 1, 0, 1, 2>(do1);
    Dart_handle dh0101010 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 2>(do1);
    //4
    Dart_handle dh01010101 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 2>(do1);
    Dart_handle dh010101010 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 0, 2>(do1);
    //5
    Dart_handle dh0101010101 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 2>(do1);
    Dart_handle dh01010101010 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 2>(do1);

    // 1
    if (!lcc.is_free<2>(do1)) {
        dh2 = lcc.alpha<2>(do1);
        dh0 = lcc.alpha<0, 2>(do1);
        b2 = true;
    }
    // 2
    if (!lcc.is_free<2>(lcc.alpha<0, 1>(do1))) {
        dh01 = lcc.alpha<0, 1, 2>(do1);
        dh010 = lcc.alpha<0, 1, 0, 2>(do1);
        b01 = true;
    }
    // 3
    if (!lcc.is_free<2>(lcc.alpha<0, 1, 0, 1>(do1))) {
        dh0101 = lcc.alpha<0, 1, 0, 1, 2>(do1);
        dh01010 = lcc.alpha<0, 1, 0, 1, 0, 2>(do1);
        b0101 = true;
    }
    // 4
    if (!lcc.is_free<2>(lcc.alpha<0, 1, 0, 1, 0, 1>(do1))) {
        dh010101 = lcc.alpha<0, 1, 0, 1, 0, 1, 2>(do1);
        dh0101010 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 2>(do1);
        b010101 = true;
    }
    // c3
    if (!lcc.is_free<2>(lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1>(do1))) {
        dh01010101 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 2>(do1);
        dh010101010 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 0, 2>(do1);
        b01010101 = true;
    }
    // c2
    if (!lcc.is_free<2>(lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 0, 1>(do1))) {
        dh0101010101 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 2>(do1);
        dh01010101010 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 2>(do1);
        b0101010101 = true;
    }
    lcc.set_vertex_attribute(c1, vt_pa);
    lcc.set_dart_of_attribute<0>(vt_pa, c1);
    lcc.set_vertex_attribute(lcc.alpha<0>(c5), vt_pb);
    lcc.set_dart_of_attribute<0>(vt_pb, lcc.alpha<0>(c5));
    lcc.set_vertex_attribute(c3, vt_pc);
    lcc.set_dart_of_attribute<0>(vt_pc, c3);
    lcc.set_vertex_attribute(c2, vt_pd);
    lcc.set_dart_of_attribute<0>(vt_pd, c2);

    map_cell.erase(lcc.info<2>(d)[0]);
    std::cout << "removed " << lcc.remove_cell<2>(d) << std::endl;
    //c0
    if (b2) {
        lcc.link_alpha<2>(c1, dh2);
        lcc.link_alpha<2>(lcc.alpha<0>(c1), dh0);
    }
    //c5
    if (b01) {
        lcc.link_alpha<2>(c5, dh01);
        lcc.link_alpha<2>(lcc.alpha<0>(c5), dh010);
    }
    if (b0101) {
        lcc.link_alpha<2>(lcc.alpha<0, 1>(c5), dh0101);
        lcc.link_alpha<2>(lcc.alpha<0, 1, 0>(c5), dh01010);
    }
    //c4
    if (b010101) {
        lcc.link_alpha<2>(c4, dh010101);
        lcc.link_alpha<2>(lcc.alpha<0>(c4), dh0101010);
    }
    //c3
    if (b01010101) {
        lcc.link_alpha<2>(c3, dh01010101);
        lcc.link_alpha<2>(lcc.alpha<0>(c3), dh010101010);
    }
    //c2
    if (b0101010101) {
        lcc.link_alpha<2>(c2, dh0101010101);
        lcc.link_alpha<2>(lcc.alpha<0>(c2), dh01010101010);
    }

    std::cout << "Vtx info:" << std::endl;
    std::cout << vt1->info()[0] << std::endl;
    std::cout << vt2->info()[0] << std::endl;
    std::cout << vt3->info()[0] << std::endl;
    std::cout << vt4->info()[0] << std::endl;
    std::cout << vt5->info()[0] << std::endl;
    std::cout << vt6->info()[0] << std::endl;

    std::cout << vt_pa->info()[0] << std::endl;
    std::cout << vt_pb->info()[0] << std::endl;
    std::cout << vt_pc->info()[0] << std::endl;
    std::cout << vt_pd->info()[0] << std::endl;

    lcc.set_vertex_attribute(lcc.alpha<0>(c1), vt1);
    lcc.set_vertex_attribute(lcc.alpha<0, 1, 0>(c5), vt2);
    lcc.set_vertex_attribute(lcc.alpha<0>(c0), vt3);
    lcc.set_vertex_attribute(c0, vt4);
    lcc.set_vertex_attribute(lcc.alpha<1, 0>(c0), vt5);
    lcc.set_vertex_attribute(lcc.alpha<0, 1, 0>(c0), vt6);

    lcc.set_vertex_attribute(c1, vt_pa);
    lcc.set_dart_of_attribute<0>(vt_pa, c1);
    lcc.set_vertex_attribute(lcc.alpha<0>(c5), vt_pb);
    lcc.set_dart_of_attribute<0>(vt_pb, lcc.alpha<0>(c5));
    lcc.set_vertex_attribute(c3, vt_pc);
    lcc.set_dart_of_attribute<0>(vt_pc, c3);
    lcc.set_vertex_attribute(c2, vt_pd);
    lcc.set_dart_of_attribute<0>(vt_pd, c2);


    std::cout << "Vtx info:" << std::endl;
    std::cout << vt1->info()[0] << " " << vt1->is_valid() << " " << vt1->get_nb_refs() << " " << lcc.darts_of_orbit<1,2>(lcc.dart_of_attribute<0>(vt1)).size() << std::endl;
    std::cout << vt2->info()[0] << " " << vt2->is_valid() << " " << vt2->get_nb_refs() << " " << lcc.darts_of_orbit<1,2>(lcc.dart_of_attribute<0>(vt2)).size() << std::endl;
    std::cout << vt3->info()[0] << " " << vt3->is_valid() << " " << vt3->get_nb_refs() << " " << lcc.darts_of_orbit<1,2>(lcc.dart_of_attribute<0>(vt3)).size() << std::endl;
    std::cout << vt4->info()[0] << " " << vt4->is_valid() << " " << vt4->get_nb_refs() << " " << lcc.darts_of_orbit<1,2>(lcc.dart_of_attribute<0>(vt4)).size() << std::endl;
    std::cout << vt5->info()[0] << " " << vt5->is_valid() << " " << vt5->get_nb_refs() << " " << lcc.darts_of_orbit<1,2>(lcc.dart_of_attribute<0>(vt5)).size() << std::endl;
    std::cout << vt6->info()[0] << " " << vt6->is_valid() << " " << vt6->get_nb_refs() << " " << lcc.darts_of_orbit<1,2>(lcc.dart_of_attribute<0>(vt6)).size() << std::endl;

    std::cout << vt_pa->info()[0] << " " << vt_pa->is_valid() << " " << vt_pa->get_nb_refs() << " " << lcc.darts_of_orbit<1,2>(lcc.dart_of_attribute<0>(vt_pa)).size() << std::endl;
    std::cout << vt_pb->info()[0] << " " << vt_pb->is_valid() << " " << vt_pb->get_nb_refs() << " " << lcc.darts_of_orbit<1,2>(lcc.dart_of_attribute<0>(vt_pb)).size() << std::endl;
    std::cout << vt_pc->info()[0] << " " << vt_pc->is_valid() << " " << vt_pc->get_nb_refs() << " " << lcc.darts_of_orbit<1,2>(lcc.dart_of_attribute<0>(vt_pc)).size() << std::endl;
    std::cout << vt_pd->info()[0] << " " << vt_pd->is_valid() << " " << vt_pd->get_nb_refs() << " " << lcc.darts_of_orbit<1,2>(lcc.dart_of_attribute<0>(vt_pd)).size() << std::endl;


    //lcc.correct_invalid_attributes();
    std::cout << "s_3:end valid " << vt_pa->info()[0] << " " << vt_pa->info()[0] << " " << lcc.is_valid() << "#attr<2> "
              << lcc.attributes<2>().size() << " #attr<0> " << lcc.attributes<0>().size() << " nb dart "
              << lcc.darts_of_orbit<1, 2>(lcc.alpha<1, 0>(c1)).size() << std::endl;

    update_nc(get_id(vt1));
    update_nc(get_id(vt2));
}


void LCC_HL::s_3_bis(std::size_t face_id, std::size_t offset) {
    //lcc.correct_invalid_attributes();
    assert(lcc.is_valid());
    std::cout << "s_3:begin " << lcc.one_dart_per_incident_cell<1, 0>(dart_of_vertex_id(2)).size() << std::endl;
    Dart_handle d = lcc.dart_of_attribute<2>(map_cell[face_id]);
    std::vector <std::size_t> vertex = get_vertex_of_cell(face_id);
    offset = offset % vertex.size();
    while (offset != 0) {
        d = lcc.alpha<0, 1>(d);
        offset--;
    }
    std::cout << "starter dart <x> " << lcc.info<0>(d)[0] << " <0> " << lcc.info<0>(lcc.alpha<0>(d))[0] << " <2> "
              << lcc.info<2>(d)[0] << std::endl;

    Dart_handle dn1, dn2;
    Dart_handle do1, do2, do3, do4;
    // old darts
    do1 = lcc.alpha<0, 1, 0, 1>(d);
    do2 = lcc.alpha<0, 1, 0, 1>(do1);
    do3 = lcc.alpha<0, 1, 0, 1>(do2);
    do4 = lcc.alpha<0, 1>(do3);
    Dart_handle dx = lcc.alpha<0, 1>(d);

    LCC_2::Vertex_attribute_handle vt_pa = lcc.vertex_attribute(do1), vt_pb = lcc.vertex_attribute(
            do2), vt_pc = lcc.vertex_attribute(do3), vt_pd = lcc.vertex_attribute(do4);

    LCC_2::Attribute_handle<2>::type at0 = create_face_handle(), at1 = create_face_handle(), at2 = create_face_handle();
    LCC_2::Attribute_handle<2>::type at3 = create_face_handle(), at4 = create_face_handle(), at5 = create_face_handle();
    LCC_2::Vertex_attribute_handle vt1, vt2, vt3, vt4, vt5, vt6;

    vt1 = create_vertex_handle(
            compute_point(lcc.point_of_vertex_attribute(vt_pb), lcc.point_of_vertex_attribute(vt_pa), 0.3));
    vt2 = create_vertex_handle(
            compute_point(lcc.point_of_vertex_attribute(vt_pb), lcc.point_of_vertex_attribute(vt_pc), 0.3));

    vt3 = create_vertex_handle(
            compute_point(lcc.point_of_vertex_attribute(vt_pb), lcc.point_of_vertex_attribute(vt_pd), 0.3));
    vt4 = create_vertex_handle(
            compute_point(lcc.point_of_vertex_attribute(vt_pa), lcc.point_of_vertex_attribute(vt_pc), 0.3));
    vt5 = create_vertex_handle(
            compute_point(lcc.point_of_vertex_attribute(vt_pd), lcc.point_of_vertex_attribute(vt_pb), 0.3));
    vt6 = create_vertex_handle(
            compute_point(lcc.point_of_vertex_attribute(vt_pc), lcc.point_of_vertex_attribute(vt_pa), 0.3));


    Dart_handle c0 = lcc.make_combinatorial_polygon(4);
    lcc.set_attribute<2>(c0, at0);
    Dart_handle c1 = lcc.make_combinatorial_polygon(4);
    lcc.set_attribute<2>(c1, at1);
    Dart_handle c2 = lcc.make_combinatorial_polygon(4);
    lcc.set_attribute<2>(c2, at2);
    Dart_handle c3 = lcc.make_combinatorial_polygon(4);
    lcc.set_attribute<2>(c3, at3);
    Dart_handle c4 = lcc.make_combinatorial_polygon(4);
    lcc.set_attribute<2>(c4, at4);
    Dart_handle c5 = lcc.make_combinatorial_polygon(4);
    lcc.set_attribute<2>(c5, at5);
    std::cout << lcc.is_valid() << std::endl;

    // Linking internal faces
    // 0 1
    lcc.link_alpha<2>(c0, lcc.alpha<1, 0, 1>(c1));
    lcc.link_alpha<2>(lcc.alpha<0>(c0), lcc.alpha<1, 0, 1, 0>(c1));
    // 0 2
    lcc.link_alpha<2>(lcc.alpha<1>(c0), lcc.alpha<1, 0, 1, 0>(c2));
    lcc.link_alpha<2>(lcc.alpha<1, 0>(c0), lcc.alpha<1, 0, 1>(c2));
    // 0 3
    lcc.link_alpha<2>(lcc.alpha<1, 0, 1>(c0), lcc.alpha<1, 0, 1, 0>(c3));
    lcc.link_alpha<2>(lcc.alpha<1, 0, 1, 0>(c0), lcc.alpha<1, 0, 1>(c3));
    // 0 4
    lcc.link_alpha<2>(lcc.alpha<0, 1>(c0), lcc.alpha<1, 0, 1>(c4));
    lcc.link_alpha<2>(lcc.alpha<0, 1, 0>(c0), lcc.alpha<1, 0, 1, 0>(c4));
    // 1 2
    lcc.link_alpha<2>(lcc.alpha<1>(c1), lcc.alpha<0, 1>(c2));
    lcc.link_alpha<2>(lcc.alpha<1, 0>(c1), lcc.alpha<0, 1, 0>(c2));
    // 1 5
    lcc.link_alpha<2>(lcc.alpha<0, 1>(c1), lcc.alpha<1>(c5));
    lcc.link_alpha<2>(lcc.alpha<0, 1, 0>(c1), lcc.alpha<1, 0>(c5));
    // 2 3
    lcc.link_alpha<2>(lcc.alpha<1>(c2), lcc.alpha<0, 1>(c3));
    lcc.link_alpha<2>(lcc.alpha<1, 0>(c2), lcc.alpha<0, 1, 0>(c3));
    // 3 4
    lcc.link_alpha<2>(lcc.alpha<1>(c3), lcc.alpha<0, 1>(c4));
    lcc.link_alpha<2>(lcc.alpha<1, 0>(c3), lcc.alpha<0, 1, 0>(c4));
    // 4 5
    lcc.link_alpha<2>(lcc.alpha<1>(c4), lcc.alpha<0, 1, 0, 1>(c5));
    lcc.link_alpha<2>(lcc.alpha<1, 0>(c4), lcc.alpha<0, 1, 0, 1, 0>(c5));

    std::cout << lcc.is_valid() << std::endl;

    //edge exist ?
    bool b2 = false, b01 = false, b0101 = false, b010101 = false, b01010101 = false, b0101010101 = false;

    Dart_handle dh2 = lcc.alpha<2>(do1);
    Dart_handle dh0 = lcc.alpha<0, 2>(do1);
    //1
    Dart_handle dh01 = lcc.alpha<0, 1, 2>(do1);
    Dart_handle dh010 = lcc.alpha<0, 1, 0, 2>(do1);
    //2
    Dart_handle dh0101 = lcc.alpha<0, 1, 0, 1, 2>(do1);
    Dart_handle dh01010 = lcc.alpha<0, 1, 0, 1, 0, 2>(do1);
    //3
    Dart_handle dh010101 = lcc.alpha<0, 1, 0, 1, 0, 1, 2>(do1);
    Dart_handle dh0101010 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 2>(do1);
    //4
    Dart_handle dh01010101 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 2>(do1);
    Dart_handle dh010101010 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 0, 2>(do1);
    //5
    Dart_handle dh0101010101 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 2>(do1);
    Dart_handle dh01010101010 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 2>(do1);

    if (!lcc.is_free<2>(do1)) {
        dh2 = lcc.alpha<2>(do1);
        dh0 = lcc.alpha<0, 2>(do1);
        b2 = true;
    }
    if (!lcc.is_free<2>(lcc.alpha<0, 1>(do1))) {
        dh01 = lcc.alpha<0, 1, 2>(do1);
        dh010 = lcc.alpha<0, 1, 0, 2>(do1);
        b01 = true;
    }
    if (!lcc.is_free<2>(lcc.alpha<0, 1, 0, 1>(do1))) {
        dh0101 = lcc.alpha<0, 1, 0, 1, 2>(do1);
        dh01010 = lcc.alpha<0, 1, 0, 1, 0, 2>(do1);
        b0101 = true;
    }
    if (!lcc.is_free<2>(lcc.alpha<0, 1, 0, 1, 0, 1>(do1))) {
        dh010101 = lcc.alpha<0, 1, 0, 1, 0, 1, 2>(do1);
        dh0101010 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 2>(do1);
        b010101 = true;
    }
    if (!lcc.is_free<2>(lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 0, 1>(do1))) {
        dh01010101 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 2>(do1);
        dh010101010 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 0, 2>(do1);
        b01010101 = true;
    }
    if (!lcc.is_free<2>(lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0>(do1))) {
        dh0101010101 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 2>(do1);
        dh01010101010 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 2>(do1);
        b0101010101 = true;
    }

    lcc.set_vertex_attribute(c1, vt_pa);
    lcc.set_dart_of_attribute<0>(vt_pa, c1);
    lcc.set_vertex_attribute(lcc.alpha<0>(c5), vt_pb);
    lcc.set_dart_of_attribute<0>(vt_pb, lcc.alpha<0>(c5));
    lcc.set_vertex_attribute(c3, vt_pc);
    lcc.set_dart_of_attribute<0>(vt_pc, c3);
    lcc.set_vertex_attribute(c2, vt_pd);
    lcc.set_dart_of_attribute<0>(vt_pd, c2);

    map_cell.erase(lcc.info<2>(d)[0]);
    std::cout << "removed " << lcc.remove_cell<2>(d) << std::endl;
    //c0
    if (b2) {
        lcc.link_alpha<2>(c1, dh2);
        lcc.link_alpha<2>(lcc.alpha<0>(c1), dh0);
    }
    //c5
    if (b01) {
        lcc.link_alpha<2>(c5, dh01);
        lcc.link_alpha<2>(lcc.alpha<0>(c5), dh010);
    }
    if (b0101) {
        lcc.link_alpha<2>(lcc.alpha<0, 1>(c5), dh0101);
        lcc.link_alpha<2>(lcc.alpha<0, 1, 0>(c5), dh01010);
    }
    //c4
    if (b010101) {
        lcc.link_alpha<2>(c4, dh010101);
        lcc.link_alpha<2>(lcc.alpha<0>(c4), dh0101010);
    }
    //c3
    if (b01010101) {
        lcc.link_alpha<2>(c3, dh01010101);
        lcc.link_alpha<2>(lcc.alpha<0>(c3), dh010101010);
    }
    //c2
    if (b0101010101) {
        lcc.link_alpha<2>(c2, dh0101010101);
        lcc.link_alpha<2>(lcc.alpha<0>(c2), dh01010101010);
    }

    std::cout << "Vtx info:" << std::endl;
    std::cout << vt1->info()[0] << std::endl;
    std::cout << vt2->info()[0] << std::endl;
    std::cout << vt3->info()[0] << std::endl;
    std::cout << vt4->info()[0] << std::endl;
    std::cout << vt5->info()[0] << std::endl;
    std::cout << vt6->info()[0] << std::endl;

    std::cout << vt_pa->info()[0] << std::endl;
    std::cout << vt_pb->info()[0] << std::endl;
    std::cout << vt_pc->info()[0] << std::endl;
    std::cout << vt_pd->info()[0] << std::endl;

    lcc.set_vertex_attribute(lcc.alpha<0>(c1), vt1);
    lcc.set_vertex_attribute(lcc.alpha<0, 1, 0>(c5), vt2);
    lcc.set_vertex_attribute(lcc.alpha<0>(c0), vt3);
    lcc.set_vertex_attribute(c0, vt4);
    lcc.set_vertex_attribute(lcc.alpha<1, 0>(c0), vt5);
    lcc.set_vertex_attribute(lcc.alpha<0, 1, 0>(c0), vt6);

    lcc.set_vertex_attribute(c1, vt_pa);
    lcc.set_dart_of_attribute<0>(vt_pa, c1);
    lcc.set_vertex_attribute(lcc.alpha<0>(c5), vt_pb);
    lcc.set_dart_of_attribute<0>(vt_pb, lcc.alpha<0>(c5));
    lcc.set_vertex_attribute(c3, vt_pc);
    lcc.set_dart_of_attribute<0>(vt_pc, c3);
    lcc.set_vertex_attribute(c2, vt_pd);
    lcc.set_dart_of_attribute<0>(vt_pd, c2);

    std::cout << "s_3:end valid " << vt_pa->info()[0] << " " << vt_pa->info()[0] << " " << lcc.is_valid() << "#attr<2> "
              << lcc.attributes<2>().size() << " #attr<0> " << lcc.attributes<0>().size() << " nb dart "
              << lcc.darts_of_orbit<1, 2>(lcc.alpha<1, 0>(c1)).size() << std::endl;

}

/**Return **/
std::vector<bool> LCC_HL::is_a_34_available(std::size_t face_id) {
    std::vector<bool> res;
    Dart_handle d = lcc.dart_of_attribute<2>(map_cell[face_id]), dtmp = d;
    std::size_t prev,cur,next;
    std::size_t count=0;
    bool found = false;
    do{
        prev = get_id(lcc.alpha<1,0>(dtmp));
        cur = get_id(dtmp);
        next = get_id(lcc.alpha<0,1>(dtmp));
        if (!on_boundary(prev, cur) && !on_boundary(cur, next) && ( get_constraint(dtmp) == criticity::diminution)){
            res.push_back(true);
        }else{
            res.push_back(false);
        }
        dtmp = lcc.alpha<0, 1>(dtmp);
    } while (dtmp != d);
    return res;
}

std::size_t LCC_HL::action_negativ(std::size_t face_id) {
    // Se placer sur sommet marqué valide
    CGAL_assertion(lcc.is_valid());
    std::cout << "#Valid " << std::endl;
    Dart_handle d = lcc.dart_of_attribute<2>(map_cell[face_id]),dh,dtmp;
    std::size_t prev, cur, next, v1, v2;
    bool found = false;
    dtmp = d;
    do{
        prev = get_id(lcc.alpha<1,0>(dtmp));
        cur = get_id(dtmp);
        next = get_id(lcc.alpha<0,1>(dtmp));
        if (!on_boundary(prev, cur) && !on_boundary(cur, next) && ( get_constraint(dtmp) == criticity::diminution)) {
            dh = dtmp;
            found = true;
            std::cout << "#Found " << prev << " " << cur << " " << next << std::endl;
        }
        dtmp = lcc.alpha<0, 1>(dtmp);
    } while (dtmp != d && !found);
    assert(found);
    std::cout << "#While " << get_id(dh) << std::endl;
    d = dh;

    assert( get_constraint(d) == criticity::diminution );
    // Est ce qu'un sommet non conforme est présent sur l'arête ?
    // Si il n'y en a pas on en insert un
    // Ce sommet entrainera une non conformité à la face voisine
    v1 = get_id(lcc.alpha<0>(d));
    bool mark_v1 = false;
    bool mark_v2 = false;
    if(!lcc.is_marked(lcc.alpha<0>(d),conformity_mark)){
        // Ajouter sommet nc
        std::cout << "#NotMarked " << std::endl;
        v1 = split_edge(get_id(d),get_id(lcc.alpha<0>(d)));
        //update_nc(v1);
        mark_v1 = true;
    }
    v2 = get_id(lcc.alpha<1,0>(d));
    if(!lcc.is_marked(lcc.alpha<1,0>(d),conformity_mark)){
        std::cout << "#NotMarked " << std::endl;
        v2 = split_edge(get_id(lcc.alpha<1>(d)),get_id(lcc.alpha<1,0>(d)));
        //update_nc(v2);
        mark_v2 = true;
    }

    Dart_handle dh02=lcc.alpha<0,2>(dh),
            dh2=lcc.alpha<2>(dh),
            dh102=lcc.alpha<1,0,2>(dh),
            dh12=lcc.alpha<1,2>(dh);

    Dart_handle dx = lcc.alpha<0>(d), dy = lcc.alpha<1,0>(d);
    Dart_handle dx1 = lcc.alpha<1>(dx), dy1 = lcc.alpha<1>(dy);

    Vertex_attribute_handle vt3 = create_vertex_handle(compute_point(point_of_vertex(v1), point_of_vertex(v2), 0.5));

    map_vtx.erase(v1);
    map_vtx.erase(v2);

    lcc.set_attribute<0>(dx, vt3);
    lcc.set_attribute<0>(dy, vt3);

    //=>
    lcc.unlink_alpha<1>(dx);
    lcc.unlink_alpha<1>(dy);

    lcc.unlink_alpha<2>(dh02);
    lcc.unlink_alpha<2>(dh2);

    lcc.unlink_alpha<2>(dh12);
    lcc.unlink_alpha<2>(dh102);
    //<=

    lcc.link_alpha<1>(dx1, dy1);
    lcc.link_alpha<2>(dh02, dh102);
    lcc.link_alpha<2>(dh2, dh12);
    lcc.link_alpha<1>(dx, dy);

    std::size_t removed = 0;
    removed = lcc.remove_cell<2>(d);
    std::cout << "removed " << removed << std::endl;
    std::cout << "nb att<2> " << lcc.number_of_attributes<2>() << std::endl;

    //erase_vertex(vt1->info()[0]);
    //erase_vertex(vt2->info()[0]);

    lcc.correct_invalid_attributes();
    lcc.display_darts(std::cout, true);
    std::cout << "2-cell" << std::endl;
    lcc.display_cells<2>(std::cout);
    std::cout << "0-cell" << std::endl;
    lcc.display_cells<0>(std::cout);
    std::cout << lcc.number_of_vertex_attributes() << std::endl;
    // lcc.unmark(dh,conformity_mark);
    // lcc.unmark(lcc.alpha<1>(dh),conformity_mark);
    lcc.unmark(dx1,conformity_mark);
    lcc.unmark(dy1,conformity_mark);
    if(mark_v1){
        std::cout << "HERE mv1" << std::endl;
        lcc.mark(lcc.alpha<2,1>(dx1),conformity_mark);
        lcc.mark(lcc.alpha<2>(dx1),conformity_mark);
        lcc.mark(lcc.alpha<2,1>(dx1),priority_mark);
        lcc.mark(lcc.alpha<2>(dx1),priority_mark);
        //lcc.mark_cell<2>(lcc.alpha<2>(dx1),priority_mark);
        //lcc.mark_cell<2>(lcc.alpha<2,1>(dx1),priority_mark);
    }
    if(mark_v2) {
        std::cout << "HERE mv2" << std::endl;
        lcc.mark(lcc.alpha<2, 1>(dy1), conformity_mark);
        lcc.mark(lcc.alpha<2>(dy1), conformity_mark);
        lcc.mark(lcc.alpha<2, 1>(dy1), priority_mark);
        lcc.mark(lcc.alpha<2>(dy1), priority_mark);
        //lcc.mark_cell<2>(lcc.alpha<2>(dy1),priority_mark);
        //lcc.mark_cell<2>(lcc.alpha<2,1>(dy1),priority_mark);
    }

    if(lcc.vertex_attribute(lcc.alpha<0>(dx1))->info()[1] == criticity::neutral && in_boundary(get_id(lcc.alpha<0>(dx1))) ){
        d = dx1;
        std::size_t nv = split_edge(get_id(lcc.alpha<0>(d)),get_id(lcc.alpha<0,1,0,1>(d)));
        Dart_handle dx0 = lcc.alpha<0>(dx1);
        Dart_handle dx10 = lcc.alpha<1,0>(dx1);
        std::cout << "dx" << get_id(dx1) << std::endl;//
        std::cout << "dx0" << get_id(dx0) << std::endl;
        std::cout << "dx10" << get_id(dx10) << std::endl;

        a_34_edge_placement(d);
    }

    if(lcc.vertex_attribute(lcc.alpha<0>(dy1))->info()[1] == criticity::neutral && in_boundary(get_id(lcc.alpha<0>(dy1)))){
        d = dy1;
        std::size_t nv2 = split_edge(get_id(lcc.alpha<0>(d)),get_id(lcc.alpha<0,1,0,1>(d)));
        a_34_edge_placement(dy1);
    }
    /*
    lcc.set_attribute<2>(d0,lcc.attribute<2>(d));
    lcc.set_attribute<2>(d021,lcc.attribute<2>(d0));
    lcc.set_attribute<2>(d01010,lcc.attribute<2>(d0));

    lcc.set_attribute<2>(d021,lcc.attribute<2>(d0));
    lcc.set_attribute<2>(d01,lcc.attribute<2>(d0));
    lcc.set_attribute<2>(d0,lcc.attribute<2>(d0));
    */
    lcc.correct_invalid_attributes();

    /**/

    //lcc.correct_invalid_attributes();
    //std::cout << "#ID " << get_id(dh) << std::endl;
    //std::cout << "#ID " << get_id(lcc.alpha<1>(dh)) << std::endl;
    //unmark_orb_conformite(vt3->info()[0]);
    /**
    lcc.unmark(dx,conformity_mark);
    lcc.unmark(lcc.alpha<1>(dx),conformity_mark);
    lcc.unmark(dy,conformity_mark);
    lcc.unmark(lcc.alpha<1>(dy),conformity_mark);
    **/
    //update_nc(cur);
    //unmark_orb_conformite_vec({cur});
    //update_nc(prev);
    //update_nc(get_id(vt3));
    CGAL_assertion(lcc.is_valid());
    return 1;
}

void LCC_HL::set_orientation(std::size_t id_cell, std::size_t id_vtx_a, std::size_t id_vtx_b) {
    LCC_2::Attribute_handle<2>::type at = this->map_cell[id_cell];
    Dart_handle d = lcc.dart_of_attribute<2>(at);
    if (lcc.attribute<0>(d)->info()[0] != id_vtx_a || lcc.attribute<0>(lcc.alpha<0, 1>(d))->info()[0] != id_vtx_b) {
        set_vertex_of_cell(id_cell, id_vtx_a);
        Dart_handle dh = lcc.dart_of_attribute<2>(at), dh1 = lcc.alpha<1>(dh);
        if (lcc.attribute<0>(dh)->info()[0] != id_vtx_b) {
            lcc.set_dart_of_attribute<2>(at, dh1);
        }
    }
}

void LCC_HL::s_4(std::size_t face_id) {
    c_5(face_id);
}

/** Opération sur un critique, position !**/
void LCC_HL::nc_1(std::size_t face_id, bool opposite) {
    std::vector <std::size_t> vertex = get_vertex_of_cell(face_id);
    std::size_t v1, v2, c1;
    Point p;

    if (opposite) {
        v1 = split_edge(vertex[2], vertex[3]);
        p = compute_point(point_of_vertex(vertex[1]), point_of_vertex(vertex[3]), 0.50);

    } else {
        v1 = split_edge(vertex[0], vertex[1]);
        p = compute_point(point_of_vertex(vertex[2]), point_of_vertex(vertex[0]), 0.50);
    }
    c1 = split_cell_d(face_id, v1, vertex.back());
    v2 = split_edge(v1, vertex[4]);
    if (opposite) {
        split_cell_d(face_id, v2, vertex[1]);
    } else {
        split_cell_d(c1, v2, vertex[2]);
    }
    set_point(v2, p);
}

void LCC_HL::nc_1_bis(std::size_t face_id, bool opposite) {
    Dart_handle dh = lcc.dart_of_attribute<2>(map_cell[face_id]);
    dh = lcc.alpha<1, 0, 1, 0, 1>(dh);
    lcc.set_dart_of_attribute<2>(map_cell[face_id], dh);
    nc_1(face_id, false);
}

/**Opération sur un critique, position !**/
void LCC_HL::nc_2(std::size_t face_id, bool opposite) {
    std::vector <std::size_t> vertex = get_vertex_of_cell(face_id);
    std::size_t v1, v2, v3, v4, c1, c2, c3, c4;
    if (opposite) {
        v1 = split_edge(vertex[2], vertex[3]);
        v2 = split_edge(v1, vertex[3]);
        c1 = split_cell_d(face_id, v1, vertex[5]);
        v3 = split_edge(v1, vertex[5]);
        c2 = split_cell_d(face_id, v2, vertex[4]);
        v4 = split_edge(v2, vertex[4]);
        c3 = split_cell_d(face_id, v3, v4);
        c4 = split_cell_d(face_id, v4, vertex[1]);
    } else {
        v1 = split_edge(vertex[0], vertex[1]);
        v2 = split_edge(v1, vertex[1]);
        c1 = split_cell_d(face_id, v1, vertex[5]);
        v3 = split_edge(v1, vertex[5]);
        c2 = split_cell_d(face_id, v2, vertex[4]);
        v4 = split_edge(v2, vertex[4]);
        c3 = split_cell_d(face_id, v3, v4);
        c4 = split_cell_d(face_id, v4, vertex[2]);
    }
}

void LCC_HL::a_0(std::size_t face_id) {
    s_4(face_id);
}

void LCC_HL::a_1(std::size_t face_id) {
    s_1(face_id, 0);
}

void LCC_HL::a_2(std::size_t face_id) {
    s_1(face_id, 1);
}

void LCC_HL::a_3(std::size_t face_id) {
    s_1(face_id, 2);
}

void LCC_HL::a_4(std::size_t face_id) {
    s_1(face_id, 3);
}

void LCC_HL::a_5(std::size_t face_id) {
    s_2(face_id, 0);
}

void LCC_HL::a_6(std::size_t face_id) {
    s_2(face_id, 1);
}

void LCC_HL::a_7(std::size_t face_id) {
    s_2(face_id, 2);
}

void LCC_HL::a_8(std::size_t face_id) {
    s_2(face_id, 3);
}

void LCC_HL::a_9(std::size_t face_id) {
    s_3(face_id, 0);
}

void LCC_HL::a_10(std::size_t face_id) {
    s_3(face_id, 1);
}

void LCC_HL::a_11(std::size_t face_id) {
    s_3(face_id, 2);
}

void LCC_HL::a_12(std::size_t face_id) {
    s_3(face_id, 3);
}

// TODO
void LCC_HL::a_13(std::size_t face_id) {

}

void LCC_HL::a_14(std::size_t face_id) {
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);
    std::size_t v5 = split_edge(v[1], v[2]);
    split_cell(v[4], v5);
    unmark_orb_conformite_vec({v[4]});
    update_nc(v5);
}



void LCC_HL::a_15(std::size_t face_id){
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);
    if( get_constraint(v[1]) == criticity::augmentation ){
        std::size_t v5 = split_edge(v[2], v[3]);
        split_cell(v[4], v5);
        std::size_t v6 = split_edge(v[4], v5);
        split_cell(v6, v[1]);
        set_point_from_vtx(v6, v[2], v[0], 0.33);
        update_nc(v5);
        unmark_orb_conformite_vec({v[4]});
    }else{
        std::size_t v5 = split_edge(v[0], v[1]);
        split_cell(v[4], v5);
        std::size_t v6 = split_edge(v[4], v5);
        split_cell(v6, v[2]);
        set_point_from_vtx(v6, v[1], v[3], 0.33);
        update_nc(v5);
        unmark_orb_conformite_vec({v[4]});
    }
}
// TODO REMOVE
void LCC_HL::a_16(std::size_t face_id) {
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);
    std::size_t v5 = split_edge(v[0], v[1]);
    split_cell(v[4], v5);
    std::size_t v6 = split_edge(v[4], v5);
    split_cell(v6, v[2]);
    set_point_from_vtx(v6, v[1], v[3], 0.5);
    update_nc(v5);
    unmark_orb_conformite_vec({v[4]});
}

void LCC_HL::a_17(std::size_t face_id) {
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);
    split_cell(v[5],v[2]);
    unmark_orb_conformite_vec({v[5],v[2]});
}

void LCC_HL::a_18(std::size_t face_id) {
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);
    split_cell(v[1], v[5]);
    split_cell(v[4], v[2]);
    std::size_t v6 = split_edge(v[1], v[5]);
    std::size_t v7 = split_edge(v[4], v[2]);
    split_cell(v6, v7);
    set_point_from_vtx(v6, v[1], v[3], 0.33);
    set_point_from_vtx(v7, v[2], v[0], 0.33);
    unmark_orb_conformite_vec({v[1], v[5], v[4], v[2], v6, v7});
}

void LCC_HL::a_19(std::size_t face_id) {
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);

    std::size_t v6 = split_edge(v[1],v[2]);
    std::size_t v7 = split_edge(v6,v[2]);
    set_point_from_vtx(v6, v[1], v[2], 0.33);
    set_point_from_vtx(v7, v[2], v[1], 0.33);
    split_cell(v6,v[5]);
    split_cell(v[4],v7);
    unmark_orb_conformite_vec({v[4],v[5]});
    update_nc(v6);
    update_nc(v7);
}

void LCC_HL::a_20(std::size_t face_id) {
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);
    if(get_constraint(v[2]) == criticity::augmentation) {
        std::size_t v6 = split_edge(v[0], v[1]);
        split_cell(v[4], v6);
        std::size_t v7 = split_edge(v[4], v6);
        split_cell(v7, v[2]);
        set_point_from_vtx(v7, v[2], v[0], 0.33);
        set_point_from_vtx(v6, v[0], v[1], 0.5);
        unmark_orb_conformite_vec({v[4]});
        //unmark_orb_conformite_vec({v[7]});
        update_nc(v6);
    }else{
        std::size_t v6 = split_edge(v[3], v[2]);
        split_cell(v[5], v6);
        std::size_t v7 = split_edge(v[5], v6);
        split_cell(v7, v[1]);
        set_point_from_vtx(v7, v[1], v[3], 0.33);
        set_point_from_vtx(v6, v[3], v[2], 0.5);
        unmark_orb_conformite_vec({v[5]});
        update_nc(v6);
    }
}

// TODO REMOVE
void LCC_HL::a_21(std::size_t face_id) {
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);
    std::size_t v6 = split_edge(v[3], v[2]);
    split_cell(v[5], v6);
    std::size_t v7 = split_edge(v[5], v6);
    split_cell(v7, v[1]);
    set_point_from_vtx(v7, v[1], v[3], 0.33);
    set_point_from_vtx(v6, v[3], v[2], 0.5);
    unmark_orb_conformite_vec({v[5]});
    update_nc(v6);
}

void LCC_HL::a_22(std::size_t face_id) {
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);
    split_cell(v[5], v[3]);
    std::size_t v6 = split_edge(v[5], v[3]);
    set_point(v6, compute_point(point_of_vertex(v[1]), point_of_vertex(v[4]), 0.5));
    split_cell(v[1], v6);
    unmark_orb_conformite_vec({v[5], v[3], v6});
}

// #TODO 23
//
void LCC_HL::a_23(std::size_t face_id) {
    //lcc.correct_invalid_attributes();
    assert(lcc.is_valid());
    std::cout << "s_3:begin " << lcc.one_dart_per_incident_cell<1, 0>(dart_of_vertex_id(2)).size() << std::endl;
    Dart_handle d = lcc.dart_of_attribute<2>(map_cell[face_id]);
    std::vector <std::size_t> vertex = get_vertex_of_cell(face_id);

    std::cout << "starter dart <x> " << lcc.info<0>(d)[0] << " <0> " << lcc.info<0>(lcc.alpha<0>(d))[0] << " <2> "
              << lcc.info<2>(d)[0] << std::endl;

    Dart_handle dn1, dn2;
    Dart_handle do1, do2, do3, do4;
    // old darts
    d = lcc.alpha<0,1,0,1>(d);
    do1 = d;
    do2 = lcc.alpha<0, 1,0,1>(do1);
    do3 = lcc.alpha<0, 1,0,1>(do2);
    do4 = lcc.alpha<0, 1>(do3);
    Dart_handle dx = lcc.alpha<0, 1>(d);

    LCC_2::Vertex_attribute_handle vt_pa = lcc.vertex_attribute(do1), vt_pb = lcc.vertex_attribute(
            do2), vt_pc = lcc.vertex_attribute(do3), vt_pd = lcc.vertex_attribute(do4);

    LCC_2::Attribute_handle<2>::type at0 = create_face_handle(), at1 = create_face_handle(), at2 = create_face_handle();
    LCC_2::Attribute_handle<2>::type at3 = create_face_handle(), at4 = create_face_handle(), at5 = create_face_handle();
    LCC_2::Vertex_attribute_handle vt1, vt2, vt3, vt4, vt5, vt6,vt_dx=lcc.vertex_attribute(lcc.alpha<0>(do1)),vt_dy=lcc.vertex_attribute(lcc.alpha<0>(do2));

    std::cout << "dx " << vt_dx->info()[0] << std::endl;
    std::cout << "dy " << vt_dy->info()[0] << std::endl;
    std::size_t id_vx = vt_dx->info()[0],id_vy = vt_dy->info()[0];

    vt1 = create_vertex_handle(
            compute_point(lcc.point_of_vertex_attribute(vt_pb), lcc.point_of_vertex_attribute(vt_pa), 0.3));
    vt2 = create_vertex_handle(
            compute_point(lcc.point_of_vertex_attribute(vt_pb), lcc.point_of_vertex_attribute(vt_pc), 0.3));

    vt3 = create_vertex_handle(
            compute_point(lcc.point_of_vertex_attribute(vt_pb), lcc.point_of_vertex_attribute(vt_pd), 0.3));
    vt4 = create_vertex_handle(
            compute_point(lcc.point_of_vertex_attribute(vt_pa), lcc.point_of_vertex_attribute(vt_pc), 0.3));
    vt5 = create_vertex_handle(
            compute_point(lcc.point_of_vertex_attribute(vt_pd), lcc.point_of_vertex_attribute(vt_pb), 0.3));
    vt6 = create_vertex_handle(
            compute_point(lcc.point_of_vertex_attribute(vt_pc), lcc.point_of_vertex_attribute(vt_pa), 0.3));

    Dart_handle c0 = lcc.make_combinatorial_polygon(4);
    lcc.set_attribute<2>(c0, at0);
    Dart_handle c1 = lcc.make_combinatorial_polygon(4);
    lcc.set_attribute<2>(c1, at1);
    Dart_handle c2 = lcc.make_combinatorial_polygon(4);
    lcc.set_attribute<2>(c2, at2);
    Dart_handle c3 = lcc.make_combinatorial_polygon(4);
    lcc.set_attribute<2>(c3, at3);
    Dart_handle c4 = lcc.make_combinatorial_polygon(4);
    lcc.set_attribute<2>(c4, at4);
    Dart_handle c5 = lcc.make_combinatorial_polygon(4);
    lcc.set_attribute<2>(c5, at5);
    std::cout << lcc.is_valid() << std::endl;

    // Linking internal faces
    // 0 1
    lcc.link_alpha<2>(c0, lcc.alpha<1, 0, 1>(c1));
    lcc.link_alpha<2>(lcc.alpha<0>(c0), lcc.alpha<1, 0, 1, 0>(c1));
    // 0 2
    lcc.link_alpha<2>(lcc.alpha<1>(c0), lcc.alpha<1, 0, 1, 0>(c2));
    lcc.link_alpha<2>(lcc.alpha<1, 0>(c0), lcc.alpha<1, 0, 1>(c2));
    // 0 3
    lcc.link_alpha<2>(lcc.alpha<1, 0, 1>(c0), lcc.alpha<1, 0, 1, 0>(c3));
    lcc.link_alpha<2>(lcc.alpha<1, 0, 1, 0>(c0), lcc.alpha<1, 0, 1>(c3));
    // 0 4
    lcc.link_alpha<2>(lcc.alpha<0, 1>(c0), lcc.alpha<1, 0, 1>(c4));
    lcc.link_alpha<2>(lcc.alpha<0, 1, 0>(c0), lcc.alpha<1, 0, 1, 0>(c4));
    // 1 2
    lcc.link_alpha<2>(lcc.alpha<1>(c1), lcc.alpha<0, 1>(c2));
    lcc.link_alpha<2>(lcc.alpha<1, 0>(c1), lcc.alpha<0, 1, 0>(c2));
    // 1 5
    lcc.link_alpha<2>(lcc.alpha<0, 1>(c1), lcc.alpha<1>(c5));
    lcc.link_alpha<2>(lcc.alpha<0, 1, 0>(c1), lcc.alpha<1, 0>(c5));
    // 2 3
    lcc.link_alpha<2>(lcc.alpha<1>(c2), lcc.alpha<0, 1>(c3));
    lcc.link_alpha<2>(lcc.alpha<1, 0>(c2), lcc.alpha<0, 1, 0>(c3));
    // 3 4
    lcc.link_alpha<2>(lcc.alpha<1>(c3), lcc.alpha<0, 1>(c4));
    lcc.link_alpha<2>(lcc.alpha<1, 0>(c3), lcc.alpha<0, 1, 0>(c4));
    // 4 5
    lcc.link_alpha<2>(lcc.alpha<1>(c4), lcc.alpha<0, 1, 0, 1>(c5));
    lcc.link_alpha<2>(lcc.alpha<1, 0>(c4), lcc.alpha<0, 1, 0, 1, 0>(c5));

    std::cout << lcc.is_valid() << std::endl;

    //edge exist ?
    bool b2 = false, b01 = false, b0101 = false, b010101 = false, b01010101 = false, b0101010101 = false;

    Dart_handle dh2 = lcc.alpha<2>(do1);
    Dart_handle dh0 = lcc.alpha<0, 2>(do1);
    //1
    Dart_handle dh01 = lcc.alpha<0, 1, 2>(do1);
    Dart_handle dh010 = lcc.alpha<0, 1, 0, 2>(do1);
    //2
    Dart_handle dh0101 = lcc.alpha<0, 1, 0, 1, 2>(do1);
    Dart_handle dh01010 = lcc.alpha<0, 1, 0, 1, 0, 2>(do1);
    //3
    Dart_handle dh010101 = lcc.alpha<0, 1, 0, 1, 0, 1, 2>(do1);
    Dart_handle dh0101010 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 2>(do1);
    //4
    Dart_handle dh01010101 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 2>(do1);
    Dart_handle dh010101010 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 0, 2>(do1);
    //5
    Dart_handle dh0101010101 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 2>(do1);
    Dart_handle dh01010101010 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 2>(do1);

    if (!lcc.is_free<2>(do1)) {
        dh2 = lcc.alpha<2>(do1);
        dh0 = lcc.alpha<0, 2>(do1);
        b2 = true;
    }
    if (!lcc.is_free<2>(lcc.alpha<0, 1>(do1))) {
        dh01 = lcc.alpha<0, 1, 2>(do1);
        dh010 = lcc.alpha<0, 1, 0, 2>(do1);
        b01 = true;
    }
    if (!lcc.is_free<2>(lcc.alpha<0, 1, 0, 1>(do1))) {
        dh0101 = lcc.alpha<0, 1, 0, 1, 2>(do1);
        dh01010 = lcc.alpha<0, 1, 0, 1, 0, 2>(do1);
        b0101 = true;
    }
    if (!lcc.is_free<2>(lcc.alpha<0, 1, 0, 1, 0, 1>(do1))) {
        dh010101 = lcc.alpha<0, 1, 0, 1, 0, 1, 2>(do1);
        dh0101010 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 2>(do1);
        b010101 = true;
    }
    if (!lcc.is_free<2>(lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1>(do1))) {
        dh01010101 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 2>(do1);
        dh010101010 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 0, 2>(do1);
        b01010101 = true;
    }
    if (!lcc.is_free<2>(lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0>(do1))) {
        dh0101010101 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 2>(do1);
        dh01010101010 = lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 2>(do1);
        b0101010101 = true;
    }

    lcc.set_vertex_attribute(c1, vt_pa);
    lcc.set_dart_of_attribute<0>(vt_pa, c1);
    lcc.set_vertex_attribute(lcc.alpha<0>(c5), vt_pb);
    lcc.set_dart_of_attribute<0>(vt_pb, lcc.alpha<0>(c5));
    lcc.set_vertex_attribute(c3, vt_pc);
    lcc.set_dart_of_attribute<0>(vt_pc, c3);
    lcc.set_vertex_attribute(c2, vt_pd);
    lcc.set_dart_of_attribute<0>(vt_pd, c2);

    map_cell.erase(lcc.info<2>(d)[0]);
    std::cout << "removed " << lcc.remove_cell<2>(d) << std::endl;
    //c0
    if (b2) {
        lcc.link_alpha<2>(c1, dh2);
        lcc.link_alpha<2>(lcc.alpha<0>(c1), dh0);
    }
    //c5
    if (b01) {
        lcc.link_alpha<2>(c5, dh01);
        lcc.link_alpha<2>(lcc.alpha<0>(c5), dh010);
    }
    if (b0101) {
        lcc.link_alpha<2>(lcc.alpha<0, 1>(c5), dh0101);
        lcc.link_alpha<2>(lcc.alpha<0, 1, 0>(c5), dh01010);
    }
    //c4
    if (b010101) {
        lcc.link_alpha<2>(c4, dh010101);
        lcc.link_alpha<2>(lcc.alpha<0>(c4), dh0101010);
    }
    //c3
    if (b01010101) {
        lcc.link_alpha<2>(c3, dh01010101);
        lcc.link_alpha<2>(lcc.alpha<0>(c3), dh010101010);
    }
    //c2
    if (b0101010101) {
        lcc.link_alpha<2>(c2, dh0101010101);
        lcc.link_alpha<2>(lcc.alpha<0>(c2), dh01010101010);
    }

    std::cout << "Vtx info:" << std::endl;
    std::cout << vt1->info()[0] << std::endl;
    std::cout << vt2->info()[0] << std::endl;
    std::cout << vt3->info()[0] << std::endl;
    std::cout << vt4->info()[0] << std::endl;
    std::cout << vt5->info()[0] << std::endl;
    std::cout << vt6->info()[0] << std::endl;

    std::cout << vt_pa->info()[0] << std::endl;
    std::cout << vt_pb->info()[0] << std::endl;
    std::cout << vt_pc->info()[0] << std::endl;
    std::cout << vt_pd->info()[0] << std::endl;



    lcc.set_vertex_attribute(lcc.alpha<0>(c1), vt1);
    lcc.set_vertex_attribute(lcc.alpha<0, 1, 0>(c5), vt2);
    lcc.set_vertex_attribute(lcc.alpha<0>(c0), vt3);
    lcc.set_vertex_attribute(c0, vt4);
    lcc.set_vertex_attribute(lcc.alpha<1, 0>(c0), vt5);
    lcc.set_vertex_attribute(lcc.alpha<0, 1, 0>(c0), vt6);

    std::cout << "check_point" << std::endl;

    lcc.set_vertex_attribute(c1, vt_pa);
    lcc.set_dart_of_attribute<0>(vt_pa, c1);
    lcc.set_vertex_attribute(lcc.alpha<0>(c5), vt_pb);
    lcc.set_dart_of_attribute<0>(vt_pb, lcc.alpha<0>(c5));
    lcc.set_vertex_attribute(c3, vt_pc);
    lcc.set_dart_of_attribute<0>(vt_pc, c3);
    lcc.set_vertex_attribute(c2, vt_pd);
    lcc.set_dart_of_attribute<0>(vt_pd, c2);


    std::cout << "s_3:end valid " << vt_pa->info()[0] << " " << vt_pa->info()[0] << " " << lcc.is_valid() << "#attr<2> "
              << lcc.attributes<2>().size() << " #attr<0> " << lcc.attributes<0>().size() << " nb dart "
              << lcc.darts_of_orbit<1, 2>(lcc.alpha<1, 0>(c1)).size() << std::endl;

    map_vtx.erase(id_vx);
    map_vtx.erase(id_vy);
    update_nc(get_id(vt1));
    update_nc(get_id(vt2));
    update_nc(get_id(vt3));
    update_nc(get_id(vt4));
    update_nc(get_id(vt5));
    update_nc(get_id(vt6));

}
// TODO pouvoir delete tout brin appartenant à la couche du bord
void LCC_HL::add_ghost_layer(){
    std::cout<< "cp1" << std::endl;
    std::vector<Dart_handle> vd;
    size_type curr;
    lcc.set_automatic_attributes_management(1);
    try {
        this->border_mark = lcc.get_new_mark();
    }
    catch (LCC_2::Exception_no_more_available_mark) {
        std::cerr << "No more free mark, exit." << std::endl;
        exit(-1);
    }
    try {
        curr = lcc.get_new_mark();
    }
    catch (LCC_2::Exception_no_more_available_mark) {
        std::cerr << "No more free mark, exit." << std::endl;
        exit(-1);
    }
    //! Select all dart of border
    std::cout<< "cp2" << std::endl;
    for (LCC_2::One_dart_per_cell_range<1>::iterator it=lcc.one_dart_per_cell<1>().begin(),itend=lcc.one_dart_per_cell<1>().end(); it!=itend; ++it)
    {
        if( lcc.alpha<2>(it) == it ) {
            vd.push_back(it);
            vd.push_back(lcc.alpha<0>(it));
            lcc.mark(it, border_mark);
            lcc.mark(lcc.alpha<0>(it),border_mark);
        }
    }
    std::cout<< "cp3" << std::endl;
    //! Create the pattern and link to d
    Dart_handle d2, d21, d210, d2101, first_marked;
    std::vector<Dart_handle> first_marked_vector;
    // first_marked_vector.reserve(vd.size());
    bool found= false;
    std::vector<Dart_handle> dhbind;
    std::cout << "taille vd " << vd.size() << std::endl;
    //! Save
    for(auto it = vd.begin(), itend = vd.end(); it != itend ; ++it){
        //! Save the first_marked_dart encounter on the <1,2> orbit
        lcc.mark(*it,curr);
        std::cout << "#" << std::endl;
        lcc.display_dart(*it); // 58
        std::cout << " o<2,1> " << lcc.darts_of_orbit<1,2>(*it).size() << std::endl;
        found = false;

        for(LCC_2::Dart_of_orbit_range<1,2>::iterator ito = lcc.darts_of_orbit<1,2>(*it).begin(),itoend= lcc.darts_of_orbit<1,2>(*it).end() ; ito!=itoend ; ++ito ){
            lcc.display_dart(ito);

            std::cout << "ito free" << lcc.is_free(ito,2) << std::endl;
            std::cout << "ito != it" << bool(*it != ito) << std::endl;
            std::cout << std::endl;
            assert(lcc.is_free(*it,2));
            if( bool(*it != ito) && (lcc.is_free(ito,2)) ){
                first_marked_vector.push_back(ito);
                std::cout << "added " ;
                lcc.display_dart(*it);
                lcc.display_dart(ito);
                std::cout << std::endl;
                found = true;
            }
        }
        assert(found == true);
    }
    std::vector<Point> points;
    Point tmp;
    Point cur;
    for( auto it = vd.begin(), itend = vd.end(); it!= itend; ++it ){
        cur = lcc.point(*it);
        tmp = barycenter_orbit(*it);
        points.push_back(Point(cur.x() + ( cur.x() - tmp.x() ) , cur.y() + ( cur.y() - tmp.y() ) ));
    }
    std::size_t dist;
    std::cout << "first_marked_vector " << first_marked_vector.size() << std::endl;
    for( auto it = vd.begin(), itend = vd.end(); it!= itend; ++it ){
        dist = std::distance(vd.begin(),it);
        found = false;
        lcc.display_dart(*it);std::cout << std::endl;
        //! CREATE d in D'
        d2 = lcc.create_dart();     // 8
        d21 = lcc.create_dart();    // 9
        d210 = lcc.create_dart();   // 10
        d2101 = lcc.create_dart();  // 11
        //! Link 1/2
        lcc.link_alpha<0>(d21,d210);
        lcc.link_alpha<1>(d2,d21);
        lcc.link_alpha<1>(d210,d2101);
        lcc.link_alpha<2>(*it,d2);
        dhbind.push_back(d210);
    }
    std::cout<< "cp4 " << vd.size() << " " << first_marked_vector.size() << std::endl;
    lcc.display_darts(std::cout,true);
    //! Link with others darts 2/2
    for( auto it = vd.begin(), itend = vd.end(); it!= itend; ++it ){
        std::cout << std::distance(vd.begin(),it) << std::endl;
        first_marked = first_marked_vector[std::distance(vd.begin(),it)];
        d2 = lcc.alpha<2>(*it);
        d21 = lcc.alpha<2,1>(*it);
        d210 = lcc.alpha<2,1,0>(*it);
        d2101 = lcc.alpha<2,1,0,1>(*it);
        lcc.link_alpha<0>(d2,lcc.alpha<0,2>(*it));
        lcc.link_alpha<2>(d21,lcc.alpha<2,1>(first_marked));
        lcc.link_alpha<2>(d210,lcc.alpha<2,1,0>(first_marked));
        lcc.link_alpha<0>(d2101,lcc.alpha<0,2,1,0,1>(*it));
        lcc.display_dart(*it);
        lcc.display_dart(first_marked);
        std::cout << std::endl;
    }

    std::cout<< "cp4b" << std::endl;
    lcc.display_darts(std::cout,true);
    Vertex_attribute_handle vh;
    Dart_handle dx;
    Point pn, p,A,B;
    int att0=0,att2=0,natt0=0,natt2=0;
    // Compute point
    for( auto it = dhbind.begin(), itend = dhbind.end(); it!= itend; ++it ) {
        //! Check if a dart have a nonnull attr and assign it.
        if (lcc.attribute<0>(*it) == NULL) {
            //! Compute vertex coord and assign to new 2 1 orbits
            dx = vd[std::distance(dhbind.begin(),it)];
            A = lcc.point(lcc.alpha<0, 1, 0>(dx));
            B = lcc.point(dx);
            p = compute_point(A, B, 0.33);
            //p = barycenter_orbit(*it);
            Point pn(B.x() + (B.x()-A.x())*0.33 , B.y() + (B.y() - A.y())*0.33);
            //Point pn(B.x() + (B.x()-p.x()) , B.y() + (B.y() - p.y()));
            vh = create_vertex_handle(points[std::distance(dhbind.begin(),it)]);
            lcc.set_vertex_attribute(*it, vh);
            natt0++;
        }
        if (lcc.attribute<2>(*it) == NULL) {
            lcc.set_attribute<2>(*it, create_face_handle());
            natt2++;
            lcc.mark_cell<2>(*it,ghost_layer_mark);
        }
    }
    lcc.display_darts(std::cout,true);
    lcc.correct_invalid_attributes();
    lcc.unmark_all(curr);
    lcc.free_mark(curr);

    std::cout << "border_mark " << lcc.number_of_marked_darts(border_mark) << std::endl;
    std::cout << "ghost_layer_mark " << lcc.number_of_marked_darts(ghost_layer_mark) << std::endl;
}




Dart_handle LCC_HL::first_marked_dart(Dart_handle d){
    Dart_handle dh = lcc.alpha<1>(d);


}

void LCC_HL::pillowing_2D(std::vector<std::size_t> ep){
    lcc.display_darts(std::cout);
    //! D_e := ensemble de brin valid à pillow
    //! - Validation chemin
    //! - Creation brins + Link 1/2
    Dart_handle d,d2,d21,d210,d2102,d212,d2101, old_d2;
    std::vector<Dart_handle> vd;    //! Vector init
    std::vector<Dart_handle> mem;   //! Memory
    std::vector<Dart_handle> dhbind;    //! Dart to bind
    std::cout << "Generating marks" << std::endl;
    //! - Select all dart half edge without border
    lcc.set_automatic_attributes_management(false);

    size_type db =  lcc.number_of_darts();

    for(LCC_2 ::Dart_range ::iterator it = lcc.darts().begin(),itend = lcc.darts().end() ; it != itend ; ++it){
        if(lcc.is_marked(it,selection)){
            vd.push_back(it);
        }
    }

    std::cout << "#dart selected " << lcc.number_of_marked_darts(selection) << " " << vd.size() << " on " << lcc.number_of_darts() << std::endl;
    //! - fm & fm_d2
    std::cout << "Create and linking 1/2" << std::endl;
    //! Create and link 1/2 forall dart of De
    std::vector<Dart_handle> patterns;
    Vertex_attribute_handle vtx;
    Dart_handle dx;
    Vertex_attribute_handle vh,vx,vx2;
    Point A,B,p;
    float alpha=0.33;
    std::vector<std::pair<Vertex_attribute_handle,Vertex_attribute_handle>> new_vertex;
    for( auto it = vd.begin(), itend = vd.end(); it!= itend; ++it ){
        std::cout << "id curr dart: ";
        lcc.display_dart(*it);
        std::cout << " fm: " ;
        std::cout << " fm 2: " ;
        std::cout << "barycenters " << lcc.barycenter<2>(*it) << " " << lcc.barycenter<2>(lcc.alpha<2>(*it)) << std::endl;

        // cpt point
        B = lcc.point(*it);
        Point ba = lcc.barycenter<2>(*it);
        Point ba2 = lcc.barycenter<2>(lcc.alpha<2>(*it));
        ba = Point(B.x() + (ba.x()-B.x())*alpha,B.y() + (ba.y() - B.y())*alpha);
        ba2 = Point(B.x() + (ba2.x() - B.x())*alpha,B.y() +(ba2.y() - B.y())*alpha);
        // Work better
        if(lcc.is_marked(lcc.alpha<2>(*it),ghost_layer_mark)){
            vx = create_vertex_handle(ba);
            vx2 = create_vertex_handle(B);
        }else{
            vx = create_vertex_handle( ba);
            vx2 = create_vertex_handle( ba2);
        }

        assert(ba != ba2);
        //! - CREATE d in D'
        d2 = lcc.create_dart(vx);       // 0
        d21 = lcc.create_dart(vx);      // 1
        d210 = lcc.create_dart(vx2);    // 2
        d2102 = lcc.create_dart(vx2);   // 3
        d212 = lcc.create_dart(vx);     // 4
        d2101 = lcc.create_dart(vx2);   // 5
        //! - store
        mem.push_back(lcc.alpha<2>(*it));

        //! - Link 1/2
        lcc.link_alpha<0>(d21,d210);    // l3
        lcc.link_alpha<0>(d212,d2102);  // l7
        lcc.link_alpha<1>(d2,d21);      // 2
        lcc.link_alpha<1>(d210,d2101);  // l4
        lcc.link_alpha<2>(d21,d212);    // 5
        lcc.link_alpha<2>(d210,d2102);  //
        if( !lcc.is_free(*it,2) ){
            std::cout << "is i free" << std::endl;
            lcc.link_alpha<2>(lcc.alpha<2>(*it),d2101);
            if(lcc.is_marked(lcc.alpha<2>(*it),ghost_layer_mark) && lcc.is_marked(*it,ghost_layer_mark)){
                lcc.mark(d2,ghost_layer_mark);
                lcc.mark(d21,ghost_layer_mark);
                lcc.mark(d210,ghost_layer_mark);
                lcc.mark(d2102,ghost_layer_mark);
                lcc.mark(d212,ghost_layer_mark);
                lcc.mark(d2101,ghost_layer_mark);
            }
        }
        lcc.link_alpha<2>(*it,d2);
        patterns.push_back(d2);
        dhbind.push_back(d2101);
        std::cout << vx->info()[0] << " " << vx2->info()[0] << std::endl;


    }
    std::size_t dist;
    std::cout << " " << std::endl;
    lcc.display_darts(std::cout,true);
    //! - Link 2/2
    for( auto it = vd.begin(), itend = vd.end(); it!= itend; ++it ){
        dist = std::distance(vd.begin(), it);
        lcc.display_dart(*it);
        std::cout << std::endl;
        d2 = lcc.alpha<2>(*it);
        d21 = lcc.alpha<2, 1>(*it);
        d210 = lcc.alpha<2, 1, 0>(*it);
        d2102 = lcc.alpha<2, 1, 0, 2>(*it);
        d212 = lcc.alpha<2, 1, 2>(*it);
        d2101 = lcc.alpha<2, 1, 0, 1>(*it);
        if(lcc.is_free<0>(d2)){
            lcc.link_alpha<0>(d2, lcc.alpha<0, 2>(*it));
            lcc.link_alpha<0>(d2101, lcc.alpha<0, 2, 1, 0, 1>(*it));
        }

    }

    Vertex_attribute_handle v_temp;
    std::cout << "link <1> attr<0>" << lcc.number_of_attributes<0>() << std::endl;//lcc.is_valid() << std::endl;
    for( auto it = vd.begin(), itend = vd.end(); it!= itend; ++it ) {
        dist = std::distance(vd.begin(), it);
        d2102 = lcc.alpha<2,1,0,2>(*it);
        d212 = lcc.alpha<2,1,2>(*it);
        lcc.display_dart( *it ); std::cout << ":\n"  ;
        lcc.display_dart( d2102 ); std::cout << " " << lcc.is_free(d2102,1) << "\n";
        lcc.display_dart( d212 ); std::cout << " " << lcc.is_free(d212,1) << "\n";
        if(lcc.is_free(d2102,1)){
            for(LCC_2::Dart_of_orbit_range<1,2>::iterator ito = lcc.darts_of_orbit<1,2>(d2102).begin(),itoend= lcc.darts_of_orbit<1,2>(d2102).end() ; ito!=itoend ; ++ito ){
                if( lcc.is_free(ito,1) && ito != d2102 ){
                    lcc.display_dart(d2102); std::cout <<  " "; lcc.display_dart( ito );
                    lcc.link_alpha<1>(d2102, ito);
                    lcc.set_vertex_attribute(d2102,lcc.vertex_attribute(ito));
                    break;
                }
            }
        }
        if(lcc.is_free(d212,1)){
            for(LCC_2::Dart_of_orbit_range<1,2>::iterator ito = lcc.darts_of_orbit<1,2>(d212).begin(),itoend= lcc.darts_of_orbit<1,2>(d212).end() ; ito!=itoend ; ++ito ){
                if( lcc.is_free(ito,1) && ito != d212 ){
                    lcc.display_dart(d212); std::cout << " "; lcc.display_dart( ito );
                    lcc.link_alpha<1>(d212, ito);
                    lcc.set_vertex_attribute(d212,lcc.vertex_attribute(ito));
                    break;
                }
            }
        }
        if(lcc.is_free(d2102,1)){
            lcc.set_vertex_attribute(d212,lcc.vertex_attribute(d212));
            lcc.set_vertex_attribute(d2102,lcc.vertex_attribute(d2102));
        }
    }

    std::cout << "number of attributes<0> " << lcc.number_of_attributes<0>() << std::endl;
    std::cout << "number of 2 cells " << lcc.one_dart_per_cell<2>().size() << std::endl;

    //! - Link 2 bis
    //! - Compute attr <0> <2>

    std::cout << "Vector vd:" << vd.size() << ", " << dhbind.size() << std::endl;
    std::vector<Point> points_computed;
    for( auto it = dhbind.begin(), itend = dhbind.end(); it!= itend; ++it ) {
        //! Check if a dart have a nonnull attr and assign it.
        if (lcc.attribute<2>(*it) == NULL) {
            std::cout << "checkb" << std::endl;
            lcc.set_attribute<2>(*it, create_face_handle());
        }
        if (lcc.attribute<2>(lcc.alpha<1,2>(*it)) == NULL) {
            std::cout << "checkb" << std::endl;
            lcc.set_attribute<2>(lcc.alpha<1,2>(*it), create_face_handle());
        }

    }
    lcc.display_darts(std::cout,true);

    std::cout << "Remove" << std::endl;
    remove_cell_0101();
    std::cout << "Correct" << std::endl;


    for(auto it = dhbind.begin() ; it != dhbind.end() ; ++it){
        if (lcc.attribute<2>(lcc.alpha<1,2>(*it)) == NULL) {
            std::cout << "checkb" << std::endl;
            lcc.set_attribute<2>(*it, create_face_handle());
        }
    }

    lcc.correct_invalid_attributes();
    std::cout << "attr_vtx " << lcc.attributes<0>().size() << std::endl;
    for (LCC_2::Vertex_attribute_range::iterator v=lcc.vertex_attributes().begin(), vend=lcc.vertex_attributes().end(); v!=vend; ++v){
        std::cout << v->info()[0] << " " << v->point() << " ref " << v->get_nb_refs() << " valid " << v->is_valid() << std::endl;
    }
    std::cout << "map_vtx " << map_vtx.size() << std::endl;
    for(auto it = map_vtx.cbegin(); it != map_vtx.cend(); ++it)
    {
        std::cout << it->first << " " << it->second->is_valid() << " " << it->second->get_nb_refs() << " " << lcc.is_attribute_used<0>(it->second) <<"\n";
    }

    map_vtx.clear();
    for (LCC_2::Vertex_attribute_range::iterator v=lcc.vertex_attributes().begin(), vend=lcc.vertex_attributes().end(); v!=vend; ++v){
        std::cout << v->info()[0] << " " << v->point() << " ref " << v->get_nb_refs() << " valid " << v->is_valid() << std::endl;
        map_vtx.insert(std::pair<std::size_t,Vertex_attribute_handle>(v->info()[0],v));
    }

    std::cout << "MAP SIZE " << map_vtx.size() << std::endl;
    std::cout << "ATTR SIZE " << lcc.vertex_attributes().size() << std::endl;
    std::cout << "Valid" << std::endl;
    assert(lcc.is_valid());
    std::cout << "#dart before" << db << "#dart after" << lcc.number_of_darts() << std::endl;
    //! - Remove cells Border
    lcc.display_darts(std::cout,true);
    std::cout << "#dart before" << db << "#dart after" << lcc.number_of_darts() << std::endl;
    //! -
    assert(lcc.is_valid());
    std::cout << "#Number of attributes " << lcc.number_of_attributes<0>() << std::endl;
    //lcc.correct_invalid_attributes();
    std::cout << "#Number of attributes " << lcc.number_of_attributes<0>() << std::endl;


}

void LCC_HL::remove_cell_0101(){

    Dart_handle d2,d12;
    bool autoattr = lcc.are_attributes_automatically_managed();
    lcc.set_automatic_attributes_management(false);
    std::cout << "NB ATTR<2>" << lcc.one_dart_per_cell<2>().size() << " " << lcc.number_of_attributes<2>() << std::endl;
    std::cout << "remove "  << lcc.one_dart_per_cell<1>().size()  << std::endl;
    Dart_handle dh;
    for(LCC_2 ::Dart_range ::iterator it = lcc.darts().begin(),itend = lcc.darts().end(); it != itend ; ++it){
        dh = lcc.dart_handle(*it);
        if(lcc.is_free(dh,1)){
            lcc.display_dart(dh); std::cout << " ";
            lcc.unlink_alpha<2>(dh);
            lcc.erase_dart(dh);
        }
    }
    std::cout << std::endl;
    lcc.display_darts(std::cout,true);


    std::cout << "Number of darts: " << lcc.one_dart_per_cell<2>().size() << std::endl;
    for (LCC_2::One_dart_per_cell_range<2>::iterator it=lcc.one_dart_per_cell<2>().begin(),itend=lcc.one_dart_per_cell<2>().end(); it!=itend; ++it){
        lcc.display_dart(it); std::cout << std::endl;
        if( it == lcc.alpha<0,1,0,1>(it) ){
            std::cout << "<0,1,0,1>" << std::endl;
            d2 = lcc.alpha<2>(it);
            d12 = lcc.alpha<1,2>(it);
            lcc.unsew<2>(d2);
            lcc.unsew<2>(d12);
            std::cout << "it ";
            lcc.display_dart(it);
            std::cout << " d2 ";
            lcc.display_dart(d2);
            std::cout << " d12 ";
            lcc.display_dart(d12);
            lcc.sew<2>(lcc.alpha<2>(it),lcc.alpha<1,2>(it));
            lcc.erase_attribute<2>(lcc.attribute<2>(it));
            //map_cell.erase(lcc.info<2>(it)[0]);
            lcc.remove_cell<2>(it);
        }
    }

    lcc.set_automatic_attributes_management(autoattr);
    std::cout << "NB ATTR<2>" << lcc.one_dart_per_cell<2>().size() << " " << lcc.number_of_attributes<2>() << std::endl;
}

// TODO test
void LCC_HL::a_24(std::size_t face_id) {
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);
    if(is_vtx_prio_marked(v[5],face_id)){
        std::size_t v7 = split_edge(v[1], v[0]);
        split_cell(v[3], v7);
        unmark_orb_conformite_vec({v[3]});
        update_nc(v7);
    }else if(is_vtx_prio_marked(v[3],face_id)){
        std::size_t v7 = split_edge(v[1], v[2]);
        split_cell(v[5], v7);
        unmark_orb_conformite_vec({v[5]});
        update_nc(v7);
    }else{
        std::size_t v7 = split_edge(v[1], v[2]);
        split_cell(v[5], v7);
        unmark_orb_conformite_vec({v[5]});
        update_nc(v7);
    }
}

void LCC_HL::a_24_bis(std::size_t face_id) {
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);
    split_cell(v[3], v[7]);
    unmark_orb_conformite_vec({v[3],v[7]});
}

void LCC_HL::a_25(std::size_t face_id) {
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);

    split_cell(v[2], v[7]);
    split_cell(v[3], v[6]);
    unmark_orb_conformite_vec({v[2], v[7], v[3], v[6]});
    /*update_nc(v[2]);
    update_nc(v[7]);
    update_nc(v[3]);
    update_nc(v[6]);*/
}

void LCC_HL::a_26(std::size_t face_id) {
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);
    split_cell(v[6], v[2]);
    split_cell(v[7], v[1]);
    std::size_t v8 = split_edge(v[6], v[2]);
    std::size_t v9 = split_edge(v[7], v[1]);
    Point tmp = compute_point(point_of_vertex(v[1]), point_of_vertex(v[3]), 0.33);
    set_point(v8, compute_point(point_of_vertex(v[6]), point_of_vertex(v[2]), 0.5));
    set_point(v9, compute_point(tmp, point_of_vertex(v[7]), 0.5));
    split_cell(v9, v8);
    split_cell(v8, v[4]);
    unmark_orb_conformite_vec({v[6], v[2], v[7], v[1], v8, v9, v[4]});
}

void LCC_HL::a_27(std::size_t face_id) {
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);
    if( v[1] == criticity::augmentation ){
        split_cell(v[3], v[7]);
        std::size_t v8 = split_edge(v[3], v[7]);
        set_point(v8, compute_point(point_of_vertex(v[0]), point_of_vertex(v[2]), 0.5));
        split_cell(v8, v[1]);
        unmark_orb_conformite_vec({v[3], v[7], v8});
    }else{
        std::size_t v8 = split_edge(v[1], v[0]);
        split_cell(v[3],v8);
        set_point(v8, compute_point(point_of_vertex(v[1]), point_of_vertex(v[0]), 0.33));
        unmark_orb_conformite_vec({v[3]});
        update_nc(v8);
    }
}

void LCC_HL::a_28(std::size_t face_id) {
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);
    /*split_cell(v[2], v[7]);
    unmark_orb_conformite_vec({v[2], v[7]});
     */
    std::size_t v8 = split_edge(v[0],v[1]);
    std::size_t v9 = split_edge(v8,v[1]);
    split_cell(v[5],v8);
    split_cell(v[4],v9);
    set_point(v8, compute_point(point_of_vertex(v[0]), point_of_vertex(v[1]), 0.33));
    set_point(v9, compute_point(point_of_vertex(v[1]), point_of_vertex(v[0]), 0.33));
    unmark_orb_conformite_vec({v[5],v[4]});

    update_nc(v8);
    update_nc(v9);
}

void LCC_HL::a_29(std::size_t face_id) {
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);
    split_cell(v[1], v[6]);
    unmark_orb_conformite_vec({v[1], v[6]});
}

void LCC_HL::a_30(std::size_t face_id) {
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);
    split_cell(v[2], v[9]);
    unmark_orb_conformite_vec({v[2], v[9]});
}

void LCC_HL::a_31(std::size_t face_id) {
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);
    split_cell(v[1], v[3]);
    std::size_t v10 = split_edge(v[1], v[3]);
    set_point(v10, compute_point(point_of_vertex(v[3]), point_of_vertex(v[9]), 0.33));
    split_cell(v[5], v10);
    split_cell(v[9], v10);
    unmark_orb_conformite_vec({v[1], v[3], v10, v[5], v[9]});
}

void LCC_HL::a_32(std::size_t face_id) {
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);
    split_cell(v[1], v[8]);
    split_cell(v[2], v[7]);
    unmark_orb_conformite_vec({v[1], v[2], v[7], v[8]});
}

void LCC_HL::a_33(std::size_t face_id) {
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);
    split_cell(v[1], v[5]);
    unmark_orb_conformite_vec({v[1], v[5]});
}

// a_34(std::size_t face_id ) :=> action_negativ()

void LCC_HL::a_35(std::size_t face_id){
    CGAL_assertion(lcc.is_valid());
    std::cout << "#Valid " << std::endl;
    Dart_handle d = lcc.dart_of_attribute<2>(map_cell[face_id]),dh,dtmp;
    std::size_t prev,cur, next;
    CGAL_assertion(lcc.is_valid());
    bool found = false;
    dtmp = d;
    std::size_t a,b;
    do{
        prev = get_id(lcc.alpha<1,0>(dtmp));
        cur = get_id(dtmp);a = cur;
        next = get_id(lcc.alpha<0,1>(dtmp));

        if (!on_boundary(prev, cur) && !on_boundary(cur, next) && ( get_constraint(dtmp) == criticity::diminution)) {
            dh = dtmp;
            found = true;
            std::cout << "#Found " << prev << " " << cur << " " << next << std::endl;
        }
        dtmp = lcc.alpha<0, 1>(dtmp);
    } while (dtmp != d && !found);
    assert(found);
    b = get_id(lcc.alpha<0,1,0,1>(dh));
    d = dh;
    Dart_handle d1=lcc.alpha<1>(d) ,d101=lcc.alpha<1,0,1>(d)
    ,d01=lcc.alpha<0,1>(d),d021 = lcc.alpha<0,2,1>(d),d120 = lcc.alpha<1,2,0>(d),d1010=lcc.alpha<1,0,1,0>(d);

    lcc.remove_cell<1>(d);
    lcc.remove_cell<1>(d1);

    CGAL_assertion(lcc.is_valid());

    std::cout << "d01 " << lcc.darts_of_orbit<1,2>(d01).size() << std::endl;
    std::cout << "d101 " << lcc.darts_of_orbit<1,2>(d101).size() << std::endl;

    std::cout << "d01 " << lcc.info<0>(d01)[0] << std::endl;
    std::cout << "d101 " << lcc.info<0>(d101)[0] << std::endl;

    split_cell(a,b);

    remove_vtx(prev);
    remove_vtx(next);

    CGAL_assertion(lcc.is_valid());
}

void LCC_HL::a_36(std::size_t face_id){
    Dart_handle d = lcc.dart_of_attribute<2>(map_cell[face_id]);
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);
    std::size_t v7;
    if(is_pillowing_marked(v[3])){
        v7 = split_edge(v[1],v[2]);
        unmark_pillowing(v[3]);
    }else{
        v7 = split_edge(v[2],v[3]);
        unmark_pillowing(v[1]);
    }
    update_nc(v7);
}

void LCC_HL::a_37(std::size_t face_id){
    Dart_handle d = lcc.dart_of_attribute<2>(map_cell[face_id]);
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);
    std::size_t v7 = split_edge(v[0],v[1]);
    update_nc(v7);
}

void LCC_HL::a_38(std::size_t face_id){
    Dart_handle d = lcc.dart_of_attribute<2>(map_cell[face_id]);
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);
    std::size_t v7 = split_edge(v[1],v[2]);
    std::size_t v8 = split_edge(v[2],v7);

    set_point_from_vtx(v7,v[1],v[2],0.33);
    set_point_from_vtx(v8,v[2],v[1],0.33);
    update_nc(v7);
    update_nc(v8);
}

void LCC_HL::a_39(std::size_t face_id){
    Dart_handle d = lcc.dart_of_attribute<2>(map_cell[face_id]);
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);
    std::size_t v7 = split_edge(v[0],v[1]);
    update_nc(v7);
}

void LCC_HL::a_40(std::size_t face_id){
    Dart_handle d = lcc.dart_of_attribute<2>(map_cell[face_id]);
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);
    split_cell(v[1],v[5]);
    unmark_orb_conformite_vec({v[1],v[5]});
}

void LCC_HL::a_41(std::size_t face_id){
    Dart_handle d = lcc.dart_of_attribute<2>(map_cell[face_id]);
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);
    std::size_t vx = split_edge(v[1],v[0]);
    update_nc(vx);
}

void LCC_HL::a_42(std::size_t face_id){
    Dart_handle d = lcc.dart_of_attribute<2>(map_cell[face_id]);
    std::vector <std::size_t> v = get_vertex_of_cell(face_id);
    split_cell(v[9],v[4]);
    split_cell(v[10],v[3]);
    unmark_orb_conformite_vec({v[9],v[4],v[10],v[3]});
}

void LCC_HL::c_3(std::size_t face_id) {
    std::vector <std::size_t> vertex = get_vertex_of_cell(face_id);
    std::size_t new_cell;
    assert(vertex.size() == 4);
    std::size_t a = split_edge(vertex[0], vertex[1]);
    std::size_t b = split_edge(vertex[2], vertex[3]);
    new_cell = split_cell_d(face_id, a, b);
    set_field(a, 3, !in_boundary(a));
    set_field(b, 3, !in_boundary(b));
}

void LCC_HL::c_4(std::size_t face_id) {
    std::vector <std::size_t> vertex = get_vertex_of_cell(face_id);
    assert(vertex.size() == 4);
    std::vector <std::size_t> v;
    std::size_t a = split_edge(vertex[0], vertex[1]);
    std::size_t b = split_edge(vertex[2], vertex[3]);
    std::size_t c = split_edge(vertex[1], vertex[2]);
    std::size_t d = split_edge(vertex[3], vertex[0]);

    std::size_t c_a = split_cell_d(face_id, a, b);
    std::size_t e = split_edge(a, b);
    auto va = get_vertex_of_cell(face_id);
    auto vb = get_vertex_of_cell(c_a);
    std::size_t cell_a = face_id, cell_b = c_a;
    if (std::find(va.begin(), va.end(), d) != va.end() && std::find(va.begin(), va.end(), e) != va.end()) {
        cell_a = c_a;
        cell_b = face_id;
    }
    std::cout << "cell_a " << cell_a << " cell_b " << cell_b << std::endl;
    split_cell_d(cell_b, d, e);
    split_cell_d(cell_a, c, e);
    set_field(e, 3, 0);
    set_field(a, 3, 1 && !in_boundary(a));
    set_field(b, 3, 1 && !in_boundary(b));
    set_field(c, 3, 1 && !in_boundary(c));
    set_field(d, 3, 1 && !in_boundary(d));
}

void LCC_HL::c_5(std::size_t face_id) {
    std::vector <std::size_t> vertex = get_vertex_of_cell(face_id);
    assert(vertex.size() == 4);
    std::size_t side_edg = vertex[vertex.size() - 1];
    vertex.push_back(vertex[0]);
    pillowing(vertex, side_edg, 0.3, 0);
}

//! Non conforme
void LCC_HL::nc_5(std::size_t face_id) {
    std::vector <std::size_t> vertex = get_vertex_of_cell(face_id);
    assert(vertex.size() == 5);
    Dart_handle dh, d, target, dcrit;
    std::vector <std::size_t> nc_vtx = get_non_conform_vertex(face_id);
    std::cout << " " << nc_vtx.size() << " " << nc_vtx[0] << std::endl;
    assert(nc_vtx.size() == 1);
    dh = lcc.dart_of_attribute<2>(map_cell[face_id]);
    dh = lcc.dart_of_attribute<2>(map_cell[face_id]);
    std::cout << "Before" << std::endl;
    for (LCC_2::Dart_of_cell_range<2>::iterator it =
            lcc.darts_of_cell<2>(dh).begin(),
                 itend = lcc.darts_of_cell<2>(dh).end(); it != itend; ++it) {
        if (nc_vtx[0] == get_id(it)) {
            d = it;
            break;
        }
    }
    std::cout << "After" << std::endl;
    target = lcc.alpha<0, 1, 0, 1>(d);
    Point a, b;
    a = lcc.point_of_vertex_attribute(lcc.vertex_attribute(target));
    b = lcc.point_of_vertex_attribute(lcc.vertex_attribute(lcc.alpha<0>(target)));
    // Mean of the two points
    Vertex_attribute_handle vh = create_vertex_handle(Point((a.x() + b.x()) / 2, (a.y() + b.y()) / 2));
    lcc.insert_cell_0_in_cell_1(target, vh);
    set_field(vh->info()[0], 3, !in_boundary(vh->info()[0]));
    set_field(lcc.vertex_attribute(dh)->info()[0], 3, !in_boundary(vh->info()[0]));
    split_cell_d(face_id, nc_vtx[0], vh->info()[0]);
    assert(lcc.is_valid());
}

void LCC_HL::nc_6(std::size_t face_id) {
    std::vector <std::size_t> vertex = get_vertex_of_cell(face_id);
    std::cout << vertex.size() << std::endl;
    //assert(vertex.size() == 6 );
    std::vector <std::size_t> nc_vtx = get_non_conform_vertex(face_id);
    std::vector <std::size_t> common_vertex(vertex.size());
    //assert(nc_vtx.size() == 2);
    // Todo remove
    if (is_neigh(nc_vtx[0], nc_vtx[1]) || vertex.size() == 6 || nc_vtx.size() == 2) {
        return;
    }

    std::vector <std::size_t> nb_a, nb_b;
    std::cout << "Here" << std::endl;
    nb_a = neigborhood(nc_vtx[0]);
    std::cout << "Here" << std::endl;
    nb_b = neigborhood(nc_vtx[1]);

    std::sort(nb_a.begin(), nb_a.end());
    std::sort(nb_b.begin(), nb_b.end());
    //iterator to store return type
    std::vector<std::size_t>::iterator it;
    it = set_intersection(nb_a.begin(), nb_a.end(), nb_b.begin(), nb_b.end(), common_vertex.begin());
    common_vertex.resize(it - common_vertex.begin());

    if (common_vertex.size()) {
        std::cout << "6a" << std::endl;
        nc_6b(face_id);
    } else {
        std::cout << "6b" << std::endl;
        nc_6a(face_id);
    }

}


void LCC_HL::nc_6a(std::size_t face_id) {
    std::vector <std::size_t> vertex = get_vertex_of_cell(face_id);
    assert(vertex.size() == 6);
    std::vector <std::size_t> nc_vtx = get_non_conform_vertex(face_id);
    assert(nc_vtx.size() == 2);
    split_cell_d(face_id, nc_vtx[0], nc_vtx[1]);
}

void LCC_HL::nc_6b(std::size_t face_id) {
    std::vector <std::size_t> vertex = get_vertex_of_cell(face_id);
    assert(vertex.size() == 6);
    std::vector <std::size_t> nc_vtx = get_non_conform_vertex(face_id);
    assert(nc_vtx.size() == 2);
    std::size_t new_cell, new_vtx, target;
    // Get the target vtx_id and a dart
    Dart_handle d = lcc.dart_of_attribute<2>(map_cell[face_id]), d_target;
    bool found = false;
    for (LCC_2::Dart_of_cell_range<2>::iterator it =
            lcc.darts_of_cell<2>(d).begin(),
                 itend = lcc.darts_of_cell<2>(d).end(); it != itend; ++it) {
        target = lcc.info<0>(lcc.alpha<0, 1, 0, 1, 0>(it))[0];

        if ((target == nc_vtx[0]) || (target == nc_vtx[1])) {
            found = true;
            d_target = it;
            break;
        }
    }
    assert(found);

    new_cell = split_cell_d(face_id, nc_vtx[0], nc_vtx[1]);
    new_vtx = split_edge(nc_vtx[0], nc_vtx[1]);


    Dart_handle dh = lcc.dart_of_attribute<2>(map_cell[lcc.info<2>(d_target)[0]]);
    std::cout << "Before" << std::endl;
    CGAL::draw(lcc);
    found = false;
    for (LCC_2::Dart_of_cell_range<2>::iterator it =
            lcc.darts_of_cell<2>(dh).begin(),
                 itend = lcc.darts_of_cell<2>(dh).end(); it != itend; ++it) {
        target = lcc.info<0>(lcc.alpha<0, 1, 0, 1, 0>(it))[0];
        if (target == new_vtx) {
            found = true;
            target = lcc.info<0>(it)[0];
            break;
        }
    }
    assert(found);

    std::cout << "Last split " << new_cell << " " << new_vtx << " " << target << " " << lcc.info<2>(d_target)[0]
              << std::endl;
    split_cell_d(lcc.info<2>(d_target)[0], new_vtx, target);
    set_field(nc_vtx[0], 3, 0);
    set_field(nc_vtx[1], 3, 0);
    set_field(new_vtx, 3, 0);
}

void LCC_HL::nc_7(std::size_t face_id) {
    std::vector <std::size_t> vertex = get_vertex_of_cell(face_id);
    //assert(vertex.size() == 7);
    std::vector <std::size_t> nc_vertex = get_non_conform_vertex(face_id);
    //assert(nc_vertex.size() == 3);
    assert(lcc.is_valid());
    Dart_handle dh = lcc.dart_of_attribute<2>(map_cell[face_id]);
    std::size_t a, b;
    for (LCC_2::Dart_of_cell_range<2>::iterator it = lcc.darts_of_cell<2>(dh).begin(), itend = lcc.darts_of_cell<2>(
            dh).end(); it != itend; ++it) {
        // Si it conforme et
        // Si voisin 0  est conforme
        a = lcc.info<0>(it)[0];
        if (!lcc.info<0>(it)[3] && !lcc.info<0>(lcc.alpha<0>(it))[3]) {
            b = lcc.info<0>(lcc.alpha<0>(it))[0];
            break;
        } else if (!lcc.info<0>(it)[3] && !lcc.info<0>(lcc.alpha<1, 0>(it))[3]) {// Si voisin 1  conforme
            b = lcc.info<0>(lcc.alpha<1, 0>(it))[0];
            break;
        }
    }
    std::cout << a << " " << b << std::endl;
    split_edge(a, b);
    nc_vertex = get_non_conform_vertex(face_id);
    Dart_handle d_a, d_b, d_c, d_d;
    for (LCC_2::Dart_of_cell_range<2>::iterator it = lcc.darts_of_cell<2>(dh).begin(), itend = lcc.darts_of_cell<2>(
            dh).end(); it != itend; ++it) {
        if (!lcc.info<0>(lcc.alpha<0>(it))[3]) {
            d_a = it;
            d_b = lcc.alpha<0, 1, 0>(d_a);
            d_c = lcc.alpha<1, 0, 1, 0>(d_b);
            d_d = lcc.alpha<1, 0, 1, 0>(d_a);
            break;
        }
    }
    /// a / d
    std::size_t e;
    split_cell_d(face_id, lcc.info<0>(d_a)[0], lcc.info<0>(d_c)[0]);
    std::cout << "s1" << std::endl;
    std::cout << lcc.info<0>(d_a)[0] << " " << lcc.info<0>(d_b)[0] << " " << lcc.info<0>(d_c)[0] << " "
              << lcc.info<0>(d_d)[0] << std::endl;
    ///
    e = split_edge(lcc.info<0>(d_a)[0], lcc.info<0>(d_c)[0]);
    split_cell_d(lcc.info<2>(d_b)[0], e, lcc.info<0>(d_b)[0]);
    std::cout << "s2" << std::endl;
    ///
    split_cell_d(lcc.info<2>(d_d)[0], e, lcc.info<0>(d_d)[0]);
    std::cout << "s3" << std::endl;
}

void LCC_HL::nc_8(std::size_t face_id) {
    std::vector <std::size_t> vertex = get_vertex_of_cell(face_id);
    assert(vertex.size() == 8);
    std::vector <std::size_t> nc_vertex = get_non_conform_vertex(face_id);
    assert(nc_vertex.size() == 4);
    Dart_handle dh = lcc.dart_of_attribute<2>(map_cell[face_id]);
    Dart_handle d_a, d_b, d_c, d_d;
    for (LCC_2::Dart_of_cell_range<2>::iterator it = lcc.darts_of_cell<2>(dh).begin(), itend = lcc.darts_of_cell<2>(
            dh).end(); it != itend; ++it) {
        if (!lcc.info<0>(lcc.alpha<0>(it))[3]) {
            d_a = it;
            d_b = lcc.alpha<0, 1, 0>(d_a);
            d_c = lcc.alpha<1, 0, 1, 0>(d_b);
            d_d = lcc.alpha<1, 0, 1, 0>(d_a);
            break;
        }
    }
    split_cell_d(face_id, lcc.info<0>(d_a)[0], lcc.info<0>(d_c)[0]);
    ///
    std::size_t e = split_edge(lcc.info<0>(d_a)[0], lcc.info<0>(d_c)[0]);
    split_cell_d(lcc.info<2>(d_b)[0], e, lcc.info<0>(d_b)[0]);
    ///
    split_cell_d(lcc.info<2>(d_d)[0], e, lcc.info<0>(d_d)[0]);
}


void LCC_HL::c_9(std::size_t face_id) {
    Dart_handle dh = lcc.dart_of_attribute<2>(map_cell[face_id]);
    // to d
    // place
    Dart_handle d = lcc.alpha<1, 0, 1>(dh);
    Dart_handle dx, dy;


}

//! Cells type
std::vector <std::size_t> LCC_HL::critical_cells() {
    std::vector <std::size_t> cells, res, vtx;
    cells = get_cells_id();
    bool run = true;
    for (auto it = cells.begin(); it != cells.end() && run; ++it) {
        vtx = get_vertex_of_cell(*it);
        for (auto v_it = vtx.begin(); v_it != vtx.end() && run; ++v_it) {
            if (get_field(*v_it, 1)) {
                res.push_back(*it);
                break;
            }
        }
    }
    return res;
}

std::vector <std::size_t> LCC_HL::conform_cells() {
    std::vector <std::size_t> cells, res, vtx;
    cells = get_cells_id();
    bool run = true;
    for (auto it = cells.begin(); it != cells.end() && run; ++it) {
        vtx = get_vertex_of_cell(*it);
        for (auto v_it = vtx.begin(); v_it != vtx.end() && run; ++v_it) {
            if (get_field(*v_it, 3)) {
                res.push_back(*it);
                break;
            }
        }
    }
    return res;
}

std::vector <std::size_t> LCC_HL::non_conform_cells() {
    std::vector <std::size_t> cells, res, vtx;
    cells = get_cells_id();
    bool run = true;
    for (auto it = cells.begin(); it != cells.end() && run; ++it) {
        vtx = get_vertex_of_cell(*it);
        for (auto v_it = vtx.begin(); v_it != vtx.end() && run; ++v_it) {
            if (!get_field(*v_it, 3)) {
                res.push_back(*it);
                break;
            }
        }
    }
    return res;
}

void LCC_HL::cells_connected_components() {

    using namespace boost;{

        struct Vertex {
            std::size_t id;
        };
        using graph_t = adjacency_list<listS, vecS, directedS, Vertex>;
        using vertex_t = graph_traits<graph_t>::vertex_descriptor;

        graph_t G;
        std::vector <std::size_t> cells, vertex, neigh_cells, neigh_vertex, intersect;
        std::vector<std::size_t>::iterator pos;
        cells = get_cells_id();
        std::map <std::size_t, std::size_t> id_cell;
        std::size_t count = 0;
        vertex_t tmp;
        for (auto it = cells.begin(); it != cells.end(); ++it) {
            tmp = boost::add_vertex(Vertex{*it}, G);
            id_cell.insert(std::pair<std::size_t, std::size_t>(*it, count));
            count++;
        }
        // Building the cell graph
        for (auto it = cells.begin(); it != cells.end(); ++it) {
            vertex = get_vertex_of_cell(*it);
            std::sort(vertex.begin(), vertex.end());
            neigh_cells = neighbor_cells(*it);
            for (auto c_it = neigh_cells.begin(); c_it != neigh_cells.end(); ++c_it) {
                neigh_vertex = get_vertex_of_cell(*c_it);
                intersect.reserve(vertex.size() + neigh_vertex.size());
                std::sort(neigh_vertex.begin(), neigh_vertex.end());
                pos = std::set_intersection(vertex.begin(), vertex.end(), neigh_vertex.begin(), neigh_vertex.end(),
                                            intersect.begin());
                intersect.resize(pos - intersect.begin());
                if (intersect.size() == 1) {
                    add_edge(id_cell[*it], id_cell[*c_it], G);
                }
                intersect.clear();
            }
        }

        std::vector<int> component(num_vertices(G));
        int num = connected_components(G, &component[0]);
        boost::write_graphviz(std::cout, G, [&](auto &out, auto v) {
            out << "[label=\"" << G[v].id << "\"]";
        });
        /*
        boost::write_graphviz(std::cout, g, [&] (auto& out, auto v) {
                                  out << "[label=\"" << g[v].foo << "\"]";
                              },
                              [&] (auto& out) {
                                  out << "[label=\"" << g[e].bla << "\"]";
                              });
        */


        std::vector<int>::size_type i;
        std::cout << "Total number of components: " << num << std::endl;
        for (i = 0; i != component.size(); ++i)
            std::cout << "Vertex " << i << " is in component " << component[i] << std::endl;
        std::cout << std::endl;
    }
}

void placer_conformite(Dart_handle dh, std::string observation_conformite) {
}

void placer_criticite(Dart_handle dh, std::string observation_criticite) {

}


LCC_HL::LCC_HL(std::string observation_conformity, std::string observation_criticity) {
    Dart_handle dh, dtmp;
    this->current_id = 0;
    this->current_face_id = 0;
    std::size_t nbvtx = observation_conformity.size();
    if (observation_conformity.size() <= 4)
        nbvtx = 4;

    dh = this->lcc.make_combinatorial_polygon(nbvtx);

    std::size_t id;
    std::vector <Point> vp = {Point(0.0, 0.0), Point(0.0, 10.0), Point(10.0, 10.0), Point(10.0, 0.0)};
    std::size_t vp_index = 0, last = 0;
    std::vector <Point> truepoint;

    for (int i = 0; i < observation_conformity.size(); i++) {
        if (observation_conformity[i] == '0') {
            truepoint.push_back(vp[vp_index]);
            last = vp_index;
            vp_index++;
        } else if (i + 1 <= observation_conformity.size() && observation_conformity[i + 1] == '1') {
            truepoint.push_back(compute_point(vp[last], vp[(last + 1) % vp.size()], 0.33));
            truepoint.push_back(compute_point(vp[last], vp[(last + 1) % vp.size()], 0.66));
            i++;
        } else {
            truepoint.push_back(compute_point(vp[last], vp[(last + 1) % vp.size()], 0.5));
        }
    }

    /**Allocating mark**/

    try {
        this->conformity_mark = lcc.get_new_mark();
    }
    catch (LCC_2::Exception_no_more_available_mark) {
        std::cerr << "No more free mark, exit." << std::endl;
        exit(-1);
    }
    try {
        this->priority_mark = lcc.get_new_mark();
    }
    catch (LCC_2::Exception_no_more_available_mark) {
        std::cerr << "No more free mark, exit." << std::endl;
        exit(-1);
    }
    try {
        this->pillowing_mark = lcc.get_new_mark();
    }
    catch (LCC_2::Exception_no_more_available_mark) {
        std::cerr << "No more free mark, exit." << std::endl;
        exit(-1);
    }
    try {
        this->ghost_layer_mark = lcc.get_new_mark();
    }
    catch (LCC_2::Exception_no_more_available_mark) {
        std::cerr << "No more free mark, exit." << std::endl;
        exit(-1);
    }
    if (nbvtx == 4)
        truepoint = vp;
    char tmp_char;
    auto fhandle = create_face_handle();
    lcc.set_attribute<2>(dh, fhandle);
    std::cout << lcc.darts_of_orbit<0, 1>(dh).size() << std::endl;
    dtmp = dh;
    std::size_t count = 0;
    Vertex_attribute_handle tmp;
    // Liens entre points et brins
    do {
        tmp = create_vertex_handle(truepoint[count]);
        lcc.set_attribute<0>(dtmp, tmp);
        lcc.set_attribute<0>(lcc.alpha<1>(dtmp), tmp);
        std::cout << lcc.info<0>(dtmp)[0] << std::endl;

        //update_nc(lcc.info<0>(dtmp)[0]);
        //update_nc()
        dtmp = lcc.alpha<0, 1>(dtmp);
        std::cout << count << std::endl;
        count++;
    } while (dtmp != dh);
    // Placement des non conformités
    count = 0;
    dtmp = dh;
    if (nbvtx > 4) {
        do {
            // conformity_mark
            if (observation_conformity[count] == '1') {
                update_nc(lcc.info<0>(dtmp)[0]);
            }
            dtmp = lcc.alpha<0, 1>(dtmp);
            count++;
        } while (dtmp != dh);
    }
    // Placement des criticités
    if (observation_criticity.size()) {
        count = 0;
        dtmp = dh;
        do {
            // conformity_mark
            if (observation_conformity[count] != '1') {
                // Set criticity
                tmp_char = observation_criticity[count];
                set_constraint_vtx(lcc.info<0>(dtmp)[0], atoi(&tmp_char));
                //set_constraint_vtx(lcc.info<0>(dtmp)[0],std::stoi(&observation_criticity[count]));
                count++;
            }
            dtmp = lcc.alpha<0, 1>(dtmp);
        } while (dtmp != dh);
    }
    load_maps();
    //representation_unique(0);
}

LCC_HL::LCC_HL(std::vector<float> x, std::vector<float> y, std::vector <std::vector<std::size_t>> adj,
               std::vector <std::size_t> constraints) {
    std::size_t id;
    this->lcc = LinearCellComplexBuilder::grid_like(x, y, adj, constraints);
    this->show = false;
    this->current_id = 0;
    /** Vertex Map **/
    for (LCC_2::Vertex_attribute_range::iterator
                 it = this->lcc.vertex_attributes().begin(),
                 itend = this->lcc.vertex_attributes().end();
         it != itend; ++it) {
        id = get_id(it);
        lcc.info_of_attribute<0>(it) = {id, get_constraint(it), 0, 0};
        this->map_vtx.insert(std::pair<std::size_t, LCC_2::Vertex_attribute_handle>(id, it));
        this->current_id = std::max(id, this->current_id);
    }
    for (LCC_2::One_dart_per_cell_range<0, 1>::iterator
                 it = this->lcc.one_dart_per_cell<0, 1>().begin(),
                 itend = this->lcc.one_dart_per_cell<0, 1>().end(); it != itend; ++it) {
        set_map_nc(it, 0);
    }

    this->current_id++;
    /** Cell Map **/

    

    this->current_face_id = 0;
    for (LCC_2::Attribute_range<2>::type::iterator it = lcc.attributes<2>().begin(), itend = lcc.attributes<2>().end();
         it != itend; ++it) {
        auto v = lcc.info_of_attribute<2>(it);
        id = v[0];
        this->map_cell.insert(std::pair<std::size_t, LCC_2::Attribute_handle<2>::type>(id, it));
        this->current_face_id++;
    }

    this->lcc.correct_invalid_attributes();

    std::vector <std::size_t> v;
    for (std::map<std::size_t, Vertex_attribute_handle>::iterator it(this->map_vtx.begin()), itend(this->map_vtx.end());
         it != itend; ++it) {
        std::cout << it->first << " ";
        v = lcc.info_of_attribute<0>(it->second);
        lcc.info_of_attribute<0>(it->second)[2] = this->valence(it->first);
    }
    std::cout << "\nATTR<0>LCC_HL\n" << std::endl;
    for (LCC_2::Attribute_range<0>::type::iterator it = lcc.attributes<0>().begin(), itend = lcc.attributes<0>().end();
         it != itend; ++it) {
        v = lcc.info_of_attribute<0>(it);
        std::cout << v[0] << " " << v[1] << std::endl;
    }
    std::cout << "\nMAP<0>LCC_HL\n" << std::endl;
    for (std::map<std::size_t, Vertex_attribute_handle>::iterator it(this->map_vtx.begin()), itend(this->map_vtx.end());
         it != itend; ++it) {
        v = lcc.info_of_attribute<0>(it->second);
        std::cout << v[0] << " " << v[1] << std::endl;
    }
    try {
        conformity_mark = lcc.get_new_mark();
    }
    catch (LCC_2::Exception_no_more_available_mark) {
        std::cerr << "No more free mark, exit." << std::endl;
        exit(-1);
    }
    try {
        priority_mark = lcc.get_new_mark();
    }
    catch (LCC_2::Exception_no_more_available_mark) {
        std::cerr << "No more free mark, exit." << std::endl;
        exit(-1);
    }

    try {
        pillowing_mark = lcc.get_new_mark();
    }
    catch (LCC_2::Exception_no_more_available_mark) {
        std::cerr << "No more free mark, exit." << std::endl;
        exit(-1);
    }
    try {
        this->selection = lcc.get_new_mark();
    }

    catch (LCC_2::Exception_no_more_available_mark) {
        std::cerr << "No more free mark, exit." << std::endl;
        exit(-1);
    }
    try {
        this->ghost_layer_mark = lcc.get_new_mark();
    }
    catch (LCC_2::Exception_no_more_available_mark) {
        std::cerr << "No more free mark, exit." << std::endl;
        exit(-1);
    }
    load_maps();
}


std::size_t LCC_HL::get_id(Dart_handle dh) {
    auto v = lcc.info_of_attribute<0>(lcc.vertex_attribute(dh));

    return v[0];
}

std::size_t LCC_HL::get_constraint(Dart_handle dh) {
    auto v = lcc.info_of_attribute<0>(lcc.vertex_attribute(dh));
    return v[1];
}

std::size_t LCC_HL::get_constraint(std::size_t vtx) {
    std::size_t contrainte = 0;
    if( this->map_vtx.find(vtx) != this->map_vtx.end() )
        contrainte= get_constraint(this->map_vtx[vtx]);
    return contrainte;
}

std::size_t LCC_HL::get_id(Vertex_attribute_handle vah) {
    auto v = lcc.info_of_attribute<0>(vah);
    return v[0];
}

std::size_t LCC_HL::get_constraint(Vertex_attribute_handle vah) {
    auto v = lcc.info_of_attribute<0>(vah);
    return v[1];
}

/**0 id 1 constraint 2 previous value */
std::size_t LCC_HL::get_field(std::size_t vtx_id, std::size_t rank) {
    auto v = lcc.info_of_attribute<0>(map_vtx[vtx_id]);
    assert(v.size() > rank);
    return v[rank];
}

std::vector <std::size_t> LCC_HL::get_fields(std::size_t vtx_id) {
    auto v = lcc.info_of_attribute<0>(map_vtx[vtx_id]);
    return v;
}

/** Not the id field 0 1 unused crit 2 conformity 3  */

void LCC_HL::set_field(std::size_t vtx_id, std::size_t rank, std::size_t value) {
    auto v = lcc.info_of_attribute<0>(map_vtx[vtx_id]);
    assert((v.size() > rank) && (rank > 0));
    v[rank] = value;
    lcc.info_of_attribute<0>(map_vtx[vtx_id]) = v;
}

/**Dangerous func**/
void LCC_HL::set_fields(std::size_t vtx_id, std::vector <std::size_t> vnew) {
    auto v = lcc.info_of_attribute<0>(map_vtx[vtx_id]);
    std::copy(vnew.begin(), vnew.end(), std::next(v.begin()));
}

LCC_HL::LCC_HL(const LCC_2 &lcc) {
    this->current_id = 0;
    assert(lcc.is_valid());
    this->lcc = lcc;
    this->show = false;
    std::size_t id;
    for (LCC_2::Vertex_attribute_range::iterator
                 it = this->lcc.vertex_attributes().begin(),
                 itend = this->lcc.vertex_attributes().end();
         it != itend; ++it) {
        id = get_id(it);
        this->map_vtx.insert(std::pair<std::size_t, LCC_2::Vertex_attribute_handle>(id, it));
        this->current_id++;
    }
    try {
        this->ghost_layer_mark = lcc.get_new_mark();
    }
    catch (LCC_2::Exception_no_more_available_mark) {
        std::cerr << "No more free mark, exit." << std::endl;
        exit(-1);
    }
}


void LCC_HL::reset() {
    this->map_vtx.clear();
    lcc.free_mark(conformity_mark);
    this->lcc.clear();
    this->current_id = 0;
    assert(lcc.is_valid());
    this->lcc = LinearCellComplexBuilder::basic_modelf(false);
    this->show = false;
    std::size_t id;
    for (LCC_2::Vertex_attribute_range::iterator
                 it = this->lcc.vertex_attributes().begin(),
                 itend = this->lcc.vertex_attributes().end();
         it != itend; ++it) {
        id = get_id(it);
        this->map_vtx.insert(std::pair<std::size_t, LCC_2::Vertex_attribute_handle>(id, it));
        this->current_id++;
    }
}


void LCC_HL::draw() {
    if (this->show) {
        CGAL::draw(this->lcc, "Viewer");
    }
}

void LCC_HL::display() {
    this->lcc.display_characteristics(std::cout);
    std::cout << ",valid=" << lcc.is_valid() << std::endl;
}

Vertex_attribute_handle LCC_HL::create_vertex_handle(Point p) {
    std::pair<std::map<std::size_t, Vertex_attribute_handle>::iterator, bool> ret;
    Vertex_attribute_handle vh = this->lcc.create_vertex_attribute(p);
    // id, constraint, previous valence, NC (non conform)
    lcc.info_of_attribute<0>(vh) = std::vector<std::size_t>({this->current_id, 0, 0, 0});
    for (std::map<std::size_t, Vertex_attribute_handle>::iterator it(this->map_vtx.begin()), itend(this->map_vtx.end());
         it != itend; ++it) {
        std::cout << it->first << " ";
    }
    std::cout << this->current_id << std::endl;
    ret = this->map_vtx.insert(std::pair<std::size_t, LCC_2::Vertex_attribute_handle>(this->current_id, vh));
    assert(ret.second);
    this->current_id++;
    return vh;
}

LCC_2::Attribute_handle<2>::type LCC_HL::create_face_handle() {
    LCC_2::Attribute_handle<2>::type fh = this->lcc.create_attribute<2>();
    lcc.info_of_attribute<2>(fh) = {this->current_face_id};
    this->map_cell.insert(std::pair<std::size_t, LCC_2::Attribute_handle<2>::type>(this->current_face_id, fh));
    this->current_face_id++;
    return fh;
}

void LCC_HL::set_draw(bool show) {
    this->show = show;
}

void LCC_HL::erase_vertex(std::size_t id) {
    lcc.erase_vertex_attribute(this->map_vtx[id]);
    this->map_vtx.erase(id);
}

void LCC_HL::erase_cell(std::size_t id_cell) {
    lcc.erase_attribute<2>(this->map_cell[id_cell]);
    this->map_cell.erase(id_cell);
}

Vertex_attribute_handle LCC_HL::vertex_handle_of_vertex_id(std::size_t id) {
    return this->map_vtx[id];
}

Dart_handle LCC_HL::dart_of_vertex_id(std::size_t id) {
    return this->lcc.dart_of_attribute<0>(this->map_vtx[id]);
}

std::vector <size_t> LCC_HL::get_cells_id() {
    std::vector <size_t> v_res;
    for (LCC_2::Attribute_range<2>::type::iterator it = lcc.attributes<2>().begin(), itend = lcc.attributes<2>().end();
         it != itend; ++it) {
        v_res.push_back(lcc.info_of_attribute<2>(it)[0]);
    }
    /*for(LCC_2::One_dart_per_cell_range<2>::iterator it = lcc.one_dart_per_cell<2>().begin(), itend = it = lcc.one_dart_per_cell<2>().begin() ; it !=itend ; ++it){
        v_res.push_back(lcc.info())
    }*/
    return v_res;
}


void LCC_HL::set_vertex_of_cell_of_attr(std::size_t face_id, std::size_t vertex_id) {
    LCC_2::Attribute_handle<2>::type at = this->map_cell[face_id];
    Dart_handle dh = lcc.dart_of_attribute<2>(at), dtmp = lcc.alpha<0, 1>(dh);
    if (lcc.info<0>(dh)[0] != vertex_id) {
        bool run = true;
        while ((dh != dtmp) && run) {
            if (lcc.info<0>(dtmp)[0] == vertex_id) {
                run = false;
                lcc.set_dart_of_attribute<2>(at, dtmp);
            } else {
                dtmp = lcc.alpha<0, 1>(dtmp);
            }
        }
    }

}

std::vector <std::size_t> LCC_HL::get_vertex_of_cell(std::size_t id_cell) {
    LCC_2::Attribute_handle<2>::type at = this->map_cell[id_cell];
    Dart_handle dh = lcc.dart_of_attribute<2>(at), dtmp;
    std::vector <std::size_t> vertex_ids;
    dtmp = lcc.alpha<0, 1>(dh);
    vertex_ids.push_back(lcc.info<0>(dh)[0]);
    while (dh != dtmp) {
        vertex_ids.push_back(lcc.info<0>(dtmp)[0]);
        dtmp = lcc.alpha<0, 1>(dtmp);
    }
    return vertex_ids;
}


std::vector <std::size_t> LCC_HL::neighbor_cells(std::size_t id_cell) {
    Dart_handle dh = lcc.dart_of_attribute<2>(map_cell[id_cell]);
    std::unordered_set <std::size_t> set_neigh;
    LCC_2::Attribute_handle<2>::type curr;
    std::size_t id;
    for (LCC_2::One_dart_per_incident_cell_range<1, 2>::iterator it =
            lcc.one_dart_per_incident_cell<1, 2>(dh).begin(),
                 itend = lcc.one_dart_per_incident_cell<1, 2>(dh).end(); it != itend; ++it) {
        auto v = lcc.info_of_attribute<2>(lcc.attribute<2>(lcc.alpha<2>(it)));
        if (it != lcc.alpha<2>(it))
            set_neigh.insert(v[0]);
    }
    std::vector <std::size_t> res(set_neigh.size());
    std::copy(set_neigh.begin(), set_neigh.end(), res.begin());
    return res;
}

std::vector <std::size_t> LCC_HL::iteration_vertex(std::size_t id_vertex) {
    //std::cout << "id_vertex " << id_vertex << std::endl;
    // Marks
    std::map<std::size_t, bool> visited;
    // Queue
    std::queue <std::size_t> queue;
    // Results
    std::vector <std::size_t> res;
    // Neighbors
    std::vector <std::size_t> neighbors;
    // Current vertice
    std::size_t cur;
    bool b_cur = true;
    queue.push(id_vertex);
    visited[id_vertex] = true;
    while (!queue.empty()) {
        cur = queue.front();
        //std::cout << "cur " << cur << std::endl;
        queue.pop();
        res.push_back(cur);
        neighbors = neigborhood(cur);
        for (std::vector<std::size_t>::iterator it = neighbors.begin(); it != neighbors.end(); ++it) {
            //Exist
            //std::cout << "neig " << *it << std::endl;
            if (visited.count(*it) == 0) {
                visited[*it] = true;
                queue.push(*it);
            }
        }
    }
    return res;
}

std::vector <std::size_t> LCC_HL::iteration_cell(std::size_t id_cell) {
    //std::cout << "id_cell " << id_cell << std::endl;
    // Marks
    std::map<std::size_t, bool> visited;
    // Queue
    std::queue <std::size_t> queue;
    // Results
    std::vector <std::size_t> res;
    // Neighbors
    std::vector <std::size_t> neighbors;
    // Current vertice
    std::size_t cur;
    bool b_cur = true;
    queue.push(id_cell);
    visited[id_cell] = true;
    while (!queue.empty()) {
        cur = queue.front();
        //std::cout << "cur " << cur << std::endl;
        queue.pop();
        res.push_back(cur);
        neighbors = neighbor_cells(cur);
        for (std::vector<std::size_t>::iterator it = neighbors.begin(); it != neighbors.end(); ++it) {
            //Exist
            //std::cout << "neig " << *it << std::endl;
            if (visited.count(*it) == 0) {
                visited[*it] = true;
                queue.push(*it);
            }
        }
    }
    return res;
}

std::vector <Dart_handle> LCC_HL::get_compressed_faces(std::size_t id) {
    Dart_handle d = dart_of_vertex_id(id);
    std::vector <Dart_handle> v;
    for (LCC_2::Dart_of_orbit_range<1, 2>::iterator
                 it(lcc.darts_of_orbit<1, 2>(d).begin()),
                 itend(lcc.darts_of_orbit<1, 2>(d).end()); it != itend; ++it) {
        if (lcc.alpha<0, 1, 0, 1>(it) == it) {
            v.push_back(it);
        }
    }
    return v;
}

std::vector <std::size_t> LCC_HL::get_all_vertex_ids() {
    std::vector <std::size_t> v;
    v.reserve(this->map_vtx.size());
    std::transform(this->map_vtx.begin(), this->map_vtx.end(),
                   std::back_inserter(v),
                   boost::bind(&Map_id_vtx::value_type::first, _1));
    return v;
}


std::vector <std::size_t> LCC_HL::neighbor(std::size_t id_vertex) {
    Dart_handle dh;
    std::unordered_set <std::size_t> set_neigh;
    LCC_2::Vertex_attribute_handle curr, other_vtx = lcc.vertex_attribute(dh);
    for (LCC_2::Dart_of_orbit_range<1, 2>::iterator
                 it(lcc.darts_of_orbit<1, 2>(dh).begin()),
                 itend(lcc.darts_of_orbit<1, 2>(dh).end()); it != itend; ++it) {
        set_neigh.insert(get_id(lcc.alpha<0>(it)));
    }
    std::vector <std::size_t> res(set_neigh.size());
    std::copy(set_neigh.begin(), set_neigh.end(), res.begin());
    return res;
}

bool LCC_HL::is_neigh(size_t id_a, size_t id_b) {
    Dart_handle d1, d2;
    try {
        d1 = this->lcc.dart_of_attribute<0>(this->map_vtx.at(id_a));
        d2 = this->lcc.dart_of_attribute<0>(this->map_vtx.at(id_b));
    } catch (const std::out_of_range &oor) {
        std::cerr << "Out of Range error: " << '\n';
    }
    LCC_2::Vertex_attribute_handle curr, other_vtx = lcc.vertex_attribute(d2);
    for (LCC_2::Dart_of_orbit_range<1, 2>::iterator
                 it(lcc.darts_of_orbit<1, 2>(d1).begin()),
                 itend(lcc.darts_of_orbit<1, 2>(d1).end()); it != itend; ++it) {

        curr = lcc.vertex_attribute(lcc.alpha<0>(lcc.dart_handle(*it)));
        if (curr == other_vtx) {
            return true;
        }
    }
    return false;
}


Dart_handle LCC_HL::darts_of_edge(std::size_t id_a, std::size_t id_b) {
    /** To get the other dart, just apply lcc.alpha<0> to dart returned
     * */
    Dart_handle d1, d2;
    d1 = this->lcc.dart_of_attribute<0>(this->map_vtx[id_a]);
    d2 = this->lcc.dart_of_attribute<0>(this->map_vtx[id_b]);
    LCC_2::Vertex_attribute_handle curr, other_vtx = lcc.vertex_attribute(d2);
    Dart_handle d1p;//, d2p;
    bool found = false;
    for (LCC_2::Dart_of_orbit_range<1, 2>::iterator
                 it(lcc.darts_of_orbit<1, 2>(d1).begin()),
                 itend(lcc.darts_of_orbit<1, 2>(d1).end()); it != itend; ++it) {

        curr = lcc.vertex_attribute(lcc.alpha<0>(lcc.dart_handle(*it)));
        if (curr == other_vtx) {
            d1p = lcc.dart_handle(*it);
            return d1p;
        }
    }
    std::cout << get_id(d1) << " " << get_id(d2) << std::endl;
    assert(found == true);

}

std::vector <std::size_t> LCC_HL::neigborhood(std::size_t id_vertex) {
    Dart_handle dh = dart_of_vertex_id(id_vertex);
    std::size_t id;
    std::unordered_set <std::size_t> set_neigh;
    // would prefer to std::prev() to not take the last element but not working
    /*for (LCC_2::One_dart_per_incident_cell_range<1,2>::iterator
                 it = lcc.one_dart_per_incident_cell<1,2>(dh).begin(),
                 itend = lcc.one_dart_per_incident_cell<1,2>(dh).end();
         it != itend; ++it) {
        id = get_id(it);
        //id = get_id(lcc.alpha<0>(it));
        //if (id != id_vertex) {
        v.push_back(id);
        //}
    }*/
    for (LCC_2::Dart_of_orbit_range<1, 2>::iterator
                 it(lcc.darts_of_orbit<1, 2>(dh).begin()),
                 itend(lcc.darts_of_orbit<1, 2>(dh).end()); it != itend; ++it) {
        id = get_id(lcc.alpha<0>(it));
        set_neigh.insert(id);
    }
    std::vector <std::size_t> res(set_neigh.size());
    std::copy(set_neigh.begin(), set_neigh.end(), res.begin());
    //v.pop_back();
    return res;
}

void LCC_HL::writer() {
    /// https://kitware.github.io/vtk-examples/site/VTKFileFormats/
    std::string filename;
    std::ofstream myfile("example.vtk");
    if (myfile.is_open()) {
        // Header
        myfile << "# vtk DataFile Version 2.0\n";
        // Title
        myfile << "# My Super Title\n";
        // Data type
        myfile << "ASCII\n";
        // Geometry Topology Type
        myfile << "DATASET UNSTRUCTURED_GRID\n\n";
        // Points
        myfile << "POINTS " << map_vtx.size() << " float\n";
        auto vtx_ids = get_all_vertex_ids();
        std::map <std::size_t, std::size_t> ids_index;
        auto cells_ids = get_cells_id();
        std::sort(vtx_ids.begin(), vtx_ids.end());
        std::sort(cells_ids.begin(), cells_ids.end());
        std::size_t cpt = 0;
        std::vector <std::size_t> vertex_of_cell;
        for (std::vector<std::size_t>::iterator it(vtx_ids.begin()), itend(vtx_ids.end()); it != itend; ++it) {
            std::cout << *it << " ";
            ids_index.insert(std::pair<std::size_t, std::size_t>(*it, cpt));
            Point p = point_of_vertex(*it);
            myfile << p.x() << " " << p.y() << " " << 0.0 << "\n";
            cpt++;
        }

        // Compute size
        std::size_t connexity = 0;
        for (std::vector<std::size_t>::iterator it(cells_ids.begin()), itend(cells_ids.end()); it != itend; ++it) {
            vertex_of_cell = get_vertex_of_cell(*it);
            connexity += vertex_of_cell.size();
        }

        myfile << "\nCELLS " << cells_ids.size() << " " << connexity + cells_ids.size() << "\n"; /// Connexity

        std::cout << std::endl;
        for (std::vector<std::size_t>::iterator it(cells_ids.begin()), itend(cells_ids.end()); it != itend; ++it) {
            std::cout << *it << " ";
        }

        std::cout << std::endl;
        for (std::vector<std::size_t>::iterator it(cells_ids.begin()), itend(cells_ids.end()); it != itend; ++it) {
            vertex_of_cell = get_vertex_of_cell(*it);
            myfile << vertex_of_cell.size() << " ";
            for (std::vector<std::size_t>::iterator it_v(vertex_of_cell.begin()), itend_v(vertex_of_cell.end());
                 it_v != itend_v; ++it_v) {
                myfile << ids_index.find(*it_v)->second << " ";
            }
            myfile << "\n";
        }
        std::cout << std::endl;
        myfile << "\nCELL_TYPES " << cells_ids.size() << "\n";
        //myfile << "1\n";    /// VERTEX
        for (std::vector<std::size_t>::iterator it(cells_ids.begin()), itend(cells_ids.end()); it != itend; ++it) {
            myfile << "7\n";    /// POLYGON
        }

        myfile << "\nPOINT_DATA " << vtx_ids.size() << "\n";
        myfile << "SCALARS scalars int 1" << "\n";

        // Ids
        myfile << "LOOKUP_TABLE  Point_id" << "\n";
        //lcc.dart_of_attribute<0>(dart_of_vertex_id(*it)) == lcc.null_dart_handle ;
        std::string red = "1 0 0", blue = "0 0 1", green = "0 1 0", black = "0 0 0", white = "1 1 1";
        for (std::vector<std::size_t>::iterator it(vtx_ids.begin()), itend(vtx_ids.end()); it != itend; ++it) {
            myfile << *it << "\n";
        }
        myfile << "\nLOOKUP_TABLE Point_id " << vtx_ids.size() << "\n";
        std::string temp;
        for (std::vector<std::size_t>::iterator it(vtx_ids.begin()), itend(vtx_ids.end()); it != itend; ++it) {
            Point p = lcc.point(dart_of_vertex_id(*it));

            std::cout << "id_vtx " << *it << " conformity " << is_orb_conform(*it) << " constraint "
                      << get_constraint(*it) << " " << p.x() << ", " << p.y() << std::endl;
            int criticity = get_constraint(*it);

            int conformity = is_orb_conform(*it);//get_field(*it, 3);
            int color_me = criticity;
            if (criticity == 0) {
                color_me = conformity;
                if (conformity != 0)
                    color_me++;
            }
            switch (color_me) {
                case 0:// Green
                    temp = green;
                    break;
                case 1:// Red
                    temp = red;
                    break;
                case 2:// Blue
                    temp = blue;
                    break;
                default:// Black
                    temp = black;
            }
            // Opacity
            myfile << temp << " 1" << "\n";
        }
        myfile << "\nCELL_DATA " << cells_ids.size() << "\n";
        myfile << "SCALARS scalars int 1" << "\n";
        myfile << "LOOKUP_TABLE face_id" << "\n";
        for (std::vector<std::size_t>::iterator it(cells_ids.begin()), itend(cells_ids.end()); it != itend; ++it) {
            myfile << *it << "\n";
        }
        /*myfile << "\nLOOKUP_TABLE CellColors " << cells_ids.size() << "\n";
        for (std::vector<std::size_t>::iterator it(cells_ids.begin()), itend(cells_ids.end()); it != itend; ++it) {
            myfile << ".4 .4 1 1" << "\n";
        }*/
        for (std::vector<std::size_t>::iterator it(cells_ids.begin()), itend(cells_ids.end()); it != itend; ++it) {
            //myfile << "7" << "\n";
        }
        //myfile << "9\n";    /// QUAD
        for (std::vector<std::size_t>::iterator it(cells_ids.begin()), itend(cells_ids.end()); it != itend; ++it) {
            std::cout << *it << " ";
        }
        std::cout << std::endl;
    }
    myfile.close();
}

void LCC_HL::writer_paraview(std::string filename) {
    /// https://kitware.github.io/vtk-examples/site/VTKFileFormats/
    std::ofstream myfile(filename);
    if (myfile.is_open()) {
        // Header
        myfile << "# vtk DataFile Version 2.0\n";
        // Title
        myfile << "# My Super Title\n";
        // Data type
        myfile << "ASCII\n";
        // Geometry Topology Type
        myfile << "DATASET UNSTRUCTURED_GRID\n\n";
        // Points
        myfile << "POINTS " << map_vtx.size() << " float\n";
        auto vtx_ids = get_all_vertex_ids();
        std::map <std::size_t, std::size_t> ids_index;
        auto cells_ids = get_cells_id();
        std::sort(vtx_ids.begin(), vtx_ids.end());
        std::sort(cells_ids.begin(), cells_ids.end());
        std::size_t cpt = 0;
        std::vector <std::size_t> vertex_of_cell;
        for (std::vector<std::size_t>::iterator it(vtx_ids.begin()), itend(vtx_ids.end()); it != itend; ++it) {
            std::cout << *it << " ";
            ids_index.insert(std::pair<std::size_t, std::size_t>(*it, cpt));
            Point p = point_of_vertex(*it);
            myfile << p.x() << " " << p.y() << " " << 0.0 << "\n";
            cpt++;
        }

        // Compute size
        std::size_t connexity = 0;
        for (std::vector<std::size_t>::iterator it(cells_ids.begin()), itend(cells_ids.end()); it != itend; ++it) {
            vertex_of_cell = get_vertex_of_cell(*it);
            connexity += vertex_of_cell.size();
        }

        myfile << "\nCELLS " << cells_ids.size() << " " << connexity + cells_ids.size() << "\n"; /// Connexity

        std::cout << std::endl;
        for (std::vector<std::size_t>::iterator it(cells_ids.begin()), itend(cells_ids.end()); it != itend; ++it) {
            std::cout << *it << " ";
        }

        std::cout << std::endl;
        for (std::vector<std::size_t>::iterator it(cells_ids.begin()), itend(cells_ids.end()); it != itend; ++it) {
            vertex_of_cell = get_vertex_of_cell(*it);
            myfile << vertex_of_cell.size() << " ";
            for (std::vector<std::size_t>::iterator it_v(vertex_of_cell.begin()), itend_v(vertex_of_cell.end());
                 it_v != itend_v; ++it_v) {
                myfile << ids_index.find(*it_v)->second << " ";
            }
            myfile << "\n";
        }
        std::cout << std::endl;
        myfile << "\nCELL_TYPES " << cells_ids.size() << "\n";
        //myfile << "1\n";    /// VERTEX
        for (std::vector<std::size_t>::iterator it(cells_ids.begin()), itend(cells_ids.end()); it != itend; ++it) {
            myfile << "7\n";    /// POLYGON
        }

        myfile << "\nPOINT_DATA " << vtx_ids.size() << "\n";
        myfile << "SCALARS scalars float 1" << "\n";

        // Ids
        myfile << "LOOKUP_TABLE Default" << "\n";

        std::string red = "1 0 0", blue = "0 0 1", green = "0 1 0", black = "0 0 0", white = "1 1 1";
        for (std::vector<std::size_t>::iterator it(vtx_ids.begin()), itend(vtx_ids.end()); it != itend; ++it) {
            myfile << *it << "\n";
        }
        myfile << "\nVECTORS vectors float\n";
        std::string temp;
        for (std::vector<std::size_t>::iterator it(vtx_ids.begin()), itend(vtx_ids.end()); it != itend; ++it) {
            Point p = lcc.point(dart_of_vertex_id(*it));

            std::cout << "id_vtx " << *it << " conformity " << is_orb_conform(*it) << " constraint "
                      << get_constraint(*it) << " " << p.x() << ", " << p.y() << std::endl;
            int criticity = get_constraint(*it);

            int conformity = is_orb_conform(*it);//get_field(*it, 3);
            int color_me = criticity;
            if (criticity == 0) {
                color_me = conformity;
                if (conformity != 0)
                    color_me++;
            }
            switch (color_me) {
                case 0:// Green
                    temp = green;
                    break;
                case 1:// Red
                    temp = red;
                    break;
                case 2:// Blue
                    temp = blue;
                    break;
                default:// Black
                    temp = black;
            }
            // Opacity
            myfile << temp << "\n";
        }
        myfile << "\nCELL_DATA " << cells_ids.size() << "\n";
        myfile << "SCALARS scalars int 1" << "\n";
        myfile << "LOOKUP_TABLE face_id" << "\n";
        for (std::vector<std::size_t>::iterator it(cells_ids.begin()), itend(cells_ids.end()); it != itend; ++it) {
            myfile << *it << "\n";
        }
        /*myfile << "\nLOOKUP_TABLE CellColors " << cells_ids.size() << "\n";
        for (std::vector<std::size_t>::iterator it(cells_ids.begin()), itend(cells_ids.end()); it != itend; ++it) {
            myfile << ".4 .4 1 1" << "\n";
        }*/
        for (std::vector<std::size_t>::iterator it(cells_ids.begin()), itend(cells_ids.end()); it != itend; ++it) {
            //myfile << "7" << "\n";
        }
        //myfile << "9\n";    /// QUAD
        for (std::vector<std::size_t>::iterator it(cells_ids.begin()), itend(cells_ids.end()); it != itend; ++it) {
            std::cout << *it << " ";
        }
        std::cout << std::endl;
    }
    myfile.close();
}

std::size_t LCC_HL::valence(std::size_t id_vertex) {
    Dart_handle dh = dart_of_vertex_id(id_vertex);
    return lcc.darts_of_orbit<1, 2>(dh).size() / 2;
}

bool LCC_HL::in_boundary(std::size_t id) {
    // If a dart among all darts of the vertex "id" verify the condition alpha<2>d ==d
    Dart_handle dh = dart_of_vertex_id(id);
    bool in_boundary = false;
    for (LCC_2::Dart_of_orbit_range<1, 2>::iterator
                 it = lcc.darts_of_orbit<1, 2>(dh).begin(),
                 itend = lcc.darts_of_orbit<1, 2>(dh).end(); it != itend; ++it) {
        if (this->lcc.alpha<2>(dh) == dh) {
            in_boundary = true;
            break;
        }
    }
    return in_boundary;
}

bool LCC_HL::on_boundary(std::size_t id_a, std::size_t id_b) {
    if (!is_neigh(id_a, id_b))
        throw "Have to be neighbor\n";
    Dart_handle d = darts_of_edge(id_a, id_b);
    bool on_boundary = false;
    if (this->lcc.alpha<2>(d) == d) {
        on_boundary = true;
    }
    return on_boundary;
}

// Autre opération donné en réunion en insérant d'abord un ensemble de dart et ensuite en faisant les liaisons ou non.
void LCC_HL::open_edge(std::size_t id_a, std::size_t id_b) {
    Dart_handle d = darts_of_edge(id_a, id_b);
    std::cout << "Start: open_edge " << std::endl;
    bool on_border = false;
    // d == lcc.alpha<2>(d) means d has no link in 2 (2-free)
    if (lcc.alpha<2>(d) == d) {
        on_border = true;
    }
    LCC_2::Attribute_handle<2>::type ah = create_face_handle();
    Dart_handle d0 = lcc.alpha<0>(d);
    Dart_handle d_tmp = lcc.alpha<2>(d);

    Dart_handle d1p = lcc.create_dart(lcc.point_of_vertex_attribute(lcc.vertex_attribute(d)));
    Dart_handle d2p = lcc.create_dart(lcc.point_of_vertex_attribute(lcc.vertex_attribute(lcc.alpha<0>(d))));
    // Link between new darts
    lcc.link_alpha<0>(d1p, d2p);
    // Link between new and old
    lcc.link_alpha<2>(d, d1p);
    lcc.link_alpha<2>(d0, d2p);

    Dart_handle d1 = lcc.create_dart(lcc.point_of_vertex_attribute(lcc.vertex_attribute(d)));
    Dart_handle d2 = lcc.create_dart(lcc.point_of_vertex_attribute(lcc.vertex_attribute(lcc.alpha<0>(d))));
    lcc.link_alpha<0>(d1, d2);
    //no link if on border
    if (!on_border) {
        lcc.link_alpha<2>(d_tmp, d1);
        lcc.link_alpha<2>(lcc.alpha<0>(d_tmp), d2);
    }
    lcc.link_alpha<1>(d1p, d1);
    lcc.link_alpha<1>(d2p, d2);

    // Set attribute of new darts
    lcc.set_attribute<2>(d1p, ah);
    lcc.set_attribute<2>(d2p, ah);
    lcc.set_attribute<2>(d1, ah);
    lcc.set_attribute<2>(d2, ah);
    assert(lcc.is_valid());
    assert(lcc.is_valid_attribute<2>(ah));
    std::cout << "End: open_edge" << std::endl;
}

void LCC_HL::locate_all_vertex() {
    std::size_t id;
    for (LCC_2::Vertex_attribute_range::iterator
                 it = lcc.vertex_attributes().begin(),
                 itend = lcc.vertex_attributes().end();
         it != itend; ++it) {
        id = get_id(it);
        std::cout << "point: " << lcc.point_of_vertex_attribute(it) << ", " << "id: " << id << std::endl;
    }
}

std::vector <std::size_t> LCC_HL::incident_edge(std::size_t id, bool return_self_id) {
    // Vector for res
    std::vector <std::size_t> edges;
    // Set to remove duplicates
    std::unordered_set <std::size_t> set_edges;
    Dart_handle dh = dart_of_vertex_id(id);
    for (LCC_2::Dart_of_orbit_range<1, 2>::iterator
                 it(lcc.darts_of_orbit<1, 2>(dh).begin()),
                 itend(lcc.darts_of_orbit<1, 2>(dh).end()); it != itend; ++it) {
        //edges.push_back(id);
        //edges.push_back(get_id(lcc.vertex_attribute(lcc.alpha<0>(it))));
        set_edges.insert(get_id(lcc.vertex_attribute(lcc.alpha<0>(it))));
    }
    // Remove duplicates and add "id"
    if (return_self_id) {
        edges.reserve(2 * set_edges.size());
        for (auto it = set_edges.begin(); it != set_edges.end(); ++it) {
            edges.push_back(id);
            edges.push_back(*it);
        }
    } else {
        edges.reserve(set_edges.size());
        std::copy(set_edges.begin(), set_edges.end(), edges.end());
    }
    return edges;
}

std::vector <std::size_t> LCC_HL::incident_tri(std::size_t id) {
    std::vector <std::size_t> tri;
    Dart_handle dh = dart_of_vertex_id(id);
    for (LCC_2::Dart_of_orbit_range<1, 2>::iterator
                 it(lcc.darts_of_orbit<1, 2>(dh).begin()),
                 itend(lcc.darts_of_orbit<1, 2>(dh).end()); it != itend; ++it) {
        if (lcc.alpha<0, 1, 0, 1, 0, 1>(it) == it) {
            tri.push_back(id);
            tri.push_back(get_id(lcc.vertex_attribute(lcc.alpha<0, 1>(it))));
            tri.push_back(get_id(lcc.vertex_attribute(lcc.alpha<0, 1, 0, 1>(it))));
            if (lcc.alpha<1>(it) == std::next(it)) {
                ++it;
            }
        }
    }
    return tri;
}

std::vector <std::size_t> LCC_HL::incident_quad(std::size_t id) {
    std::vector <std::size_t> quad;
    Dart_handle dh = dart_of_vertex_id(id);
    for (LCC_2::Dart_of_orbit_range<1, 2>::iterator
                 it(lcc.darts_of_orbit<1, 2>(dh).begin()),
                 itend(lcc.darts_of_orbit<1, 2>(dh).end()); it != itend; ++it) {
        /// Check if it is a quads and test if it is not a compressed face
        if ((lcc.alpha<0, 1, 0, 1, 0, 1, 0, 1>(it) == it) && (lcc.alpha<0, 1, 0, 1>(it) != it)) {
            quad.push_back(id);
            quad.push_back(get_id(lcc.vertex_attribute(lcc.alpha<0, 1>(it))));
            quad.push_back(get_id(lcc.vertex_attribute(lcc.alpha<0, 1, 0, 1>(it))));
            quad.push_back(get_id(lcc.vertex_attribute(lcc.alpha<0, 1, 0, 1, 0, 1>(it))));
            if (lcc.alpha<1>(it) == std::next(it)) {
                ++it;
            }
        }
    }
    return quad;
}

void LCC_HL::show_edge(std::size_t id) {
    auto edge = incident_edge(id);
    std::cout << "#Edges " << edge.size() << std::endl;
    for (auto it = edge.begin();
         it != edge.end(); it += 2) {
        std::cout << *it << " " << *(std::next(it)) << std::endl;
    }
}

void LCC_HL::show_tri(std::size_t id) {
    auto tri = incident_tri(id);
    std::cout << "#Tri " << tri.size() << std::endl;
    for (auto it = tri.begin();
         it != tri.end(); it += 3) {
        std::cout << *it << " " << *(std::next(it, 1)) << " " << *(std::next(it, 2)) << std::endl;
    }
}

void LCC_HL::show_quad(std::size_t id) {
    auto quad = incident_quad(id);
    std::cout << "#Quad " << quad.size() << std::endl;
    for (auto it = quad.begin();
         it != quad.end(); it += 4) {
        std::cout << *it << " " << *(std::next(it, 1)) << " " << *(std::next(it, 2)) << " " << *(std::next(it, 3))
                  << std::endl;
    }
}

/**
 * If possible switch side of the path edge
 * */
void LCC_HL::switch_side(std::vector <Dart_handle> &v_dh) {
    std::cout << "Start:switch_side" << std::endl;
    for (auto it = v_dh.begin(); it != v_dh.end(); ++it) {
        *it = lcc.alpha<2>(*it);
    }
    std::cout << "End:switch_side" << std::endl;
}

/**
 * TODO Test
 * Ensure dart edge is correct
 * */
//, std::vector<Dart_handle>::iterator it_curr
void LCC_HL::check_path(std::vector <Dart_handle> &path, std::vector<Dart_handle>::iterator &it_curr) {
    std::cout << "Start:check_path" << std::endl;
    assert(lcc.is_valid());
    bool path_ok = true;
    std::size_t prev, cur, between;
    Dart_handle dh, dh_temp;
    std::vector <Dart_handle> checked;
    std::vector <std::size_t> checked_ids;
    for (auto it = it_curr; it != path.end(); ++it) {
        prev = get_id(lcc.alpha<0>(*(std::prev(it))));
        cur = get_id(*it);
        /// Ids of vertex have to be the same
        if (prev != cur) {
            std::cout << "in check_path " << prev << " " << cur << std::endl;
            std::cout << "modified edges" << std::endl;
            between = get_id(lcc.alpha<0>(*(std::prev(it))));
            dh = darts_of_edge(between, cur);
            dh = same_side_fix(lcc.alpha<0>(*(std::prev(it))), dh);
            std::cout << between << std::endl;
            show_edge(between);
            std::size_t previous = get_id(*(std::prev(it)));
            checked_ids = {previous, between, cur};
            checked = same_side(checked_ids);
            std::cout << "prev between cur " << previous << ' ' << between << ' ' << cur << std::endl;
            for (auto myit = path.begin(); myit != path.end(); ++myit) {
                std::cout << get_id(*myit) << " ";
            }
            std::cout << std::endl;
            path.insert(it, dh);
            //open_edge(between, cur);
            std::cout << "New edges" << std::endl;
            assert(lcc.is_valid());
            for (auto myit = path.begin(); myit != path.end(); ++myit) {
                std::cout << get_id(*myit) << " ";
            }
            std::cout << std::endl;
        }
    }
    std::cout << "end:check_path" << std::endl;
    std::flush(std::cout);
}

std::vector <Dart_handle> LCC_HL::same_side_with(std::vector <std::size_t> v, std::size_t required_id) {
    std::cout << "Start:same_side_with" << std::endl;
    std::vector <Dart_handle> edge;
    /// Select darts for each edges of the path
    for (auto it = v.begin(); it != v.end() - 1; ++it) {
        std::cout << *it << " " << *(std::next(it)) << std::endl;
        edge.push_back(darts_of_edge(*it, *(std::next(it))));
    }
    /// Compute first dart
    Dart_handle dh0;
    for (LCC_2::Dart_of_orbit_range<1, 2>::iterator
                 it(lcc.darts_of_orbit<1, 2>(edge.front()).begin()),
                 itend(lcc.darts_of_orbit<1, 2>(edge.front()).end()); it != itend; ++it) {
        if ((get_id(lcc.alpha<1, 0>(it)) == required_id) &&
            (get_id(lcc.alpha<0>(it)) == *(std::next(v.begin())))) {
            std::cout << "Dart not changed" << std::endl;
            edge[0] = it;
            break;
        }
    }
    /// Check if the path is corect. If it is not, any of darts edges can be swapped (not the first)
    for (auto it_curr = edge.begin(); it_curr != edge.end() - 1; ++it_curr) {
        dh0 = lcc.alpha<0>(*it_curr);
        for (LCC_2::Dart_of_orbit_range<1, 2>::iterator
                     it(lcc.darts_of_orbit<1, 2>(dh0).begin()),
                     itend(lcc.darts_of_orbit<1, 2>(dh0).end()); it != itend; ++it) {
            if (it == *(std::next(it_curr))) {
                std::cout << "Dart not changed" << std::endl;
                break;
            } else if (it == lcc.alpha<2>(*(std::next(it_curr)))) {
                std::cout << "Dart changed" << std::endl;
                // not on border
                assert(lcc.alpha<2>(*(std::next(it_curr))) != *(std::next(it_curr)));
                *(std::next(it_curr)) = lcc.alpha<2>(*(std::next(it_curr)));
                break;
            }
        }
    }
    for (auto it = edge.begin(); it != edge.end(); ++it) {
        std::cout << "cur " << get_id(*it) << "next " << get_id(lcc.alpha<0>(*it)) << "side"
                  << get_id(lcc.alpha<1, 0>(*it)) << std::endl;
    }

    std::cout << "End:same_side_with" << std::endl;
    return edge;
}

/**
 * return true if dr and dl are on the same side <=> there is no darts with the same edge of
 * S(dr) S(<0>(dr)) found before the dart dr.
 * */
bool LCC_HL::same_side(Dart_handle dl, Dart_handle dr) {
    std::size_t id_dr = get_id(lcc.alpha<0>(dr));
    for (LCC_2::Dart_of_orbit_range<1, 2>::iterator
                 it(lcc.darts_of_orbit<1, 2>(dl).begin()),
                 itend(lcc.darts_of_orbit<1, 2>(dl).end()); it != itend; ++it) {
        //std::cout << get_id(lcc.alpha<0>(it)) << ' ' << id_dr << std::endl;
        if ((get_id(lcc.alpha<0>(it)) == id_dr) && (it == dr)) {
            return true;
        } else if ((get_id(lcc.alpha<0>(it)) == id_dr) && (it != dr)) {
            return false;
        }
    }
    return false;
}

/**
 * return the dart "dr" with the same side of "dl".
 * If the darts dr and dl are on the side, it may not be as expected
 * */
Dart_handle LCC_HL::same_side_fix(Dart_handle dl, Dart_handle dr) {
    //assert(get_id(dl) != get_id(dr));
    //assert(get_id(lcc.alpha<0>(dl)) == get_id(dr));
    Dart_handle res;
    std::size_t id_dr = get_id(lcc.alpha<0>(dr));
    for (LCC_2::Dart_of_orbit_range<1, 2>::iterator
                 it(lcc.darts_of_orbit<1, 2>(dl).begin()),
                 itend(lcc.darts_of_orbit<1, 2>(dl).end()); it != itend; ++it) {
        //std::cout << get_id(lcc.alpha<0>(it)) << ' ' << id_dr << std::endl;
        if ((get_id(lcc.alpha<0>(it)) == id_dr) && (it == dr)) {
            res = dr;
            break;
        } else if ((get_id(lcc.alpha<0>(it)) == id_dr) && (it != dr)) {
            res = it;
            break;
        }
    }
    return res;
}

/**
 * Generate a path of Dart_handle from a vector of ids "ids"
 * If possible all Dart_handle will be at the same side of edges
 * */
std::vector <Dart_handle> LCC_HL::same_side(std::vector <std::size_t> ids) {
    std::cout << "Start:same_side" << std::endl;
    std::vector <Dart_handle> edge;
    for (auto it = ids.begin(); it != ids.end() - 1; ++it) {
        std::cout << *it << " " << *(std::next(it)) << std::endl;
        edge.push_back(darts_of_edge(*it, *(std::next(it))));
    }
    Dart_handle dh0;
    std::cout << "Num edges " << edge.size() << std::endl;
    for (auto it_curr = edge.begin(); it_curr != edge.end() - 1; ++it_curr) {
        dh0 = lcc.alpha<0>(*it_curr);
        for (LCC_2::Dart_of_orbit_range<1, 2>::iterator
                     it(lcc.darts_of_orbit<1, 2>(dh0).begin()),
                     itend(lcc.darts_of_orbit<1, 2>(dh0).end()); it != itend; ++it) {
            //std::cout << "LOOP " << get_id(dh0) << " " << get_id(it) << std::endl;
            if (it == *(std::next(it_curr))) {
                std::cout << "Dart not changed" << std::endl;
                break;
            } else if (it == lcc.alpha<2>(*(std::next(it_curr)))) {
                std::cout << "Dart changed" << std::endl;
                // not on border
                assert(lcc.alpha<2>(*(std::next(it_curr))) != *(std::next(it_curr)));
                *(std::next(it_curr)) = lcc.alpha<2>(*(std::next(it_curr)));
                break;
            }
        }
    }
    std::cout << "End:same_side" << std::endl;
    return edge;
}

Point LCC_HL::point_of_vertex(std::size_t id) {
    Vertex_attribute_handle vh = vertex_handle_of_vertex_id(id);
    return lcc.point_of_vertex_attribute(vh);
}




void LCC_HL::pillowing_out(std::vector <std::size_t> v, float w, bool open_one_compressed_vertice) {
    assert(lcc.is_valid());
    std::cout << "Start:pillowing_out" << std::endl;
    std::vector <std::size_t> new_vtx_ids;

    auto edge_side = same_side(v);
    std::size_t id_orientation = get_id(lcc.alpha<1, 0>(edge_side[0]));
    std::cout << "oriented to " << id_orientation << std::endl;
    bool loop_vector = false;
    if (v.front() == v.back()) {
        std::cout << "Loop vector " << std::endl;
        loop_vector = true;
    }
    // Open edges
    for (auto it = v.begin(); it != v.end() - 1; ++it) {
        std::cout << *it << " " << *(std::next(it)) << std::endl;
        open_edge(*it, *(std::next(it)));
    }
    assert(lcc.is_valid());
    // Compute points
    std::cout << "Point compute" << std::endl;
    std::vector <Point> points;
    Dart_handle dl, dr;
    Point p;
    std::size_t i = 0;

    for (auto it = std::next(edge_side.begin()); it != edge_side.end(); ++it) {
        assert(lcc.is_valid());
        dl = lcc.alpha<0>(*(std::prev(it)));
        dr = *it;
        p = compute_new_point(lcc.alpha<2>(dl), lcc.alpha<2>(dr), w, false);
        // Problème si le point est dans un équilibre parfait la moyenne le replace au meme endroit A(x,y) == A'(x,y)
        // Donc on selectionne un sous ensemble d'arrête
        if (p == lcc.point(dl) ||
            distance(p, lcc.point(dl)) < distance(lcc.point(dl), lcc.point(lcc.alpha<0>(dl))) / 7) {
            std::cout << "POINT IS THE SAME dl " << lcc.point(lcc.alpha<0>(dl)) << "dr " << lcc.point(lcc.alpha<0>(dr))
                      << std::endl;
            p = compute_new_point(lcc.alpha<1>(dl), lcc.alpha<1>(dr), w, false);
        }
        std::cout << "For " << get_id(dl) << "new point " << p << std::endl;
        points.push_back(p);
    }

    std::cout << "#Points" << std::endl;
    for (auto it = std::begin(points); it != points.end(); ++it) {
        std::cout << *it << std::endl;
    }

    if (loop_vector) {
        assert(lcc.is_valid());
        dl = lcc.alpha<0>(edge_side.back());
        dr = edge_side.front();
        p = compute_new_point(lcc.alpha<2>(dl), lcc.alpha<2>(dr), w, false);
        std::cout << "For " << get_id(dl) << "new point " << p << std::endl;
        points.push_back(p);
    }
    // Split inside vertices with points computed
    assert(lcc.is_valid());
    i = 0;
    for (auto it = std::next(edge_side.begin()); it != edge_side.end(); ++it) {
        dl = lcc.alpha<0>(*(std::prev(it)));
        dr = *it;
        //two_split_vertice(lcc.alpha<2, 1>(dl), lcc.alpha<2, 1>(dr), points[i]);
        two_split_vertice(lcc.alpha<2, 1>(dl), lcc.alpha<2, 1>(dr), points[i]);
        i++;
        draw();
    }
    // If it is a loop vector like {1,2,3,4,1}
    if (loop_vector) {
        assert(lcc.is_valid());
        dl = lcc.alpha<0>(*(edge_side.end() - 1));
        dr = edge_side.front();
        std::cout << get_id(lcc.alpha<0>(dl)) << " " << get_id(lcc.alpha<2>(dr)) << std::endl;
        std::cout << edge_side.size() << std::endl;
        two_split_vertice(lcc.alpha<2, 1>(dl), lcc.alpha<2, 1>(dr), points[i]);
    }
    // If the user want to make quads appear on boundary
    // Split border vertices with new points
    if (open_one_compressed_vertice) {
        dr = lcc.alpha<2, 1>(edge_side.front());
        dl = lcc.alpha<2>(edge_side.front());
        p = compute_new_point_border(dl, dr, w, false);
        //p = Point(0.0,1.8);
        one_split_vertice(dl, dr, p);
        dr = lcc.alpha<0, 2, 1>(edge_side.back());
        dl = lcc.alpha<0, 2>(edge_side.back());
        p = compute_new_point_border(dl, dr, w, false);
        //p = Point(3.0,1.8);
        one_split_vertice(dl, dr, p);
        // #TAGHERE
    }
    assert(lcc.is_valid());
    std::cout << "End:pillowing_out" << std::endl;
}

void LCC_HL::set_constraint_vtx(std::size_t id_vtx, std::size_t constraint) {
    Vertex_attribute_handle vh = lcc.vertex_attribute(dart_of_vertex_id(id_vtx));
    vh->info()[1] = constraint;
}

/**
 * Low level split
 * */
std::vector <std::size_t> LCC_HL::pillowing(std::vector <std::size_t> v, std::size_t require_id_orientation, float w,
                                            bool open_one_compressed_vertice) {
    //assert(lcc.is_valid());
    assert(0.0 < w && w < 1.0);
    std::cout << "Start:pillowing" << std::endl;
    std::vector <std::size_t> new_vtx_ids;
    if (!is_neigh(v[0], require_id_orientation)) {
        std::cout << "Orientation " << v[0] << " " << require_id_orientation << std::endl;
        throw "Not in neighborhood";
    }
    for (auto it = std::begin(v); it != std::end(v) - 1; ++it) {
        if (!is_neigh(*it, *(std::next(it)))) {
            std::cout << "Iner_vtx " << *it << " " << *(std::next(it)) << std::endl;
            throw "Not in neighborhood";
        }
    }



    // Check if the path is ok:
    // - v[0] is neigh with require_id_orientation
    // - v[i] is neigh with v[i+1]
    // - According to policy (triangle, improper, conform) somme pillowing could not be done
    // Triangle :
    // Improper :
    // Conform :
    auto edge_side = same_side_with(v, require_id_orientation);
    Dart_handle dl, dr;
    std::size_t id_orientation = get_id(lcc.alpha<1, 0>(edge_side[0]));
    std::cout << "oriented to " << id_orientation << std::endl;
    bool loop_vector = false;
    if (v.front() == v.back()) {
        std::cout << "Loop vector " << std::endl;
        loop_vector = true;
    }
    if (id_orientation != require_id_orientation) {
        std::cout << "Changement orientation " << id_orientation << std::endl;
        switch_side(edge_side);
        id_orientation = get_id(lcc.alpha<1, 0>(edge_side[0]));
        std::cout << "Maintenant dirigé vers " << id_orientation << std::endl;
    }
    std::cout << "#same_side" << std::endl;
    for (auto it = std::next(edge_side.begin()); it != edge_side.end(); ++it) {
        dl = lcc.alpha<0>(*(std::prev(it)));
        dr = *it;
        std::cout << same_side(dl, dr) << " ";
    }
    std::cout << std::endl << "#same_side" << std::endl;
    // Open edges
    /*
    for (auto it = v.begin(); it != v.end() - 1; ++it) {
        std::cout << *it << " " << *(std::next(it)) << std::endl;
        open_edge(*it, *(std::next(it)));
    }
    */
    std::cout << "#same_side" << std::endl;
    for (auto it = std::next(edge_side.begin()); it != edge_side.end(); ++it) {
        std::cout << get_id(*it) << std::endl;
        dl = lcc.alpha<0>(*(std::prev(it)));
        dr = *it;
        std::cout << same_side(dl, dr) << " ";
    }

    std::cout << "VERTEX_IDS" << std::endl;
    for (auto it = edge_side.begin(); it != edge_side.end(); ++it) {
        std::cout << get_id(*it) << std::endl;
    }

    std::cout << std::endl << "#same_side" << std::endl;
    assert(lcc.is_valid());
    // Compute points
    std::cout << "Point compute" << std::endl;
    std::vector <Point> points;
    Point p;
    std::size_t i = 0;

    edge_side.reserve(3 * edge_side.capacity()); // ?????


    /*
    for (auto it = std::next(edge_side.begin()); it != edge_side.end(); ++it) {
        assert(lcc.is_valid());
        dl = lcc.alpha<0>(*(std::prev(it)));
        dr = *it;
        p = compute_new_point(lcc.alpha<2>(dl), lcc.alpha<2>(dr), w);
        std::cout << "For " << get_id(dl) << " new point " << p << std::endl;
        if (p == lcc.point(dl)) {
            std::cout << "POINT IS THE SAME dl " << lcc.point(lcc.alpha<0>(dl)) << "dr " << lcc.point(lcc.alpha<0>(dr))
                      << std::endl;
            p = compute_new_point(lcc.alpha<1>(dl), lcc.alpha<1>(dr), w, true);
        }
        p = compute_point(lcc.point(lcc.alpha<2,0,1,0,1>(dr)),lcc.point(lcc.alpha<2,0,1,0,1>(dl)),0.33);
        points.push_back(p);
    }*/
    Point first_loop;
    std::size_t id_last = get_id(edge_side.back());
    if (loop_vector) {
        assert(lcc.is_valid());
        dl = lcc.alpha<0>(edge_side.back());
        dr = edge_side.front();
        p = compute_new_point(lcc.alpha<2>(dl), lcc.alpha<2>(dr), w);
        std::cout << "For " << get_id(dl) << "new point " << p << std::endl;
        first_loop = compute_point(lcc.point(edge_side.front()),lcc.point(lcc.alpha<1,0>(edge_side.back())),0.33);
        points.push_back(p);

    }
    // Split inside vertices with points computed
    i = 0;
    bool treat_five = false;
    Point p_tmp;
    Dart_handle dl_tmp, dr_tmp;
    std::cout << get_id(*edge_side.begin()) << get_id(lcc.alpha<0>(*edge_side.begin())) << std::endl;
    open_edge(get_id(*edge_side.begin()), get_id(lcc.alpha<0>(*edge_side.begin())));
    //std::cout << get_id(*edge_side.end()) << get_id(lcc.alpha<0>(*edge_side.end())) << std::endl;
    //open_edge(get_id(*edge_side.end()), get_id(lcc.alpha<0>(*edge_side.end())));
    for (auto it = std::next(edge_side.begin()); it != edge_side.end(); ++it) {
        dl = lcc.alpha<0>(*(std::prev(it)));
        dr = *it;
        std::cout << get_id(lcc.alpha<2>(dl)) << " " << get_id(lcc.alpha<2>(dr)) << std::endl;
        std::cout << get_id(lcc.alpha<1, 0>(dl)) << " " << get_id(lcc.alpha<1, 0>(dr)) << std::endl;
        std::cout << get_id(lcc.alpha<0>(dl)) << " " << get_id(lcc.alpha<0>(dr)) << std::endl;
        auto cmprssd = get_compressed_faces(get_id(lcc.alpha<2>(dl)));
        std::cout << "Compressed" << std::endl;
        for (auto cmp_it = cmprssd.begin(); cmp_it != cmprssd.end(); ++cmp_it) {
            std::cout << get_id(lcc.alpha<0>(*cmp_it)) << std::endl;
        }
        p = compute_new_point(lcc.alpha<2>(dl), lcc.alpha<2>(dr), w, true);
        p = compute_point(lcc.point(dl),lcc.point(lcc.alpha<0,1,0,1>(dr)),0.33);
        open_edge(get_id(*it), get_id(lcc.alpha<0>(*it)));
        std::cout << "Compressed after open" << std::endl;
        for (auto cmp_it = cmprssd.begin(); cmp_it != cmprssd.end(); ++cmp_it) {
            std::cout << get_id(lcc.alpha<0>(*cmp_it)) << std::endl;
        }

        // TESTING
        //

        std::cout << "For " << get_id(dl) << "new point " << p << std::endl;
        if (p == lcc.point(dl)) {
            std::cout << "POINT IS THE SAME dl " << lcc.point(lcc.alpha<0>(dl)) << "dr " << lcc.point(lcc.alpha<0>(dr))
                      << std::endl;
            p = compute_new_point(lcc.alpha<1>(dl), lcc.alpha<1>(dr), w, true);
        }
        new_vtx_ids.push_back(two_split_vertice(lcc.alpha<2>(dl), lcc.alpha<2>(dr), p));
        //draw();
        std::cout << "Before_edges: ";
        for (auto myit = std::next(edge_side.begin()); myit != edge_side.end(); ++myit) {
            std::cout << get_id(*myit) << ' ';
        }
        std::cout << std::endl;
        std::flush(std::cout);
        //check_path(edge_side,it);
        std::cout << get_id(*it) << std::endl;
        std::flush(std::cout);
        std::cout << "Recap" << std::endl;
        bool res;
        for (auto myit = std::next(edge_side.begin()); myit != edge_side.end(); ++myit) {
            res = same_side(lcc.alpha<0>(*(std::prev(myit))), *myit);
            std::cout << "#same side " << get_id(*(std::prev(myit))) << " " << get_id(*myit) << " same: "
                      << res << " alpha<1,0> " << get_id(lcc.alpha<1, 0>(*(std::prev(myit)))) << " "
                      << get_id(lcc.alpha<1, 0>(*myit)) << std::endl;
            if (myit > it) {
                assert(res);
            }
        }
        assert(lcc.is_valid());
        i++;
    }
    if (loop_vector) {
        std::cout << "As a loop" << std::endl;
        assert(lcc.is_valid());
        dl = lcc.alpha<0>(edge_side.back());
        dr = edge_side.front();
        //p = compute_new_point(lcc.alpha<2, 1>(dl), lcc.alpha<2, 1>(dr), w);
        p = compute_new_point(lcc.alpha<2>(dl), lcc.alpha<2>(dr), w);
        p = compute_point(lcc.point(dr),lcc.point(lcc.alpha<1,0,2,1,0>(edge_side.back())),0.33);
        new_vtx_ids.push_back(two_split_vertice(lcc.alpha<2>(dl), lcc.alpha<2>(dr), p));
        set_point(new_vtx_ids[new_vtx_ids.size()-2],compute_point(point_of_vertex(id_last),lcc.point(lcc.alpha<0,2,1,0>(edge_side.front())),0.33));
    }
    // Split border vertices with new points
    if (open_one_compressed_vertice && !loop_vector) {
        dr = lcc.alpha<2, 1>(edge_side.front());
        dl = lcc.alpha<2>(edge_side.front());
        p = compute_new_point_border(dl, dr, w);
        p = compute_point(lcc.point(edge_side.front()),lcc.point(lcc.alpha<1,0>(edge_side.front())),0.33);
        Point p2 = compute_point(lcc.point(lcc.alpha<0>(edge_side.back())),lcc.point(lcc.alpha<0,1,0>(edge_side.back())),0.33);
        std::cout << get_id(dl) << " " << get_id(dr) << std::endl;
        if (lcc.alpha<2, 1, 2>(dl) == lcc.alpha<2, 1>(dl)) {
            // border
            new_vtx_ids.push_back(one_split_vertice(dr, dl, p));
        } else {
            // not border
            new_vtx_ids.push_back(one_split_vertice_bis(dr, dl, p));
        }
        dr = lcc.alpha<0, 2, 1>(edge_side.back());
        dl = lcc.alpha<0, 2>(edge_side.back());

        //p = compute_new_point_border(dl, dr, w);

        std::cout << get_id(dl) << " " << get_id(dr) << std::endl;

        if (lcc.alpha<2, 1, 2>(dl) == lcc.alpha<2, 1>(dl)) {
            // border
            new_vtx_ids.push_back(one_split_vertice(dr, dl, p2));
        } else {
            // not border
            new_vtx_ids.push_back(one_split_vertice_bis(dr, dl, p2));
        }
    }
    //assert(lcc.is_valid());

    std::cout << "End:pillowing added: " << new_vtx_ids.size() << std::endl;
    return new_vtx_ids;
}

Point LCC_HL::compute_new_point_border(Dart_handle dl, Dart_handle dr, float w, bool in) {
    std::cout << "Start:compute_new_point_border" << std::endl;
    assert(w > 0.0 && w <= 1.0);
    Dart_handle ndr, ndl, ndr1, ndl1;
    Point a(0.0, 0.0);
    ndr = dr;
    ndl = dl;
    // Save their <1> before unlink
    ndr1 = lcc.alpha<1>(ndr);
    ndl1 = lcc.alpha<1>(ndl);
    // Unlink
    lcc.unlink_alpha<1>(ndr);

    float count = 0;
    std::cout << "Compute point" << std::endl;
    Point curr;
    /*
    for (LCC_2::Dart_of_orbit_range<1, 2>::iterator
                 it(lcc.darts_of_orbit<1, 2>(ndr).begin()),
                 itend(lcc.darts_of_orbit<1, 2>(ndr).end()); it != itend; ++it) {
        //lcc.display_dart(it);

        curr = point_of_vertex(get_id(lcc.alpha<0>(it)));
        std::cout << curr << std::endl;
        a = Point(a.x() + curr.x(), a.y() + curr.y());
        count++;
    }
    Point mean(a.x() / count, a.y() / count);
    */
    Point mean;
    mean = lcc.point(lcc.alpha<2, 1, 0>(dl));
    std::cout << "Mean " << mean << std::endl;
    // Vector
    Point b = point_of_vertex(get_id(ndr));
    std::cout << "b " << b << std::endl;
    //
    Point v;
    if (in) {
        v = Point((mean.x() - b.x()) * w, (mean.y() - b.y()) * w);
    } else {
        v = Point((b.x() - mean.x()) * w, (b.y() - mean.y()) * w);
    }
    std::cout << "v " << v << std::endl;
    Point b_in(b.x() + v.x(), b.y() + v.y());
    std::cout << "bin " << b_in << std::endl;
    lcc.link_alpha<1>(ndr, ndl);
    std::cout << "End:compute_new_point_border" << std::endl;
    return b_in;
}


Point LCC_HL::compute_new_point(Dart_handle dl, Dart_handle dr, float w, bool in) {
    std::cout << "Start:compute_new_point" << std::endl;
    assert(w > 0.0);
    if (in) {
        assert(w <= 1.0);
    }
    bool are_linked_1 = false;
    if (dl == lcc.alpha<1>(dr)) {
        are_linked_1 = true;
    }
    assert(lcc.is_valid());
    Dart_handle ndr, ndl, ndr1, ndl1;
    Point a(0.0, 0.0);
    ndl = dl;
    ndr = dr;
    // Save their <1> before unlink
    ndl1 = lcc.alpha<1>(ndl);
    ndr1 = lcc.alpha<1>(ndr);
    // Unlink
    if (!are_linked_1) {
        lcc.unlink_alpha<1>(ndl);
    }
    lcc.unlink_alpha<1>(ndr);
    float count = 0.0;
    std::cout << "Compute point" << std::endl;
    Point curr;
    Point tot(0.0, 0.0);
    Point b = point_of_vertex(get_id(ndr));
    for (LCC_2::Dart_of_orbit_range<1, 2>::iterator
                 it(lcc.darts_of_orbit<1, 2>(ndl).begin()),
                 itend(lcc.darts_of_orbit<1, 2>(ndl).end()); it != itend; ++it) {
        //lcc.display_dart(it);
        curr = point_of_vertex(get_id(lcc.alpha<0>(it)));
        std::cout << curr << std::endl;
        a = Point(a.x() + curr.x(), a.y() + curr.y());
        count += 1.0;
        tot = Point(tot.x() + (b.x() - curr.x()), tot.y() + (b.y() - curr.y()));
    }
    Point mean(a.x() / count, a.y() / count);
    std::cout << "Mean " << mean << std::endl;
    //Vector
    std::cout << "b " << b << std::endl;
    Point v;
    if (in) {
        v = Point((mean.x() - b.x()) * w, (mean.y() - b.y()) * w);
    } else {
        v = Point((b.x() - mean.x()) * w, (b.y() - mean.y()) * w);
    }
    std::cout << "v " << v << std::endl;
    Point b_in(b.x() + v.x(), b.y() + v.y());
    std::cout << "bin " << b_in << std::endl;
    lcc.link_alpha<1>(ndr, ndr1);
    lcc.link_alpha<1>(ndl, ndl1);
    std::cout << "End:compute_new_point" << std::endl;
    assert(lcc.is_valid());
    // Autre moyen de calculer un nouveau point (uniquement pour b_out)
    //tot = Point(b.x() + (tot.x() * w), b.y() + (tot.y() * w));
    return b_in;
}

std::size_t LCC_HL::one_split_vertice(Dart_handle dl, Dart_handle dr, const Point &p) {
    /*Split a vertice when one face is compressed 1-split*/
    assert(lcc.alpha<1>(dr) == dl);
    assert(lcc.vertex_attribute(dr) == lcc.vertex_attribute(dr));
    // Vertex have to be in boundary
    std::cout << "Start: one_split_vertice" << std::endl;
    // Unlink to attach darts to new vertex attribute
    lcc.unlink_alpha<1>(dl);
    // New vertex from point in param
    LCC_2::Vertex_attribute_handle vh = create_vertex_handle(p);
    // Determine the near_face dart if there is one a near dart is a dart that is near a face (link<2>)
    // dl and dr can be both near darts or not once
    // If we need to split with a near darts
    // Attach darts to new vertex attribute
    for (LCC_2::Dart_of_orbit_range<1, 2>::iterator
                 it(lcc.darts_of_orbit<1, 2>(dr).begin()),
                 itend(lcc.darts_of_orbit<1, 2>(dr).end()); it != itend; ++it) {
        lcc.set_vertex_attribute_of_dart(it, vh);
    }
    // Create new darts
    Dart_handle a_dn = lcc.create_dart(lcc.vertex_attribute(dl));
    Dart_handle b_dn = lcc.create_dart(vh);
    // Link
    lcc.link_alpha<0>(a_dn, b_dn);
    lcc.link_alpha<1>(a_dn, dl);
    lcc.link_alpha<1>(b_dn, dr);
    // Set Attribute Cell
    lcc.set_attribute<2>(b_dn, lcc.attribute<2>(dr));
    lcc.set_attribute<2>(a_dn, lcc.attribute<2>(dr));
    std::cout << "End: one_split_vertice" << std::endl;
    std::size_t id = get_id(vh), nc;
    lcc.info_of_attribute<0>(vh) = {id, 0, 0, !in_boundary(id)};
    return id;
}

/** Border*/
std::size_t LCC_HL::one_split_vertice_bis(Dart_handle dl, Dart_handle dr, const Point &p) {
    std::cout << "Start: one_split_vertice_bis" << std::endl;
    /* Split a vertice when one face is compressed 1-split*/
    assert(lcc.alpha<1>(dr) == dl);
    assert(lcc.vertex_attribute(dr) == lcc.vertex_attribute(dr));
    Dart_handle tmp = lcc.alpha<2, 1, 2>(dr), tmp1 = lcc.alpha<1>(tmp);

    // Unlink to attach darts to new vertex attribute
    lcc.unlink_alpha<1>(dl);

    //
    lcc.unlink_alpha<1>(tmp);//tmp1
    //
    // New vertex from point in param
    LCC_2::Vertex_attribute_handle vh = create_vertex_handle(p);
    // Determine the near_face dart if there is one a near dart is a dart that is near a face (link<2>)
    // dl and dr can be both near darts or not once
    // If we need to split with a near darts
    // Attach darts to new vertex attribute
    for (LCC_2::Dart_of_orbit_range<1, 2>::iterator
                 it(lcc.darts_of_orbit<1, 2>(dr).begin()),
                 itend(lcc.darts_of_orbit<1, 2>(dr).end()); it != itend; ++it) {
        lcc.set_vertex_attribute_of_dart(it, vh);
    }
    Dart_handle a_dn = lcc.create_dart(lcc.vertex_attribute(dl));
    Dart_handle b_dn = lcc.create_dart(vh);
    //
    Dart_handle a_dn2 = lcc.create_dart(lcc.vertex_attribute(dl));
    Dart_handle b_dn2 = lcc.create_dart(vh);
    //
    lcc.link_alpha<0>(a_dn, b_dn);
    lcc.link_alpha<1>(a_dn, dl);
    lcc.link_alpha<1>(b_dn, dr);
    //
    lcc.link_alpha<2>(a_dn, a_dn2);
    lcc.link_alpha<2>(b_dn, b_dn2);
    lcc.link_alpha<1>(a_dn2, tmp1);
    lcc.link_alpha<1>(b_dn2, tmp);
    lcc.link_alpha<0>(a_dn2, b_dn2);

    // Set the cell attribute for new darts
    lcc.set_attribute<2>(a_dn, lcc.attribute<2>(dl));
    lcc.set_attribute<2>(b_dn, lcc.attribute<2>(dl));
    lcc.set_attribute<2>(a_dn2, lcc.attribute<2>(tmp));
    lcc.set_attribute<2>(b_dn2, lcc.attribute<2>(tmp));
    std::cout << "End: one_split_vertice_bis" << std::endl;
    std::size_t id = get_id(vh), nc;
    lcc.info_of_attribute<0>(vh) = {id, 0, 0, !in_boundary(id)};
    return id;
}


std::size_t LCC_HL::two_split_vertice(Dart_handle dl, Dart_handle dr, const Point &p) {
    assert(lcc.alpha<1>(dl) != dl);
    assert(lcc.alpha<1>(dl) != dr);
    assert(lcc.is_valid());
    std::cout << "Start: two_split_vertice " << get_id(dl) << " " << get_id(dr) << std::endl;
    // dr1 is the alpha<1> (before unlink) of dr
    Dart_handle dr1, dl1;
    //Quick fix qui marche pas
    /*if (get_id(dl) == 2)
        dl = lcc.alpha<1>(dl);
    */
    // Save their <1> before unlink
    dr1 = lcc.alpha<1>(dr);
    dl1 = lcc.alpha<1>(dl);
    // Unlink
    lcc.unlink_alpha<1>(dr);
    lcc.unlink_alpha<1>(dl);
    // Create new vertex
    LCC_2::Vertex_attribute_handle vh = create_vertex_handle(p);
    //LCC_2::Attribute_handle<2>::type old_ah = lcc.attribute<2>(dr);
    // Attach darts to new vertex attribute
    for (LCC_2::Dart_of_orbit_range<1, 2>::iterator
                 it(lcc.darts_of_orbit<1, 2>(dr).begin()),
                 itend(lcc.darts_of_orbit<1, 2>(dr).end()); it != itend; ++it) {
        //lcc.display_dart(it);
        lcc.set_vertex_attribute_of_dart(it, vh);
    }
    // New darts for the old vertex
    Dart_handle a_dn = lcc.create_dart(lcc.vertex_attribute(dl1));
    Dart_handle a_dn2 = lcc.create_dart(lcc.vertex_attribute(dl1));
    // New darts for the new vertex
    Dart_handle b_dn = lcc.create_dart(vh);
    Dart_handle b_dn2 = lcc.create_dart(vh);
    // Link new darts
    lcc.link_alpha<0>(a_dn, b_dn);
    lcc.link_alpha<0>(a_dn2, b_dn2);
    lcc.link_alpha<2>(a_dn, a_dn2);
    lcc.link_alpha<2>(b_dn, b_dn2);
    // Link new darts with older
    lcc.link_alpha<1>(a_dn, dr1);
    lcc.link_alpha<1>(a_dn2, dl1);
    lcc.link_alpha<1>(b_dn, dr);
    lcc.link_alpha<1>(b_dn2, dl);
    // Set Attributes
    lcc.set_attribute<2>(a_dn, lcc.attribute<2>(lcc.alpha<1>(a_dn)));
    lcc.set_attribute<2>(b_dn, lcc.attribute<2>(lcc.alpha<1>(b_dn)));
    lcc.set_attribute<2>(a_dn2, lcc.attribute<2>(lcc.alpha<1>(a_dn2)));
    lcc.set_attribute<2>(b_dn2, lcc.attribute<2>(lcc.alpha<1>(b_dn2)));
    std::cout << "End: XF" << std::endl;
    assert(lcc.is_valid_attribute<2>(lcc.attribute<2>(a_dn)));
    for (std::map<std::size_t, LCC_2::Attribute_handle<2>::type>::iterator it = this->map_cell.begin(), itend =
            this->map_cell.end(); it != itend; ++it) {
        assert(lcc.is_valid_attribute<2>(it->second));
    }

    for (LCC_2::Attribute_range<2>::type::iterator it = lcc.attributes<2>().begin(), itend = lcc.attributes<2>().end();
         it != itend; ++it) {
        assert(lcc.is_valid_attribute<2>(it));
    }
    std::cout << "End: two_split_vertice" << std::endl;
    //lcc.display_darts(true);
    lcc.display_darts(std::cout, true);
    assert(lcc.is_valid());
    std::size_t id = get_id(vh);
    lcc.info_of_attribute<0>(vh) = {id, 0, 0, 0};
    return id;
}


